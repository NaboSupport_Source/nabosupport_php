import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapPage } from './map';
import { TranslateModule } from '@ngx-translate/core';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
@NgModule({
  declarations: [MapPage],
  imports: [IonicPageModule.forChild(MapPage),TranslateModule.forChild(),AgmCoreModule,AgmJsMarkerClustererModule],
  exports:[MapPage]
})
export class MapPageModule { }