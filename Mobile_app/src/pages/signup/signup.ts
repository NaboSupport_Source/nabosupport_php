// Page name: Signup page (This page validate user credentials and send to api for register)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component, ElementRef, Renderer, ViewChild } from '@angular/core';
import { ToastController, NavController, MenuController, LoadingController, Platform } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http'
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';

import { SigninPage } from '../signin/signin';
import { createTranslateLoader } from '../../app/app.module';

declare var google:any;
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})

export class SignupPage {
  options: GeolocationOptions;
  currentPos: Geoposition;
  map: any;
  LastLng1: any;
  LastLat1: any;
  deviceID:any;
  platformname:any;
  toastmessage:any;
  loadercontent:any;
  form = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    mail: new FormControl('', [Validators.required, Validators.email])
  });

  //get user data from signup form
  get username() {
    return this.form.get('username');

  }
  get mail() {
    return this.form.get('mail');
  }
  get password() {
    return this.form.get('password');
  }
  userallData: any;
  locdata: any;
  locArray: any;
  responseData: any;
  userData = { "username": "", "email_id": "", "password": "" };
  constructor(
    public navCtrl: NavController,
    private menu: MenuController,
    private renderer: Renderer,
    private elementRef: ElementRef,
    public keyboard: Keyboard,
    public AuthServiceProvider: AuthServiceProvider,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public geolocation: Geolocation,
    public http: Http) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.getUserPosition();
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //Toast contrlloer
  toastViewer(message:any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  //loading controller
  loadingViewer(content:any) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: content
    });
    return loading;
  }

  //JSON concadination
  jsonConcat(o1, o2) {
    for (var key in o2) {
      o1[key] = o2[key];
    }
    return o1;
  }

  //signup function 
  signup() {
    setTimeout(() => {
      let loader = this.loadingViewer(this.loadercontent);
      loader.dismiss();
    }, 300);
    this.userallData = {};
    console.log(this.locdata);
    this.userallData = this.jsonConcat(this.userData, this.locdata);
    this.platformname = localStorage.getItem('paltform');
    this.userallData.platform = this.platformname;
    console.log(this.userallData);
    if (this.userallData.username && this.userallData.password && this.userallData.email_id) {
      this.loadercontent = 'Signin you up...';
      let loader = this.loadingViewer(this.loadercontent);
      loader.present();
      this.AuthServiceProvider.postData(this.userallData, 'signup').then((result) => {
        this.responseData = result;
        if (true == this.responseData.status) {
           console.log(this.responseData);
           loader.dismiss();
          this.toastmessage = 'Account created successfully';
          this.toastViewer(this.toastmessage);
          this.navCtrl.push(SigninPage);
        } else {
          loader.dismiss();
          this.toastmessage = 'User already exist';
          this.toastViewer(this.toastmessage);
        }
      }, (err) => {
          loader.dismiss();
      });
    } else {
      this.toastmessage = 'Please fillup the detail to sign up';
      this.toastViewer(this.toastmessage);
    }

  }

  //redirect to login page
  login() {
    this.navCtrl.push(SigninPage);
  }


  //get user location detail from geocode
  getUserPosition() {
    let geocoder = new google.maps.Geocoder();
    this.options = {
      enableHighAccuracy: true
    };

    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {

      this.currentPos = pos;
      var coord = new Array();
      coord.push(pos.coords.latitude, pos.coords.longitude);
      console.log(coord[0] + ',' + coord[1]);
      let latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      let request = { latLng: latlng };
      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            var data = new Array;
            var locationdata = new Array();
            for (var i = 0; i < rsltAdrComponent.length; i++) {
              var obj = rsltAdrComponent[i];
              data.push(obj.long_name);

            }
            this.deviceID = localStorage.getItem('deviceID');
            if (data.length == 5) {
              this.locdata = {

                "street_address": data[1] + " " + data[0],
                "city": data[2],
                "country": data[3],
                "zipcode": data[4],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":5000,
                "deviceID":this.deviceID
              };
              console.log(this.locdata);
            }
            else if (data.length == 6) {
              this.locdata = {

                "street_address": data[1] + " " + data[0],
                "city": data[2],
                "country": data[4],
                "zipcode": data[5],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":5000,
                "deviceID":this.deviceID
              };
              console.log(this.locdata);
            }
            else if (data.length == 7) {
              this.locdata = {

                "street_address": data[1] + " " + data[0],
                "city": data[3],
                "country": data[5],
                "zipcode": data[6],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":5000,
                "deviceID":this.deviceID
              };
              console.log(this.locdata);
            }
            else if (data.length == 8) {
              this.locdata = {
                "street_address": data[1] + " " + data[0],
                "city": data[4],
                "country": data[6],
                "zipcode": data[7],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":5000,
                "deviceID":this.deviceID
              };
              console.log(this.locdata);
            } else if (data.length == 9) {
              this.locdata = {
                "street_address": data[1] + " " + data[0],
                "city": data[4],
                "country": data[7],
                "zipcode": data[8],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":5000,
                "deviceID":this.deviceID
              };

              console.log(this.locdata);
            } else if (data.length == 10) {

              this.locdata = {
                "street_address": data[1] + " " + data[0],
                "city": data[5],
                "country": data[8],
                "zipcode": data[9],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":5000,
                "deviceID":this.deviceID
              };

              console.log(this.locdata);
            }
            console.log(this.locdata);
          } else {
            alert("No address available");
          }
        }
      });
    }, (err: PositionError) => {
      console.log("error : " + err.message);
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
  }
  

}
