// Page name: Security page (This page load the list of security device connected with app)
// created: Feb 18, 2018
// Author: Bharath
// Revisions:
import { Component } from "@angular/core";
import {
  NavController,
  MenuController,
  NavParams,
  ViewController,
  AlertController,
  ToastController,
  PopoverController,
  LoadingController,
  ActionSheetController,
  Platform,
  IonicPage
} from "ionic-angular";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import {
  TranslateModule,
  TranslateLoader,
  TranslateService
} from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { MapPage } from "../map/map";

@IonicPage()
@Component({
  selector: "page-security",
  templateUrl: "security.html"
})
export class SecurityPage {
  addresses: any;
  updateAddress: boolean = true;
  // Jayamurugan
  country_title: { title: string };
  isEdit = [];
  selecRadio = [];
  _isPhone: boolean = false;
  _ifEdit: boolean = false;
  _isCountry: boolean = false;
  _ispaid: boolean = false;
  _isnormal: boolean = false;
  _isFirst = [];
  phoneview=[];
  idview=[];
  viewCard: boolean = true;
  responseData: any;
  editDBtn: any = [];
  updateDBtn: any = [];
  _selectOpt: any = [
    {
      value: "+45"
    },
    {
      value: "+46"
    },
    {
      value: "+47"
    }
  ];
  didVal:any;
  didArr = [];
  constructor(
    public navCtrl: NavController,
    private menu: MenuController,
    public AuthServiceProvider: AuthServiceProvider,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public platform: Platform,
    private translate: TranslateService,
    public actionSheetCtrl: ActionSheetController,
    public popoverCtrl: PopoverController
  ) {
    this.get_user();
    this.country_title = { title: "Select country" };
    let backAction = platform.registerBackButtonAction(() => {
      console.log("second");
      this.navCtrl.pop();
      backAction();
    }, 2);
  }

  //redirect to map page
  home() {
    var user = JSON.parse(localStorage.getItem("userData"));
    var lat = user.latitude;
    var lng = user.longitude;
    // this.navCtrl.setRoot(MapPage,{lat:lat,lng:lng});
    localStorage.setItem("lastLat", lat);
    localStorage.setItem("lastLng", lng);
    this.navCtrl.setRoot(MapPage);
  }

  // edit time change radio checkbox action
  radioSelect(val, i) {
    this._isFirst[i] = true;
    if (val === "device_detail") {
      this.phoneview[i] = false;
      this.idview[i]=true;
    } else if (val === "phone") {
      this.phoneview[i] = true;
      this.idview[i]=false;
    }
  }

  //get the user address and details
  get_user() {
    var userdata = JSON.parse(localStorage.getItem("userData"));
    var user = {
      user_id: userdata.user_id,
      address_id: userdata.address_id
    };
    this.AuthServiceProvider.postData(userdata, "getUserdata").then(result => {
      this.responseData = result;
      var user_detail = this.responseData;
      localStorage.setItem("userData", JSON.stringify(user_detail[0]));
      if (user_detail[0].paid_member == 1) {
        this.AuthServiceProvider.postData(user, "getPaidUserAddress").then(
          result => {
            this.addresses = result;
            this.addresses.forEach((val,i) => {
              //console.log(val.camtoken_type);
              if(val.camtoken_type == 'phone'){
                this.addresses[i]["cam_phone"]=val.camtoken;
                this.phoneview[i] = true;
                this.idview[i]=false;
              } else if(val.camtoken_type == 'device_detail') {
                this.addresses[i]["cam_did"]=val.camtoken;
                this.phoneview[i] = false;
                this.idview[i]=true;
              }
            });
            console.log(this.addresses);
            this._ispaid = true;
          },
          err => {
            console.log(err);
          }
        );
      } else {
        this._ispaid = false;
        this._isnormal = true;
      }
    });
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //edit device id
  editDID(i) {
    if (this.addresses[i].country_code === "") {
      this.addresses[i].country_code = "+45";
    }
    this._ifEdit = true;
    this.isEdit[i] = true;
    this.editDBtn[i] = true;
    this.updateDBtn[i] = true;
    if (this.addresses[i].camtoken_type === "phone") {
      this._isFirst[i] = true;
      this._isPhone = true;
      this._isCountry = true;
      this.didVal = false;
    } else if (this.addresses[i].camtoken_type === "device_detail") {
      this._isFirst[i] = true;
      this._isPhone = false;
      this._isCountry = false;
    } else {
      this._isFirst[i] = false;
      this._isCountry = false;
      this._isPhone = false;
    }
  }

  //update device id
  updateDID(add, i, did) {
    console.log(this.addresses[i]);
    let updatetxt = '';
    this.translate.get('Device ID updated successfully').subscribe(res => {
      updatetxt = res;
    });
    let toast = this.toastCtrl.create({
      message: updatetxt,
      duration: 3000,
      position: "bottom"
    });
    let toastfail = this.toastCtrl.create({
      message: "DeviceID update failed",
      duration: 3000,
      position: "bottom"
    });
    let loading = this.loadingCtrl.create({
      spinner: "crescent",
      content: "DeviceID updating..."
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);
    this.AuthServiceProvider.postData(this.addresses[i], "updateCamToken").then(
      result => {
        this.responseData = result;
        var user_detail = this.responseData;
        if (true == this.responseData.status) {
          this.editDBtn[i] = false;
          this.updateDBtn[i] = false;
          this._ifEdit = false;
          this.isEdit[i] = false;
          this._isFirst[i] = false;
          this.get_user();
          toast.present();
        } else {
          toastfail.present();
        }
      }
    );
  }
}
