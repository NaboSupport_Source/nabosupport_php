// Page name: popover page (This display the more address from myaccountpage for paid users only)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})

export class PopoverPage {
  address:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
    this.address = this.navParams.data;
    console.log(this.address);
  }
  close(data) {
    this.viewCtrl.dismiss(data);
  }
}
