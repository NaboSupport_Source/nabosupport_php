// Page name: Setting page (This page load the user alert setting)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, MenuController, IonicPage } from 'ionic-angular';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { POINTER_EVENT_TYPE_MOUSE } from 'ionic-angular/gestures/pointer-events';
import {MapPage} from '../map/map';
 
declare var google:any;
@IonicPage() 
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  @ViewChild('map') mapElement: ElementRef;
 
  out_of_home: any;
  dnd: any;
 
  setData: any;
  responsedata:any;
  responseData: any;
  options: GeolocationOptions;
  currentPos: Geoposition;
  userallData: any;
  currentregional: any;
  valueData: any;
  RvalueData: any;
  map: any;
  submitProfile: any;
  range: any;
  recrange: any;
  nearData: any;
  unit:any;
  Runit:any;
  base_url:any = 'http://34.216.55.85/naboapi/';
  constructor(
    public navCtrl: NavController,
    private menu:MenuController,
    public AuthServiceProvider: AuthServiceProvider,
    public geolocation: Geolocation,
    public navParams: NavParams) {
    var dist = JSON.parse(localStorage.getItem('userData'));
    this.valueData=dist.distance;
    this.RvalueData = dist.r_distance;
    console.log(this.valueData);
    console.log(this.RvalueData);
    if(this.valueData <= 999) {
      this.unit = 'Mtrs ';
    } else {
      console.log('yes');
      this.unit = 'Kms ';
      this.valueData = this.valueData/1000;
    }
    if(this.RvalueData <= 999) {
      this.Runit = 'Mtrs ';
    } else {
      console.log('yes');
      this.Runit = 'Kms ';
      this.RvalueData = this.RvalueData/1000;
    }
    this.get_user();
  }

  //redirect to main page
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat',lat);
    localStorage.setItem('lastLng',lng);
    this.navCtrl.setRoot(MapPage);
  }

  //get user details from api
  get_user() {
    var userdata = JSON.parse(localStorage.getItem('userData'));
    console.log(userdata['user_id']);
    this.AuthServiceProvider.postData(userdata, 'getUser').then((result) => {
      this.responseData = result;
      var user_detail = this.responseData;
      console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
      this.out_of_home=user_detail[0].out_of_home;
      if(user_detail[0].do_not_distrub==1){
        this.dnd="true";
      } else {
      this.dnd="false";
      }
      this.valueData=user_detail[0].distance;
      this.RvalueData=user_detail[0].r_distance;
      if(this.valueData <= 999) {
        this.unit = 'Mtrs ';
      } else {
        this.unit = 'Kms ';
        this.valueData = this.valueData/1000;
      }
      if(this.RvalueData <= 999) {
        this.Runit = 'Mtrs ';
      } else {
        this.Runit = 'Kms ';
        this.RvalueData = this.RvalueData/1000;
      }
      this.range=user_detail[0].distance;
      this.recrange=user_detail[0].r_distance;
      this.addMap(user_detail[0].latitude, user_detail[0].longitude);
    });
  }

  //load  user location map
  addMap(lat, long) {
    let latLng = new google.maps.LatLng(lat, long);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      fullscreenControl: false,
      mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker();


  }

  //add marker in map
  addMarker() {
    let nabo_img = this.base_url+'mapicon/new-markers/blue.png';
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,

      position: this.map.getCenter(),
      icon: nabo_img,
    });

    let content = "<p>This is your current position !</p>";
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    this.rangeUser("start");
    this.recrangeUser("start");
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  //ionic lifecycle event
  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }


//event for change user alert sending range when range for change
  rangeUser(event) {
    this.submitProfile = true;
    var simpleObject = {};
    for (var prop in event) {
      if (!event.hasOwnProperty(prop)) {
        continue;
      }
      if (typeof (event[prop]) == 'object') {
        continue;
      }
      if (typeof (event[prop]) == 'function') {
        continue;
      }
      simpleObject[prop] = event[prop];
    }
    console.log(this.valueData);
    var user_detail = this.responseData;
    if(event != "start"){
      console.log(simpleObject);
      var value = JSON.stringify(simpleObject);
      this.valueData = JSON.parse(value)._value;
      user_detail[0]['distance'] = this.valueData;
      if(this.valueData <= 999) {
        this.unit = 'Mtrs ';
      } else {
        this.unit = 'Kms ';
        this.valueData = this.valueData/1000;
      }
    }else{
      user_detail[0]['distance'] = user_detail[0]['distance'];
      this.valueData = user_detail[0]['distance'];
      if(this.valueData <= 999) {
        this.unit = 'Mtrs ';
      } else {
        this.unit = 'Kms ';
        this.valueData = this.valueData/1000;
      }
    }
    
    this.AuthServiceProvider.postData(user_detail[0], 'pushNearuser').then((result) => {
      this.nearData = result;
      
      this.nearData.forEach(function (o) {
        Object.keys(o).forEach(function (k) {
          if (isFinite(o[k])) {
            o[k] = +o[k];
          }
        });
      });
      console.log(this.nearData);
      if (this.nearData) {
        let markers = [];
        for (let regional of this.nearData) {
          regional.distance = 10;
          regional.visible = false;
          regional.current = false;
          let nabo_img = this.base_url+'mapicon/new-markers/blue.png';
          console.log(regional.latitude + ',' + regional.longitude);
          let markerData = {
            position: {
              lat: regional.latitude,
              lng: regional.longitude
            },
            map: this.map,
            icon: nabo_img,
          };

          regional.marker = new google.maps.Marker(markerData);
          markers.push(regional.marker);
          let content = "naboUSer";
          let infoWindow = new google.maps.InfoWindow({
            content: content
          });
         
          regional.marker.addListener('click', () => {
            for (let c of this.nearData) {
              c.current = false;
            }
            this.currentregional = regional;
            regional.current = true;
            let alert_img = this.base_url+'mapicon/alert_home.png';
            let markerData = {
              position: {
                lat: regional.latitude,
                lng: regional.longitude
              },
              map: this.map,
              icon: alert_img,
            };
            infoWindow.open(this.map, regional.marker);
            this.map.panTo(regional.marker.getPosition());
          });
        }

      }
      this.AuthServiceProvider.postData(user_detail[0], 'rangevalue').then((result) => {
        this.responsedata = result;
        console.log(this.responsedata);
        if(true == this.responsedata.status){ 
        }
      });

    }, (err) => {
      // Error log
    });
    
  }

  //event for change user alert receiving range when range for change
  recrangeUser(event) {
    this.submitProfile = true;
    var simpleRObject = {};
    for (var prop in event) {
      if (!event.hasOwnProperty(prop)) {
        continue;
      }
      if (typeof (event[prop]) == 'object') {
        continue;
      }
      if (typeof (event[prop]) == 'function') {
        continue;
      }
      simpleRObject[prop] = event[prop];
    }
    console.log(this.RvalueData);
    var user_detail = this.responseData;
    if(event != "start"){
      console.log(simpleRObject);
      var value = JSON.stringify(simpleRObject);
      this.RvalueData = JSON.parse(value)._value;
      user_detail[0]['r_distance'] = this.RvalueData;
      if(this.RvalueData <= 999) {
        this.Runit = 'Mtrs ';
      } else {
        this.Runit = 'Kms ';
        this.RvalueData = this.RvalueData/1000;
      }
    }else{
      user_detail[0]['r_distance'] = user_detail[0]['r_distance'];
      this.RvalueData = user_detail[0]['r_distance'];
      if(this.RvalueData <= 999) {
        this.Runit = 'Mtrs ';
      } else {
        this.Runit = 'Kms ';
        this.RvalueData = this.RvalueData/1000;
      }
    }
    
    this.AuthServiceProvider.postData(user_detail[0], 'receiveNearuser').then((result) => {
      this.nearData = result;
      
      this.nearData.forEach(function (o) {
        Object.keys(o).forEach(function (k) {
          if (isFinite(o[k])) {
            o[k] = +o[k];
          }
        });
      });
      console.log(this.nearData);
      if (this.nearData) {
        let markers = [];
        for (let regional of this.nearData) {
          regional.distance = 10;
          regional.visible = false;
          regional.current = false;
          let nabo_img = this.base_url+'mapicon/new-markers/blue.png';
          console.log(regional.latitude + ',' + regional.longitude);
          let markerData = {
            position: {
              lat: regional.latitude,
              lng: regional.longitude
            },
            map: this.map,
            icon: nabo_img,
          };

          regional.marker = new google.maps.Marker(markerData);
          markers.push(regional.marker);
          let content = "naboUSer";
          let infoWindow = new google.maps.InfoWindow({
            content: content
          });
          regional.marker.addListener('click', () => {
            for (let c of this.nearData) {
              c.current = false;
            }
            this.currentregional = regional;
            regional.current = true;
            let alert_img = this.base_url+'mapicon/alert_home.png';
            let markerData = {
              position: {
                lat: regional.latitude,
                lng: regional.longitude
              },
              map: this.map,
              icon: alert_img,
            };
            infoWindow.open(this.map, regional.marker);
            this.map.panTo(regional.marker.getPosition());
          });
        }

      }
      this.AuthServiceProvider.postData(user_detail[0], 'recRangevalue').then((result) => {
        this.responsedata = result;
        console.log(this.responsedata);
        if(true == this.responsedata.status){
          
        }
      });

    }, (err) => {
      // Error log
    });
  }

  //set user outofhome
  outofhome(event) {
    this.submitProfile = true;
    console.log("test" + JSON.stringify(event));
    console.log(event);
    var outofhomedata = this.responseData;
    outofhomedata[0]['outofhome'] = event;
    this.AuthServiceProvider.postData(outofhomedata[0], 'outofhome').then((result) => {
      this.responsedata = result;
      if(true == this.responsedata.status){
    
      //  this.get_user();
      }
      console.log(this.out_of_home);
    })
  }

  //set do not distrub mode
  notify() {
    this.submitProfile = true;
    console.log("test" + JSON.stringify(this.dnd));
    var notifydata=this.responseData;
    notifydata[0]["notify"] = this.dnd;
    this.AuthServiceProvider.postData(notifydata[0], 'dnd').then((result) => {
      this.responsedata = result;
      if(true == this.responsedata.status){
        
           // this.get_user();
          }
    })
  }

}
