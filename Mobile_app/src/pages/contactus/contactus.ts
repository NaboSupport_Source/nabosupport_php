// Page name: contact us page (This page will load nabo support contact form)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component } from '@angular/core';
import { ToastController, NavController, NavParams, AlertController, MenuController, IonicPage } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MapPage } from '../map/map';


@IonicPage()
@Component({
  selector: 'page-contactus',
  templateUrl: 'contactus.html',
})
export class ContactusPage {
  userData = { "name": "", "mail": "", "subject": "", "message": "" };
  responseData: any;
  form = new FormGroup({
    name: new FormControl('', Validators.required),
    mail: new FormControl('', Validators.required),
    subject: new FormControl('', Validators.required),
    message: new FormControl('', Validators.required),
  });

  //get the field details from contact us form
  get name() {
    return this.form.get('name');
  }
  get mail() {
    return this.form.get('mail');
  }
  get subject() {
    return this.form.get('subject');
  }
  get message() {
    return this.form.get('message');
  }
  constructor(public navCtrl: NavController, private menu: MenuController, private toastCtrl: ToastController, public AuthServiceProvider: AuthServiceProvider, public emailComposer: EmailComposer, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  //redirect to map page
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat', lat);
    localStorage.setItem('lastLng', lng);
    this.navCtrl.setRoot(MapPage);
  }

  //default Ionic lifecycle event
  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactusPage');
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //send user details as a mail to nabosupport
  sendemail() {
    console.log("mail");
    let toast = this.toastCtrl.create({
      message: 'Your message sent successfully',
      duration: 3000,
      position: 'bottom'
    });
    let toast1 = this.toastCtrl.create({
      message: 'Please fillup the detail to Contact Nabo Support Team',
      duration: 3000,
      position: 'bottom'
    });
    let toast2 = this.toastCtrl.create({
      message: 'Invalid Email format',
      duration: 3000,
      position: 'bottom'
    });

    var userdata = JSON.parse(localStorage.getItem('userData'));
    this.userData['user_id'] = userdata['user_id'];
    console.log(this.userData);
    console.log(this.userData);
    if (this.userData.name && this.userData.mail && this.userData.subject && this.userData.message) {
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (reg.test(this.userData.mail) == true) {
        console.log(this.userData);
        this.AuthServiceProvider.postData(this.userData, 'sendmail').then((result) => {
          this.responseData = result;
          if (true == this.responseData.status) {

            let alert = this.alertCtrl.create({
              title: 'Sent',
              subTitle: 'Your Message sent successfully.',
              buttons: ['Ok']
            });
            alert.present();
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
          } else {
            let alert = this.alertCtrl.create({
              title: 'Failed',
              subTitle: 'Your Message sent Failed.',
              buttons: ['Ok']
            });
            alert.present();
          }
        }, (err) => {
          // Error log
        });
      } else {
        toast2.present();
      }
    } else {
      toast1.present();
    }






  }
}
