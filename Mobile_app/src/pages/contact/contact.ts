
// Page name: contact page (This page will load user contacts)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, PopoverController, ViewController,AlertController, MenuController, IonicPage } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MapPage } from '../map/map';
import { PopoverPage } from '../popover/popover';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  mycontact: any;
  responseData: any;
  user_id: any
  contactdetail: any;
  editData: any;
  itemsList: any;
  isEmpty: boolean = false;
  countryCode: any;
  tabname: any;
  btnclear: boolean = false;
  btnName: any = 'Add contact';
  userData = { "name": "", "mail": "", "phone": "", "id": "", "type": "", "code": "" };
  selectCode: { title: string, subTitle: string };
  countryPHCode: any = [];
  form = new FormGroup({
    id: new FormControl(''),
    type: new FormControl(''),
    userid: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    mail: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]),
    code: new FormControl('')
  });

  //get the field details from contact form
  get userid() {
    return this.form.get('userid');
  }
  get name() {
    return this.form.get('name');
  }
  get mail() {
    return this.form.get('mail');
  }
  get phone() {
    return this.form.get('phone');
  }
  get code() {
    return this.form.get('code');
  }

  constructor(private translate: TranslateService,
              private alertCtrl:AlertController,
              public navCtrl: NavController, 
              private toastCtrl: ToastController, 
              public navParams: NavParams, 
              public AuthServiceProvider: AuthServiceProvider, 
              public popoverCtrl: PopoverController, 
              private menu: MenuController) {
    this.mycontact = "contactlist";
    this.userData.type = 'save';
   
    this.translate.get("New Contact").subscribe(res=>{
      this.tabname = res;
    });
    this.getContact();
    this.selectCode = {
      title: 'Select Code',
      subTitle: ''
    };
    this.countryPHCode = [
      {
        name: 'IN',
        value: '+91'
      },
      {
        name: 'DK',
        value: '+45'
      }
    ];
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //Onclicking home button redirect to map page
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat',lat);
    localStorage.setItem('lastLng',lng);
    this.navCtrl.setRoot(MapPage);
  }
  optionsFn(start) {
    console.log(start);
    this.countryCode = start;
    // this.userData.phone = start+this.userData.phone;
    // console.log(this.userData.phone);
  }

  //For paid user to add multiple addresses in popover
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  //get the user contact details from api
  getContact() {
    console.log('ionViewDidLoad ContactPage');
    var userdata = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(userdata, 'getMycontact').then((result) => {
      this.responseData = result;
      var status: any = JSON.parse(JSON.stringify(this.responseData));
      if (typeof status.status == 'undefined') {
        this.isEmpty = false;
        this.itemsList = this.responseData;
      } else {
        this.isEmpty = true;
      }

    }, (err) => {

    });

  }

  //get local date and time
  getDateTime() {
    var today = new Date();
    //var dateTime = today.toLocaleString();
    var date = today.getDate()+ '-' + (today.getMonth() + 1)+'-' +today.getFullYear();
    var time = new Date();
    //var dateTime = date + ' : ' + time.toLocaleString('en-DN', { hour: 'numeric', minute: 'numeric', hour12: true });
    var dateTime = date + ' ' + time.toLocaleString('en-DN', { hour: 'numeric', minute: 'numeric', hour12: false });
    return dateTime;
  }

  //add user contact details to api
  contact() {

    // save
    if (this.userData.type == 'save') {
      var success ='';
      var err='';
      var err1='';
      var err2='';
      var err3='';
      this.translate.get('Contact added successfully').subscribe(res=>{
        success = res;
      });
      this.translate.get('Please fillup the detail to contact').subscribe(res=>{
        err = res;
      });
      this.translate.get('Invalid Email format').subscribe(res => {
        err1  =res;
      });
      this.translate.get('exst').subscribe(res => {
        err2  =res;
      });
      this.translate.get('own').subscribe(res => {
        err3  =res;
      });
      let toast = this.toastCtrl.create({
        message: success,
        duration: 3000,
        position: 'bottom'
      });
      let toast1 = this.toastCtrl.create({
        message: err,
        duration: 3000,
        position: 'bottom'
      });
      let toast2 = this.toastCtrl.create({
        message: err1,
        duration: 3000,
        position: 'bottom'
      });
      let toast3 = this.toastCtrl.create({
        message: err2,
        duration: 3000,
        position: 'bottom'
      });
      let toast4 = this.toastCtrl.create({
        message: err3,
        duration: 3000,
        position: 'bottom'
      });

      var userdata = JSON.parse(localStorage.getItem('userData'));
      this.userData['user_id'] = userdata['user_id'];
      this.userData['username'] = userdata['user_login'];
      this.userData['time'] = this.getDateTime();
      console.log(this.userData);
      if (this.userData.name && this.userData.mail) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(this.userData.mail) == true) {
          console.log(this.userData);
          this.AuthServiceProvider.postData(this.userData, 'mycontact').then((result) => {
            this.responseData = result;
            if (this.responseData.status == true) {
              console.log(this.responseData);
              toast.present();
              //this.getContact();
              this.form.reset();
              this.clearForm();
              this.navCtrl.setRoot(this.navCtrl.getActive().component);
              // this.navCtrl.setRoot(this.navCtrl.getActive().component);
              this.mycontact = 'contact';
            } else if(this.responseData.status == false) {
              toast3.present();
            } else if(this.responseData.status == 'own') {
              toast4.present();
            }

          }, (err) => {
            // Error log
          });
        } else {
          toast2.present();
        }
      } else {
        toast1.present();
      }
    } else {
      //update the  contact details to api
      var usuccess ='';
      var uerr='';
      var uerr1='';
      this.translate.get('Contact updated successfully').subscribe(res=>{
        usuccess = res;
      });
      this.translate.get('Please fillup the detail to contact').subscribe(res=>{
        uerr = res;
      });
      this.translate.get('Invalid Email format').subscribe(res => {
        uerr1 =res;
      });
      let toast = this.toastCtrl.create({
        message: success,
        duration: 3000,
        position: 'bottom'
      });
      let toast1 = this.toastCtrl.create({
        message: err,
        duration: 3000,
        position: 'bottom'
      });
      let toast2 = this.toastCtrl.create({
        message: err1,
        duration: 3000,
        position: 'bottom'
      });
      let toastU = this.toastCtrl.create({
        message: usuccess,
        duration: 3000,
        position: 'bottom'
      });
      let toast1U = this.toastCtrl.create({
        message: uerr,
        duration: 3000,
        position: 'bottom'
      });
      let toast2U = this.toastCtrl.create({
        message: uerr1,
        duration: 3000,
        position: 'bottom'
      });
      var userdata = JSON.parse(localStorage.getItem('userData'));
      this.userData['user_id'] = userdata['user_id'];
      console.log(this.userData);
      if (this.userData.name && this.userData.mail) {
        var regu = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (regu.test(this.userData.mail) == true) {

          this.AuthServiceProvider.postData(this.userData, 'mycontactUpdate').then((result) => {
            this.responseData = result;
            if (true == this.responseData.status) {
              this.btnName = 'Add contact';
              toastU.present();
              this.getContact();
              this.form.reset();
              this.clearForm();
              this.mycontact = 'contactlist';
            }

          }, (err) => {
            // Error log
          });
        } else {
          toast2U.present();
        }
      } else {
        toast1U.present();
      }
    }

  }

  //edit contact details and update the api
  editContact(id: number) {
    this.btnclear = true;
    for (let i = 0; i < this.itemsList.length; i++) {
      if (id == this.itemsList[i].my_contact_id) {
        this.editData = this.itemsList[i];
      }
    }
    this.setEditValue();
  }

  //set edit value updated in local variable
  setEditValue() {
    // "name": "", "countrycode": "", "mail": "", "phone": "", "id": "", "type": ""
    this.userData.name = this.editData.name;
    this.userData.mail = this.editData.email_id;
    this.userData.phone = this.editData.phone_number.substring(3, 20);
    this.userData.id = this.editData.my_contact_id;
    this.userData.code = this.editData.phone_number.substring(0, 3);
    this.userData.type = 'update';
    let trans_btn_update ='';
    let trans_btn_update_tab ='';
    this.translate.get('Update').subscribe(res=>{
     trans_btn_update =res;
    });
    this.translate.get('Update Contact').subscribe(res=>{
      trans_btn_update_tab=res;
    });
    this.btnName = trans_btn_update;
    this.mycontact = 'contact';
    this.tabname = trans_btn_update_tab;
  }

  //delete user contact from api
  deleteContact(id: number) {
    let trans_role_cancel = '';
    let trans_role_yes = '';
    let trans_role_no = '';
    let trans_role_title = '';
    let trans_role_msg = '';
    this.translate.get("cancel").subscribe(res=>{
      trans_role_cancel=res;
    });
    this.translate.get("Yes").subscribe(res=>{
      trans_role_yes=res;
    });
    this.translate.get("No").subscribe(res=>{
      trans_role_no=res;
    });
    this.translate.get("My contact").subscribe(res=>{
      trans_role_title=res;
    });
    this.translate.get("Do you want to delete?").subscribe(res=>{
      trans_role_msg=res;
    });
    let alert = this.alertCtrl.create({
      title: trans_role_title,
      message: trans_role_msg,
      buttons: [
        {
          text: trans_role_no,
          role: trans_role_cancel,
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: trans_role_yes,
          handler: () => {
            this.AuthServiceProvider.postData(JSON.parse('{"id":' + id + '}'), 'myContactDelete')
            .then((result) => {
              this.getContact();
            }, (err) => {
              console.log(err);
            });
          }
        }
      ]
    });
    alert.present();

   
  }

  //clear contact form
  clearForm() {
    let trans_tab_name = "";
    let trans_tab_add = "";
    this.translate.get("New Contact").subscribe(res=>{
      trans_tab_name=res;
    });
    this.translate.get("Add").subscribe(res=>{
      trans_tab_add=res;
    });
    this.userData.name = '';
    this.userData.mail = '';
    this.userData.phone = '';
    this.userData.id = '';
    this.userData.type = 'save';
    this.tabname = trans_tab_name;
    this.btnclear = false;
    this.btnName = trans_tab_add;
  }
  isValidNumber(event) {
    //return /\d|Backspace/.test(event.key);
    if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.keyCode) > -1) {
      // backspace, enter, escape, arrows
      return true;
    } else if (event.keyCode >= 48 && event.keyCode <= 57) {
      // numbers 0 to 9
      return true;
    } else if (event.keyCode >= 96 && event.keyCode <= 105) {
      // numpad number
      return true;
    }
    return false;
  }
  
  pendingInfo(){
    let msg:any;
    this.translate.get('reqpend').subscribe(res => {
      msg = res;
    });
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  rejectInfo(){
    let msg:any;
    this.translate.get('reqrej').subscribe(res => {
      msg = res;
    });
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}