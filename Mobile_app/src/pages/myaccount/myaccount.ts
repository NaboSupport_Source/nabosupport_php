// Page name: Myaccount page (This page load the user profile details and map)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { NavController, MenuController, NavParams, ViewController, ToastController, PopoverController, AlertController, LoadingController, ActionSheetController, Platform, Events, IonicPage, Alert } from 'ionic-angular';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MapPage } from '../map/map';
import { PopoverPage } from '../popover/popover';

declare var google;
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html',
})
export class MyaccountPage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('username') uelementRef: ElementRef;
  @ViewChild('email') eelementRef: ElementRef;
  @ViewChild('pass') pelementRef: ElementRef;
  imageURI: any;
  imageFileName: any;
  options: GeolocationOptions;
  currentPos: Geoposition;
  map: any;
  LastLng1: any;
  LastLat1: any;
  marker: any;
  userdetail: {};
  editProfile: any;
  autocompleteItems: any;
  autocomplete: any;
  acService: any;
  placesService: any;
  responseData: any;
  isDisable: any;
  isDisabled: any;
  users: any;
  submitProfile: any;
  cancelProfile: any;
  lastImage: string = null;
  fileopen = false;
  loading: any;
  content: any;
  search: any;
  geocoder: any;
  markers: any;
  serachopen: any;
  _addressid: any;
  _ispaid: boolean = false;
  addresses: any;
  updateAddress: boolean = true;
  addAddress: boolean = false;
  _addCtrl: boolean = false;
  _editCtrl: boolean = false;
  toastmessage: any;
  addupdate: any;
  selectedData: any;
  addaddress: boolean;
  viewCard: boolean = true;
  viewMap: boolean = false;
  editDisabled: boolean = true;
  recrange: any;
  RvalueData: any;
  Runit: any;
  dnd: any;
  newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "camtoken": "" };
  public mapcondition: boolean = false;
  public imgoption: boolean = false;
  locationdata = { "hus": "", "street": "", "city": "" };
  marker_data = { "latitude": "", "longitude": "" };

  form1 = new FormGroup({
    username: new FormControl('', Validators.required),
    mail: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
    user_id: new FormControl(''),
    latitude: new FormControl(''),
    longitude: new FormControl('')
  });

  get username() {
    return this.form1.get('username');
  }
  get mail() {
    return this.form1.get('mail');
  }
  get password() {
    return this.form1.get('password');
  }
  constructor(
    private transfer: FileTransfer,
    private camera: Camera,
    private crop: Crop,
    private renderer: Renderer2,
    public navCtrl: NavController,
    private menu: MenuController,
    public AuthServiceProvider: AuthServiceProvider,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public geolocation: Geolocation,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private filePath: FilePath,
    public platform: Platform,
    private file: File,
    private translate: TranslateService,
    private transferObject: FileTransferObject,
    public actionSheetCtrl: ActionSheetController,
    public popoverCtrl: PopoverController) {
    this.get_user();

    this.ionViewDidLoad();
    let backAction = platform.registerBackButtonAction(() => {
      console.log("second");
      this.navCtrl.pop();
      backAction();
    }, 2);
    var dist = JSON.parse(localStorage.getItem('userData'));
    this.RvalueData = dist.r_distance;
    this.dnd = dist.do_not_distrub;
    this.recrange = this.RvalueData;
    // if(this.RvalueData <= 999) {
    this.Runit = ' M';
    // } else {
    // console.log('yes');
    // this.Runit = 'Kms ';
    //alert(this.RvalueData);
    // this.RvalueData = this.RvalueData/1000;
    // }
  }
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    // this.navCtrl.setRoot(MapPage,{lat:lat,lng:lng});
    localStorage.setItem('lastLat', lat);
    localStorage.setItem('lastLng', lng);
    this.navCtrl.setRoot(MapPage);
  }
  get_position() {
    this.options = {
      enableHighAccuracy: true
    };

    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {

      this.currentPos = pos;
      console.log(pos);
      console.log(pos.coords.latitude + ',' + pos.coords.longitude)
      //this.addMap(pos.coords.latitude,pos.coords.longitude);
      this.addMap(pos.coords.latitude, pos.coords.longitude);
    }, (err: PositionError) => {
      console.log("error : " + err.message);
    });
  }
  get_user() {
    var userdata = JSON.parse(localStorage.getItem('userData'));
    console.log(userdata['user_id']);
    var user = {
      'user_id': userdata.user_id,
      'address_id': userdata.address_id
    };
    this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
      this.responseData = result;
      var user_detail = this.responseData;
      this.marker_data.latitude = this.responseData[0]['latitude'];
      this.marker_data.longitude = this.responseData[0]['longitude'];
      localStorage.setItem('userData', JSON.stringify(user_detail[0]));
      console.log(user_detail[0].paid_member);
      if (user_detail[0].paid_member == 1) {
        this.AuthServiceProvider.postData(user, 'getPaidUserAddress').then((result) => {
          this.addresses = result;
          this._ispaid = true;
          console.log('user saved addresses ' + this.addresses);
        }, (err) => {
          console.log(err);
        });
      }
      console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
      this.userdetail = user_detail;
      this.isDisable = true;
      this.editProfile = true;
      this.submitProfile = false;
      this.cancelProfile = false;
      this.isDisabled = true;
      // this.addMap(user_detail[0].latitude, user_detail[0].longitude);
    });
  }
  /*##get login user details end##*/
  profileactive() {
    this.isDisable = false;
    this.editProfile = false;
    this.submitProfile = true;
    this.cancelProfile = true;
    this.mapcondition = true;
    this.addaddress = true;
    this._editCtrl = false;
    //zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
    // this.map.set('draggable', true);
    // this.map.set('scrollwheel', true);
    // this.map.set('zoomControl', true);
    // this.map.set('disableDoubleClickZoom', true);

  }
  profilecancel() {
    this.isDisable = true;
    this.editProfile = true;
    this.submitProfile = false;
    this.cancelProfile = false;
    this.mapcondition = false;
    this.serachopen = false;
    this.addAddress = false;
    this.updateAddress = true;
    this._editCtrl = false;
    this.addaddress = false;
    var user = JSON.parse(localStorage.getItem('userData'));
    if (user.paid_member == 1) {
      this._ispaid = true;
    } else {
      this._ispaid = false;
    }
    // console.log("lat lon:",parseFloat(this.responseData[0]['latitude']),parseFloat(this.responseData[0]['longitude']))
    this.get_user();
    //this.addMap(parseFloat(this.responseData[0]['latitude']),parseFloat(this.responseData[0]['longitude']));
    this.map.set('draggable', false);
    this.map.set('scrollwheel', false);
    this.map.set('zoomControl', false);
    this.map.set('disableDoubleClickZoom', false);
    var userData = JSON.parse(localStorage.getItem('userData'));
    this.userdetail[0] = userData;
  }

  profileUpdate() {
    let suc = '';
    let lod = '';
    this.translate.get('Profile updated successfully').subscribe(res => {
      suc = res;
    });
    this.translate.get('Profile updating...').subscribe(res => {
      lod = res;
    });
    let toast = this.toastCtrl.create({
      message: suc,
      duration: 3000,
      position: 'bottom'
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: lod,
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);

    console.log(this.userdetail[0]);
    if (this.userdetail[0].user_login && this.userdetail[0].user_pass && this.userdetail[0].user_email) {
      this.AuthServiceProvider.postData(this.userdetail[0], 'updateProfile').then((result) => {
        // loading.present();
        this.responseData = result;
        var user_detail = this.responseData;
        if (true == this.responseData.status) {
          // console.log(user_detail[0].longitude+','+user_detail[0].longitude);
          this.userdetail[0] = user_detail;
          this.isDisable = true;
          this.editProfile = true;
          this.submitProfile = false;
          this.cancelProfile = false;
          toast.present();
          this.navCtrl.setRoot(this.navCtrl.getActive().component);

        } else {

        }
        // this.addMap(user_detail[0].longitude,user_detail[0].longitude);
      });
    } else {
      let trans_toast_msg = "";
      this.translate.get('Please fillup the detail to sign up').subscribe(res => {
        trans_toast_msg = res;
      });
      this.toastmessage = trans_toast_msg;
      this.toastViewer(this.toastmessage);
    }
  }
  toastViewer(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  addMoreAddress() {
    let position = JSON.parse(localStorage.getItem('accLatLng'));
    let trans_load_msg = '';
    this.translate.get('Profile updating...').subscribe(res => {
      trans_load_msg = res;
    });

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: trans_load_msg
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);

    console.log(this.userdetail[0]);
    this.newAddAry.user_id = this.userdetail[0].user_id;
    this.newAddAry.latitude = position.lat;
    this.newAddAry.longitude = position.lng;
    this.AuthServiceProvider.postData(this.newAddAry, 'addMoreAddress').then((result) => {
      // loading.present();
      this.responseData = result;
      var user_detail = this.responseData;
      if (true == this.responseData.status) {
        // console.log(user_detail[0].longitude+','+user_detail[0].longitude);
        localStorage.removeItem('accLatLng');
        this.addAddress = false;
        this.isDisable = true;
        this.editProfile = true;
        this.submitProfile = false;
        this.cancelProfile = false;
        this._addCtrl = false;
        this._ispaid = true;
        this.updateAddress = true;
        this.addAddress = false;
        let trans_toast_msg = "";
        this.translate.get('Address added successfully').subscribe(res => {
          trans_toast_msg = res;
        });
        this.toastmessage = trans_toast_msg;
        this.toastViewer(this.toastmessage);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);

      } else {
        this._addCtrl = false;
        this._ispaid = true;
        this.updateAddress = true;
        this.addAddress = false;
      }
      // this.addMap(user_detail[0].longitude,user_detail[0].longitude);
    });
  }

  updateEditAddress() {
    let trans_load_msg = "";
    this.translate.get('Profile updating...').subscribe(res => {
      trans_load_msg = res;
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: trans_load_msg
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);

    console.log(this.userdetail[0]);
    let updateAddress = JSON.parse(localStorage.getItem('accLatLng'));
    //alert(updateAddress);
    this.userdetail[0].latitude = updateAddress.lat;
    this.userdetail[0].longitude = updateAddress.lng;
    this.AuthServiceProvider.postData(this.userdetail[0], 'updateEditAddress').then((result) => {
      // loading.present();
      this.responseData = result;
      var user_detail = this.responseData;
      if (true == this.responseData.status) {
        // console.log(user_detail[0].longitude+','+user_detail[0].longitude);
        this.addAddress = false;
        this.isDisable = true;
        this.editProfile = true;
        this._editCtrl = false;
        this._addCtrl = false;
        this._ispaid = true;
        this.updateAddress = true;
        let trans_toast_msg = "";
        this.translate.get('Address added successfully').subscribe(res => {
          trans_toast_msg = res;
        });
        this.toastmessage = trans_toast_msg;
        this.toastViewer(this.toastmessage);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);

      } else {
        this._addCtrl = false;
        this._ispaid = true;
        this.updateAddress = true;
        this.addAddress = false;
      }
      // this.addMap(user_detail[0].longitude,user_detail[0].longitude);
    });

  }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  clearMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers.length = 0;
  }

  chooseItem(item: any) {
    console.log('modal > chooseItem > item > ', item);
    //this.viewCtrl.dismiss(item);
    //console.log(item);
    this.geocoder = new google.maps.Geocoder;
    this.markers = [];
    this.clearMarkers();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
    this.geocoder.geocode({ 'placeId': item.place_id }, (results, status) => {
      console.log(results);
      // if (status === 'OK' && results[0]) {
      let position = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      };
      localStorage.setItem('accLatLng', JSON.stringify(position));
      console.log(position);
      if (results[0] != null) {
        // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
        //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
        var data = new Array;
        let rsltAdrComponent = results[0].address_components;
        let rslength = rsltAdrComponent.length;
        for (var i = 0; i < rslength; i++) {
          var obj = rsltAdrComponent[i];
          data.push(obj.long_name);

        }
        //console.log(locdata);
        if (this.addAddress == true) {
          this.newAddAry.latitude = position.lat;
          this.newAddAry.longitude = position.lng;
          console.log('new address');
          if (data.length == 5) {
            this.newAddAry.street_address = data[1] + " " + data[0];
            this.newAddAry.country = data[3];
            this.newAddAry.city = data[2];
            this.newAddAry.zipcode = data[4];
          }
          else if (data.length == 6) {
            this.newAddAry.street_address = data[1] + " " + data[0];
            this.newAddAry.country = data[4];
            this.newAddAry.city = data[2];
            this.newAddAry.zipcode = data[5];
          } else if (data.length == 7) {
            this.newAddAry.street_address = data[1] + " " + data[0];
            this.newAddAry.country = data[5];
            this.newAddAry.city = data[2];
            this.newAddAry.zipcode = data[6];
          }
          else if (data.length == 8) {
            this.newAddAry.street_address = data[1] + " " + data[0];
            this.newAddAry.country = data[6];
            this.newAddAry.city = data[4];
            this.newAddAry.zipcode = data[7];
          } else if (data.length == 9) {
            this.newAddAry.street_address = data[1] + " " + data[0];
            this.newAddAry.country = data[7];
            this.newAddAry.city = data[4];
            this.newAddAry.zipcode = data[8];
          } else if (data.length == 10) {
            this.newAddAry.street_address = data[1] + " " + data[0];
            this.newAddAry.country = data[8];
            this.newAddAry.city = data[5];
            this.newAddAry.zipcode = data[9];
          }
          console.log('new addres:' + JSON.stringify(this.newAddAry));
        } else {
          this.userdetail[0].latitude = position.lat;
          this.userdetail[0].longitude = position.lng;
          if (data.length == 5) {
            this.userdetail[0].street_address = data[1] + " " + data[0];
            this.userdetail[0].country = data[3];
            this.userdetail[0].city = data[2];
            this.userdetail[0].zipcode = data[4];
          }
          else if (data.length == 6) {
            this.userdetail[0].street_address = data[1] + " " + data[0];
            this.userdetail[0].country = data[4];
            this.userdetail[0].city = data[2];
            this.userdetail[0].zipcode = data[5];
          } else if (data.length == 7) {
            this.userdetail[0].street_address = data[1] + " " + data[0];
            this.userdetail[0].country = data[5];
            this.userdetail[0].city = data[2];
            this.userdetail[0].zipcode = data[6];
          }
          else if (data.length == 8) {
            this.userdetail[0].street_address = data[1] + " " + data[0];
            this.userdetail[0].country = data[6];
            this.userdetail[0].city = data[4];
            this.userdetail[0].zipcode = data[7];
          } else if (data.length == 9) {
            this.userdetail[0].street_address = data[1] + " " + data[0];
            this.userdetail[0].country = data[7];
            this.userdetail[0].city = data[4];
            this.userdetail[0].zipcode = data[8];
          } else if (data.length == 10) {
            this.userdetail[0].street_address = data[1] + " " + data[0];
            this.userdetail[0].country = data[8];
            this.userdetail[0].city = data[5];
            this.userdetail[0].zipcode = data[9];
          }
        }
        console.log(this.userdetail);
      } else {
        //alert("No address available");
        console.log("No address available");
      }

      let nabo_img = 'http://rayi.in/naboapi/mapicon/new-markers/blue.png';
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,

        position: results[0].geometry.location,
        icon: nabo_img,
      });
      // let marker = new google.maps.Marker({
      //   position: results[0].geometry.location,
      //   map: this.map,
      // });
      this.markers.push(marker);
      this.map.setCenter(position);
      // }
    })
  }

  getNewAddressByName() {
    var address = `${this.newAddAry.street_address}, ${this.newAddAry.city}, ${this.newAddAry.country} `;
    console.log(address);
    this.geocoder = new google.maps.Geocoder;
    this.geocoder.geocode({ 'address': address }, function (results, status) {
      if (status === 'OK') {
        console.log(results);
        //alert(address);
        let position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };
        console.log(position);
        localStorage.setItem('accLatLng', JSON.stringify(position));
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
    return true;
  }

  updateOldAddress() {
    var address = `${this.userdetail[0].street_address}, ${this.userdetail[0].city}, ${this.userdetail[0].country} `;
    console.log(address);
    this.geocoder = new google.maps.Geocoder;
    this.geocoder.geocode({ 'address': address }, function (results, status) {
      if (status === 'OK') {
        console.log(results);
        //alert(address);
        let position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };
        console.log(position);
        localStorage.setItem('accLatLng', JSON.stringify(position));
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
    return true;
  }

  validateAddressForm() {
    if (!(this.newAddAry.street_address) && !(this.newAddAry.city) && !(this.newAddAry.country) && !(this.newAddAry.zipcode)) {
      this.toastmessage = 'Please fill all the field.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.newAddAry.street_address)) {
      this.toastmessage = 'Please provide street name.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.newAddAry.city)) {
      this.toastmessage = 'Please provide city name.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.newAddAry.country)) {
      this.toastmessage = 'Please provide country name.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.newAddAry.zipcode)) {
      this.toastmessage = 'Please provide postal code.';
      this.toastViewer(this.toastmessage);
    } else if ((this.newAddAry.street_address) && (this.newAddAry.city) && (this.newAddAry.country) && (this.newAddAry.zipcode)) {
      this.defaultAddConfirm();
    }
  }

  validateEditAddressForm() {
    if (!(this.userdetail[0].street_address) && !(this.userdetail[0].city) && !(this.userdetail[0].country) && !(this.userdetail[0].zipcode)) {
      this.toastmessage = 'Please fill all the field.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.userdetail[0].street_address)) {
      this.toastmessage = 'Please provide street name.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.userdetail[0].city)) {
      this.toastmessage = 'Please provide city name.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.userdetail[0].country)) {
      this.toastmessage = 'Please provide country name.';
      this.toastViewer(this.toastmessage);
    } else if (!(this.userdetail[0].zipcode)) {
      this.toastmessage = 'Please provide postal code.';
      this.toastViewer(this.toastmessage);
    } else if ((this.userdetail[0].street_address) && (this.userdetail[0].city) && (this.userdetail[0].country) && (this.userdetail[0].zipcode)) {
      this.editAddConfirm();
    }
  }

  updateSearch() {
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types: ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocomplete.query,
      //componentRestrictions: { country: 'AR' } 
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      self.autocompleteItems = [];
      console.log(predictions);
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      });
    });
  }
  /*##Map function Start##*/
  // getUserPosition(){
  //   this.options = {
  //        enableHighAccuracy : true
  //   };

  //    this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {

  //        this.currentPos = pos;      
  //        console.log(pos.coords.latitude+','+pos.coords.longitude);
  //        this.addMap(pos.coords.latitude,pos.coords.longitude);

  //    },(err : PositionError)=>{
  //       console.log("error : " + err.message);
  //    });
  // }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    // this.getUserPosition();
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }
  addInfoWindow(marker, content) {

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }

  addMap(lat, long) {
    let latLng = new google.maps.LatLng(lat, long);
    let mapOptions = {
      center: latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      fullscreenControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      draggable: false,
      zoomControl: false
      //draggable: this.mapcondition, zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
    }

    //  var searchBox = new google.maps.places.SearchBox(this.search);
    //  this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(this.search);
    //  google.maps.event.addListener(searchBox, 'places_changed', function() {
    //    searchBox.set('map', null);


    //    var places = searchBox.getPlaces();

    //    var bounds = new google.maps.LatLngBounds();
    //    var i, place;
    //    for (i = 0; place = places[i]; i++) {
    //      (function(place) {
    //        var marker = new google.maps.Marker({

    //          position: place.geometry.location
    //        });
    //        marker.bindTo('map', searchBox, 'map');
    //        google.maps.event.addListener(marker, 'map_changed', function() {
    //          if (!this.getMap()) {
    //            this.unbindAll();
    //          }
    //        });
    //        bounds.extend(place.geometry.location);


    //      }(place));

    //    }
    //    this.map.fitBounds(bounds);
    //    searchBox.set('map', this.map);
    //    this.map.setZoom(Math.min(this.map.getZoom(),12));

    //  });

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker();
    this.updateSearch();
  }
  addMarker() {
    let nabo_img = 'http://rayi.in/naboapi/mapicon/new-markers/blue.png';
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(parseFloat(this.marker_data.latitude), parseFloat(this.marker_data.longitude)),
      icon: nabo_img,
    });
    let trans_msg = '';
    this.translate.get('This is your position').subscribe(res => {
      trans_msg = res;
    });
    let content = "<p>" + trans_msg + "</p>";
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    this.lastLatLng(marker)
    google.maps.event.addListener(marker, 'click', (event) => {
      //infoWindow.open(this.map, marker);
      alert(JSON.stringify(event));
    });
  }

  addressDisplayToast(address) {

    let toast = this.toastCtrl.create({
      message: address,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  lastLatLng(marker) {
    let geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(this.map, 'dragend', () => {
      //  this.LastLat1= marker.position.lat();
      //  this.LastLng1= marker.position.lng();
      this.LastLat1 = this.map.getCenter().lat();
      this.LastLng1 = this.map.getCenter().lng();
      var coord = new Array();
      coord.push(this.LastLat1, this.LastLng1);
      console.log(coord[0] + ',' + coord[1]);
      let latlng = new google.maps.LatLng(this.LastLat1, this.LastLng1);
      let request = { latLng: latlng };
      console.log(this.LastLat1 + ',' + this.LastLng1);
      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          //console.log(rsltAdrComponent);
          console.log(result.formatted_address);
          this.addressDisplayToast(result.formatted_address);
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
            //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
            var data = new Array;
            var locationdata = new Array();
            for (var i = 0; i < rsltAdrComponent.length; i++) {
              var obj = rsltAdrComponent[i];
              data.push(obj.long_name);

            }
            //console.log(locdata);
            if (this.addAddress == true) {
              console.log('new address');
              if (data.length == 5) {
                this.newAddAry.street_address = data[1] + " " + data[0];
                this.newAddAry.country = data[3];
                this.newAddAry.city = data[2];
                this.newAddAry.zipcode = data[4];
                this.newAddAry.latitude = coord[0];
                this.newAddAry.longitude = coord[1];
              }
              else if (data.length == 6) {
                this.newAddAry.street_address = data[1] + " " + data[0];
                this.newAddAry.country = data[4];
                this.newAddAry.city = data[2];
                this.newAddAry.zipcode = data[5];
                this.newAddAry.latitude = coord[0];
                this.newAddAry.longitude = coord[1];
              } else if (data.length == 7) {
                this.newAddAry.street_address = data[1] + " " + data[0];
                this.newAddAry.country = data[5];
                this.newAddAry.city = data[2];
                this.newAddAry.zipcode = data[6];
                this.newAddAry.latitude = coord[0];
                this.newAddAry.longitude = coord[1];
              }
              else if (data.length == 8) {
                this.newAddAry.street_address = data[1] + " " + data[0];
                this.newAddAry.country = data[6];
                this.newAddAry.city = data[4];
                this.newAddAry.zipcode = data[7];
                this.newAddAry.latitude = coord[0];
                this.newAddAry.longitude = coord[1];
              } else if (data.length == 9) {
                this.newAddAry.street_address = data[1] + " " + data[0];
                this.newAddAry.country = data[7];
                this.newAddAry.city = data[4];
                this.newAddAry.zipcode = data[8];
                this.newAddAry.latitude = coord[0];
                this.newAddAry.longitude = coord[1];
              } else if (data.length == 10) {
                this.newAddAry.street_address = data[1] + " " + data[0];
                this.newAddAry.country = data[8];
                this.newAddAry.city = data[5];
                this.newAddAry.zipcode = data[9];
                this.newAddAry.latitude = coord[0];
                this.userdetail[0].longitude = coord[1];
              }
            } else {
              if (data.length == 5) {
                this.userdetail[0].street_address = data[1] + " " + data[0];
                this.userdetail[0].country = data[3];
                this.userdetail[0].city = data[2];
                this.userdetail[0].zipcode = data[4];
                this.userdetail[0].latitude = coord[0];
                this.userdetail[0].longitude = coord[1];
              }
              else if (data.length == 6) {
                this.userdetail[0].street_address = data[1] + " " + data[0];
                this.userdetail[0].country = data[4];
                this.userdetail[0].city = data[2];
                this.userdetail[0].zipcode = data[5];
                this.userdetail[0].latitude = coord[0];
                this.userdetail[0].longitude = coord[1];
              } else if (data.length == 7) {
                this.userdetail[0].street_address = data[1] + " " + data[0];
                this.userdetail[0].country = data[5];
                this.userdetail[0].city = data[2];
                this.userdetail[0].zipcode = data[6];
                this.userdetail[0].latitude = coord[0];
                this.userdetail[0].longitude = coord[1];
              }
              else if (data.length == 8) {
                this.userdetail[0].street_address = data[1] + " " + data[0];
                this.userdetail[0].country = data[6];
                this.userdetail[0].city = data[4];
                this.userdetail[0].zipcode = data[7];
                this.userdetail[0].latitude = coord[0];
                this.userdetail[0].longitude = coord[1];
              } else if (data.length == 9) {
                this.userdetail[0].street_address = data[1] + " " + data[0];
                this.userdetail[0].country = data[7];
                this.userdetail[0].city = data[4];
                this.userdetail[0].zipcode = data[8];
                this.userdetail[0].latitude = coord[0];
                this.userdetail[0].longitude = coord[1];
              } else if (data.length == 10) {
                this.userdetail[0].street_address = data[1] + " " + data[0];
                this.userdetail[0].country = data[8];
                this.userdetail[0].city = data[5];
                this.userdetail[0].zipcode = data[9];
                this.userdetail[0].latitude = coord[0];
                this.userdetail[0].longitude = coord[1];
              }
            }
            console.log(this.userdetail);
          } else {
            let trans_no_address = '';
            this.translate.get('No address available').subscribe(res => {
              trans_no_address = res;
            });
            alert(trans_no_address);
          }
        }
      });
    });
  }
  // google.maps.event.addDomListener(window, 'load', init);
  /*##Map function end##*/
  /*##User image upload function Start##*/
  ImagepresentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  // getImage() {
  //   const options: CameraOptions = {
  //     quality: 100,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
  //   }

  //   this.camera.getPicture(options).then((imageData) => {
  //     this.imageURI = imageData;
  //   }, (err) => {
  //     console.log(err);
  //     //this.presentToast(err);
  //   });
  // }

  // uploadFile() {
  //   let loader = this.loadingCtrl.create({
  //     content: "Uploading..."
  //   });
  //   loader.present();
  //   const fileTransfer: FileTransferObject = this.transfer.create();

  //   let options: FileUploadOptions = {
  //     fileKey: 'ionicfile',
  //     fileName: 'ionicfile',
  //     chunkedMode: false,
  //     mimeType: "image/jpeg",
  //     headers: {}
  //   }

  //   fileTransfer.upload(this.imageURI, 'http://rayi.in/naboApi/profileImageupload', options)
  //     .then((data) => {
  //     console.log(data+" Uploaded Successfully");
  //     this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
  //     loader.dismiss();
  //     this.ImagepresentToast("Image uploaded successfully");
  //   }, (err) => {
  //     console.log(err);
  //     loader.dismiss();
  //     this.ImagepresentToast(err);
  //   });
  // }

  public chooseimg() {
    this.translate.get('Load from Library').subscribe(res => {
      this.content = res;
    });
    let action_cancel = '';
    this.translate.get('cancel').subscribe(res => {
      action_cancel = res;
    });
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: this.content,
          handler: () => {
            this.platform.ready().then(() => {
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            });
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.platform.ready().then(() => {
              this.takePicture(this.camera.PictureSourceType.CAMERA);
            });
          }
        },
        {
          text: action_cancel,
          role: 'cancel',
          handler: () => {
            this.fileopen = false;
          }
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.crop.crop(imagePath, { quality: 75 }).then(
          (newImage) => {
            console.log('new image path is: ' + newImage);
            this.filePath.resolveNativePath(newImage)
              .then(filePath => {
                let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                let currentName = newImage.substring(newImage.lastIndexOf('/') + 1, newImage.lastIndexOf('?'));
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
              });
          },
          (error) => {
            console.error('Error cropping image', error);
          });
      } else {
        this.crop.crop(imagePath, { quality: 75 }).then(
          (newImage) => {
            this.filePath.resolveNativePath(newImage).then(filePath => {
              var currentName = filePath.substr(filePath.lastIndexOf('/') + 1);
              var correctPath = newImage.substr(0, newImage.lastIndexOf('/') + 1);
              this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            });
          },
          (error) => {
            console.error('Error cropping image', error);
          });
      }
    }, (err) => {
      let trans_img_err = "";
      this.translate.get('Error while selecting image.').subscribe(res => {
        trans_img_err = res;
      });
      this.ImagepresentToast(trans_img_err);
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      let trans_img_err_msg = '';
      this.translate.get('Error while storing file.').subscribe(res => {
        trans_img_err_msg = res;
      });
      this.ImagepresentToast(trans_img_err_msg);
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      //return 'assets/imgs/user.jpg';
      var imgData = JSON.parse(localStorage.getItem('userData'));
      var image = "http://rayi.in/naboapi/uploads/" + imgData['photo'];
      return image;
    } else {
      this.fileopen = true;
      return cordova.file.dataDirectory + img;
    }
  }

  public uploadImage() {
    // Destination URL
    var url = "http://rayi.in/naboapi/profileImageupload";

    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);

    // File name only
    var filename = this.lastImage;
    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: { 'fileName': filename }
    };

    const fileTransfer: FileTransferObject = this.transfer.create();

    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();

    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then((data) => {
      this.loading.dismissAll();
      console.log(data);
      var result = data.response;
      if (result == 'true') {
        var userData = {}
        userData = JSON.parse(localStorage.getItem('userData'));
        userData['photo'] = this.lastImage;
        localStorage.setItem('userData', JSON.stringify(userData));
        this.AuthServiceProvider.postData(userData, 'updateUserImagename').then((result) => {
          // loading.present();
          this.responseData = result;
          var user_detail = this.responseData;
          if (true == this.responseData.status) {
            this.fileopen = false;
            this.ImagepresentToast('Image succesful uploaded.');
            this.refresh();
          }
        }, (err) => {
          this.fileopen = false;
          let trans_img_up_err = '';
          this.translate.get('Image upload failed.').subscribe(res => {
            trans_img_up_err = res;
          });
          this.ImagepresentToast(trans_img_up_err);
        });
        // this.ImagepresentToast('Image succesful uploaded.');
        // this.fileopen = false;
      }
    }, err => {
      this.loading.dismissAll()
      this.fileopen = false;
      let trans_img_up_err_while = '';
      this.translate.get('Error while uploading file.').subscribe(res => {
        trans_img_up_err_while = res;
      });
      this.ImagepresentToast('trans_img_up_err_while');
    });
  }

  getSelectAddress() {
    console.log("selected address id", this._addressid);
    this.addupdate = this.addresses.filter(myOdbj => myOdbj.address_id == this._addressid);
    console.log(this.addupdate);
    this.userdetail[0].address_id = this.addupdate[0].address_id;
    this.userdetail[0].street_address = this.addupdate[0].street_address;
    this.userdetail[0].city = this.addupdate[0].city;
    this.userdetail[0].country = this.addupdate[0].country;
    this.userdetail[0].zipcode = this.addupdate[0].zipcode;
    this.userdetail[0].camtoken = this.addupdate[0].camtoken;
  }

  addNewAddress() {
    this.updateAddress = false;
    this.addAddress = true;
    this._addCtrl = true;
    this._ispaid = false;
    this.addaddress = false;
    this.serachopen = true;
    // let alert = this.alertCtrl.create({
    //   title:'Address',
    //   message: 'Address search from map?',
    //   buttons: [
    //     {
    //       text: 'Yes',
    //       role: 'cancel',
    //       handler: () => {
    //         this.viewCard=false;
    //         this.viewMap=true;
    //       }
    //     },
    //     {
    //       text: 'No',
    //       handler: () => {
    //         console.log('No clicked');
    //       }
    //     }
    //   ]
    // });
    // alert.present();
  }

  editAddress() {
    this.addaddress = false;
    this.updateAddress = true;
    this.addAddress = false;
    this._addCtrl = false;
    this._editCtrl = true;
    this._ispaid = false;
    this.editDisabled = false;
    this.serachopen = true;
  }

  addCancel() {
    this._addCtrl = false;
    this._ispaid = true;
    this.updateAddress = true;
    this.addAddress = false;
    this.serachopen = false;
  }
  editCancel() {
    this.viewCard = true;
    this.viewMap = false;
    this._editCtrl = false;
    this.editDisabled = true;
    this.serachopen = false;
  }
  addClear() {
    this.newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "camtoken": "" };
  }
  addAdd() {
    this._addCtrl = false;
    this._ispaid = true;
    this.updateAddress = true;
    this.addAddress = false;
  }
  returnPage() {
    this.viewCard = true;
    this.viewMap = false;
  }
  /*##User image upload function end##*/
  refresh() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
    console.log('page refreshed');
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage, this.addresses);
    popover.present({
      ev: myEvent
      //data:this.addresses;
    });
    popover.onDidDismiss(data => {
      console.log(data);
      if (data != null) {
        this.selectedData = data
      }
      this.userdetail[0].address_id = this.selectedData.address_id;
      this.userdetail[0].street_address = this.selectedData.street_address;
      this.userdetail[0].city = this.selectedData.city;
      this.userdetail[0].country = this.selectedData.country;
      this.userdetail[0].zipcode = this.selectedData.zipcode;
      this.userdetail[0].camtoken = this.selectedData.camtoken;
      this.marker_data.latitude = this.selectedData.latitude;
      this.marker_data.longitude = this.selectedData.longitude;
      this.addMap(this.marker_data.latitude, this.marker_data.longitude);
    });
  }

  //confirm alert for new address set as primary address
  defaultAddConfirm() {
    let trans_alert_title = '';
    let trans_alert_msg = '';
    let trans_alert_btn_no = '';
    let trans_alert_btn_yes = '';
    this.translate.get('Confirm this address').subscribe(res => {
      trans_alert_title=res;
    }); 
    this.translate.get('Do you want to set this as primary address?').subscribe(res => {
      trans_alert_msg=res;
    }); 
    this.translate.get('No').subscribe(res => {
      trans_alert_btn_no =res;
    }); 
    this.translate.get('Yes').subscribe(res => {
      trans_alert_btn_yes=res;
    });
    let alert = this.alertCtrl.create({
      title: trans_alert_title,
      message: trans_alert_msg,
      buttons: [
        {
          text: trans_alert_btn_no,
          handler: () => {
            console.log('Cancel clicked');
            var state = this.getNewAddressByName();
            if (state)
              this.newAddAry.primary_address = "no";
            this.addMoreAddress();
          }
        },
        {
          text: trans_alert_btn_yes,
          handler: () => {
            console.log('yes clicked');
            var state = this.getNewAddressByName();
            if (state)
              this.newAddAry.primary_address = "yes";
            this.addMoreAddress();
          }
        }
      ]
    });
    alert.present();
  }

  editAddConfirm() {
    let trans_alert_title = '';
    let trans_alert_msg = '';
    let trans_alert_btn_no = '';
    let trans_alert_btn_yes = '';
    this.translate.get('Confirm this address').subscribe(res => {
      trans_alert_title=res;
    }); 
    this.translate.get('Do you want to set this as primary address?').subscribe(res => {
      trans_alert_msg=res;
    }); 
    this.translate.get('No').subscribe(res => {
      trans_alert_btn_no =res;
    }); 
    this.translate.get('Yes').subscribe(res => {
      trans_alert_btn_yes=res;
    });
    let alert = this.alertCtrl.create({
      title: trans_alert_title,
      message: trans_alert_msg,
      buttons: [
        {
          text: trans_alert_btn_no,
          handler: () => {
            console.log('Cancel clicked');
            var state = this.updateOldAddress();
            if (state)
              this.userdetail[0].primary_address = "no";
            this.updateEditAddress();
          }
        },
        {
          text: trans_alert_btn_yes,
          handler: () => {
            console.log('yes clicked');
            var state = this.updateOldAddress();
            if (state)
              this.userdetail[0].primary_address = "yes";
            this.updateEditAddress();
          }
        }
      ]
    });
    alert.present();
  }

  //confirm alert for registered address set as primary address
  defaultUpdateConfirm() {
    let trans_alert_title = '';
    let trans_alert_msg = '';
    let trans_alert_btn_no = '';
    let trans_alert_btn_yes = '';
    this.translate.get('Confirm this address').subscribe(res => {
      trans_alert_title=res;
    }); 
    this.translate.get('Do you want to set this as primary address?').subscribe(res => {
      trans_alert_msg=res;
    }); 
    this.translate.get('No').subscribe(res => {
      trans_alert_btn_no =res;
    }); 
    this.translate.get('Yes').subscribe(res => {
      trans_alert_btn_yes=res;
    });
    let alert = this.alertCtrl.create({
      title: trans_alert_title,
      message: trans_alert_msg,
      buttons: [
        {
          text: trans_alert_btn_no,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: trans_alert_btn_yes,
          handler: () => {
            console.log('yes clicked');
            this.setPrimaryAddress();
          }
        }
      ]
    });
    alert.present();
  }
  //api call to set address primary
  setPrimaryAddress() {
    let trans_load_cont = '';
    this.translate.get('Profile updating...').subscribe(res=>{
      trans_load_cont=res;
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: trans_load_cont
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);
    console.log(this.userdetail[0]);
    this.AuthServiceProvider.postData(this.userdetail[0], 'updatePrimaryAddress').then((result) => {
      this.responseData = result;
      var user_detail = this.responseData;
      if (true == this.responseData.status) {
        let trans_toast_update='';
        this.translate.get('updated as primary address.').subscribe(res=>{
          trans_toast_update=res;
        });
        this.toastmessage = trans_toast_update;
        this.toastViewer(this.toastmessage);

      } else {

      }
    });
  }

  recrangeUser($event) {
    console.log($event._value);
    this.recrange = $event._value;
    let user = JSON.parse(localStorage.getItem('userData'));
    let rngObj = {
      "user_id": user.user_id,
      "r_distance": this.recrange
    };
    this.AuthServiceProvider.postData(rngObj, 'updateRDistance').then((result) => {
      console.log(result);
      this.responseData = result;
      if (this.responseData.status == true) {
        this.RvalueData = this.recrange;
        // if(this.RvalueData <= 999) {
        this.Runit = ' M';
        // } else {
        // console.log('yes');
        // this.Runit = 'Kms ';
        //alert(this.RvalueData);
        // this.RvalueData = this.RvalueData/1000;
        // }
      }
    },
      (err) => {
        console.log(err);
      });
  }

  toggle($event) {
    console.log($event);
    this.dnd = $event;
    let user = JSON.parse(localStorage.getItem('userData'));
    let dndObj = {
      "user_id": user.user_id,
      "notify": this.dnd
    };
    this.AuthServiceProvider.postData(dndObj, 'dnd').then((result) => {
      this.responseData = result;
      if (true == this.responseData.status) {
        console.log('Mode changed.');
        let userDND = JSON.parse(localStorage.getItem('userData'));
        let dnd_chg:any;
        if(this.dnd == true){
          dnd_chg=1;
        }else{
          dnd_chg=0;
        }
        userDND.do_not_distrub = dnd_chg;
        localStorage.setItem('userData', JSON.stringify(userDND));
      } else {
        console.log('Failed.');
      }
    });
  }

}