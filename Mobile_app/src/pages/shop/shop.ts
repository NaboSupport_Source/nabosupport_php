// Page name: Shop page (This page load product from api for shop)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, MenuController, IonicPage } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-shop',
  templateUrl: 'shop.html',
})
export class ShopPage {
  base_url='http://rayi.in/nabosupport-web/api/upload/product/';
  shop: any;
  itemCount: any;
  products: any;
  categoryList: any = [];
  chooseCategory: any;
  rs:any;
  isEmpty:boolean;
  adminData:any;
  shopURL:any;
  categoryTitle: { title: string, subTitle: string };
  category: any;
  constructor(
    public provider: AuthServiceProvider,
    public navCtrl: NavController,
    private menu: MenuController,
    public navParams: NavParams,
    public alertCtrl:AlertController,
    public browser:InAppBrowser,
    platform: Platform,
  ) {
    this.categoryTitle = {
      title: 'Category',
      subTitle: ''
    };

  }

  //redirected to Nabosupport webpage
  webpage() {
    let alert = this.alertCtrl.create({
      title:'Shop page will be redirected to browser',
      message: 'Press Yes to Continue',
      buttons: [
        {
          text: 'Yes',
          role: 'cancel',
          handler: () => {
            console.log('Yes clicked');
            let openBrowser = this.browser.create('http://rayi.in/nabosupport/'+this.shopURL, '_system','location=no')
          }
        },
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    alert.present();
  }

  //ionic lifecycle event
  ionViewDidLoad() {
    this.getCategoryList();
    this.adminData = JSON.parse(localStorage.getItem('adminData'));
    console.log( this.adminData);
      for(let i=0; i<this.adminData.data.length; i++){
        console.log('service:'+this.adminData.data[i].data);
      }
      this.shopURL = this.adminData.data[3].data;
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //get category list from api
  getCategoryList() {
    this.provider.getData('getCategory').then((res) => {
      this.rs = res;
      if (this.rs.status === true) {
        this.categoryList = this.rs.data;
        if (localStorage.getItem('cat_id')) {
          this.chooseCategory = this.categoryList[0].cat_id;
          this.onCategoryChange(localStorage.getItem('cat_id'));
        } else {
          // chooseCategory
          this.chooseCategory = this.categoryList[0].cat_id;
          this.onCategoryChange(this.categoryList[0].cat_id);
        }

      }
    }, (err) => {
      console.log(err);
    })
  }

  //category on change event capture
  onCategoryChange(event) {
    if (event) {
      localStorage.setItem('cat_id', event);
      var pro_data:any = {
        "cat_id":event
      }
      this.provider.postData(pro_data,'getProduct').then((res) => {
        this.rs = res;
        if (this.rs.status === true) {
          this.products = this.rs.data;
          if (this.rs.data.length === 0) {
            this.isEmpty = true;
          } else {
            this.isEmpty = false;
          }
        } else {
          this.isEmpty = true;
        }
      }, (err) => {
        console.log(err);
      })
    }
  }
  cart() {
    console.log('cart click.');
  }
}