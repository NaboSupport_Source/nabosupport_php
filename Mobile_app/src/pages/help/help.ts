import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }

  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat',lat);
    localStorage.setItem('lastLng',lng);
    this.navCtrl.setRoot(MapPage);
  }

}
