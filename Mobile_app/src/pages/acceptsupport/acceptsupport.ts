import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-acceptsupport',
  templateUrl: 'acceptsupport.html',
})
export class AcceptsupportPage {
  responsedata:any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public authservice:AuthServiceProvider, 
    public viewCtrl:ViewController,
    public toastCtrl:ToastController,
    public events: Events,
    public translate:TranslateService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AcceptsupportPage');
  }
  acceptReq() {
    let locdata = JSON.parse(localStorage.getItem('userData'));
    let recdata = JSON.parse(localStorage.getItem('accData'));
    let reqData = {
      'user_id':locdata.user_id,
      'sender_id':recdata.sender_id,
      'email':recdata.email
    };
    this.authservice.postData(reqData,'acceptReq').then(res => {
      this.responsedata = res;
      console.log(this.responsedata);
      if(this.responsedata.status == true) {
        let flaf = 1; 
        this.events.publish('user:ref',flaf);
        this.toastMessage('thankreq');
        this.viewCtrl.dismiss();
      } else {
        this.toastMessage('reqfail');
      }
    }, err => {
      console.log(err);
      this.toastMessage('reqfail');
    });
  }

  rejectReq() {
    let locdata = JSON.parse(localStorage.getItem('userData'));
    let recdata = JSON.parse(localStorage.getItem('accData'));
    let reqData = {
      'user_id':locdata.user_id,
      'sender_id':recdata.sender_id,
      'email':recdata.email
    };
    this.authservice.postData(reqData,'acceptReject').then(res => {
      this.responsedata = res;
      console.log(this.responsedata);
      if(this.responsedata.status == true) {
        let flaf = 1; 
        this.events.publish('user:ref',flaf);
        this.toastMessage('reject');
        this.viewCtrl.dismiss();
      } else {
        this.toastMessage('reqfail');
      }
    }, err => {
      console.log(err);
      this.toastMessage('reqfail');
    });
  }

  toastMessage(msg:any) {
    let toasttxt:any;
    this.translate.get(msg).subscribe(res =>{
      toasttxt = res;
    });
    let toast = this.toastCtrl.create({
      message: toasttxt,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  modalClose() {
    this.viewCtrl.dismiss();
  }
}
