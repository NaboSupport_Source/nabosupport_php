import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcceptsupportPage } from './acceptsupport';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AcceptsupportPage,
  ],
  imports: [
    IonicPageModule.forChild(AcceptsupportPage),
    TranslateModule.forChild()],
  exports:[
    AcceptsupportPage
  ]
})
export class AcceptsupportPageModule {}
