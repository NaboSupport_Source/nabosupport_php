// Page name: Reset password page (This page send data to api and validate then send reset link inbox)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component } from '@angular/core';
import { ToastController, NavController,LoadingController, MenuController,IonicPage } from 'ionic-angular';
import {ForgotpasswordPage} from '../forgotpassword/forgotpassword';
import { SigninPage} from '../signin/signin';
import { NgForm} from '@angular/forms';
import {NativeStorage} from '@ionic-native/native-storage';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {FormGroup, FormControl,Validators} from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {

  form2= new FormGroup({
    password: new FormControl(),
    cpassword: new FormControl()
  });
  //get data from form field
  get password(){
    return this.form2.get('password');
  }
 
  get cpassword(){
    return this.form2.get('cpassword');
  }
  responseData : any;
  userResetData = {"user_id": "","password": "", "cpassword": ""};
  constructor(
              public navCtrl: NavController,
              private menu: MenuController,
              private nativeStorage:NativeStorage,
              private toastCtrl:ToastController,
              private loadingCtrl:LoadingController, 
              public AuthServiceProvider: AuthServiceProvider
              ) {
                localStorage.setItem('userResetData', "");
  }

  //ionic lifecycle event
  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //send emailID to api for password reset
  reset(){
    let toast = this.toastCtrl.create({
      message: 'Password not matched',
      duration: 3000,
      position: 'bottom'
    });
    let toast1 = this.toastCtrl.create({
      message: 'Please provide Password',
      duration: 3000,
      position: 'bottom'
    });
    let toast2 = this.toastCtrl.create({
      message: 'Your password changed..',
      duration: 3000,
      position: 'bottom'
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Authenticating...'
    });
 
    setTimeout(() => {
      loading.dismiss();
    }, 500);

if(this.userResetData.password && this.userResetData.cpassword){
  if(this.userResetData.password != this.userResetData.cpassword) {
     toast.present();
  } else {
    var userdata= JSON.parse(localStorage.getItem('userForgotData'));
    this.userResetData.user_id = userdata.user_id;
    this.AuthServiceProvider.postData(this.userResetData,'resetpassword').then((result) => {
      this.responseData = result;
      console.log(result);
      if(true == this.responseData.status){
      localStorage.setItem('userResetData', JSON.stringify(this.responseData));
      console.log( JSON.parse(localStorage.getItem('userResetData')));
      toast2.present();
      this.navCtrl.setRoot(SigninPage);
      }else{
        toast.present();
      }
    }, (err) => {
      // Error log
    });
  }
  
}else{
  if(!(this.userResetData.password)||!(this.userResetData.cpassword)){
    toast1.present();
  }
 
}
  }

}
