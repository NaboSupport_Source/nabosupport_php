import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InvitefriendPage } from './invitefriend';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    InvitefriendPage,
  ],
  imports: [
    IonicPageModule.forChild(InvitefriendPage),
    TranslateModule.forChild()
  ],
  exports:[
    InvitefriendPage
  ]
})
export class InvitefriendPageModule {}
