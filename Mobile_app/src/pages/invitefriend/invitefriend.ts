import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TranslateService } from '@ngx-translate/core';
import { MapPage } from '../map/map';
@IonicPage()
@Component({
  selector: 'page-invitefriend',
  templateUrl: 'invitefriend.html',
})
export class InvitefriendPage {
  inviteForm: any;
  emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
  name:any = '';
  email:any = ''
  txtview:boolean = false;
  formview:boolean = true;
  alertMessage:any;
  formData:any;
  resdata:any;
  adminData:any;
  promocode:any;
  en:boolean;
  dn:boolean;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public toastCtrl:ToastController,
              public AuthServiceProvider: AuthServiceProvider,
              private translate: TranslateService) {

    this.inviteForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('',[Validators.required,Validators.pattern(this.emailPattern)])
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitefriendPage');
    this.adminData = JSON.parse(localStorage.getItem('adminData'));
    this.promocode = this.adminData.data[6].data;
    let user = JSON.parse(localStorage.getItem('userData'));
    let lang = user.language;
    if(lang == 'en') {
      this.en = true;
      this.dn = false;
    } else {
      this.dn = true;
      this.en = false;
    }
  }
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat',lat);
    localStorage.setItem('lastLng',lng);
    this.navCtrl.setRoot(MapPage);
  }
  alertToast(msg:any) {
    let message:any;
    this.translate.get(msg).subscribe(res => {
      message = res;
    });
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  onSubmit(value: any): void {
    let userdata = JSON.parse(localStorage.getItem('userData'));
    value.username = userdata.user_login;
    this.formData = value;
    console.log(value);
    if (this.inviteForm.valid) {
      window.localStorage.setItem('username', value.username);
      window.localStorage.setItem('password', value.password);
      this.formview = false;
      this.txtview = true;
      //alert('works!');
    } else if(this.inviteForm.controls.name.hasError('required') == true) {
      this.alertMessage = 'Please enter name';
      this.alertToast(this.alertMessage);
    } else if(this.inviteForm.controls.email.hasError('required') == true) {
      this.alertMessage = 'Please enter email';
      this.alertToast(this.alertMessage);
    } else if(this.inviteForm.controls.email.hasError('pattern') == true) {
      this.alertMessage = 'Please enter valid email';
      this.alertToast(this.alertMessage);
    }
  }

  sendInvite() {
    this.formview = true;
    this.txtview = false;
    this.inviteForm.reset();
    this.alertMessage = 'Thank you for inviting your friend.';
    this.alertToast(this.alertMessage);
    this.AuthServiceProvider.postData(this.formData,'inviteFriend').then(result => {
      console.log(result);
      this.resdata = result;
    }, err => {
      
    });
  }

}
