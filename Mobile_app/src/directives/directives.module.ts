import { NgModule } from '@angular/core';
import { FocuserDirective } from './focuser/focuser';
import { LanguageDirective } from './language/language';
@NgModule({
	declarations: [FocuserDirective,
    LanguageDirective],
	imports: [],
	exports: [FocuserDirective,
    LanguageDirective]
})
export class DirectivesModule {}
