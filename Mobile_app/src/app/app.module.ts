import { BrowserModule } from '@angular/platform-browser';
import { Device } from '@ionic-native/device';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule,IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HttpModule, Http } from '@angular/http';
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage'; 
import { ForgotpasswordPage } from '../pages/forgotpassword/forgotpassword';
import { ForgotpasswordPageModule } from '../pages/forgotpassword/forgotpassword.module';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { ResetpasswordPageModule } from '../pages/resetpassword/resetpassword.module';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { MyaccountPageModule } from '../pages/myaccount/myaccount.module';
import { SigninPage } from '../pages/signin/signin';
import { SigninPageModule } from '../pages/signin/signin.module';
import { SignupPage } from '../pages/signup/signup';
import { SignupPageModule } from '../pages/signup/signup.module';
import { NewaddressPage } from '../pages/newaddress/newaddress';
import { NewaddressPageModule } from '../pages/newaddress/newaddress.module';
import { AddressPage } from '../pages/address/address';
import { AddressPageModule } from '../pages/address/address.module';
import { MapPage } from '../pages/map/map';
import { MapPageModule } from '../pages/map/map.module';
import { ContactPage } from '../pages/contact/contact';
import { ContactPageModule } from '../pages/contact/contact.module';
import { ContactusPage } from '../pages/contactus/contactus';
import { ContactusPageModule } from '../pages/contactus/contactus.module'; 
import { SecurityPage } from '../pages/security/security';
import { SecurityPageModule } from '../pages/security/security.module';
import { NotificationPage} from '../pages/notification/notification';
import { NotificationPageModule} from '../pages/notification/notification.module';
import { SettingPage } from '../pages/setting/setting';
import { SettingPageModule } from '../pages/setting/setting.module';
import { ShopPage } from './../pages/shop/shop';
import { ShopPageModule } from './../pages/shop/shop.module';
import { HelpPage} from './../pages/help/help';
import { HelpPageModule } from './../pages/help/help.module';
import { OfferPage } from './../pages/offer/offer';
import { OfferPageModule } from './../pages/offer/offer.module';
import { ModalPage } from './../pages/modal/modal';
import { ModalPageModule } from './../pages/modal/modal.module';
import { AcceptsupportPage } from './../pages/acceptsupport/acceptsupport';
import { AcceptsupportPageModule } from './../pages/acceptsupport/acceptsupport.module';
import { EmailComposer} from '@ionic-native/email-composer';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'
import { Keyboard } from '@ionic-native/keyboard';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Diagnostic } from '@ionic-native/diagnostic';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { FilePath } from '@ionic-native/file-path';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { Directive, HostListener } from '@angular/core';
import { GoogleMaps } from '../providers/google-service/google-maps';
import { AgmMap,AgmCoreModule,GoogleMapsAPIWrapper } from '@agm/core';
import { AgmJsMarkerClustererModule,AgmMarkerCluster,ClusterManager } from '@agm/js-marker-clusterer';
import { Network } from '@ionic-native/network';
import { AppVersion } from '@ionic-native/app-version';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Crop } from '@ionic-native/crop';
import { PopoverPage} from './../pages/popover/popover';
import { PopoverPageModule } from './../pages/popover/popover.module'
import { GetUserAddressProvider } from '../providers/get-user-address/get-user-address';
import { InvitefriendPage} from '../pages/invitefriend/invitefriend';
import { InvitefriendPageModule } from '../pages/invitefriend/invitefriend.module';
import {AboutusPage} from '../pages/aboutus/aboutus';
import {AboutusPageModule} from '../pages/aboutus/aboutus.module';
export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    FormsModule,ReactiveFormsModule,
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
    loader: {
         provide: TranslateLoader,
         useFactory: (createTranslateLoader),
         deps: [Http]
       }
    }),
    IonicModule.forRoot(MyApp),
    MyaccountPageModule,
    SigninPageModule,
    SignupPageModule,
    AddressPageModule,
    NewaddressPageModule, 
    ForgotpasswordPageModule,
    ResetpasswordPageModule,
    ContactPageModule,
    NotificationPageModule, 
    SecurityPageModule,
    ShopPageModule,
    HelpPageModule,
    OfferPageModule,
    ContactusPageModule,
    SettingPageModule,
    MapPageModule, 
    ModalPageModule,
    AcceptsupportPageModule,
    PopoverPageModule,
    InvitefriendPageModule,
    AboutusPageModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAlJZd_87izwp5nfqW1hoqbSwAWmX8X4Cc',
      libraries: ['places']
    }),
    AgmJsMarkerClustererModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MyaccountPage,
    SigninPage,
    SignupPage,
    AddressPage, 
    NewaddressPage,
    ForgotpasswordPage,
    ResetpasswordPage,
    ContactPage,
    NotificationPage, 
    SecurityPage,
    ShopPage,
    HelpPage,
    OfferPage,
    ContactusPage,
    SettingPage,
    MapPage, 
    ModalPage,
    AcceptsupportPage,
    PopoverPage,
    InvitefriendPage,
    AboutusPage
  ],
  providers: [
    GooglePlus,
    Push,
    Device,
    NativeStorage,
    Keyboard ,
    FileTransfer,
    FileTransferObject,
    File,
    EmailComposer,
    FilePath,
    Camera,
    Crop,
    StatusBar,
    SplashScreen,
    Geolocation,
    Network,
    AppVersion,
    Diagnostic,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    GoogleMaps,
    InAppBrowser,
    TranslateService,
    LocationAccuracy,
    AgmMap,
    AgmMarkerCluster,
    ClusterManager,
    GetUserAddressProvider
  ]
})
export class AppModule {}
