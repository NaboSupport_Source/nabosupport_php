import { SigninPage } from './../pages/signin/signin';
import { AddressPage } from './../pages/address/address';
import { ForgotpasswordPage } from './../pages/forgotpassword/forgotpassword';
import { SignupPage } from './../pages/signup/signup';
import { ShopPage } from './../pages/shop/shop';
import { HelpPage } from './../pages/help/help';
import { OfferPage } from './../pages/offer/offer';
import { ModalPage } from './../pages/modal/modal';
import { Component, ViewChild, NgZone } from '@angular/core';
import { Platform, MenuController, Nav, AlertController, Events, ToastController, Config, ModalController } from 'ionic-angular';
import { HttpModule, Http } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { GooglePlus } from '@ionic-native/google-plus';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Device } from '@ionic-native/device';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { AppVersion } from '@ionic-native/app-version';

import { AcceptsupportPage } from '../pages/acceptsupport/acceptsupport';
import { MapPage } from '../pages/map/map';
import { NewaddressPage } from '../pages/newaddress/newaddress';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { ContactPage } from '../pages/contact/contact';
import { ContactusPage } from '../pages/contactus/contactus';
import { SecurityPage } from '../pages/security/security';
import { NotificationPage } from '../pages/notification/notification';
import { SettingPage } from '../pages/setting/setting';
import { InvitefriendPage } from '../pages/invitefriend/invitefriend';
import { AboutusPage } from '../pages/aboutus/aboutus';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Base64 } from 'js-base64';
import * as moment from 'moment';
import ontime from 'ontime';
declare var google: any;
export interface PageInterface {
  title: string;
  component: any;
  icon: string;
}
@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage: any = MapPage;
  pages: Array<{ title: string, component: any, icon: any }>;
  pages_1: Array<{ title: string, component: any, icon: any }>;
  subpages: Array<{ title: string, component: any, icon: any }>;
  deviceID: any;
  @ViewChild(Nav) nav: Nav;
  displayName: any;
  email: any;
  familyName: any;
  givenName: any;
  userId: any;
  imageUrl: any;
  isLoggedIn: any;
  userData: any;
  responseData: any;
  victimData: any;
  vdata: any;
  sdata: any;
  ldata: any;
  lat: any;
  lng: any;
  options: any;
  trackData: any;
  _menustate: boolean = true;
  _ispaid: boolean;
  lang: any = 'en';
  id: any;
  user: any;
  viewSub: boolean = false;
  langData: any;
  timeLeft:number;
  interval: any;
  userDatachange:any;
  aData:any;
  version:any;
  //rootPage:any;

  constructor(
    public platform: Platform,
    private device: Device,
    public toastCtrl: ToastController,
    statusBar: StatusBar,
    public AuthServiceProvider: AuthServiceProvider,
    private googlePlus: GooglePlus,
    splashScreen: SplashScreen,
    private push: Push,
    public alertCtrl: AlertController,
    private translate: TranslateService,
    public events: Events,
    public menuCtrl: MenuController,
    public iab: InAppBrowser,
    public zone: NgZone,
    private geolocation: Geolocation,
    private network: Network,
    private appVersion: AppVersion,
    private config: Config,
    public modalCtrl:ModalController) {
    this.langData = localStorage.getItem('userData');
    if (this.langData === '' || this.langData === undefined || this.langData === null) {
      this.lang = 'dn';
      this.initTranslate();
      translate.use(this.lang);
      console.log('selected language is : ' + this.lang);
    } else if (this.langData) {
      this.langData = JSON.parse(this.langData);
      this.lang = this.langData.language;
      console.log('selected language is : ' + this.lang);
      this.initTranslate();
      translate.use(this.lang);
    }
    this.getSettings();
    this.events.subscribe('user:changed', user => {
      this.user = user;
      console.log('this from ap comp:' + JSON.stringify(this.user));
      this.initAutoPull();
      this.lang = this.user.lang;
      console.log('component refresh' + 'lang is' + this.lang);
      this.initTranslate();
      translate.use(this.lang);
    });
    localStorage.removeItem('onetimepush');
    localStorage.removeItem('timeCheck');
    localStorage.removeItem('sendtype');
    let vData = localStorage.getItem('victimData');
    if (vData === 'undefined' || vData === '') {
      localStorage.removeItem('victimData');
    }
    console.log("platform=" + this.device.platform);
    var plat = this.device.platform;
    localStorage.setItem('paltform', plat);
    this.pages = [
      { title: 'Map', component: MapPage, icon: 'ios-navigate' },
      // { title: 'My Account', component: MyaccountPage, icon: 'ios-contact' },
      { title: 'Notification', component: NotificationPage, icon: 'ios-notifications' },
      // { title: 'My Contacts', component: ContactPage, icon: 'ios-contacts' },
    ];
    this.pages_1 = [
      { title: 'My Alarm', component: SecurityPage, icon: 'ios-nuclear' },
      // { title: 'Shop', component: ShopPage, icon: 'ios-cart' },
      { title: 'Help', component: HelpPage, icon: 'ios-help-buoy' },
      { title: 'Offers on Alarm', component: OfferPage, icon: 'ios-pricetag' },
      // { title: 'User Settings', component: SettingPage, icon: 'settings' },
      { title: 'invite friend', component: InvitefriendPage, icon: 'ios-paper-plane' },
      { title: 'About us', component: AboutusPage, icon: 'ios-information-circle' },
    ];
    this.subpages = [
      { title: 'Addresses', component: AddressPage, icon: 'ios-home' },
      { title: 'Supporters', component: ContactPage, icon: 'ios-contacts' },
      { title: 'Account', component: MyaccountPage, icon: 'ios-contact' }
    ]
    platform.ready().then(() => {
      this.appVersion.getVersionNumber().then((version) => {
        this.version = version;
        console.log('app version : '+this.version);
      });
      events.subscribe('user:paid', (paid) => {
        console.log('Welcome user was', paid);
        if (paid == 1) {
          this._ispaid = false;
        } else {
          this._ispaid = true;
        }
      });
      if (this.platform.is('android')) {
        console.log("running on Android device!");
        var plat = 'Android';
        localStorage.setItem('paltform', plat);
      }
      if (this.platform.is('ios')) {
        console.log("running on iOS device!");
        var plat = 'ios';
        localStorage.setItem('paltform', plat);
      }
      if (this.platform.is('mobileweb')) {
        console.log("running in a browser on mobile!");
      }

      statusBar.styleDefault();
      splashScreen.hide();
      this.menuCtrl.swipeEnable(false);
      var lastTimeBackPress = 0;
      var timePeriodToExit = 2000;
      localStorage.setItem('tog', '');
      platform.registerBackButtonAction(() => {
        // get current active page
        if (this._menustate == false) {
          this.menuCtrl.close();
        } else {
          let view = this.nav.getActive();
          if (view.component.name == "MapPage") {
            //Double check to exit app
            if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
              this.platform.exitApp(); //Exit from app
            } else {
              let title='';
              let text='';
              let yes='';
              let no='';
              this.translate.get('app exit alert title').subscribe(res => {
                title = res;
              });
              this.translate.get('app exit alert text').subscribe(res => {
                text = res;
              });
              this.translate.get('Yes').subscribe(res => {
                yes = res;
              });
              this.translate.get('No').subscribe(res => {
                no = res;
              });
              let alert = this.alertCtrl.create({
                title: title,
                message: text,
                buttons: [
                  {
                    text: yes,
                    role: 'cancel',
                    handler: () => {
                      console.log('Yes clicked');
                      this.platform.exitApp();
                    }
                  },
                  {
                    text: no,
                    handler: () => {
                      console.log('No clicked');
                    }
                  }
                ]
              });
              alert.present();
              lastTimeBackPress = new Date().getTime();
            }
          } else if (view.component.name == "SigninPage" || view.component.name == "SignupPage") {
            // go to previous page
            //this.nav.pop({});
            this.platform.exitApp();
          } else if (view.component.name == "ForgotpasswordPage") {
            this.nav.setRoot(SigninPage);
          } else if (view.component.name == "NewaddressPage") {
            this.nav.setRoot(SigninPage);
          } else {
            this.nav.setRoot(MapPage);
          }
        }
      });

      //check app has push notification permission
      this.push.hasPermission()
        .then((res: any) => {

          if (res.isEnabled) {
            console.log('We have permission to send push notifications');
          } else {
            console.log('We do not have permission to send push notifications');
          }

        });
      localStorage.setItem('deviceID', "");
      const options: any = {
        android: {
          // SENDER_ID:'357015216809'
        },
        ios: {
          alert: 'true',
          badge: true,
          sound: 'false',
          fcmSandbox: true
        },
        windows: {},
        browser: {
          pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        }
      };

      const pushObject: PushObject = this.push.init(options);
      pushObject.on('notification').subscribe((notification: any) => {
        console.log('Received a notification', notification);
        localStorage.setItem('push','');
        var titletxt;
        if (this.platform.is('ios')) {
          this.translate.get(notification.additionalData['gcm.notification.tickerText']).subscribe(res =>{
            titletxt = res;
          });
          titletxt = notification.additionalData['gcm.notification.tickerText'];
        } else {
          this.translate.get(notification.additionalData.tickerText).subscribe(res =>{
            titletxt = res;
          });
          titletxt = notification.additionalData.tickerText;
        }
        var msg;
        this.translate.get(notification.additionalData.subtitle).subscribe(res=>{
          msg = res;
        })
        this.vdata = notification.additionalData['vData'];
        this.sdata = notification.additionalData['sData'];
        let alert = this.alertCtrl.create({
          title: titletxt,
          message:msg,
          // message: notification.additionalData.subtitle,
          buttons: [
            {
              text: 'Ok',
              handler: () => {
                console.log('called');
                if (this.platform.is('ios')) {
                  localStorage.setItem('lastLat', notification.additionalData['gcm.notification.latitude']);
                  localStorage.setItem('lastLng', notification.additionalData['gcm.notification.longitude']);
                  localStorage.setItem('camdata',JSON.stringify(notification.additionalData));
                  console.log('flag:'+notification.additionalData['gcm.notification.flag']);
                  if(typeof(notification.additionalData['gcm.notification.user_count']) != undefined) {
                    localStorage.setItem('user_count', notification.additionalData['gcm.notification.user_count']);
                  }
                  if(typeof(notification.additionalData['gcm.notification.user_count']) != undefined) {
                    localStorage.setItem('user_count', notification.additionalData['gcm.notification.user_count']);
                  }
                  if(typeof(notification.additionalData['gcm.notification.flag']) != undefined && notification.additionalData['gcm.notification.flag'] != null) {
                    let accdata = {
                      'sender_id': notification.additionalData['gcm.notification.sender_id'],
                      'username': notification.additionalData['gcm.notification.username'],
                      'email':notification.additionalData['gcm.notification.email']
                    };
                    localStorage.setItem('accData',JSON.stringify(accdata));
                    this.supportModal();
                  }
                  this.nav.setRoot(MapPage, {
                    'page': 'appcomp'
                  });
                } else {
                  if(notification.additionalData.flag == 'ASR') {
                    this.aData = notification.additionalData['aData'];
                    localStorage.setItem('accData',JSON.stringify(this.aData));
                    this.supportModal();
                  }
                  if (this.vdata) {
                    console.log('vdata', this.vdata);
                    localStorage.setItem('lastLat', this.vdata.latitude);
                    localStorage.setItem('lastLng', this.vdata.longitude);
                    if(typeof (this.vdata.user_count) != undefined) {
                      console.log('user count is : '+this.vdata.user_count);
                      localStorage.setItem('user_count', this.vdata.user_count);
                    }                   
                    this.nav.setRoot(MapPage, {
                      'page': 'appcomp'
                    });
                  } else if (this.sdata) {
                    console.log('sdata', this.sdata);
                    localStorage.setItem('lastLat', this.sdata.latitude);
                    localStorage.setItem('lastLng', this.sdata.longitude);
                    localStorage.setItem('camdata',JSON.stringify(this.sdata));
                    if(typeof(this.sdata.user_count) != undefined) {
                      console.log('user count is : '+this.sdata.user_count);
                      localStorage.setItem('user_count', this.sdata.user_count);
                    }                  
                    this.nav.setRoot(MapPage, {
                      'page': 'appcomp'
                    });
                  } 
                }
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.present();
        console.log('Received a notification', notification);
      });


      //if(this.device.platform == '')
      pushObject.on('registration').subscribe((registration: any) => {
        console.log('Device registered', registration);
        this.deviceID = {};
        this.deviceID["deviceid"] = registration['registrationId'];
        //this.deviceID["deviceid"]='cxHIRxjGUdE:APA91bGJqzhwsBbhxDLnaBXwcl7PLHZu3fDbm9pZ4QMN1kzRiJP5MFRAksg37aySQvhHoKfHTaDlt80AreZIeP9JG5MJc0AYGWAM1v3kT58sQsEgxZO74RdwNAaVazG2PZPlkKqE9m4M';
        console.log("deviceid =" + this.deviceID["deviceid"]);
        localStorage.setItem('deviceID', this.deviceID["deviceid"]);
        console.log(localStorage.getItem('deviceID'));
      });

      pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));

    });
  }
  get_user() {
    var userdata = JSON.parse(localStorage.getItem('userData'));

    this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {

      localStorage.setItem('userData', JSON.stringify(result[0]));
      console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));

    });
  }
  getCurrentPosition() {
    this.options = {
      timeout: 10000, enableHighAccuracy: true, maximumAge: 3600
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      console.log('from app comp', pos);
      localStorage.setItem('lastLat', JSON.stringify(pos.coords.latitude));
      localStorage.setItem('lastLng', JSON.stringify(pos.coords.longitude));
    });
  }

  menuClosed() {
    //this.events.publish('menu:closed', '');
    console.log('menu closeded');
    this._menustate = true;
  }

  menuOpened() {
    //this.events.publish('menu:opened', '');
    console.log('menu opened');
    this._menustate = false;
  }

  openPage(page) {
    if (page.title === 'Shop') {
      let dynamic_data = JSON.parse(localStorage.getItem('adminData'));
      console.log(dynamic_data.data[2].data);
      // dynamic_data[3].data;
      let alert_Bro = this.alertCtrl.create({
        title: 'Shop page will be redirected to browser',
        message: 'Press Yes to Continue',
        buttons: [
          {
            text: 'Yes',
            role: 'cancel',
            handler: () => {
              console.log('Yes clicked');
              let openBrowser = this.iab.create(dynamic_data.data[2].data, 'location=no')

            }
          },
          {
            text: 'No',
            handler: () => {
              console.log('No clicked');
            }
          }
        ]
      });
      alert_Bro.present();
    }
    // else if (page.title === 'Offers on alarm') {
    //   let dynamic_data = JSON.parse(localStorage.getItem('adminData'));
    //   // dynamic_data[3].data;
    //   let alert_Bro_offer = this.alertCtrl.create({
    //     title: 'Shop page will be redirected to browser',
    //     message: 'Press Yes to Continue',
    //     buttons: [
    //       {
    //         text: 'Yes',
    //         role: 'cancel',
    //         handler: () => {
    //           console.log('Yes clicked');
    //           let openBrowser = this.iab.create(dynamic_data.data[3].data, 'location=no')
    //         }
    //       },
    //       {
    //         text: 'No',
    //         handler: () => {
    //           console.log('No clicked');
    //         }
    //       }
    //     ]
    //   });
    //   alert_Bro_offer.present();
    // } 
    else {
      this.nav.setRoot(page.component);
    }
  }

  logout() {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(this.userData, 'logout').then((result) => {
      this.responseData = result;
      if (true == this.responseData.status) {
        console.log(this.responseData);
        localStorage.setItem('session', '_logged_out');
        localStorage.setItem('userData', "");
        //localStorage.setItem('deviceID', "");
        localStorage.setItem('userAllData', "");
        localStorage.setItem('alertdata', "");
        localStorage.setItem('userFBData', "");
        localStorage.setItem('userGoogleData', "");
        localStorage.setItem('tog', "");
        localStorage.setItem('push', "");
        localStorage.removeItem('victimData');
        localStorage.removeItem('cancel');
        localStorage.removeItem('stop');
        localStorage.removeItem('onetimepush');
        localStorage.removeItem('sdata');
        localStorage.removeItem('lastLat');
        localStorage.removeItem('lastLng');
        localStorage.removeItem('lastAdd');
        localStorage.removeItem('adminData');
        this.Googlelogout();
        this.nav.setRoot(SigninPage);
      } else {

      }
    }, (err) => {
      // Error log
    });
  }

  //check user is paid or not
  checkPaid() {
    var userdata = JSON.parse(localStorage.getItem('userData'));
    console.log(userdata['user_id']);
    var user = {
      'user_id': userdata.user_id,
      'address_id': userdata.address_id
    };
    this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
      this.responseData = result;
      var user_detail = this.responseData;
      localStorage.setItem('userData', JSON.stringify(user_detail[0]));
      console.log(user_detail[0].paid_member);
      if (user_detail[0].paid_member === 1) {
        this._ispaid = false;
      } else {
        this._ispaid = true;
      }
    });
  }

  //redirect to payment 
  upgrade() {
    console.log('upgrade');
    this.platform.ready().then(() => {
      var userdata = JSON.parse(localStorage.getItem('userData'));
      this.id = userdata.user_id;
      let payScript = "localStorage.setItem('user_id'," + this.id + ");";
      const browser = this.iab.create('http://rayi.in/nabosupport/upgrade', "_blank", "location=no,clearsessioncache=yes");
      //on url load stop
      browser.on("loadstop")
        .subscribe(
          event => {
            browser.executeScript({
              code: payScript
            });
            console.log("loadstart -->", event);
            this.logout();
          },
          err => {
            console.log("InAppBrowser loadstop Event Error: " + err);
          });
      browser.on('exit').subscribe(() => {
        console.log('inapp browser closed');
        this.checkPaid();
      }, err => {
        console.error(err);
      });
    });
  }

  //alert for upgrade success
  upgradeConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm action',
      message: 'Relevant message to be here',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('no clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('yes clicked');
            this.upgrade();
          }
        }
      ]
    });
    alert.present();
  }

  //language change
  selectLang(event) {
    console.log(event.srcElement.id);
    this.lang = event.srcElement.id;
    this.translate.use(this.lang);
    let userdata = JSON.parse(localStorage.getItem('userData'));
    let setData = {
      'user_id': userdata.user_id,
      'lang': this.lang
    };
    this.AuthServiceProvider.postData(setData, 'setLang').then((rs) => {
      console.log(rs);
      this.responseData = rs;
      if (this.responseData.status == true) {
        this.get_user();
      }
    }, (err) => {
      console.log(err);
    })
  }

  //google logout
  Googlelogout() {
    this.googlePlus.logout()
      .then(res => {
        console.log(res);
        this.displayName = "";
        this.email = "";
        this.familyName = "";
        this.givenName = "";
        this.userId = "";
        this.imageUrl = "";

        this.isLoggedIn = false;
      })
      .catch(err => console.error(err));
  }
  shop(url) {
    var data = Base64.encode(localStorage.getItem('userData'));
    let browser = this.iab.create(url);
    browser.show();
  }
  offer(url) {
    let browser = this.iab.create(url);
    browser.show();
  }
  paid() {
    let browser = this.iab.create('http://rayi.in/nabosupport-web');
    browser.show();
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  initAutoPull() {
    let dynamic_data = JSON.parse(localStorage.getItem('adminData'));
    let time = dynamic_data.data[4].data;
    console.log("24 time is:" + this.convertTime12to24(time));
    let pullTime = this.convertTime12to24(time);
    let start: boolean = false;
    setInterval(() => {
      if (start == true) {
        this.getSettings();
        start = false;
      } else {
        //console.log('not yet...');
      }
    }, 1000);
    ontime({
      cycle: pullTime
    }, function (ot) {
      console.log('pull time!');
      start = true;
      ot.done()
      return
    });
  }

  convertTime12to24(time12h) {
    const [time, modifier] = time12h.split(' ');
    let [hours, minutes] = time.split(':');
    let sec = '00';
    if (hours === '12') {
      hours = '00';
    }
    if (modifier === 'PM') {
      hours = parseInt(hours, 10) + 12;
    }
    return hours + ':' + minutes + ':' + sec;
  }

  getSettings() {
    this.AuthServiceProvider.getData('getAdminData').then((result) => {
      this.responseData = result;
      console.log(this.responseData);
      if (this.responseData.status == true) {
        localStorage.setItem('adminData', JSON.stringify(this.responseData));
        this.getCurrentPosition();
      } else {
        let toast = this.toastCtrl.create({
          message: 'Network error please try again',
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
        this.nav.setRoot(SigninPage);
      }
    });
  }

  toggleSub() {
    this.viewSub = !this.viewSub;
  }

  checkInternet() {
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
        }
      }, 3000);
    });
    connectSubscription.unsubscribe();
  }

  autoAlertEventStart() {
    let adminData = JSON.parse(localStorage.getItem('adminData'));
    let a_time = adminData.data[0].data;
    //let g_time = a_time + 3;
    let g_time = 1;
    this.timeLeft = g_time * 60;
    //this.timeLeft = 30;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        console.log('time remin:' + this.timeLeft);
        if(this.timeLeft == 0){
          console.log('time to call');
          let condition =  localStorage.getItem('push');
          if(condition != 'true') {
            let user = JSON.parse(localStorage.getItem('userData'));
            this.AuthServiceProvider.postData(user, 'camPushall').then((result) => {
              console.log(result);
              this.responseData = result;
              if (true === this.responseData.status) {
                console.log(this.responseData);
                localStorage.setItem('tog', '');
                this.userDatachange = JSON.parse(localStorage.getItem("userData"));
                this.userDatachange['alert_type'] = "BP";
                localStorage.setItem("userData", JSON.stringify(this.userDatachange));
                localStorage.setItem('timeCheck', '');
                console.log("page move");
                localStorage.setItem('onetimepush', 'true');
                localStorage.setItem('push','true');
                let stat;
                let msg;
                this.translate.get('SENT').subscribe(res => {
                  stat = res;
                });
                this.translate.get('Automatically forwarded the alert notification to your neighbours.').subscribe(res => {
                  msg = res;
                });
                this.pushSendAlertOwner(stat, msg);
              }
            });  
          }
        } else {
          this.get_user();
        }
      }
    }, 1000);
  }

  //alert for notifying the user(owner) alert was sent
  pushSendAlertOwner(status, msg) {
    let alert = this.alertCtrl.create({
      title: status,
      subTitle: msg,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Yes clicked');
    
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  supportModal() {
    let modal = this.modalCtrl.create(AcceptsupportPage);
    modal.present()
  } 
}

