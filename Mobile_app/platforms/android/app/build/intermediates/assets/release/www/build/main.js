webpackJsonp([0],{

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_map__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AboutusPage = /** @class */ (function () {
    function AboutusPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutusPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutusPage');
    };
    //redirect to map page
    AboutusPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__map_map__["a" /* MapPage */]);
    };
    AboutusPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-aboutus',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/aboutus/aboutus.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n  </button>\n  <ion-title>{{\'About us\' | translate}}</ion-title>\n  <a (click)="home()" class="home">\n    <ion-icon name="home"></ion-icon>\n  </a>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-content>\n      <b>Nabosupport - Tyverialarmen som aktiverer hele dit nabolag!</b>\n      <p>Hos Nabosupport ønsker vi at skabe danmarks største netværk af aktive naboer og tyverialarmer. Når vi er mange i samme netværk, som ønsker at hjælpe hinanden står vi rigtig stærkt over eventuelle indbrudsforsøg.</p>\n      <p>Det er unægteligt bevist flere gange, at synlige naboer og skiltning herom, er den bedste måde at holde tyveknægten på afstand. Nabosupport er i stand til at skabe Danmarks største netværk af naboer og især tyverialarmer. Kombinationen af naboer og tyverialarmer i den her konstellation er unik.</p>\n      <p>Fordelen ved kombinationen af en tyverialarm og aktive naboer er, at reaktionen kan ske inden for ganske få minutter. Der er derfor stor sandsynlighed for at dine naboer kan være synlige omkring dit hus på meget kort tid.</p>\n      <p>At være en del af Nabosupport, får du en brugerflade som giver dig et unikt overblik over hele dit nærområde, hvad angår alarmer, indbrud og suspekt adfærd og det er helt gratis!</p>\n      <p>Bliv medlem allerede i dag, ved at besøge os på Nabosupport.dk og få ubetinget adgang til alle vores services med det samme.</p>\n      <p>Vi glæder os til at se dig hos os :)</p>\n      <p>Med venlig hilsen<br>\n      <b>Hele teamet hos Nabosupport</b></p>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/aboutus/aboutus.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], AboutusPage);
    return AboutusPage;
}());

//# sourceMappingURL=aboutus.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: modal page (This page load popup(modal) messages )
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 



var ModalPage = /** @class */ (function () {
    function ModalPage(translate, navCtrl, menu, navParams, viewCtrl) {
        this.translate = translate;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    //default ionic lifecycle event
    ModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalPage');
    };
    ModalPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    ModalPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //close the popup message
    ModalPage.prototype.modalClose = function () {
        this.viewCtrl.dismiss();
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-modal',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/modal/modal.html"*/'<ion-content padding>\n  <ion-icon class="exit-btn" ios="ios-close-circle" md="md-close-circle" (click)="modalClose()"></ion-icon>\n  <div class="flex">\n    <h2>{{\'Thanks for your help\' | translate}}</h2>\n  </div>\n  <img src="assets/imgs/shield.svg" alt="">\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/modal/modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ViewController */]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotpasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signin_signin__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_storage__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ForgotpasswordPage = /** @class */ (function () {
    function ForgotpasswordPage(translate, navCtrl, menu, nativeStorage, toastCtrl, loadingCtrl, AuthServiceProvider, alertCtrl, menuCtrl, browser) {
        this.translate = translate;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.nativeStorage = nativeStorage;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.AuthServiceProvider = AuthServiceProvider;
        this.alertCtrl = alertCtrl;
        this.menuCtrl = menuCtrl;
        this.browser = browser;
        this.form1 = new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */]()
        });
        this.userForgotData = { "email": "" };
        localStorage.setItem('userForgotData', "");
        this.menuCtrl.swipeEnable(false, 'loggedInMenu');
        this.menuCtrl.enable(false);
    }
    Object.defineProperty(ForgotpasswordPage.prototype, "email", {
        //get emailID from form
        get: function () {
            return this.form1.get('email');
        },
        enumerable: true,
        configurable: true
    });
    //Ionic lifecycle events
    ForgotpasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotpasswordPage');
    };
    ForgotpasswordPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    ForgotpasswordPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //get eamil from forgot password form and send to api for validation
    ForgotpasswordPage.prototype.forgot = function () {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: 'Invaild Email',
            duration: 3000,
            position: 'bottom'
        });
        var toast1 = this.toastCtrl.create({
            message: 'Please provide a registered Email',
            duration: 3000,
            position: 'bottom'
        });
        var toast2 = this.toastCtrl.create({
            message: 'Your Account verified.. Please check your Inbox to get reset link. ',
            duration: 3000,
            position: 'bottom'
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Processing..'
        });
        loading.present();
        var regu = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (regu.test(this.userForgotData.email) == true) {
            this.AuthServiceProvider.postData(this.userForgotData, 'forgotpassword').then(function (result) {
                _this.responseData = result;
                console.log(result);
                if (true == _this.responseData.status) {
                    var trans_success_1 = '';
                    _this.translate.get('Success').subscribe(function (res) {
                        trans_success_1 = res;
                    });
                    var trans_btn_ok_1 = '';
                    _this.translate.get('ok').subscribe(function (res) {
                        trans_btn_ok_1 = res;
                    });
                    var alert_1 = _this.alertCtrl.create({
                        title: trans_success_1,
                        subTitle: 'Your Account verified.. Please check your Inbox to get reset link.',
                        buttons: [trans_btn_ok_1]
                    });
                    loading.dismiss();
                    alert_1.present();
                    localStorage.setItem('userForgotData', JSON.stringify(_this.responseData));
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signin_signin__["a" /* SigninPage */]);
                    console.log(JSON.parse(localStorage.getItem('userForgotData')));
                }
                else if (false == _this.responseData.status) {
                    loading.dismiss();
                    var trans_title_fail_1 = '';
                    _this.translate.get('Failed').subscribe(function (res) {
                        trans_title_fail_1 = res;
                    });
                    var trans_btn_ok_2 = '';
                    _this.translate.get('ok').subscribe(function (res) {
                        trans_btn_ok_2 = res;
                    });
                    var alert_2 = _this.alertCtrl.create({
                        title: trans_title_fail_1,
                        subTitle: 'Account not found please signup now.',
                        buttons: [
                            {
                                text: trans_btn_ok_2,
                                handler: function () {
                                    console.log('Yes clicked');
                                }
                            }
                        ]
                    });
                    alert_2.present();
                }
            }, function (err) {
                // Error log
            });
        }
        else if (!(this.userForgotData.email)) {
            loading.dismiss();
            toast1.present();
        }
        else {
            loading.dismiss();
            var trans_btn_ok_3 = '';
            this.translate.get('ok').subscribe(function (res) {
                trans_btn_ok_3 = res;
            });
            var alert_3 = this.alertCtrl.create({
                title: 'Invalid',
                subTitle: 'Please provide valid Email.',
                buttons: [
                    {
                        text: trans_btn_ok_3,
                        handler: function () {
                            console.log('Yes clicked');
                        }
                    }
                ]
            });
            alert_3.present();
        }
    };
    //button click for back to main page
    ForgotpasswordPage.prototype.gotoBack = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signin_signin__["a" /* SigninPage */]);
    };
    //redirected to Nabosupport web
    ForgotpasswordPage.prototype.webpage = function () {
        var _this = this;
        var trans_btn_yes = '';
        var trans_btn_no = '';
        this.translate.get('Yes').subscribe(function (res) {
            trans_btn_yes = res;
        });
        this.translate.get('No').subscribe(function (res) {
            trans_btn_no = res;
        });
        var alert = this.alertCtrl.create({
            title: 'This page will be redirected to browser',
            message: 'Press Yes to Continue',
            buttons: [
                {
                    text: trans_btn_yes,
                    role: 'cancel',
                    handler: function () {
                        console.log('Yes clicked');
                        //let openBrowser = this.browser.create('http://rayi.in/nabosupport/'+this.offerURL, '_system','location=no');
                        var openBrowser = _this.browser.create('http://nabosupport.dk/min-konto/lost-password/', '_system', 'location=no');
                    }
                },
                {
                    text: trans_btn_no,
                    handler: function () {
                        console.log('No clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    ForgotpasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-forgotpassword',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/forgotpassword/forgotpassword.html"*/'<ion-content class="signin" padding center text-center>\n  <ion-title color="wht">Forgot Password</ion-title>\n  <img src="assets/imgs/logo.png">\n  <ion-list>\n    <form [formGroup]="form1"  (ngSubmit)="forgot()">\n      <ion-item>\n        <ion-label floating>\n          <ion-icon name="ios-mail-open-outline"></ion-icon> {{ \'Email id\' | translate }}</ion-label>\n        <ion-input formControlName="email" type="text" [(ngModel)]="userForgotData.email" ></ion-input>\n      </ion-item>\n      <button type="submit" class="sign_btn" ion-button  round full>{{ \'Submit\' | translate }}</button>\n      <button type="button" class="sign_btn" ion-button  round full (click)="gotoBack()">{{\'Back\' | translate}}</button>\n    </form>\n  </ion-list>   \n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/forgotpassword/forgotpassword.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], ForgotpasswordPage);
    return ForgotpasswordPage;
}());

//# sourceMappingURL=forgotpassword.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__signin_signin__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Signup page (This page validate user credentials and send to api for register)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 








var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, menu, renderer, elementRef, keyboard, AuthServiceProvider, loadingCtrl, toastCtrl, geolocation, http) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.keyboard = keyboard;
        this.AuthServiceProvider = AuthServiceProvider;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.geolocation = geolocation;
        this.http = http;
        this.form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            username: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
            password: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
            mail: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].email])
        });
        this.userData = { "username": "", "email_id": "", "password": "" };
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        this.getUserPosition();
    }
    Object.defineProperty(SignupPage.prototype, "username", {
        //get user data from signup form
        get: function () {
            return this.form.get('username');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SignupPage.prototype, "mail", {
        get: function () {
            return this.form.get('mail');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SignupPage.prototype, "password", {
        get: function () {
            return this.form.get('password');
        },
        enumerable: true,
        configurable: true
    });
    SignupPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    SignupPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //Toast contrlloer
    SignupPage.prototype.toastViewer = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    //loading controller
    SignupPage.prototype.loadingViewer = function (content) {
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: content
        });
        return loading;
    };
    //JSON concadination
    SignupPage.prototype.jsonConcat = function (o1, o2) {
        for (var key in o2) {
            o1[key] = o2[key];
        }
        return o1;
    };
    //signup function 
    SignupPage.prototype.signup = function () {
        var _this = this;
        setTimeout(function () {
            var loader = _this.loadingViewer(_this.loadercontent);
            loader.dismiss();
        }, 300);
        this.userallData = {};
        console.log(this.locdata);
        this.userallData = this.jsonConcat(this.userData, this.locdata);
        this.platformname = localStorage.getItem('paltform');
        this.userallData.platform = this.platformname;
        console.log(this.userallData);
        if (this.userallData.username && this.userallData.password && this.userallData.email_id) {
            this.loadercontent = 'Signin you up...';
            var loader_1 = this.loadingViewer(this.loadercontent);
            loader_1.present();
            this.AuthServiceProvider.postData(this.userallData, 'signup').then(function (result) {
                _this.responseData = result;
                if (true == _this.responseData.status) {
                    console.log(_this.responseData);
                    loader_1.dismiss();
                    _this.toastmessage = 'Account created successfully';
                    _this.toastViewer(_this.toastmessage);
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__signin_signin__["a" /* SigninPage */]);
                }
                else {
                    loader_1.dismiss();
                    _this.toastmessage = 'User already exist';
                    _this.toastViewer(_this.toastmessage);
                }
            }, function (err) {
                loader_1.dismiss();
            });
        }
        else {
            this.toastmessage = 'Please fillup the detail to sign up';
            this.toastViewer(this.toastmessage);
        }
    };
    //redirect to login page
    SignupPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__signin_signin__["a" /* SigninPage */]);
    };
    //get user location detail from geocode
    SignupPage.prototype.getUserPosition = function () {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        this.options = {
            enableHighAccuracy: true
        };
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            _this.currentPos = pos;
            var coord = new Array();
            coord.push(pos.coords.latitude, pos.coords.longitude);
            console.log(coord[0] + ',' + coord[1]);
            var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            var request = { latLng: latlng };
            geocoder.geocode(request, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var result = results[0];
                    var rsltAdrComponent = result.address_components;
                    var resultLength = rsltAdrComponent.length;
                    if (result != null) {
                        var data = new Array;
                        var locationdata = new Array();
                        for (var i = 0; i < rsltAdrComponent.length; i++) {
                            var obj = rsltAdrComponent[i];
                            data.push(obj.long_name);
                        }
                        _this.deviceID = localStorage.getItem('deviceID');
                        if (data.length == 5) {
                            _this.locdata = {
                                "street_address": data[1] + " " + data[0],
                                "city": data[2],
                                "country": data[3],
                                "zipcode": data[4],
                                "latitude": coord[0],
                                "longitude": coord[1],
                                "is_active": 1,
                                "status_id": 0,
                                "alert_type": "N",
                                "distance": 5000,
                                "deviceID": _this.deviceID
                            };
                            console.log(_this.locdata);
                        }
                        else if (data.length == 6) {
                            _this.locdata = {
                                "street_address": data[1] + " " + data[0],
                                "city": data[2],
                                "country": data[4],
                                "zipcode": data[5],
                                "latitude": coord[0],
                                "longitude": coord[1],
                                "is_active": 1,
                                "status_id": 0,
                                "alert_type": "N",
                                "distance": 5000,
                                "deviceID": _this.deviceID
                            };
                            console.log(_this.locdata);
                        }
                        else if (data.length == 7) {
                            _this.locdata = {
                                "street_address": data[1] + " " + data[0],
                                "city": data[3],
                                "country": data[5],
                                "zipcode": data[6],
                                "latitude": coord[0],
                                "longitude": coord[1],
                                "is_active": 1,
                                "status_id": 0,
                                "alert_type": "N",
                                "distance": 5000,
                                "deviceID": _this.deviceID
                            };
                            console.log(_this.locdata);
                        }
                        else if (data.length == 8) {
                            _this.locdata = {
                                "street_address": data[1] + " " + data[0],
                                "city": data[4],
                                "country": data[6],
                                "zipcode": data[7],
                                "latitude": coord[0],
                                "longitude": coord[1],
                                "is_active": 1,
                                "status_id": 0,
                                "alert_type": "N",
                                "distance": 5000,
                                "deviceID": _this.deviceID
                            };
                            console.log(_this.locdata);
                        }
                        else if (data.length == 9) {
                            _this.locdata = {
                                "street_address": data[1] + " " + data[0],
                                "city": data[4],
                                "country": data[7],
                                "zipcode": data[8],
                                "latitude": coord[0],
                                "longitude": coord[1],
                                "is_active": 1,
                                "status_id": 0,
                                "alert_type": "N",
                                "distance": 5000,
                                "deviceID": _this.deviceID
                            };
                            console.log(_this.locdata);
                        }
                        else if (data.length == 10) {
                            _this.locdata = {
                                "street_address": data[1] + " " + data[0],
                                "city": data[5],
                                "country": data[8],
                                "zipcode": data[9],
                                "latitude": coord[0],
                                "longitude": coord[1],
                                "is_active": 1,
                                "status_id": 0,
                                "alert_type": "N",
                                "distance": 5000,
                                "deviceID": _this.deviceID
                            };
                            console.log(_this.locdata);
                        }
                        console.log(_this.locdata);
                    }
                    else {
                        alert("No address available");
                    }
                }
            });
        }, function (err) {
            console.log("error : " + err.message);
        });
    };
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapPage');
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/signup/signup.html"*/'\n    \n<ion-content class="signup" padding center text-center>\n  \n \n  <ion-title color="wht">{{ \'SIGN UP\' | translate }}</ion-title>\n  <img src="assets/imgs/logo.png">\n     <ion-list>\n       <!--sign in form input field start-->\n       <form [formGroup]="form" (ngSubmit)="signup()" >\n        <ion-item>          \n          <ion-thumbnail class="icn" item-start>\n         <ion-icon name="ios-contact"></ion-icon> \n       </ion-thumbnail>\n          <ion-label  floating> {{ \'Username\' | translate }}</ion-label>\n          <ion-input formControlName="username" focuser id="usersignup" type="text" [(ngModel)]="userData.username"></ion-input>\n         \n       </ion-item>\n       <div *ngIf="username.touched && username.invalid" class="alertstyle">\n         <div *ngIf="username.errors.required">{{ \'Please provide a username\' | translate }}</div>\n       </div>\n\n       <ion-item>\n           <ion-thumbnail class="icn" item-start>\n               <ion-icon name="ios-mail"></ion-icon> \n             </ion-thumbnail>\n         <ion-label floating> {{ \'Email id\' | translate }}</ion-label>\n         <ion-input formControlName="mail" type="email" [(ngModel)]="userData.email_id" email="true"></ion-input>\n       </ion-item>\n       <div *ngIf="mail.touched && mail.invalid" class="alertstyle">\n          <div *ngIf="mail.errors.email">{{ \'Invalid Email format\' | translate }}</div>\n       </div>\n\n       <ion-item>\n           <ion-thumbnail class="icn" item-start>\n               <ion-icon name="ios-lock"></ion-icon> \n             </ion-thumbnail>\n         <ion-label floating> {{ \'Password\' | translate }}</ion-label>\n         <ion-input formControlName="password" type="password" [(ngModel)]="userData.password"></ion-input>\n       </ion-item>\n       <div *ngIf="password.touched && password.invalid" class="alertstyle">\n         <div *ngIf="password.errors.required">{{ \'Please provide a password\' | translate }}</div>\n        \n       </div>\n      \n       <button ion-button type="submit" class="sign_btn"   round full>{{ \'Create Account\' | translate }}</button>\n       </form>\n        <!--sign in form input field end-->\n       <span class="acc">{{ \'ALREADY HAVE AN ACCOUNT ?\' | translate }}</span> <a href="#" (click)="login()">{{ \'Sign In\' | translate }}</a>\n     </ion-list>\n   \n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/signup/signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Renderer */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewaddressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__security_security__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NewaddressPage = /** @class */ (function () {
    function NewaddressPage(translate, navCtrl, navParams, AuthServiceProvider, alertCtrl, loadingCtrl, toastCtrl, geolocation, events) {
        this.translate = translate;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.AuthServiceProvider = AuthServiceProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.geolocation = geolocation;
        this.events = events;
        this.newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "country_code": "", "camtoken": "", "paid_member": "" };
        this._isPhone = false;
        this._ifEdit = true;
        this._isCountry = false;
        this._isnormal = false;
        this._selectOpt = [
            {
                value: "+45"
            },
            {
                value: "+46"
            },
            {
                value: "+47"
            }
        ];
    }
    NewaddressPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewaddressPage');
        this.country_title = { title: "Select country" };
        var user = JSON.parse(localStorage.getItem('userData'));
        var paid = user.membership_id;
        if (paid == 1) {
            this._ispaid == true;
            this.newAddAry.paid_member = "1";
        }
        else {
            this._ispaid == false;
            this.newAddAry.paid_member = "0";
        }
        this.initMap();
    };
    NewaddressPage.prototype.ngOnInit = function () {
        this.acService = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    };
    //initiate google map
    NewaddressPage.prototype.initMap = function () {
        var _this = this;
        this.geocoder = new google.maps.Geocoder();
        this.options = {
            enableHighAccuracy: true
        };
        var trans_map_load = '';
        this.translate.get('Map loading...').subscribe(function (res) {
            trans_map_load = res;
        });
        var loader = this.loadingCtrl.create({
            spinner: 'crescent',
            content: trans_map_load
        });
        loader.present();
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            _this.currentPos = pos;
            console.log(pos);
            console.log(pos.coords.latitude + ',' + pos.coords.longitude);
            _this.latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            _this.mapOptions = {
                center: _this.latlng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                fullscreenControl: false,
                streetViewControl: false,
                mapTypeControl: false,
                clickableIcons: false,
                draggable: true
            };
            console.log('map initiated');
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, _this.mapOptions);
            _this.addMarker(pos.coords.latitude, pos.coords.longitude);
            _this.updateSearch();
            loader.dismiss();
        }, function (err) {
            console.log("error : " + err.message);
            loader.dismiss();
        });
    };
    NewaddressPage.prototype.addMap = function () {
        var latLng = new google.maps.LatLng(this.LastLat1, this.LastLng1);
        var mapOptions = {
            center: latLng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            clickableIcons: false,
            draggable: false,
            zoomControl: false
            //draggable: this.mapcondition, zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.addMarker(this.LastLat1, this.LastLng1);
        this.updateSearch();
    };
    NewaddressPage.prototype.addMarker = function (lat, lng) {
        var _this = this;
        this.nabo_img = 'http://nabosupport.dk/api/v1/mapicon/new-markers/blue.svg';
        this.marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
            icon: this.nabo_img,
        });
        var trans_cnt = '';
        this.translate.get('This is your position').subscribe(function (res) {
            trans_cnt = res;
        });
        var content = "<p>" + trans_cnt + "</p>";
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        google.maps.event.addListener(this.marker, 'click', function (event) {
            //infoWindow.open(this.map, marker);
            //alert(JSON.stringify(event));
        });
        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(this.map, 'dragend', function () {
            //console.log('marker length : '+this.markers.length);
            if (_this.markers != undefined) {
                _this.clearMarkers();
            }
            _this.LastLat1 = _this.map.getCenter().lat();
            _this.LastLng1 = _this.map.getCenter().lng();
            //this.addMarker(this.LastLat1,this.LastLng1);
            var markerpos = new google.maps.LatLng(_this.LastLat1, _this.LastLng1);
            _this.marker.setPosition(markerpos);
            var coord = new Array();
            coord.push(_this.LastLat1, _this.LastLng1);
            var user = JSON.parse(localStorage.getItem('userData'));
            _this.newAddAry.user_id = user.ID;
            _this.newAddAry.latitude = coord[0];
            _this.newAddAry.longitude = coord[1];
            console.log(coord[0] + ',' + coord[1]);
            var latlng = new google.maps.LatLng(_this.LastLat1, _this.LastLng1);
            var request = { latLng: latlng };
            console.log(_this.LastLat1 + ',' + _this.LastLng1);
            geocoder.geocode(request, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var result = results[0];
                    var rsltAdrComponent = result.address_components;
                    //console.log(rsltAdrComponent);
                    console.log(result.formatted_address);
                    //this.addressDisplayToast(result.formatted_address);
                    _this.autocomplete.query = result.formatted_address;
                    var resultLength = rsltAdrComponent.length;
                    if (result != null) {
                        var data = new Array;
                        var locationdata = new Array();
                        for (var i = 0; i < rsltAdrComponent.length; i++) {
                            var obj = rsltAdrComponent[i];
                            data.push(obj.long_name);
                        }
                        if (data.length == 5) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[3];
                            _this.newAddAry.city = data[2];
                            _this.newAddAry.zipcode = data[4];
                        }
                        else if (data.length == 6) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[4];
                            _this.newAddAry.city = data[2];
                            _this.newAddAry.zipcode = data[5];
                        }
                        else if (data.length == 7) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[5];
                            _this.newAddAry.city = data[2];
                            _this.newAddAry.zipcode = data[6];
                        }
                        else if (data.length == 8) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[6];
                            _this.newAddAry.city = data[4];
                            _this.newAddAry.zipcode = data[7];
                        }
                        else if (data.length == 9) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[7];
                            _this.newAddAry.city = data[4];
                            _this.newAddAry.zipcode = data[8];
                        }
                        else if (data.length == 10) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[8];
                            _this.newAddAry.city = data[5];
                            _this.newAddAry.zipcode = data[9];
                        }
                    }
                }
            });
        });
    };
    NewaddressPage.prototype.addressDisplayToast = function (address) {
        var toast = this.toastCtrl.create({
            message: address,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    NewaddressPage.prototype.updateSearch = function () {
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var self = this;
        var config = {
            types: ['geocode'],
            input: this.autocomplete.query,
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            self.autocompleteItems = [];
            console.log(predictions);
            predictions.forEach(function (prediction) {
                self.autocompleteItems.push(prediction);
            });
        });
    };
    NewaddressPage.prototype.toastViewer = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    NewaddressPage.prototype.clearMarkers = function () {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers.length = 0;
    };
    NewaddressPage.prototype.chooseItem = function (item) {
        var _this = this;
        console.log('modal > chooseItem > item > ', item);
        //this.viewCtrl.dismiss(item);
        //console.log(item);
        this.addInput = this.autocomplete.query;
        console.log('query:' + this.addInput);
        this.geocoder = new google.maps.Geocoder;
        this.markers = [];
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
        this.geocoder.geocode({ 'placeId': item.place_id }, function (results, status) {
            console.log(results);
            // if (status === 'OK' && results[0]) {
            var position = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
            _this.LastLat1 = results[0].geometry.location.lat();
            _this.LastLng1 = results[0].geometry.location.lng();
            localStorage.setItem('accLatLng', JSON.stringify(position));
            console.log(position);
            if (results[0] != null) {
                // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
                //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
                var data = new Array;
                var rsltAdrComponent = results[0].address_components;
                var rslength = rsltAdrComponent.length;
                for (var i = 0; i < rslength; i++) {
                    var obj = rsltAdrComponent[i];
                    data.push(obj.long_name);
                }
                //console.log(locdata);
                var user = JSON.parse(localStorage.getItem('userData'));
                _this.newAddAry.user_id = user.ID;
                _this.newAddAry.latitude = position.lat;
                _this.newAddAry.longitude = position.lng;
                console.log('new address');
                if (data.length == 5) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[3];
                    _this.newAddAry.city = data[2];
                    _this.newAddAry.zipcode = data[4];
                }
                else if (data.length == 6) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[4];
                    _this.newAddAry.city = data[2];
                    _this.newAddAry.zipcode = data[5];
                }
                else if (data.length == 7) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[5];
                    _this.newAddAry.city = data[2];
                    _this.newAddAry.zipcode = data[6];
                }
                else if (data.length == 8) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[6];
                    _this.newAddAry.city = data[4];
                    _this.newAddAry.zipcode = data[7];
                }
                else if (data.length == 9) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[7];
                    _this.newAddAry.city = data[4];
                    _this.newAddAry.zipcode = data[8];
                }
                else if (data.length == 10) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[8];
                    _this.newAddAry.city = data[5];
                    _this.newAddAry.zipcode = data[9];
                }
                console.log('new addres:' + JSON.stringify(_this.newAddAry));
                _this.autocomplete.query = item.description;
            }
            else {
                //alert("No address available");
                console.log("No address available");
            }
            var markerpos = new google.maps.LatLng(_this.LastLat1, _this.LastLng1);
            _this.marker.setPosition(markerpos);
            _this.map.setCenter({ lat: _this.LastLat1, lng: _this.LastLng1 });
        });
    };
    // edit time change radio checkbox action
    NewaddressPage.prototype.radioSelect = function (val) {
        this._isFirst = true;
        if (val === "device_detail") {
            this._isPhone = false;
            this._isCountry = false;
        }
        else if (val === "phone") {
            this._isPhone = true;
            this._isCountry = true;
        }
    };
    NewaddressPage.prototype.addNewAddress = function () {
        var _this = this;
        console.log(this.newAddAry);
        var trans_load_cnt = '';
        this.translate.get('Profile updating...').subscribe(function (res) {
            trans_load_cnt = res;
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: trans_load_cnt
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        if (this.newAddAry.latitude == '' && this.newAddAry.longitude == '' && this.newAddAry.street_address == '') {
            var trans_toast_msg_1 = '';
            this.translate.get('No address get please select correct address.').subscribe(function (res) {
                trans_toast_msg_1 = res;
            });
            this.toastmessage = trans_toast_msg_1;
            this.toastViewer(this.toastmessage);
        }
        else {
            this.newAddAry.primary_address = 'yes';
            this.AuthServiceProvider.postData(this.newAddAry, 'addFirstAddress').then(function (result) {
                // loading.present();
                _this.responseData = result;
                if (true == _this.responseData.status) {
                    var trans_toast_update_add_1 = '';
                    _this.translate.get('Address added successfully').subscribe(function (res) {
                        trans_toast_update_add_1 = res;
                    });
                    _this.toastmessage = trans_toast_update_add_1;
                    _this.toastViewer(_this.toastmessage);
                    localStorage.setItem('userData', JSON.stringify(_this.responseData.data[0]));
                    var paidData = JSON.parse(localStorage.getItem('userData'));
                    var paid = paidData.paid_member;
                    if (paid === "1") {
                        //alert('securitypage');
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__security_security__["a" /* SecurityPage */]);
                    }
                    else {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__map_map__["a" /* MapPage */]);
                        //alert('mappage');
                    }
                    localStorage.setItem('session', '_logged_in');
                    var langData = JSON.parse(localStorage.getItem('userData'));
                    var data = { "type": "loggedin", "lang": langData.language };
                    _this.events.publish('user:changed', data);
                }
                else {
                    var trans_msg_try_1 = '';
                    _this.translate.get('failed please re-try.').subscribe(function (res) {
                        trans_msg_try_1 = res;
                    });
                    _this.toastmessage = trans_msg_try_1;
                    _this.toastViewer(_this.toastmessage);
                }
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], NewaddressPage.prototype, "mapElement", void 0);
    NewaddressPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-newaddress',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/newaddress/newaddress.html"*/'<!--\n  \n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{\'newadd\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list class="mapAddAddress-cls">\n    <div>\n      <ion-item>\n        <div class="search-add">\n          <ion-searchbar class="s-box" id="pac-input" [(ngModel)]="autocomplete.query" [showCancelButton]="false" (ionInput)="updateSearch()"\n            (ionCancel)="dismiss()" placeholder="Enter your address here..."></ion-searchbar>\n          <ion-list id="sug">\n            <ion-item *ngFor="let item of autocompleteItems" (click)="chooseItem(item)" class="search-item-cls">\n              {{ item.description }}\n            </ion-item>\n          </ion-list>\n        </div>\n      </ion-item>\n      <ion-item>\n        <div #map id="map"></div>\n        <!-- <div id="marker"></div> -->\n      </ion-item>\n      <ion-item>\n        <button class="sign_btn" ion-button round full [disabled]="addDisable" (click)="addNewAddress();">{{\'Add\' | translate}}</button>\n      </ion-item>\n    </div>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/newaddress/newaddress.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */]])
    ], NewaddressPage);
    return NewaddressPage;
}());

//# sourceMappingURL=newaddress.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_map__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HelpPage = /** @class */ (function () {
    function HelpPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HelpPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpPage');
    };
    HelpPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__map_map__["a" /* MapPage */]);
    };
    HelpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-help',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/help/help.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Help\' | translate}}</ion-title>\n    <a (click)="home()" class="home">\n      <ion-icon name="home"></ion-icon>\n    </a>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card class="cfaq">\n    <ion-card-header>\n      {{\'FAQ\' | translate}}\n    </ion-card-header>\n    <ion-card-content>\n        <h3><b>Søger du svar på noget omkring vores produkt, kan du måske finde svar her. Du er også velkommen til at kontakte os på Nabosupport.dk.</b></h3>\n      <ion-list>\n        <ul class="faq-ul">\n          <li>\n            <b>Hvad betyder de forskellige farvet ikoner?</b>\n          </li>\n        </ul>\n        <p>\n          <span class="blue">Blå</span> = Nabosupporter\n          <br>\n          <span class="red">Rød</span> = Alarm - Dette kan f.eks. være et indbrud.\n          <br>\n          <span class="Orange">Orange</span> = Mistænkelig adfærd - Det kan f.eks. være en mistænkelig bil som kører rundt i området. Ved at trykke på ikonet kan du bl.a. bekræfte at du har set det samme eller at du allerede har informeret politiet.\n        </p>\n      </ion-list>\n      <ion-list>\n        <ul class="faq-ul">\n          <li>\n            <b>Min placering på kortet vises ikke.</b>\n          </li>\n        </ul>\n        <p>\n          Er din adresse ikke markeret på kortet, kan der være gået noget galt med din konto. Kontakt Nabosupport.dk og vi vil hjælpe dig i gang.\n        </p>\n      </ion-list>\n      <ion-list>\n          <ul class="faq-ul">\n            <li>\n              <b>Der er ingen nabosupportere i området.</b>\n            </li>\n          </ul>\n          <p>\n            Hvis der endnu ikke er andre nabosupportere i dit område, skal du tage fat i dine nærmeste naboer. Vi står stærkere når vi er flere i vores netværk.          </p>\n        </ion-list>\n        <ion-list>\n          <ul class="faq-ul">\n            <li>\n              <b>Hvordan ændrer jeg min mail?</b>\n            </li>\n          </ul>\n          <p>\n            Inde under dine kontoindstillinger i app’en kan du ændre dine informationer.\n          </p>\n        </ion-list>\n        <ion-list>\n          <ul class="faq-ul">\n            <li>\n              <b>Hvordan opretter jeg min alarm?</b>\n            </li>\n          </ul>\n          <p>\n            For at få oprettet din alarm til Nabosupport, skal du være abonnent hos os. Efterfølgende skal du indtaste dit Alarm ID eller din alarms telefonnummer på din adresse under menupunktet “Min alarm”\n            Nabosupport skal også oprettes som kontaktperson i dit alarmsystem med følgende telefonnummer: +45 2580 7338\n          </p>\n        </ion-list>\n        <ion-list>\n          <ul class="faq-ul">\n            <li>\n              <b>Mit alarmselskab er der ikke.</b>\n            </li>\n          </ul>\n          <p>\n            Hvis vi endnu ikke har tilføjet din alarmselskab hos Nabosupport, kan du kontakte os på Nabosupport.dk. Vi vil efterfølgende sørge for at også din alarm kan forbindes til Nabosupport.            \n          </p>\n        </ion-list>\n        <ion-list>\n          <ul class="faq-ul">\n            <li>\n              <b>Jeg modtager ikke en notifikation når min alarm går i gang.</b>\n            </li>\n          </ul>\n          <p>\n            Hvis ikke du modtager en notifikation ved alarm, er dit Alarm ID muligvis ikke være indtastet korrekt.\n          </p>\n        </ion-list>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/help/help.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], HelpPage);
    return HelpPage;
}());

//# sourceMappingURL=help.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvitefriendPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__map_map__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var InvitefriendPage = /** @class */ (function () {
    function InvitefriendPage(navCtrl, navParams, toastCtrl, AuthServiceProvider, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.AuthServiceProvider = AuthServiceProvider;
        this.translate = translate;
        this.emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
        this.name = '';
        this.email = '';
        this.txtview = false;
        this.formview = true;
        this.inviteForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern(this.emailPattern)])
        });
    }
    InvitefriendPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InvitefriendPage');
        this.adminData = JSON.parse(localStorage.getItem('adminData'));
        this.promocode = this.adminData.data[6].data;
        var user = JSON.parse(localStorage.getItem('userData'));
        var lang = user.language;
        if (lang == 'en') {
            this.en = true;
            this.dn = false;
        }
        else {
            this.dn = true;
            this.en = false;
        }
    };
    InvitefriendPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
    };
    InvitefriendPage.prototype.alertToast = function (msg) {
        var message;
        this.translate.get(msg).subscribe(function (res) {
            message = res;
        });
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    InvitefriendPage.prototype.onSubmit = function (value) {
        var userdata = JSON.parse(localStorage.getItem('userData'));
        value.username = userdata.user_login;
        this.formData = value;
        console.log(value);
        if (this.inviteForm.valid) {
            window.localStorage.setItem('username', value.username);
            window.localStorage.setItem('password', value.password);
            this.formview = false;
            this.txtview = true;
            //alert('works!');
        }
        else if (this.inviteForm.controls.name.hasError('required') == true) {
            this.alertMessage = 'Please enter name';
            this.alertToast(this.alertMessage);
        }
        else if (this.inviteForm.controls.email.hasError('required') == true) {
            this.alertMessage = 'Please enter email';
            this.alertToast(this.alertMessage);
        }
        else if (this.inviteForm.controls.email.hasError('pattern') == true) {
            this.alertMessage = 'Please enter valid email';
            this.alertToast(this.alertMessage);
        }
    };
    InvitefriendPage.prototype.sendInvite = function () {
        var _this = this;
        this.formview = true;
        this.txtview = false;
        this.inviteForm.reset();
        this.alertMessage = 'Thank you for inviting your friend.';
        this.alertToast(this.alertMessage);
        this.AuthServiceProvider.postData(this.formData, 'inviteFriend').then(function (result) {
            console.log(result);
            _this.resdata = result;
        }, function (err) {
        });
    };
    InvitefriendPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-invitefriend',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/invitefriend/invitefriend.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'invite friend\' | translate}}</ion-title>\n    <a (click)="home()" class="home">\n      <ion-icon name="home"></ion-icon>\n    </a>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <form [formGroup]="inviteForm" (ngSubmit)="onSubmit(inviteForm.value)" *ngIf="formview">\n    <ion-item>\n      <ion-thumbnail item-start class="icn">\n        <ion-icon name="ios-contact"></ion-icon> \n      </ion-thumbnail> \n      <ion-label floating>{{\'NAME\' | translate}}</ion-label>\n      <ion-input formControlName="name" type="text"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-thumbnail item-start class="icn">\n        <ion-icon name="mail"></ion-icon> \n      </ion-thumbnail> \n      <ion-label floating>{{\'EMAIL\' | translate}}</ion-label>\n      <ion-input formControlName="email" type="email"></ion-input>\n    </ion-item>\n\n    <button button type="submit" class="sign_btn" ion-button  round full style="margin-top: 20px;" type="submit">{{ \'Invite\' | translate }}</button>\n  </form>\n  <ion-card *ngIf="txtview"> \n    <ion-card-content>\n      <div *ngIf="dn">\n          <p>\n            Kære <b>{{formData.name}}</b>\n          </p>\n          <p>\n            Detter er en invitation fra<b>{{formData.username}}</b>,  som gerne vil have dig med som nabosupporter.\n          </p>\n          <p>\n            <b>{{formData.username}}</b> er allerede blevet en del af Nabosupports store netværk af naboer og alarmsystemer.\n          </p>\n          <p>\n            Nabosupport er en ny og moderne overvågningsplatform, som samler alle dine opmærksomme naboer i nærområdet sammen med deres tyverialarmer. På den måde kan du og dine naboer reagere hurtigt på mistænkelig adfærd og alarmer og på den måde hjælpe hinanden med at skabe et trygt nærområde.\n          </p>\n          <p>\n            Hvis det lyder interessant, så besøg os på  www.nabosupport.dk  og tilmeld dig og få gratis adgang til alle vores funktioner med det samme. Brug rabatkoden " {{promocode}} " .\n          </p>\n          <p>Hvis du har spørgsmål omkring denne mail, så kontakt os på  info@nabosupport.dk .</p>\n          <ul id="add-list">\n            <li>De bedste hilsner</li>\n            <li></li>\n            <li>NaboSupport Aps</li>\n            <li>Skolelodden 6</li>\n            <li>3550 Slangerup</li>\n            <li>Denmark</li>\n            <li></li>\n            <li> www.NaboSupport.dk</li>\n          </ul>\n      </div>\n      <div *ngIf="en">\n        <p>\n          Dear <b>{{formData.name}}</b>\n        </p>\n        <p>\n          This is an invitation from<b>{{formData.username}}</b>,  who would like you to be a nabosupporter.\n        </p>\n        <p>\n          <b>{{formData.username}}</b>  is all ready a part of Nabosupports great network of neighbours and burglar alarms.\n        </p>\n        <p>\n          Nabosupport is a new and modern monitoring platform, that combine all of you neighbours in your neighbourhood, and there burglar alarms. In that way you and your neighbours can respond quickly to suspicious behavior and alarms, and help each other to create a safe neighborhood.      </p>\n        <p>\n          If you believe that this sound interesting, go visit us at  www.nabosupport.dk  and submit, and get free access to all of our functionalities. Use the promocode " {{promocode}} " .\n        </p>\n        <p>If you have any questions about this mail, please contact us at  info@nabosupport.dk .</p>\n        <ul id="add-list">\n          <li>Best regards</li>\n          <li></li>\n          <li>NaboSupport Aps</li>\n          <li>Skolelodden 6</li>\n          <li>3550 Slangerup</li>\n          <li>Denmark</li>\n          <li></li>\n          <li> www.NaboSupport.dk</li>\n        </ul>\n      </div> \n      <button button (click)="sendInvite();" color="secondary" ion-button  round full small>{{ \'OK,send\' | translate }}</button>\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/invitefriend/invitefriend.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], InvitefriendPage);
    return InvitefriendPage;
}());

//# sourceMappingURL=invitefriend.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__acceptsupport_acceptsupport__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Notification page (This page load the user unseen notification)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 






var NotificationPage = /** @class */ (function () {
    function NotificationPage(navCtrl, menu, AuthServiceProvider, navParams, events, modalCtrl) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.AuthServiceProvider = AuthServiceProvider;
        this.navParams = navParams;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.getTime();
        this.getAlertMsg();
        this.popupRefresh();
    }
    //ionic lifecycle events
    NotificationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificationPage');
    };
    NotificationPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    NotificationPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //redirect to map page
    NotificationPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__map_map__["a" /* MapPage */]);
    };
    //get alert notification from api
    NotificationPage.prototype.getAlertMsg = function () {
        var _this = this;
        this.AuthServiceProvider.postData(this.userData, 'getAlertMsg').then(function (result) {
            console.log(result);
            _this.userMsg = result;
            _this.highlightedDiv = 1;
        }, function (err) {
            console.log(err);
        });
    };
    NotificationPage.prototype.notifypage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__map_map__["a" /* MapPage */]);
    };
    //notification onclick for locate the alert location
    NotificationPage.prototype.locatAlert = function (message) {
        console.log(message);
        localStorage.setItem('lastLat', message.latitude);
        localStorage.setItem('lastLng', message.longitude);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__map_map__["a" /* MapPage */], {
            'page': 'notify'
        });
    };
    //delete notification from user device
    NotificationPage.prototype.removeItem = function (message) {
        var _this = this;
        console.log(message);
        this.AuthServiceProvider.postData(message, 'removeAlert').then(function (result) {
            _this.response = result;
            if (_this.response.status == true) {
                var index = _this.userMsg.indexOf(message);
                _this.userMsg.splice(index, 1);
                console.log('deleted array' + _this.userMsg);
            }
            else {
                console.log('please try later');
            }
        });
    };
    NotificationPage.prototype.getTime = function () {
        var currentDateTime = __WEBPACK_IMPORTED_MODULE_5_moment___default()().format("MM-DD-YYYY HH:mm:ss");
        console.log(currentDateTime);
        this.alertTime = currentDateTime;
    };
    NotificationPage.prototype.supportModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_1__acceptsupport_acceptsupport__["a" /* AcceptsupportPage */]);
        modal.present();
    };
    NotificationPage.prototype.supportReq = function (message) {
        console.log(message);
        var data = message;
        localStorage.setItem('accData', JSON.stringify(data));
        this.supportModal();
    };
    NotificationPage.prototype.popupRefresh = function () {
        var _this = this;
        this.events.subscribe('user:ref', function (flag) {
            if (flag == 1) {
                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            }
        });
    };
    NotificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            selector: 'page-notification',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/notification/notification.html"*/'\n<ion-header>\n  \n    <ion-navbar>\n      <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n      <ion-title>{{ \'Notification\' | translate }}</ion-title>\n      <a (click)="home()" class="home">\n        <ion-icon name="home"></ion-icon>\n      </a>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  <!-- <ion-content >\n      <ion-list id="mySecurityDevices-list3" *ngFor="let message of this.userMsg">\n          <ion-item color="none" id="mySecurityDevices-list-item9">\n            <ion-icon name="ios-notifications-outline" item-left></ion-icon>\n              <p>{{message.message}}</p><br>\n              <p *ngIf="message.time">Date: {{message.time}}</p>\n            </ion-item>\n          <ion-item color="none" id="mySecurityDevices-list-item10">\n            <ion-icon name="ios-notifications-outline" item-left></ion-icon>\n            Great offers on stickers just today!\n            </ion-item>\n      </ion-list> \n  </ion-content> -->\n  \n  <!-- <ion-content>\n    <ion-list id="mySecurityDevices-list3" *ngFor="let message of this.userMsg">\n      <div *ngIf="message.alert_status_type==\'B\'||message.alert_status_type==\'S\'">\n        <a href="#" (click)="notifypage()">\n          <ion-item color="none" id="mySecurityDevices-list-item9">\n            <ion-icon name="ios-home" item-left></ion-icon>\n            <p>{{message.message}}</p>\n            <br>\n            <p>Date: {{message.time}}</p>\n            <ion-icon name="ios-warning" item-left></ion-icon>\n          </ion-item>\n        </a>\n      </div>\n      <div *ngIf="message.alert_status_type==\'BC\'||message.alert_status_type==\'SC\'">\n        <a href="#" (click)="notifypage()">\n          <ion-item color="none" id="mySecurityDevices-list-item9">\n            <ion-icon name="ios-notifications" item-left></ion-icon>\n            <p>{{message.message}}</p>\n            <br>\n            <p>Date: {{message.time}}</p>\n            <ion-icon name="ios-warning" item-left></ion-icon>\n          </ion-item>\n        </a>\n      </div>\n      <div *ngIf="message.alert_status_type===\'N\'">\n        <ion-item color="none" id="mySecurityDevices-list-item9">\n          <ion-icon name="ios-notifications" item-left></ion-icon>\n          <p>{{message.message}}</p>\n          <br>\n          <p>Date: {{message.time}}</p>\n        </ion-item>\n      </div>\n      <div *ngIf="message.state===\'empty\'"> \n        <ion-item color="none" id="mySecurityDevices-list-item9">\n          <ion-icon name="ios-notifications" item-left></ion-icon>\n          <p>{{message.message}}</p>\n          <br>\n        </ion-item>\n      </div>\n    </ion-list>\n  </ion-content>  -->\n  <ion-content>\n    <ion-list *ngFor="let message of userMsg">\n      <!-- <ion-item-sliding *ngFor="let message of userMsg"> -->\n        <!-- <ion-item> -->\n          <ion-card *ngIf="message.alert_status_type === \'BC\'; else susnotify">\n            <div class="item-inner">\n              <ion-icon class="alert" name="ios-notifications-outline" item-left></ion-icon>\n              <p (click)="locatAlert(message)">{{message.message}} <br>{{message.street_address}},{{message.city}},{{message.zipcode}},{{message.country}}<br><span>{{message.time}}</span></p>\n              <button class="note-trash" (click)="removeItem(message)">\n                <ion-icon name="ios-trash-outline" item-left></ion-icon>\n              </button>\n            </div>\n          </ion-card>\n        <ng-template #susnotify>\n          <ion-card *ngIf="message.alert_status_type === \'SC\'; else ownnotify">\n            <div class="item-inner">\n              <ion-icon class="sus" name="ios-notifications-outline" item-left></ion-icon>\n              <p (click)="locatAlert(message)">{{message.message}} <br>{{message.street_address}},{{message.city}},{{message.zipcode}},{{message.country}}<br><span>{{message.time}}</span></p>\n              <button class="note-trash" (click)="removeItem(message)">\n                <ion-icon name="ios-trash-outline" item-left></ion-icon>\n              </button>\n            </div>\n          </ion-card>\n        </ng-template>\n\n        <ng-template #ownnotify>\n          <ion-card *ngIf="message.alert_status_type === \'B\' || message.alert_status_type === \'S\';else supportreq">\n            <div class="item-inner">\n              <ion-icon class="own" name="md-notifications-outline" item-left></ion-icon>\n              <p (click)="locatAlert(message)">{{message.message}} <br>{{message.street_address}},{{message.city}},{{message.zipcode}},{{message.country}}<br><span>{{message.time}}</span></p>\n              <button class="note-trash" (click)="removeItem(message)">\n                <ion-icon name="ios-trash-outline" item-left></ion-icon>\n              </button>\n            </div>\n          </ion-card>\n        </ng-template>\n\n        <ng-template #supportreq>\n          <ion-card *ngIf="message.alert_status_type === \'ASR\';else nonotify">\n            <div class="item-inner">\n              <ion-icon class="own" name="ios-contacts-outline" item-left></ion-icon>\n              <p (click)="supportReq(message)">{{message.message}} <br>{{\'Username\' | translate}}: {{message.user_name}}<br><span>{{message.time}}</span></p>\n              <!-- <button class="note-trash" (click)="removeItem(message)">\n                <ion-icon name="ios-trash-outline" item-left></ion-icon>\n              </button> -->\n            </div>\n          </ion-card>\n        </ng-template>\n\n        <ng-template #nonotify>\n          <ion-card>\n              <div class="item-inner">\n                <ion-icon name="ios-notifications-outline" item-left></ion-icon>\n                <p>{{message.message | translate}} <br>{{message.time}}</p>\n              </div>\n            </ion-card>\n        </ng-template>\n      <!-- </ion-item> -->\n        <!-- <ion-item-options side="left">\n          <button danger (click)="removeItem()"><ion-icon name="trash"></ion-icon> Delete</button>\n        </ion-item-options> -->\n      <!-- </ion-item-sliding> -->\n    </ion-list>\n  </ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/notification/notification.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ModalController */]])
    ], NotificationPage);
    return NotificationPage;
}());

//# sourceMappingURL=notification.js.map

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//for develop
var apiUrl = "http://54.93.200.112/naboapi/";
//for test 
//let apiUrl="http://nabosupport.dk/api/v1/";
var AuthServiceProvider = /** @class */ (function () {
    function AuthServiceProvider(http) {
        this.http = http;
        console.log('Hello AuthServiceProvider Provider');
    }
    AuthServiceProvider.prototype.postData = function (credentials, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Accept", 'application/json');
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            //let options = new RequestOptions({headers:headers});
            _this.http.post(apiUrl + type, JSON.stringify(credentials), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthServiceProvider.prototype.getData = function (type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            //let options = new RequestOptions({headers:headers});
            _this.http.get(apiUrl + type)
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], AuthServiceProvider);
    return AuthServiceProvider;
}());

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfferPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__map_map__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Offers and alarm page (This page load the offers for users from api)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 






var OfferPage = /** @class */ (function () {
    function OfferPage(navCtrl, navParams, AuthServiceProvider, translate, alertCtrl, browser) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.AuthServiceProvider = AuthServiceProvider;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.browser = browser;
        //base_url='http://34.216.55.85/naboapi/upload/offer/';
        this.base_url = 'http://rayi.in/naboapi_v2/uploads/offer/';
        this.categoryList = [];
    }
    //redirect to map page
    OfferPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
    };
    OfferPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OfferPage');
        this.getOffers();
        this.adminData = JSON.parse(localStorage.getItem('adminData'));
        //alert(JSON.stringify(this.adminData.data[3].data));
        for (var i = 0; i < this.adminData.data.length; i++) {
            console.log('service:' + this.adminData.data[i].data);
        }
        this.offerURL = this.adminData.data[3].data;
    };
    //redirected to Nabosupport web
    OfferPage.prototype.webpage = function () {
        var _this = this;
        var trans_btn_yes = '';
        var trans_btn_no = '';
        var title = '';
        var text = '';
        this.translate.get('Yes').subscribe(function (res) {
            trans_btn_yes = res;
        });
        this.translate.get('No').subscribe(function (res) {
            trans_btn_no = res;
        });
        this.translate.get('offer alert title').subscribe(function (res) {
            title = res;
        });
        this.translate.get('offer alert text').subscribe(function (res) {
            text = res;
        });
        var alert = this.alertCtrl.create({
            title: title,
            message: text,
            buttons: [
                {
                    text: trans_btn_yes,
                    role: 'cancel',
                    handler: function () {
                        console.log('Yes clicked');
                        //let openBrowser = this.browser.create('http://rayi.in/nabosupport/'+this.offerURL, '_system','location=no');
                        var openBrowser = _this.browser.create(_this.offerURL, '_system', 'location=no');
                    }
                },
                {
                    text: trans_btn_no,
                    handler: function () {
                        console.log('No clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    //get offers from api fro paid users
    OfferPage.prototype.getOffers = function () {
        var _this = this;
        this.AuthServiceProvider.getData('getOffers').then(function (res) {
            _this.rs = res;
            if (_this.rs.status === true) {
                _this.offers = _this.rs.data;
                console.log('offer object', _this.offers);
                if (_this.rs.data.length === 0) {
                    _this.isEmpty = true;
                }
                else {
                    _this.isEmpty = false;
                }
            }
            else {
                _this.isEmpty = true;
            }
        }, function (err) {
            console.log(err);
        });
    };
    OfferPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-offer',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/offer/offer.html"*/'\n<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Offers on Alarm\' | translate}}</ion-title>\n    <a (click)="home()" class="home">\n        <ion-icon name="home" class="cus-margin"></ion-icon>\n    </a>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <div *ngIf="isEmpty; then thenTemplateName; else elseTemplateName"></div>\n    <ng-template #thenTemplateName>\n        <ion-item id="contactList-list-item" class="white-clr">Record not found.</ion-item>\n    </ng-template>\n    <ng-template #elseTemplateName>\n        <ion-list class="list-border">\n            <ion-grid>\n                <ion-row>\n                    <ion-col col-md-3 col-sm-6 col-12 *ngFor="let row of offers">\n                        <ion-card onclick="webpage()">\n                            <div class="shop-img">\n                                <!-- <img [src]="base_url+offers.img_path" onerror="this.src=\'assets/imgs/no-img.svg\';" (click)="webpage();"> -->\n                                <img [src]="base_url+row.img_path" (click)="webpage();">\n                            </div>\n                            <div class="card-title">{{row.name}}</div>\n                            <div class="card-subtitle"><ion-badge class="o-badge">{{\'Original price\' | translate}} :$ {{row.device_price | number:\'1.2-2\'}}</ion-badge></div>\n                            <div class="card-subtitle"><ion-badge class="off-badge">{{\'Offer price\' | translate}} :$ {{row.offer_amt | number:\'1.2-2\'}}</ion-badge></div>\n                            <br/>\n                        </ion-card>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-list>\n    </ng-template>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/offer/offer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], OfferPage);
    return OfferPage;
}());

//# sourceMappingURL=offer.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__popover_popover__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: contact page (This page will load user contacts)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 







var ContactPage = /** @class */ (function () {
    function ContactPage(translate, alertCtrl, navCtrl, toastCtrl, navParams, AuthServiceProvider, popoverCtrl, menu) {
        var _this = this;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.AuthServiceProvider = AuthServiceProvider;
        this.popoverCtrl = popoverCtrl;
        this.menu = menu;
        this.isEmpty = false;
        this.btnclear = false;
        this.btnName = 'Add contact';
        this.userData = { "name": "", "mail": "", "phone": "", "id": "", "type": "", "code": "" };
        this.countryPHCode = [];
        this.form = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            id: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */](''),
            type: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */](''),
            userid: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
            name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
            mail: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].email]),
            phone: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern('^(0|[1-9][0-9]*)$')]),
            code: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('')
        });
        this.mycontact = "contactlist";
        this.userData.type = 'save';
        this.translate.get("New Contact").subscribe(function (res) {
            _this.tabname = res;
        });
        this.getContact();
        this.selectCode = {
            title: 'Select Code',
            subTitle: ''
        };
        this.countryPHCode = [
            {
                name: 'IN',
                value: '+91'
            },
            {
                name: 'DK',
                value: '+45'
            }
        ];
    }
    Object.defineProperty(ContactPage.prototype, "userid", {
        //get the field details from contact form
        get: function () {
            return this.form.get('userid');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactPage.prototype, "name", {
        get: function () {
            return this.form.get('name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactPage.prototype, "mail", {
        get: function () {
            return this.form.get('mail');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactPage.prototype, "phone", {
        get: function () {
            return this.form.get('phone');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactPage.prototype, "code", {
        get: function () {
            return this.form.get('code');
        },
        enumerable: true,
        configurable: true
    });
    ContactPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    ContactPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //Onclicking home button redirect to map page
    ContactPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__map_map__["a" /* MapPage */]);
    };
    ContactPage.prototype.optionsFn = function (start) {
        console.log(start);
        this.countryCode = start;
        // this.userData.phone = start+this.userData.phone;
        // console.log(this.userData.phone);
    };
    //For paid user to add multiple addresses in popover
    ContactPage.prototype.presentPopover = function (myEvent) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_5__popover_popover__["a" /* PopoverPage */]);
        popover.present({
            ev: myEvent
        });
    };
    //get the user contact details from api
    ContactPage.prototype.getContact = function () {
        var _this = this;
        console.log('ionViewDidLoad ContactPage');
        var userdata = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(userdata, 'getMycontact').then(function (result) {
            _this.responseData = result;
            var status = JSON.parse(JSON.stringify(_this.responseData));
            if (typeof status.status == 'undefined') {
                _this.isEmpty = false;
                _this.itemsList = _this.responseData;
            }
            else {
                _this.isEmpty = true;
            }
        }, function (err) {
        });
    };
    //get local date and time
    ContactPage.prototype.getDateTime = function () {
        var today = new Date();
        //var dateTime = today.toLocaleString();
        var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        var time = new Date();
        //var dateTime = date + ' : ' + time.toLocaleString('en-DN', { hour: 'numeric', minute: 'numeric', hour12: true });
        var dateTime = date + ' ' + time.toLocaleString('en-DN', { hour: 'numeric', minute: 'numeric', hour12: false });
        return dateTime;
    };
    //add user contact details to api
    ContactPage.prototype.contact = function () {
        var _this = this;
        // save
        if (this.userData.type == 'save') {
            var success = '';
            var err = '';
            var err1 = '';
            var err2 = '';
            var err3 = '';
            this.translate.get('Contact added successfully').subscribe(function (res) {
                success = res;
            });
            this.translate.get('Please fillup the detail to contact').subscribe(function (res) {
                err = res;
            });
            this.translate.get('Invalid Email format').subscribe(function (res) {
                err1 = res;
            });
            this.translate.get('exst').subscribe(function (res) {
                err2 = res;
            });
            this.translate.get('own').subscribe(function (res) {
                err3 = res;
            });
            var toast_1 = this.toastCtrl.create({
                message: success,
                duration: 3000,
                position: 'bottom'
            });
            var toast1 = this.toastCtrl.create({
                message: err,
                duration: 3000,
                position: 'bottom'
            });
            var toast2 = this.toastCtrl.create({
                message: err1,
                duration: 3000,
                position: 'bottom'
            });
            var toast3_1 = this.toastCtrl.create({
                message: err2,
                duration: 3000,
                position: 'bottom'
            });
            var toast4_1 = this.toastCtrl.create({
                message: err3,
                duration: 3000,
                position: 'bottom'
            });
            var userdata = JSON.parse(localStorage.getItem('userData'));
            this.userData['user_id'] = userdata['user_id'];
            this.userData['username'] = userdata['user_login'];
            this.userData['time'] = this.getDateTime();
            console.log(this.userData);
            if (this.userData.name && this.userData.mail) {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (reg.test(this.userData.mail) == true) {
                    console.log(this.userData);
                    this.AuthServiceProvider.postData(this.userData, 'mycontact').then(function (result) {
                        _this.responseData = result;
                        if (_this.responseData.status == true) {
                            console.log(_this.responseData);
                            toast_1.present();
                            //this.getContact();
                            _this.form.reset();
                            _this.clearForm();
                            _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                            // this.navCtrl.setRoot(this.navCtrl.getActive().component);
                            _this.mycontact = 'contact';
                        }
                        else if (_this.responseData.status == false) {
                            toast3_1.present();
                        }
                        else if (_this.responseData.status == 'own') {
                            toast4_1.present();
                        }
                    }, function (err) {
                        // Error log
                    });
                }
                else {
                    toast2.present();
                }
            }
            else {
                toast1.present();
            }
        }
        else {
            //update the  contact details to api
            var usuccess = '';
            var uerr = '';
            var uerr1 = '';
            this.translate.get('Contact updated successfully').subscribe(function (res) {
                usuccess = res;
            });
            this.translate.get('Please fillup the detail to contact').subscribe(function (res) {
                uerr = res;
            });
            this.translate.get('Invalid Email format').subscribe(function (res) {
                uerr1 = res;
            });
            var toast = this.toastCtrl.create({
                message: success,
                duration: 3000,
                position: 'bottom'
            });
            var toast1 = this.toastCtrl.create({
                message: err,
                duration: 3000,
                position: 'bottom'
            });
            var toast2 = this.toastCtrl.create({
                message: err1,
                duration: 3000,
                position: 'bottom'
            });
            var toastU_1 = this.toastCtrl.create({
                message: usuccess,
                duration: 3000,
                position: 'bottom'
            });
            var toast1U = this.toastCtrl.create({
                message: uerr,
                duration: 3000,
                position: 'bottom'
            });
            var toast2U = this.toastCtrl.create({
                message: uerr1,
                duration: 3000,
                position: 'bottom'
            });
            var userdata = JSON.parse(localStorage.getItem('userData'));
            this.userData['user_id'] = userdata['user_id'];
            console.log(this.userData);
            if (this.userData.name && this.userData.mail) {
                var regu = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if (regu.test(this.userData.mail) == true) {
                    this.AuthServiceProvider.postData(this.userData, 'mycontactUpdate').then(function (result) {
                        _this.responseData = result;
                        if (true == _this.responseData.status) {
                            _this.btnName = 'Add contact';
                            toastU_1.present();
                            _this.getContact();
                            _this.form.reset();
                            _this.clearForm();
                            _this.mycontact = 'contactlist';
                        }
                    }, function (err) {
                        // Error log
                    });
                }
                else {
                    toast2U.present();
                }
            }
            else {
                toast1U.present();
            }
        }
    };
    //edit contact details and update the api
    ContactPage.prototype.editContact = function (id) {
        this.btnclear = true;
        for (var i = 0; i < this.itemsList.length; i++) {
            if (id == this.itemsList[i].my_contact_id) {
                this.editData = this.itemsList[i];
            }
        }
        this.setEditValue();
    };
    //set edit value updated in local variable
    ContactPage.prototype.setEditValue = function () {
        // "name": "", "countrycode": "", "mail": "", "phone": "", "id": "", "type": ""
        this.userData.name = this.editData.name;
        this.userData.mail = this.editData.email_id;
        this.userData.phone = this.editData.phone_number.substring(3, 20);
        this.userData.id = this.editData.my_contact_id;
        this.userData.code = this.editData.phone_number.substring(0, 3);
        this.userData.type = 'update';
        var trans_btn_update = '';
        var trans_btn_update_tab = '';
        this.translate.get('Update').subscribe(function (res) {
            trans_btn_update = res;
        });
        this.translate.get('Update Contact').subscribe(function (res) {
            trans_btn_update_tab = res;
        });
        this.btnName = trans_btn_update;
        this.mycontact = 'contact';
        this.tabname = trans_btn_update_tab;
    };
    //delete user contact from api
    ContactPage.prototype.deleteContact = function (id) {
        var _this = this;
        var trans_role_cancel = '';
        var trans_role_yes = '';
        var trans_role_no = '';
        var trans_role_title = '';
        var trans_role_msg = '';
        this.translate.get("cancel").subscribe(function (res) {
            trans_role_cancel = res;
        });
        this.translate.get("Yes").subscribe(function (res) {
            trans_role_yes = res;
        });
        this.translate.get("No").subscribe(function (res) {
            trans_role_no = res;
        });
        this.translate.get("My contact").subscribe(function (res) {
            trans_role_title = res;
        });
        this.translate.get("Do you want to delete?").subscribe(function (res) {
            trans_role_msg = res;
        });
        var alert = this.alertCtrl.create({
            title: trans_role_title,
            message: trans_role_msg,
            buttons: [
                {
                    text: trans_role_no,
                    role: trans_role_cancel,
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: trans_role_yes,
                    handler: function () {
                        _this.AuthServiceProvider.postData(JSON.parse('{"id":' + id + '}'), 'myContactDelete')
                            .then(function (result) {
                            _this.getContact();
                        }, function (err) {
                            console.log(err);
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    //clear contact form
    ContactPage.prototype.clearForm = function () {
        var trans_tab_name = "";
        var trans_tab_add = "";
        this.translate.get("New Contact").subscribe(function (res) {
            trans_tab_name = res;
        });
        this.translate.get("Add").subscribe(function (res) {
            trans_tab_add = res;
        });
        this.userData.name = '';
        this.userData.mail = '';
        this.userData.phone = '';
        this.userData.id = '';
        this.userData.type = 'save';
        this.tabname = trans_tab_name;
        this.btnclear = false;
        this.btnName = trans_tab_add;
    };
    ContactPage.prototype.isValidNumber = function (event) {
        //return /\d|Backspace/.test(event.key);
        if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.keyCode) > -1) {
            // backspace, enter, escape, arrows
            return true;
        }
        else if (event.keyCode >= 48 && event.keyCode <= 57) {
            // numbers 0 to 9
            return true;
        }
        else if (event.keyCode >= 96 && event.keyCode <= 105) {
            // numpad number
            return true;
        }
        return false;
    };
    ContactPage.prototype.pendingInfo = function () {
        var msg;
        this.translate.get('reqpend').subscribe(function (res) {
            msg = res;
        });
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    ContactPage.prototype.rejectInfo = function () {
        var msg;
        this.translate.get('reqrej').subscribe(function (res) {
            msg = res;
        });
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/contact/contact.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'My supporters\' | translate}}</ion-title>\n    <a (click)="home()" class="home">\n      <ion-icon name="home"></ion-icon>\n    </a>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-segment [(ngModel)]="mycontact">\n    <ion-segment-button value="contactlist">\n        {{\'Contact List\' | translate}}\n    </ion-segment-button>\n    <ion-segment-button value="contact">\n      {{\'ADD CONTACT\' | translate}}\n    </ion-segment-button>\n  </ion-segment>\n\n  <div [ngSwitch]="mycontact" padding>\n    <ion-list *ngSwitchCase="\'contactlist\'">\n      <div *ngIf="isEmpty; then thenTemplateName; else elseTemplateName"></div>\n\n      <ng-template #thenTemplateName>\n        <ion-item id="contactList-list-item">{{\'Contact not found\' | translate}}</ion-item>\n      </ng-template>\n\n      <ng-template #elseTemplateName>\n        <span *ngFor="let item of itemsList">\n\n          <ion-item id="contactList-list-item">\n\n            {{item.name}} <span *ngIf="item.status == 0;else reject"><ion-icon class="req-pend" name="ios-alert-outline" (click)="pendingInfo();"></ion-icon></span>\n            <ng-template #reject>\n              <span *ngIf="item.status == 2;else normal"><ion-icon class="req-pend" name="ios-remove-circle-outline" (click)="rejectInfo();"></ion-icon></span>\n            </ng-template>\n            <ng-template #normal>\n              <span></span>\n            </ng-template>\n            <br /> {{item.countrycode}}\n            <br /> {{item.email_id}}\n            <!-- <br /> {{item.phone_number}} -->\n            <div clear item-end>\n              <button ion-fab type="button" class="sign_btn" (click)="editContact(item.my_contact_id)" mini>\n                <ion-icon name="create"></ion-icon>\n              </button>\n              <button ion-fab type="button" class="sign_btn" (click)="deleteContact(item.my_contact_id)" mini>\n                <ion-icon name="close"></ion-icon>\n              </button>\n            </div>\n          </ion-item>\n        </span>\n      </ng-template>\n\n    </ion-list>\n    <ion-list *ngSwitchCase="\'contact\'">\n      <form [formGroup]="form" (ngSubmit)="contact()">\n        <ion-input type="hidden" formControlName="id" [(ngModel)]="userData.id"></ion-input>\n        <ion-input type="hidden" formControlName="type" [(ngModel)]="userData.type"></ion-input>\n        <ion-item>\n          <ion-thumbnail item-start>\n            <ion-icon name="ios-contact"></ion-icon>\n          </ion-thumbnail>\n          <ion-label floating>\n            {{\'NAME\' | translate}}</ion-label>\n          <ion-input class="no-left-pad" type="text" formControlName="name" [(ngModel)]="userData.name"></ion-input>\n        </ion-item>\n\n        <ion-item>\n          <ion-thumbnail item-start>\n            <ion-icon name="mail"></ion-icon>\n          </ion-thumbnail>\n          <ion-label floating>\n            {{\'EMAIL\' | translate}}</ion-label>\n          <ion-input type="text" formControlName="mail" [(ngModel)]="userData.mail"></ion-input>\n        </ion-item>\n\n        <!-- <ion-row>\n              <div>\n                <ion-thumbnail class="phone-thumb-cls" item-start>\n                  <ion-icon name="ios-phone-portrait"></ion-icon>\n                </ion-thumbnail>\n              </div>\n              <ion-col width-35>\n                <ion-item class=\'select-item\'>\n                  <ion-label class="code-cls" floating>\n                    CODE</ion-label>\n                  <ion-select class="select-cls" [(ngModel)]="userData.code" formControlName="code" [selectOptions]="selectCode">\n                    <ion-option *ngFor="let list of countryPHCode" [value]="list.value">{{list.name}}</ion-option>\n                  </ion-select>\n                </ion-item>\n              </ion-col>\n              <ion-col width-50>\n                <ion-item>\n                  <ion-label floating>\n                    {{\'PHONE NUMBER\' | translate}}</ion-label>\n                  <ion-input type="number" formControlName="phone" (keydown)="isValidNumber($event)" max="10" pattern="[0-9]" [(ngModel)]="userData.phone"></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row> -->\n\n        <button type="submit" class="sign_btn" ion-button round full>{{btnName | translate}}</button>\n        <button type="submit" class="sign_btn" ion-button round full *ngIf="btnclear" (click)="clearForm();">Cancel</button>\n      </form>\n    </ion-list>\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/contact/contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddressPage = /** @class */ (function () {
    function AddressPage(translate, navCtrl, navParams, AuthServiceProvider, alertCtrl, loadingCtrl, toastCtrl, geolocation) {
        this.translate = translate;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.AuthServiceProvider = AuthServiceProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.geolocation = geolocation;
        this.isDisabled = [];
        this.addEdit = [];
        this.addUpdate = [];
        this.addDisable = false;
        this.lastImage = null;
        this.fileopen = false;
        this.addAddress = false;
        this._addCtrl = false;
        this._editCtrl = false;
        this.viewCard = true;
        this.viewMap = false;
        this.editDisabled = true;
        this.dis = true;
        this.addE = true;
        this.addU = false;
        this.newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "camtoken": "" };
        this.mapcondition = false;
        this.imgoption = false;
        this.locationdata = { "hus": "", "street": "", "city": "" };
        this.marker_data = { "latitude": "", "longitude": "" };
        this.tranText = {};
        this.myaddress = 'addresslist';
        this.normalUser = JSON.parse(localStorage.getItem('userData'));
        console.log('nuser data: ' + this.normalUser);
    }
    AddressPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddressPage');
        this.getAddresses();
    };
    AddressPage.prototype.ngOnInit = function () {
        this.acService = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    };
    //Onclicking home button redirect to map page
    AddressPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__map_map__["a" /* MapPage */]);
    };
    AddressPage.prototype.getAddresses = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem('userData'));
        console.log(userdata['user_id']);
        var user = {
            'user_id': userdata.user_id,
            'address_id': userdata.address_id
        };
        this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            console.log(user_detail[0].paid_member);
            if (user_detail[0].paid_member == 1) {
                _this.AuthServiceProvider.postData(user, 'getPaidUserAddress').then(function (result) {
                    _this.addresses = result;
                    _this._ispaid = true;
                    console.log('user saved addresses ' + _this.addresses);
                }, function (err) {
                    console.log(err);
                });
            }
            _this.dis = true;
            console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
            _this.LastLat1 = user_detail[0].latitude;
            _this.LastLng1 = user_detail[0].longitude;
        });
    };
    AddressPage.prototype.onSegmentChanged = function (event) {
        console.log(event);
        if (this.myaddress == 'address' && this._ispaid == true) {
            this.initMap();
        }
    };
    //initiate google map
    AddressPage.prototype.initMap = function () {
        var _this = this;
        var loadtxt = '';
        this.translate.get('Map loading...').subscribe(function (res) {
            loadtxt = res;
        });
        this.geocoder = new google.maps.Geocoder();
        this.options = {
            enableHighAccuracy: true
        };
        var loader = this.loadingCtrl.create({
            spinner: 'crescent',
            content: loadtxt
        });
        loader.present();
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            _this.currentPos = pos;
            console.log(pos);
            console.log(pos.coords.latitude + ',' + pos.coords.longitude);
            _this.latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            _this.mapOptions = {
                center: _this.latlng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                fullscreenControl: false,
                streetViewControl: false,
                mapTypeControl: false,
                clickableIcons: false,
                draggable: true
            };
            console.log('map initiated');
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, _this.mapOptions);
            _this.addMarker(pos.coords.latitude, pos.coords.longitude);
            _this.updateSearch();
            loader.dismiss();
        }, function (err) {
            console.log("error : " + err.message);
            loader.dismiss();
        });
    };
    AddressPage.prototype.addMap = function () {
        var latLng = new google.maps.LatLng(this.LastLat1, this.LastLng1);
        var mapOptions = {
            center: latLng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            clickableIcons: false,
            draggable: false,
            zoomControl: false
            //draggable: this.mapcondition, zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.addMarker(this.LastLat1, this.LastLng1);
        this.updateSearch();
    };
    AddressPage.prototype.addMarker = function (lat, lng) {
        var _this = this;
        this.nabo_img = 'http://nabosupport.dk/api/v1/mapicon/new-markers/blue.svg';
        this.marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
            icon: this.nabo_img,
        });
        google.maps.event.addListener(this.marker, 'click', function (event) {
            //infoWindow.open(this.map, marker);
            //alert(JSON.stringify(event));
        });
        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(this.map, 'dragend', function () {
            _this.clearMarkers();
            _this.LastLat1 = _this.map.getCenter().lat();
            _this.LastLng1 = _this.map.getCenter().lng();
            var markerpos = new google.maps.LatLng(_this.LastLat1, _this.LastLng1);
            _this.marker.setPosition(markerpos);
            var coord = new Array();
            coord.push(_this.LastLat1, _this.LastLng1);
            var user = JSON.parse(localStorage.getItem('userData'));
            _this.newAddAry.user_id = user.user_id;
            _this.newAddAry.latitude = coord[0];
            _this.newAddAry.longitude = coord[1];
            console.log(coord[0] + ',' + coord[1]);
            var latlng = new google.maps.LatLng(_this.LastLat1, _this.LastLng1);
            var request = { latLng: latlng };
            console.log(_this.LastLat1 + ',' + _this.LastLng1);
            geocoder.geocode(request, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var result = results[0];
                    var rsltAdrComponent = result.address_components;
                    //console.log(rsltAdrComponent);
                    console.log(result.formatted_address);
                    //this.addressDisplayToast(result.formatted_address);
                    _this.autocomplete.query = result.formatted_address;
                    var resultLength = rsltAdrComponent.length;
                    if (result != null) {
                        var data = new Array;
                        var locationdata = new Array();
                        for (var i = 0; i < rsltAdrComponent.length; i++) {
                            var obj = rsltAdrComponent[i];
                            data.push(obj.long_name);
                        }
                        if (data.length == 5) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[3];
                            _this.newAddAry.city = data[2];
                            _this.newAddAry.zipcode = data[4];
                        }
                        else if (data.length == 6) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[4];
                            _this.newAddAry.city = data[2];
                            _this.newAddAry.zipcode = data[5];
                        }
                        else if (data.length == 7) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[5];
                            _this.newAddAry.city = data[2];
                            _this.newAddAry.zipcode = data[6];
                        }
                        else if (data.length == 8) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[6];
                            _this.newAddAry.city = data[4];
                            _this.newAddAry.zipcode = data[7];
                        }
                        else if (data.length == 9) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[7];
                            _this.newAddAry.city = data[4];
                            _this.newAddAry.zipcode = data[8];
                        }
                        else if (data.length == 10) {
                            _this.newAddAry.street_address = data[1] + " " + data[0];
                            _this.newAddAry.country = data[8];
                            _this.newAddAry.city = data[5];
                            _this.newAddAry.zipcode = data[9];
                        }
                    }
                }
            });
        });
    };
    AddressPage.prototype.addressDisplayToast = function (address) {
        var toast = this.toastCtrl.create({
            message: address,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    AddressPage.prototype.updateSearch = function () {
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var self = this;
        var config = {
            types: ['geocode'],
            input: this.autocomplete.query,
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            self.autocompleteItems = [];
            console.log(predictions);
            predictions.forEach(function (prediction) {
                self.autocompleteItems.push(prediction);
            });
        });
    };
    AddressPage.prototype.toastViewer = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    //confirm alert for registered address set as primary address
    AddressPage.prototype.defaultUpdateConfirm = function (add) {
        var _this = this;
        var conftxt = '';
        var confmsg = '';
        var no = '';
        var yes = '';
        this.translate.get('Confirm this address').subscribe(function (res) {
            conftxt = res;
        });
        this.translate.get('Do you want to set this as primary address?').subscribe(function (res) {
            confmsg = res;
        });
        this.translate.get('No').subscribe(function (res) {
            no = res;
        });
        this.translate.get('Yes').subscribe(function (res) {
            yes = res;
        });
        var alert = this.alertCtrl.create({
            title: conftxt,
            message: confmsg,
            buttons: [
                {
                    text: no,
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: yes,
                    handler: function () {
                        console.log('yes clicked');
                        _this.setPrimaryAddress(_this.add, _this.i);
                    }
                }
            ]
        });
        alert.present();
    };
    //api call to set address primary
    AddressPage.prototype.setPrimaryAddress = function (add, i) {
        var _this = this;
        var tit = '';
        this.translate.get("Profile updating...").subscribe(function (res) {
            tit = res;
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: tit
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        console.log(add);
        this.AuthServiceProvider.postData(add, 'updatePrimaryAddress').then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            if (true == _this.responseData.status) {
                _this.addEdit[i] = false;
                _this.addUpdate[i] = false;
                _this.isDisabled[i] = false;
                _this.getAddresses();
                _this.translate.get('updated as primary address.').subscribe(function (res) {
                    _this.toastmessage = res;
                });
                _this.toastViewer(_this.toastmessage);
            }
            else {
                _this.translate.get('update failed.').subscribe(function (res) {
                    _this.toastmessage = res;
                });
                _this.toastViewer(_this.toastmessage);
            }
        });
    };
    AddressPage.prototype.editAddress = function (i) {
        this.isDisabled[i] = true;
        this.addEdit[i] = true;
        this.addUpdate[i] = true;
    };
    AddressPage.prototype.updateOldAddress = function (add, i) {
        var _this = this;
        var address = add.street_address + ", " + add.city + ", " + add.country + ", " + add.zipcode + " ";
        console.log(address);
        this.geocoder = new google.maps.Geocoder;
        this.geocoder.geocode({ 'address': address }, function (results, status) {
            if (status === 'OK') {
                console.log(results);
                //alert(address);
                var position = {
                    lat: results[0].geometry.location.lat(),
                    lng: results[0].geometry.location.lng()
                };
                console.log(position);
                add.latitude = position.lat;
                add.longitude = position.lng;
                localStorage.setItem('accLatLng', JSON.stringify(add));
                _this.updateAddress(add, i);
            }
            else {
                _this.toastmessage = 'Geocode was not successful for the following reason: ' + status;
                _this.toastViewer(_this.toastmessage);
            }
        });
    };
    AddressPage.prototype.updateAddress = function (add, i) {
        var _this = this;
        console.log(add);
        var load_cont = '';
        this.translate.get('Address updating...').subscribe(function (res) {
            load_cont = res;
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: load_cont
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        this.AuthServiceProvider.postData(add, 'updateEditAddress').then(function (result) {
            _this.responseData = result;
            if (true == _this.responseData.status) {
                var aus_title_1 = '';
                _this.translate.get("Address updated successfully").subscribe(function (res) {
                    aus_title_1 = res;
                });
                _this.toastmessage = aus_title_1;
                _this.toastViewer(_this.toastmessage);
                _this.isDisabled[i] = false;
                _this.addEdit[i] = false;
                _this.addUpdate[i] = false;
                _this.getAddresses();
            }
            else {
                var toast_update_again_1 = '';
                _this.translate.get('Update failed try again.').subscribe(function (res) {
                    toast_update_again_1 = res;
                });
                _this.toastmessage = toast_update_again_1;
                _this.toastViewer(_this.toastmessage);
            }
        });
    };
    AddressPage.prototype.deleteAddress = function (add, i) {
        var _this = this;
        console.log(add);
        var trans_title = '';
        var trans_msg = '';
        var trans_btn_text_yes = "";
        this.translate.get("Address").subscribe(function (res) {
            trans_title = res;
        });
        this.translate.get("Do you want to delete the address?").subscribe(function (res) {
            trans_msg = res;
        });
        this.translate.get("Yes").subscribe(function (res) {
            trans_btn_text_yes = res;
        });
        var trans_btn_text_no = '';
        this.translate.get("No").subscribe(function (res) {
            trans_btn_text_no = res;
        });
        var alert = this.alertCtrl.create({
            title: trans_title,
            message: trans_msg,
            buttons: [
                {
                    text: trans_btn_text_no,
                    role: 'cancel',
                    handler: function () {
                    }
                },
                {
                    text: trans_btn_text_yes,
                    handler: function () {
                        var toast_add_success = '';
                        var toast_delete_fail = '';
                        _this.translate.get("Address deleted successfully.").subscribe(function (res) {
                            toast_add_success = res;
                        });
                        _this.translate.get("delete failed.please try again.").subscribe(function (res) {
                            toast_delete_fail = res;
                        });
                        _this.AuthServiceProvider.postData(add, 'deleteAddress').then(function (result) {
                            _this.responseData = result;
                            if (true == _this.responseData.status) {
                                _this.toastmessage = toast_add_success;
                                _this.toastViewer(_this.toastmessage);
                                _this.getAddresses();
                                _this.addUpdate[i] = true;
                                _this.addEdit[i] = false;
                            }
                            else {
                                _this.toastmessage = toast_delete_fail;
                                _this.toastViewer(_this.toastmessage);
                            }
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    AddressPage.prototype.clearMarkers = function () {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers.length = 0;
    };
    AddressPage.prototype.chooseItem = function (item) {
        var _this = this;
        console.log('modal > chooseItem > item > ', item);
        //this.viewCtrl.dismiss(item);
        //console.log(item);
        this.addInput = this.autocomplete.query;
        console.log('query:' + this.addInput);
        this.geocoder = new google.maps.Geocoder;
        this.markers = [];
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
        this.geocoder.geocode({ 'placeId': item.place_id }, function (results, status) {
            console.log(results);
            // if (status === 'OK' && results[0]) {
            var position = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
            _this.LastLat1 = results[0].geometry.location.lat();
            _this.LastLng1 = results[0].geometry.location.lng();
            localStorage.setItem('accLatLng', JSON.stringify(position));
            console.log(position);
            if (results[0] != null) {
                // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
                //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
                var data = new Array;
                var rsltAdrComponent = results[0].address_components;
                var rslength = rsltAdrComponent.length;
                for (var i = 0; i < rslength; i++) {
                    var obj = rsltAdrComponent[i];
                    data.push(obj.long_name);
                }
                //console.log(locdata);
                var user = JSON.parse(localStorage.getItem('userData'));
                _this.newAddAry.user_id = user.user_id;
                _this.newAddAry.latitude = position.lat;
                _this.newAddAry.longitude = position.lng;
                console.log('new address');
                if (data.length == 5) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[3];
                    _this.newAddAry.city = data[2];
                    _this.newAddAry.zipcode = data[4];
                }
                else if (data.length == 6) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[4];
                    _this.newAddAry.city = data[2];
                    _this.newAddAry.zipcode = data[5];
                }
                else if (data.length == 7) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[5];
                    _this.newAddAry.city = data[2];
                    _this.newAddAry.zipcode = data[6];
                }
                else if (data.length == 8) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[6];
                    _this.newAddAry.city = data[4];
                    _this.newAddAry.zipcode = data[7];
                }
                else if (data.length == 9) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[7];
                    _this.newAddAry.city = data[4];
                    _this.newAddAry.zipcode = data[8];
                }
                else if (data.length == 10) {
                    _this.newAddAry.street_address = data[1] + " " + data[0];
                    _this.newAddAry.country = data[8];
                    _this.newAddAry.city = data[5];
                    _this.newAddAry.zipcode = data[9];
                }
                console.log('new addres:' + JSON.stringify(_this.newAddAry));
                _this.autocomplete.query = item.description;
            }
            else {
                //alert("No address available");
                console.log("No address available");
            }
            var markerpos = new google.maps.LatLng(_this.LastLat1, _this.LastLng1);
            _this.marker.setPosition(markerpos);
            _this.map.setCenter({ lat: _this.LastLat1, lng: _this.LastLng1 });
        });
    };
    AddressPage.prototype.addNewAddress = function () {
        var _this = this;
        var trans_load_p_update = '';
        this.translate.get("Profile updating...").subscribe(function (res) {
            trans_load_p_update = res;
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: trans_load_p_update
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        if (this.newAddAry.latitude == '' && this.newAddAry.longitude == '' && this.newAddAry.street_address == '') {
            var toast_no_address_1 = '';
            this.translate.get("No address get please select correct address.").subscribe(function (res) {
                toast_no_address_1 = res;
            });
            this.toastmessage = toast_no_address_1;
            this.toastViewer(this.toastmessage);
        }
        else {
            this.newAddAry.primary_address = 'no';
            this.AuthServiceProvider.postData(this.newAddAry, 'addMoreAddress').then(function (result) {
                // loading.present();
                _this.responseData = result;
                var user_detail = _this.responseData;
                if (true == _this.responseData.status) {
                    var trans_toast_s_msg_1 = '';
                    _this.translate.get("Address added successfully").subscribe(function (res) {
                        trans_toast_s_msg_1 = res;
                    });
                    _this.toastmessage = trans_toast_s_msg_1;
                    _this.toastViewer(_this.toastmessage);
                    _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                }
                else {
                    var toast_re_try_1 = '';
                    _this.translate.get("failed please re-try.").subscribe(function (res) {
                        toast_re_try_1 = res;
                    });
                    _this.toastmessage = toast_re_try_1;
                    _this.toastViewer(_this.toastmessage);
                }
            });
        }
    };
    AddressPage.prototype.editNAddress = function () {
        this.dis = false;
        this.addE = false;
        this.addU = true;
    };
    AddressPage.prototype.updateNAddress = function (add) {
        var _this = this;
        console.log(add);
        var address = add.street_address + ", " + add.city + ", " + add.country + ", " + add.zipcode + " ";
        console.log(address);
        this.geocoder = new google.maps.Geocoder;
        this.geocoder.geocode({ 'address': address }, function (results, status) {
            if (status === 'OK') {
                console.log(results);
                //alert(address);
                var position = {
                    lat: results[0].geometry.location.lat(),
                    lng: results[0].geometry.location.lng()
                };
                console.log(position);
                add.latitude = position.lat;
                add.longitude = position.lng;
                localStorage.setItem('accLatLng', JSON.stringify(add));
                console.log(add);
                var trans_add_update_1 = '';
                _this.translate.get('Address updating...').subscribe(function (res) {
                    trans_add_update_1 = res;
                });
                var loading_1 = _this.loadingCtrl.create({
                    spinner: 'crescent',
                    content: trans_add_update_1
                });
                setTimeout(function () {
                    loading_1.dismiss();
                }, 300);
                _this.AuthServiceProvider.postData(add, 'updateEditAddress').then(function (result) {
                    _this.responseData = result;
                    if (true == _this.responseData.status) {
                        var trans_toast_u_msg_1 = "";
                        _this.translate.get("Address updated successfully").subscribe(function (res) {
                            trans_toast_u_msg_1 = res;
                        });
                        _this.toastmessage = trans_toast_u_msg_1;
                        _this.toastViewer(_this.toastmessage);
                        _this.dis = false;
                        _this.addE = true;
                        _this.addU = false;
                        _this.getAddresses();
                    }
                    else {
                        var trans_toast_update_fail_1 = '';
                        _this.translate.get('Update failed try again.').subscribe(function (res) {
                            trans_toast_update_fail_1 = res;
                        });
                        _this.toastmessage = trans_toast_update_fail_1;
                        _this.toastViewer(_this.toastmessage);
                    }
                });
            }
            else {
                _this.toastmessage = 'Geocode was not successful for the following reason: ' + status;
                _this.toastViewer(_this.toastmessage);
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], AddressPage.prototype, "mapElement", void 0);
    AddressPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-address',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/address/address.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'My Addresses\' | translate}}</ion-title>\n    <a (click)="home()" class="home">\n      <ion-icon name="home"></ion-icon>\n    </a>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-segment [(ngModel)]="myaddress" (ionChange)="onSegmentChanged($event)">\n    <ion-segment-button value="addresslist">\n      {{\'Address list\' | translate}}\n    </ion-segment-button>\n    <ion-segment-button value="address">\n      {{\'Add address\' | translate}}\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]="myaddress" padding>\n    <ion-list *ngSwitchCase="\'addresslist\'">\n      <div *ngIf="_ispaid;else normal_user">\n        <span *ngFor="let item of addresses;let i = index">\n          <ion-card>\n            <ion-grid>\n              <ion-row>\n                <ion-col col-9>\n                  <ion-item id="contactList-list-item">\n                    <ion-input disabled="{{!isDisabled[i]}}" [(ngModel)]="item.street_address"></ion-input>\n                  </ion-item>\n                  <ion-item id="contactList-list-item">\n                    <ion-input disabled="{{!isDisabled[i]}}" [(ngModel)]="item.city"></ion-input>\n                  </ion-item>\n                  <ion-item id="contactList-list-item">\n                    <ion-input disabled="{{!isDisabled[i]}}" [(ngModel)]="item.country"></ion-input>\n                  </ion-item>\n                  <ion-item id="contactList-list-item">\n                    <ion-input disabled="{{!isDisabled[i]}}" [(ngModel)]="item.zipcode"></ion-input>\n                  </ion-item>\n                </ion-col>\n                <ion-col col-3>\n                  <div clear item-end>\n                    <button *ngIf="i == 0" ion-fab type="button" class="sign_btn" mini>\n                      <ion-icon name="ios-home" color="secondary"></ion-icon>\n                    </button>\n                    <button *ngIf="i !== 0 && addUpdate" ion-fab type="button" class="sign_btn" (click)="setPrimaryAddress(item,i)" [class.hidden]="!addUpdate[i]"\n                      mini>\n                      <ion-icon name="ios-home" [color]="secondary"></ion-icon>\n                    </button>\n                    <button *ngIf="addEdit" ion-fab type="button" class="sign_btn" (click)="editAddress(i)" [class.hidden]="addEdit[i]" mini>\n                      <ion-icon name="create"></ion-icon>\n                    </button>\n                    <button *ngIf="addUpdate" ion-fab type="button" class="sign_btn" (click)="updateOldAddress(item,i)" [class.hidden]="!addUpdate[i]"\n                      mini>\n                      <ion-icon name="checkmark"></ion-icon>\n                    </button>\n                    <button *ngIf="i !== 0" ion-fab type="button" class="sign_btn" (click)="deleteAddress(item,i)" [class.hidden]="(item.primary_address == \'yes\') ? true : false"\n                      mini>\n                      <ion-icon name="close"></ion-icon>\n                    </button>\n                  </div>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-card>\n        </span>\n      </div>\n      <ng-template #normal_user>\n        <ion-card>\n          <ion-grid>\n            <ion-row>\n              <ion-col col-9>\n                <ion-item id="contactList-list-item">\n                  <ion-input disabled="{{dis}}" [(ngModel)]="normalUser.street_address"></ion-input>\n                </ion-item>\n                <ion-item id="contactList-list-item">\n                  <ion-input disabled="{{dis}}" [(ngModel)]="normalUser.city"></ion-input>\n                </ion-item>\n                <ion-item id="contactList-list-item">\n                  <ion-input disabled="{{dis}}" [(ngModel)]="normalUser.country"></ion-input>\n                </ion-item>\n                <ion-item id="contactList-list-item">\n                  <ion-input disabled="{{dis}}" [(ngModel)]="normalUser.zipcode"></ion-input>\n                </ion-item>\n              </ion-col>\n              <ion-col col-3>\n                <div clear item-end>\n                  <button *ngIf="addE" ion-fab type="button" class="sign_btn" (click)="editNAddress()" mini>\n                    <ion-icon name="create"></ion-icon>\n                  </button>\n                  <button *ngIf="addU" ion-fab type="button" class="sign_btn" (click)="updateNAddress(normalUser)" mini>\n                    <ion-icon name="checkmark"></ion-icon>\n                  </button>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-card>\n      </ng-template>\n    </ion-list>\n    <ion-list *ngSwitchCase="\'address\'" class="mapAddAddress-cls">\n      <div *ngIf="_ispaid;else no_add">\n        <ion-item>\n          <div class="search-add">\n            <ion-searchbar class="s-box" id="pac-input" [(ngModel)]="autocomplete.query" [showCancelButton]="false" (ionInput)="updateSearch()"\n              (ionCancel)="dismiss()" placeholder="Enter your address here..."></ion-searchbar>\n            <ion-list id="sug">\n              <ion-item *ngFor="let item of autocompleteItems" (click)="chooseItem(item)" class="search-item-cls">\n                {{ item.description }}\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div #map id="map"></div>\n          <!-- <div id="marker"></div> -->\n        </ion-item>\n        <ion-item>\n          <button class="sign_btn" ion-button round full [disabled]="addDisable" (click)="addNewAddress();">{{\'Add\' | translate}}</button>\n        </ion-item>\n      </div>\n      <ng-template #no_add>\n        <ion-card id="n-card">\n          <h2 class="card-pad">{{\'Become a paid user to add multiple address\' | translate}}</h2>\n        </ion-card>\n      </ng-template>\n    </ion-list>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/address/address.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */]])
    ], AddressPage);
    return AddressPage;
}());

//# sourceMappingURL=address.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyaccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_crop__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__popover_popover__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Myaccount page (This page load the user profile details and map)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 













var MyaccountPage = /** @class */ (function () {
    function MyaccountPage(transfer, camera, crop, renderer, navCtrl, menu, AuthServiceProvider, viewCtrl, navParams, geolocation, loadingCtrl, toastCtrl, alertCtrl, filePath, platform, file, translate, transferObject, actionSheetCtrl, popoverCtrl) {
        var _this = this;
        this.transfer = transfer;
        this.camera = camera;
        this.crop = crop;
        this.renderer = renderer;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.AuthServiceProvider = AuthServiceProvider;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.filePath = filePath;
        this.platform = platform;
        this.file = file;
        this.translate = translate;
        this.transferObject = transferObject;
        this.actionSheetCtrl = actionSheetCtrl;
        this.popoverCtrl = popoverCtrl;
        this.lastImage = null;
        this.fileopen = false;
        this._ispaid = false;
        this.updateAddress = true;
        this.addAddress = false;
        this._addCtrl = false;
        this._editCtrl = false;
        this.viewCard = true;
        this.viewMap = false;
        this.editDisabled = true;
        this.newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "camtoken": "" };
        this.mapcondition = false;
        this.imgoption = false;
        this.locationdata = { "hus": "", "street": "", "city": "" };
        this.marker_data = { "latitude": "", "longitude": "" };
        this.form1 = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            username: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
            mail: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].email]),
            password: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
            user_id: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */](''),
            latitude: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */](''),
            longitude: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('')
        });
        this.get_user();
        this.ionViewDidLoad();
        var backAction = platform.registerBackButtonAction(function () {
            console.log("second");
            _this.navCtrl.pop();
            backAction();
        }, 2);
        var dist = JSON.parse(localStorage.getItem('userData'));
        this.RvalueData = dist.r_distance;
        this.dnd = dist.do_not_distrub;
        this.recrange = this.RvalueData;
        // if(this.RvalueData <= 999) {
        this.Runit = ' M';
        // } else {
        // console.log('yes');
        // this.Runit = 'Kms ';
        //alert(this.RvalueData);
        // this.RvalueData = this.RvalueData/1000;
        // }
    }
    Object.defineProperty(MyaccountPage.prototype, "username", {
        get: function () {
            return this.form1.get('username');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyaccountPage.prototype, "mail", {
        get: function () {
            return this.form1.get('mail');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyaccountPage.prototype, "password", {
        get: function () {
            return this.form1.get('password');
        },
        enumerable: true,
        configurable: true
    });
    MyaccountPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        // this.navCtrl.setRoot(MapPage,{lat:lat,lng:lng});
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__map_map__["a" /* MapPage */]);
    };
    MyaccountPage.prototype.get_position = function () {
        var _this = this;
        this.options = {
            enableHighAccuracy: true
        };
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            _this.currentPos = pos;
            console.log(pos);
            console.log(pos.coords.latitude + ',' + pos.coords.longitude);
            //this.addMap(pos.coords.latitude,pos.coords.longitude);
            _this.addMap(pos.coords.latitude, pos.coords.longitude);
        }, function (err) {
            console.log("error : " + err.message);
        });
    };
    MyaccountPage.prototype.get_user = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem('userData'));
        console.log(userdata['user_id']);
        var user = {
            'user_id': userdata.user_id,
            'address_id': userdata.address_id
        };
        this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            _this.marker_data.latitude = _this.responseData[0]['latitude'];
            _this.marker_data.longitude = _this.responseData[0]['longitude'];
            localStorage.setItem('userData', JSON.stringify(user_detail[0]));
            console.log(user_detail[0].paid_member);
            if (user_detail[0].paid_member == 1) {
                _this.AuthServiceProvider.postData(user, 'getPaidUserAddress').then(function (result) {
                    _this.addresses = result;
                    _this._ispaid = true;
                    console.log('user saved addresses ' + _this.addresses);
                }, function (err) {
                    console.log(err);
                });
            }
            console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
            _this.userdetail = user_detail;
            _this.isDisable = true;
            _this.editProfile = true;
            _this.submitProfile = false;
            _this.cancelProfile = false;
            _this.isDisabled = true;
            // this.addMap(user_detail[0].latitude, user_detail[0].longitude);
        });
    };
    /*##get login user details end##*/
    MyaccountPage.prototype.profileactive = function () {
        this.isDisable = false;
        this.editProfile = false;
        this.submitProfile = true;
        this.cancelProfile = true;
        this.mapcondition = true;
        this.addaddress = true;
        this._editCtrl = false;
        //zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
        // this.map.set('draggable', true);
        // this.map.set('scrollwheel', true);
        // this.map.set('zoomControl', true);
        // this.map.set('disableDoubleClickZoom', true);
    };
    MyaccountPage.prototype.profilecancel = function () {
        this.isDisable = true;
        this.editProfile = true;
        this.submitProfile = false;
        this.cancelProfile = false;
        this.mapcondition = false;
        this.serachopen = false;
        this.addAddress = false;
        this.updateAddress = true;
        this._editCtrl = false;
        this.addaddress = false;
        var user = JSON.parse(localStorage.getItem('userData'));
        if (user.paid_member == 1) {
            this._ispaid = true;
        }
        else {
            this._ispaid = false;
        }
        // console.log("lat lon:",parseFloat(this.responseData[0]['latitude']),parseFloat(this.responseData[0]['longitude']))
        this.get_user();
        //this.addMap(parseFloat(this.responseData[0]['latitude']),parseFloat(this.responseData[0]['longitude']));
        this.map.set('draggable', false);
        this.map.set('scrollwheel', false);
        this.map.set('zoomControl', false);
        this.map.set('disableDoubleClickZoom', false);
        var userData = JSON.parse(localStorage.getItem('userData'));
        this.userdetail[0] = userData;
    };
    MyaccountPage.prototype.profileUpdate = function () {
        var _this = this;
        var suc = '';
        var lod = '';
        this.translate.get('Profile updated successfully').subscribe(function (res) {
            suc = res;
        });
        this.translate.get('Profile updating...').subscribe(function (res) {
            lod = res;
        });
        var toast = this.toastCtrl.create({
            message: suc,
            duration: 3000,
            position: 'bottom'
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: lod,
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        console.log(this.userdetail[0]);
        if (this.userdetail[0].user_login && this.userdetail[0].user_pass && this.userdetail[0].user_email) {
            this.AuthServiceProvider.postData(this.userdetail[0], 'updateProfile').then(function (result) {
                // loading.present();
                _this.responseData = result;
                var user_detail = _this.responseData;
                if (true == _this.responseData.status) {
                    // console.log(user_detail[0].longitude+','+user_detail[0].longitude);
                    _this.userdetail[0] = user_detail;
                    _this.isDisable = true;
                    _this.editProfile = true;
                    _this.submitProfile = false;
                    _this.cancelProfile = false;
                    toast.present();
                    _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                }
                else {
                }
                // this.addMap(user_detail[0].longitude,user_detail[0].longitude);
            });
        }
        else {
            var trans_toast_msg_1 = "";
            this.translate.get('Please fillup the detail to sign up').subscribe(function (res) {
                trans_toast_msg_1 = res;
            });
            this.toastmessage = trans_toast_msg_1;
            this.toastViewer(this.toastmessage);
        }
    };
    MyaccountPage.prototype.toastViewer = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    MyaccountPage.prototype.addMoreAddress = function () {
        var _this = this;
        var position = JSON.parse(localStorage.getItem('accLatLng'));
        var trans_load_msg = '';
        this.translate.get('Profile updating...').subscribe(function (res) {
            trans_load_msg = res;
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: trans_load_msg
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        console.log(this.userdetail[0]);
        this.newAddAry.user_id = this.userdetail[0].user_id;
        this.newAddAry.latitude = position.lat;
        this.newAddAry.longitude = position.lng;
        this.AuthServiceProvider.postData(this.newAddAry, 'addMoreAddress').then(function (result) {
            // loading.present();
            _this.responseData = result;
            var user_detail = _this.responseData;
            if (true == _this.responseData.status) {
                // console.log(user_detail[0].longitude+','+user_detail[0].longitude);
                localStorage.removeItem('accLatLng');
                _this.addAddress = false;
                _this.isDisable = true;
                _this.editProfile = true;
                _this.submitProfile = false;
                _this.cancelProfile = false;
                _this._addCtrl = false;
                _this._ispaid = true;
                _this.updateAddress = true;
                _this.addAddress = false;
                var trans_toast_msg_2 = "";
                _this.translate.get('Address added successfully').subscribe(function (res) {
                    trans_toast_msg_2 = res;
                });
                _this.toastmessage = trans_toast_msg_2;
                _this.toastViewer(_this.toastmessage);
                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            }
            else {
                _this._addCtrl = false;
                _this._ispaid = true;
                _this.updateAddress = true;
                _this.addAddress = false;
            }
            // this.addMap(user_detail[0].longitude,user_detail[0].longitude);
        });
    };
    MyaccountPage.prototype.updateEditAddress = function () {
        var _this = this;
        var trans_load_msg = "";
        this.translate.get('Profile updating...').subscribe(function (res) {
            trans_load_msg = res;
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: trans_load_msg
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        console.log(this.userdetail[0]);
        var updateAddress = JSON.parse(localStorage.getItem('accLatLng'));
        //alert(updateAddress);
        this.userdetail[0].latitude = updateAddress.lat;
        this.userdetail[0].longitude = updateAddress.lng;
        this.AuthServiceProvider.postData(this.userdetail[0], 'updateEditAddress').then(function (result) {
            // loading.present();
            _this.responseData = result;
            var user_detail = _this.responseData;
            if (true == _this.responseData.status) {
                // console.log(user_detail[0].longitude+','+user_detail[0].longitude);
                _this.addAddress = false;
                _this.isDisable = true;
                _this.editProfile = true;
                _this._editCtrl = false;
                _this._addCtrl = false;
                _this._ispaid = true;
                _this.updateAddress = true;
                var trans_toast_msg_3 = "";
                _this.translate.get('Address added successfully').subscribe(function (res) {
                    trans_toast_msg_3 = res;
                });
                _this.toastmessage = trans_toast_msg_3;
                _this.toastViewer(_this.toastmessage);
                _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
            }
            else {
                _this._addCtrl = false;
                _this._ispaid = true;
                _this.updateAddress = true;
                _this.addAddress = false;
            }
            // this.addMap(user_detail[0].longitude,user_detail[0].longitude);
        });
    };
    MyaccountPage.prototype.ngOnInit = function () {
        this.acService = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    };
    MyaccountPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MyaccountPage.prototype.clearMarkers = function () {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers.length = 0;
    };
    MyaccountPage.prototype.chooseItem = function (item) {
        var _this = this;
        console.log('modal > chooseItem > item > ', item);
        //this.viewCtrl.dismiss(item);
        //console.log(item);
        this.geocoder = new google.maps.Geocoder;
        this.markers = [];
        this.clearMarkers();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
        this.geocoder.geocode({ 'placeId': item.place_id }, function (results, status) {
            console.log(results);
            // if (status === 'OK' && results[0]) {
            var position = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
            localStorage.setItem('accLatLng', JSON.stringify(position));
            console.log(position);
            if (results[0] != null) {
                // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
                //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
                var data = new Array;
                var rsltAdrComponent = results[0].address_components;
                var rslength = rsltAdrComponent.length;
                for (var i = 0; i < rslength; i++) {
                    var obj = rsltAdrComponent[i];
                    data.push(obj.long_name);
                }
                //console.log(locdata);
                if (_this.addAddress == true) {
                    _this.newAddAry.latitude = position.lat;
                    _this.newAddAry.longitude = position.lng;
                    console.log('new address');
                    if (data.length == 5) {
                        _this.newAddAry.street_address = data[1] + " " + data[0];
                        _this.newAddAry.country = data[3];
                        _this.newAddAry.city = data[2];
                        _this.newAddAry.zipcode = data[4];
                    }
                    else if (data.length == 6) {
                        _this.newAddAry.street_address = data[1] + " " + data[0];
                        _this.newAddAry.country = data[4];
                        _this.newAddAry.city = data[2];
                        _this.newAddAry.zipcode = data[5];
                    }
                    else if (data.length == 7) {
                        _this.newAddAry.street_address = data[1] + " " + data[0];
                        _this.newAddAry.country = data[5];
                        _this.newAddAry.city = data[2];
                        _this.newAddAry.zipcode = data[6];
                    }
                    else if (data.length == 8) {
                        _this.newAddAry.street_address = data[1] + " " + data[0];
                        _this.newAddAry.country = data[6];
                        _this.newAddAry.city = data[4];
                        _this.newAddAry.zipcode = data[7];
                    }
                    else if (data.length == 9) {
                        _this.newAddAry.street_address = data[1] + " " + data[0];
                        _this.newAddAry.country = data[7];
                        _this.newAddAry.city = data[4];
                        _this.newAddAry.zipcode = data[8];
                    }
                    else if (data.length == 10) {
                        _this.newAddAry.street_address = data[1] + " " + data[0];
                        _this.newAddAry.country = data[8];
                        _this.newAddAry.city = data[5];
                        _this.newAddAry.zipcode = data[9];
                    }
                    console.log('new addres:' + JSON.stringify(_this.newAddAry));
                }
                else {
                    _this.userdetail[0].latitude = position.lat;
                    _this.userdetail[0].longitude = position.lng;
                    if (data.length == 5) {
                        _this.userdetail[0].street_address = data[1] + " " + data[0];
                        _this.userdetail[0].country = data[3];
                        _this.userdetail[0].city = data[2];
                        _this.userdetail[0].zipcode = data[4];
                    }
                    else if (data.length == 6) {
                        _this.userdetail[0].street_address = data[1] + " " + data[0];
                        _this.userdetail[0].country = data[4];
                        _this.userdetail[0].city = data[2];
                        _this.userdetail[0].zipcode = data[5];
                    }
                    else if (data.length == 7) {
                        _this.userdetail[0].street_address = data[1] + " " + data[0];
                        _this.userdetail[0].country = data[5];
                        _this.userdetail[0].city = data[2];
                        _this.userdetail[0].zipcode = data[6];
                    }
                    else if (data.length == 8) {
                        _this.userdetail[0].street_address = data[1] + " " + data[0];
                        _this.userdetail[0].country = data[6];
                        _this.userdetail[0].city = data[4];
                        _this.userdetail[0].zipcode = data[7];
                    }
                    else if (data.length == 9) {
                        _this.userdetail[0].street_address = data[1] + " " + data[0];
                        _this.userdetail[0].country = data[7];
                        _this.userdetail[0].city = data[4];
                        _this.userdetail[0].zipcode = data[8];
                    }
                    else if (data.length == 10) {
                        _this.userdetail[0].street_address = data[1] + " " + data[0];
                        _this.userdetail[0].country = data[8];
                        _this.userdetail[0].city = data[5];
                        _this.userdetail[0].zipcode = data[9];
                    }
                }
                console.log(_this.userdetail);
            }
            else {
                //alert("No address available");
                console.log("No address available");
            }
            var nabo_img = 'http://rayi.in/naboapi/mapicon/new-markers/blue.png';
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: results[0].geometry.location,
                icon: nabo_img,
            });
            // let marker = new google.maps.Marker({
            //   position: results[0].geometry.location,
            //   map: this.map,
            // });
            _this.markers.push(marker);
            _this.map.setCenter(position);
            // }
        });
    };
    MyaccountPage.prototype.getNewAddressByName = function () {
        var address = this.newAddAry.street_address + ", " + this.newAddAry.city + ", " + this.newAddAry.country + " ";
        console.log(address);
        this.geocoder = new google.maps.Geocoder;
        this.geocoder.geocode({ 'address': address }, function (results, status) {
            if (status === 'OK') {
                console.log(results);
                //alert(address);
                var position = {
                    lat: results[0].geometry.location.lat(),
                    lng: results[0].geometry.location.lng()
                };
                console.log(position);
                localStorage.setItem('accLatLng', JSON.stringify(position));
            }
            else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
        return true;
    };
    MyaccountPage.prototype.updateOldAddress = function () {
        var address = this.userdetail[0].street_address + ", " + this.userdetail[0].city + ", " + this.userdetail[0].country + " ";
        console.log(address);
        this.geocoder = new google.maps.Geocoder;
        this.geocoder.geocode({ 'address': address }, function (results, status) {
            if (status === 'OK') {
                console.log(results);
                //alert(address);
                var position = {
                    lat: results[0].geometry.location.lat(),
                    lng: results[0].geometry.location.lng()
                };
                console.log(position);
                localStorage.setItem('accLatLng', JSON.stringify(position));
            }
            else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
        return true;
    };
    MyaccountPage.prototype.validateAddressForm = function () {
        if (!(this.newAddAry.street_address) && !(this.newAddAry.city) && !(this.newAddAry.country) && !(this.newAddAry.zipcode)) {
            this.toastmessage = 'Please fill all the field.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.newAddAry.street_address)) {
            this.toastmessage = 'Please provide street name.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.newAddAry.city)) {
            this.toastmessage = 'Please provide city name.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.newAddAry.country)) {
            this.toastmessage = 'Please provide country name.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.newAddAry.zipcode)) {
            this.toastmessage = 'Please provide postal code.';
            this.toastViewer(this.toastmessage);
        }
        else if ((this.newAddAry.street_address) && (this.newAddAry.city) && (this.newAddAry.country) && (this.newAddAry.zipcode)) {
            this.defaultAddConfirm();
        }
    };
    MyaccountPage.prototype.validateEditAddressForm = function () {
        if (!(this.userdetail[0].street_address) && !(this.userdetail[0].city) && !(this.userdetail[0].country) && !(this.userdetail[0].zipcode)) {
            this.toastmessage = 'Please fill all the field.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.userdetail[0].street_address)) {
            this.toastmessage = 'Please provide street name.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.userdetail[0].city)) {
            this.toastmessage = 'Please provide city name.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.userdetail[0].country)) {
            this.toastmessage = 'Please provide country name.';
            this.toastViewer(this.toastmessage);
        }
        else if (!(this.userdetail[0].zipcode)) {
            this.toastmessage = 'Please provide postal code.';
            this.toastViewer(this.toastmessage);
        }
        else if ((this.userdetail[0].street_address) && (this.userdetail[0].city) && (this.userdetail[0].country) && (this.userdetail[0].zipcode)) {
            this.editAddConfirm();
        }
    };
    MyaccountPage.prototype.updateSearch = function () {
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var self = this;
        var config = {
            types: ['geocode'],
            input: this.autocomplete.query,
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            self.autocompleteItems = [];
            console.log(predictions);
            predictions.forEach(function (prediction) {
                self.autocompleteItems.push(prediction);
            });
        });
    };
    /*##Map function Start##*/
    // getUserPosition(){
    //   this.options = {
    //        enableHighAccuracy : true
    //   };
    //    this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {
    //        this.currentPos = pos;      
    //        console.log(pos.coords.latitude+','+pos.coords.longitude);
    //        this.addMap(pos.coords.latitude,pos.coords.longitude);
    //    },(err : PositionError)=>{
    //       console.log("error : " + err.message);
    //    });
    // }
    MyaccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapPage');
        // this.getUserPosition();
    };
    MyaccountPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    MyaccountPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    MyaccountPage.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(_this.map, marker);
        });
    };
    MyaccountPage.prototype.addMap = function (lat, long) {
        var latLng = new google.maps.LatLng(lat, long);
        var mapOptions = {
            center: latLng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            fullscreenControl: false,
            streetViewControl: false,
            mapTypeControl: false,
            clickableIcons: false,
            draggable: false,
            zoomControl: false
            //draggable: this.mapcondition, zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
        };
        //  var searchBox = new google.maps.places.SearchBox(this.search);
        //  this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(this.search);
        //  google.maps.event.addListener(searchBox, 'places_changed', function() {
        //    searchBox.set('map', null);
        //    var places = searchBox.getPlaces();
        //    var bounds = new google.maps.LatLngBounds();
        //    var i, place;
        //    for (i = 0; place = places[i]; i++) {
        //      (function(place) {
        //        var marker = new google.maps.Marker({
        //          position: place.geometry.location
        //        });
        //        marker.bindTo('map', searchBox, 'map');
        //        google.maps.event.addListener(marker, 'map_changed', function() {
        //          if (!this.getMap()) {
        //            this.unbindAll();
        //          }
        //        });
        //        bounds.extend(place.geometry.location);
        //      }(place));
        //    }
        //    this.map.fitBounds(bounds);
        //    searchBox.set('map', this.map);
        //    this.map.setZoom(Math.min(this.map.getZoom(),12));
        //  });
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.addMarker();
        this.updateSearch();
    };
    MyaccountPage.prototype.addMarker = function () {
        var nabo_img = 'http://rayi.in/naboapi/mapicon/new-markers/blue.png';
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(parseFloat(this.marker_data.latitude), parseFloat(this.marker_data.longitude)),
            icon: nabo_img,
        });
        var trans_msg = '';
        this.translate.get('This is your position').subscribe(function (res) {
            trans_msg = res;
        });
        var content = "<p>" + trans_msg + "</p>";
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        this.lastLatLng(marker);
        google.maps.event.addListener(marker, 'click', function (event) {
            //infoWindow.open(this.map, marker);
            alert(JSON.stringify(event));
        });
    };
    MyaccountPage.prototype.addressDisplayToast = function (address) {
        var toast = this.toastCtrl.create({
            message: address,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    MyaccountPage.prototype.lastLatLng = function (marker) {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(this.map, 'dragend', function () {
            //  this.LastLat1= marker.position.lat();
            //  this.LastLng1= marker.position.lng();
            _this.LastLat1 = _this.map.getCenter().lat();
            _this.LastLng1 = _this.map.getCenter().lng();
            var coord = new Array();
            coord.push(_this.LastLat1, _this.LastLng1);
            console.log(coord[0] + ',' + coord[1]);
            var latlng = new google.maps.LatLng(_this.LastLat1, _this.LastLng1);
            var request = { latLng: latlng };
            console.log(_this.LastLat1 + ',' + _this.LastLng1);
            geocoder.geocode(request, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var result = results[0];
                    var rsltAdrComponent = result.address_components;
                    //console.log(rsltAdrComponent);
                    console.log(result.formatted_address);
                    _this.addressDisplayToast(result.formatted_address);
                    var resultLength = rsltAdrComponent.length;
                    if (result != null) {
                        // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
                        //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
                        var data = new Array;
                        var locationdata = new Array();
                        for (var i = 0; i < rsltAdrComponent.length; i++) {
                            var obj = rsltAdrComponent[i];
                            data.push(obj.long_name);
                        }
                        //console.log(locdata);
                        if (_this.addAddress == true) {
                            console.log('new address');
                            if (data.length == 5) {
                                _this.newAddAry.street_address = data[1] + " " + data[0];
                                _this.newAddAry.country = data[3];
                                _this.newAddAry.city = data[2];
                                _this.newAddAry.zipcode = data[4];
                                _this.newAddAry.latitude = coord[0];
                                _this.newAddAry.longitude = coord[1];
                            }
                            else if (data.length == 6) {
                                _this.newAddAry.street_address = data[1] + " " + data[0];
                                _this.newAddAry.country = data[4];
                                _this.newAddAry.city = data[2];
                                _this.newAddAry.zipcode = data[5];
                                _this.newAddAry.latitude = coord[0];
                                _this.newAddAry.longitude = coord[1];
                            }
                            else if (data.length == 7) {
                                _this.newAddAry.street_address = data[1] + " " + data[0];
                                _this.newAddAry.country = data[5];
                                _this.newAddAry.city = data[2];
                                _this.newAddAry.zipcode = data[6];
                                _this.newAddAry.latitude = coord[0];
                                _this.newAddAry.longitude = coord[1];
                            }
                            else if (data.length == 8) {
                                _this.newAddAry.street_address = data[1] + " " + data[0];
                                _this.newAddAry.country = data[6];
                                _this.newAddAry.city = data[4];
                                _this.newAddAry.zipcode = data[7];
                                _this.newAddAry.latitude = coord[0];
                                _this.newAddAry.longitude = coord[1];
                            }
                            else if (data.length == 9) {
                                _this.newAddAry.street_address = data[1] + " " + data[0];
                                _this.newAddAry.country = data[7];
                                _this.newAddAry.city = data[4];
                                _this.newAddAry.zipcode = data[8];
                                _this.newAddAry.latitude = coord[0];
                                _this.newAddAry.longitude = coord[1];
                            }
                            else if (data.length == 10) {
                                _this.newAddAry.street_address = data[1] + " " + data[0];
                                _this.newAddAry.country = data[8];
                                _this.newAddAry.city = data[5];
                                _this.newAddAry.zipcode = data[9];
                                _this.newAddAry.latitude = coord[0];
                                _this.userdetail[0].longitude = coord[1];
                            }
                        }
                        else {
                            if (data.length == 5) {
                                _this.userdetail[0].street_address = data[1] + " " + data[0];
                                _this.userdetail[0].country = data[3];
                                _this.userdetail[0].city = data[2];
                                _this.userdetail[0].zipcode = data[4];
                                _this.userdetail[0].latitude = coord[0];
                                _this.userdetail[0].longitude = coord[1];
                            }
                            else if (data.length == 6) {
                                _this.userdetail[0].street_address = data[1] + " " + data[0];
                                _this.userdetail[0].country = data[4];
                                _this.userdetail[0].city = data[2];
                                _this.userdetail[0].zipcode = data[5];
                                _this.userdetail[0].latitude = coord[0];
                                _this.userdetail[0].longitude = coord[1];
                            }
                            else if (data.length == 7) {
                                _this.userdetail[0].street_address = data[1] + " " + data[0];
                                _this.userdetail[0].country = data[5];
                                _this.userdetail[0].city = data[2];
                                _this.userdetail[0].zipcode = data[6];
                                _this.userdetail[0].latitude = coord[0];
                                _this.userdetail[0].longitude = coord[1];
                            }
                            else if (data.length == 8) {
                                _this.userdetail[0].street_address = data[1] + " " + data[0];
                                _this.userdetail[0].country = data[6];
                                _this.userdetail[0].city = data[4];
                                _this.userdetail[0].zipcode = data[7];
                                _this.userdetail[0].latitude = coord[0];
                                _this.userdetail[0].longitude = coord[1];
                            }
                            else if (data.length == 9) {
                                _this.userdetail[0].street_address = data[1] + " " + data[0];
                                _this.userdetail[0].country = data[7];
                                _this.userdetail[0].city = data[4];
                                _this.userdetail[0].zipcode = data[8];
                                _this.userdetail[0].latitude = coord[0];
                                _this.userdetail[0].longitude = coord[1];
                            }
                            else if (data.length == 10) {
                                _this.userdetail[0].street_address = data[1] + " " + data[0];
                                _this.userdetail[0].country = data[8];
                                _this.userdetail[0].city = data[5];
                                _this.userdetail[0].zipcode = data[9];
                                _this.userdetail[0].latitude = coord[0];
                                _this.userdetail[0].longitude = coord[1];
                            }
                        }
                        console.log(_this.userdetail);
                    }
                    else {
                        var trans_no_address_1 = '';
                        _this.translate.get('No address available').subscribe(function (res) {
                            trans_no_address_1 = res;
                        });
                        alert(trans_no_address_1);
                    }
                }
            });
        });
    };
    // google.maps.event.addDomListener(window, 'load', init);
    /*##Map function end##*/
    /*##User image upload function Start##*/
    MyaccountPage.prototype.ImagepresentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    // getImage() {
    //   const options: CameraOptions = {
    //     quality: 100,
    //     destinationType: this.camera.DestinationType.FILE_URI,
    //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    //   }
    //   this.camera.getPicture(options).then((imageData) => {
    //     this.imageURI = imageData;
    //   }, (err) => {
    //     console.log(err);
    //     //this.presentToast(err);
    //   });
    // }
    // uploadFile() {
    //   let loader = this.loadingCtrl.create({
    //     content: "Uploading..."
    //   });
    //   loader.present();
    //   const fileTransfer: FileTransferObject = this.transfer.create();
    //   let options: FileUploadOptions = {
    //     fileKey: 'ionicfile',
    //     fileName: 'ionicfile',
    //     chunkedMode: false,
    //     mimeType: "image/jpeg",
    //     headers: {}
    //   }
    //   fileTransfer.upload(this.imageURI, 'http://rayi.in/naboApi/profileImageupload', options)
    //     .then((data) => {
    //     console.log(data+" Uploaded Successfully");
    //     this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
    //     loader.dismiss();
    //     this.ImagepresentToast("Image uploaded successfully");
    //   }, (err) => {
    //     console.log(err);
    //     loader.dismiss();
    //     this.ImagepresentToast(err);
    //   });
    // }
    MyaccountPage.prototype.chooseimg = function () {
        var _this = this;
        this.translate.get('Load from Library').subscribe(function (res) {
            _this.content = res;
        });
        var action_cancel = '';
        this.translate.get('cancel').subscribe(function (res) {
            action_cancel = res;
        });
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: this.content,
                    handler: function () {
                        _this.platform.ready().then(function () {
                            _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                        });
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.platform.ready().then(function () {
                            _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                        });
                    }
                },
                {
                    text: action_cancel,
                    role: 'cancel',
                    handler: function () {
                        _this.fileopen = false;
                    }
                }
            ]
        });
        actionSheet.present();
    };
    MyaccountPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.crop.crop(imagePath, { quality: 75 }).then(function (newImage) {
                    console.log('new image path is: ' + newImage);
                    _this.filePath.resolveNativePath(newImage)
                        .then(function (filePath) {
                        var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        var currentName = newImage.substring(newImage.lastIndexOf('/') + 1, newImage.lastIndexOf('?'));
                        _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                    });
                }, function (error) {
                    console.error('Error cropping image', error);
                });
            }
            else {
                _this.crop.crop(imagePath, { quality: 75 }).then(function (newImage) {
                    _this.filePath.resolveNativePath(newImage).then(function (filePath) {
                        var currentName = filePath.substr(filePath.lastIndexOf('/') + 1);
                        var correctPath = newImage.substr(0, newImage.lastIndexOf('/') + 1);
                        _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                    });
                }, function (error) {
                    console.error('Error cropping image', error);
                });
            }
        }, function (err) {
            var trans_img_err = "";
            _this.translate.get('Error while selecting image.').subscribe(function (res) {
                trans_img_err = res;
            });
            _this.ImagepresentToast(trans_img_err);
        });
    };
    // Create a new name for the image
    MyaccountPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    MyaccountPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
        }, function (error) {
            var trans_img_err_msg = '';
            _this.translate.get('Error while storing file.').subscribe(function (res) {
                trans_img_err_msg = res;
            });
            _this.ImagepresentToast(trans_img_err_msg);
        });
    };
    MyaccountPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    MyaccountPage.prototype.pathForImage = function (img) {
        if (img === null) {
            //return 'assets/imgs/user.jpg';
            var imgData = JSON.parse(localStorage.getItem('userData'));
            var image = "http://rayi.in/naboapi/uploads/" + imgData['photo'];
            return image;
        }
        else {
            this.fileopen = true;
            return cordova.file.dataDirectory + img;
        }
    };
    MyaccountPage.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        var url = "http://rayi.in/naboapi/profileImageupload";
        // File for Upload
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            _this.loading.dismissAll();
            console.log(data);
            var result = data.response;
            if (result == 'true') {
                var userData = {};
                userData = JSON.parse(localStorage.getItem('userData'));
                userData['photo'] = _this.lastImage;
                localStorage.setItem('userData', JSON.stringify(userData));
                _this.AuthServiceProvider.postData(userData, 'updateUserImagename').then(function (result) {
                    // loading.present();
                    _this.responseData = result;
                    var user_detail = _this.responseData;
                    if (true == _this.responseData.status) {
                        _this.fileopen = false;
                        _this.ImagepresentToast('Image succesful uploaded.');
                        _this.refresh();
                    }
                }, function (err) {
                    _this.fileopen = false;
                    var trans_img_up_err = '';
                    _this.translate.get('Image upload failed.').subscribe(function (res) {
                        trans_img_up_err = res;
                    });
                    _this.ImagepresentToast(trans_img_up_err);
                });
                // this.ImagepresentToast('Image succesful uploaded.');
                // this.fileopen = false;
            }
        }, function (err) {
            _this.loading.dismissAll();
            _this.fileopen = false;
            var trans_img_up_err_while = '';
            _this.translate.get('Error while uploading file.').subscribe(function (res) {
                trans_img_up_err_while = res;
            });
            _this.ImagepresentToast('trans_img_up_err_while');
        });
    };
    MyaccountPage.prototype.getSelectAddress = function () {
        var _this = this;
        console.log("selected address id", this._addressid);
        this.addupdate = this.addresses.filter(function (myOdbj) { return myOdbj.address_id == _this._addressid; });
        console.log(this.addupdate);
        this.userdetail[0].address_id = this.addupdate[0].address_id;
        this.userdetail[0].street_address = this.addupdate[0].street_address;
        this.userdetail[0].city = this.addupdate[0].city;
        this.userdetail[0].country = this.addupdate[0].country;
        this.userdetail[0].zipcode = this.addupdate[0].zipcode;
        this.userdetail[0].camtoken = this.addupdate[0].camtoken;
    };
    MyaccountPage.prototype.addNewAddress = function () {
        this.updateAddress = false;
        this.addAddress = true;
        this._addCtrl = true;
        this._ispaid = false;
        this.addaddress = false;
        this.serachopen = true;
        // let alert = this.alertCtrl.create({
        //   title:'Address',
        //   message: 'Address search from map?',
        //   buttons: [
        //     {
        //       text: 'Yes',
        //       role: 'cancel',
        //       handler: () => {
        //         this.viewCard=false;
        //         this.viewMap=true;
        //       }
        //     },
        //     {
        //       text: 'No',
        //       handler: () => {
        //         console.log('No clicked');
        //       }
        //     }
        //   ]
        // });
        // alert.present();
    };
    MyaccountPage.prototype.editAddress = function () {
        this.addaddress = false;
        this.updateAddress = true;
        this.addAddress = false;
        this._addCtrl = false;
        this._editCtrl = true;
        this._ispaid = false;
        this.editDisabled = false;
        this.serachopen = true;
    };
    MyaccountPage.prototype.addCancel = function () {
        this._addCtrl = false;
        this._ispaid = true;
        this.updateAddress = true;
        this.addAddress = false;
        this.serachopen = false;
    };
    MyaccountPage.prototype.editCancel = function () {
        this.viewCard = true;
        this.viewMap = false;
        this._editCtrl = false;
        this.editDisabled = true;
        this.serachopen = false;
    };
    MyaccountPage.prototype.addClear = function () {
        this.newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "camtoken": "" };
    };
    MyaccountPage.prototype.addAdd = function () {
        this._addCtrl = false;
        this._ispaid = true;
        this.updateAddress = true;
        this.addAddress = false;
    };
    MyaccountPage.prototype.returnPage = function () {
        this.viewCard = true;
        this.viewMap = false;
    };
    /*##User image upload function end##*/
    MyaccountPage.prototype.refresh = function () {
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
        console.log('page refreshed');
    };
    MyaccountPage.prototype.presentPopover = function (myEvent) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_12__popover_popover__["a" /* PopoverPage */], this.addresses);
        popover.present({
            ev: myEvent
            //data:this.addresses;
        });
        popover.onDidDismiss(function (data) {
            console.log(data);
            if (data != null) {
                _this.selectedData = data;
            }
            _this.userdetail[0].address_id = _this.selectedData.address_id;
            _this.userdetail[0].street_address = _this.selectedData.street_address;
            _this.userdetail[0].city = _this.selectedData.city;
            _this.userdetail[0].country = _this.selectedData.country;
            _this.userdetail[0].zipcode = _this.selectedData.zipcode;
            _this.userdetail[0].camtoken = _this.selectedData.camtoken;
            _this.marker_data.latitude = _this.selectedData.latitude;
            _this.marker_data.longitude = _this.selectedData.longitude;
            _this.addMap(_this.marker_data.latitude, _this.marker_data.longitude);
        });
    };
    //confirm alert for new address set as primary address
    MyaccountPage.prototype.defaultAddConfirm = function () {
        var _this = this;
        var trans_alert_title = '';
        var trans_alert_msg = '';
        var trans_alert_btn_no = '';
        var trans_alert_btn_yes = '';
        this.translate.get('Confirm this address').subscribe(function (res) {
            trans_alert_title = res;
        });
        this.translate.get('Do you want to set this as primary address?').subscribe(function (res) {
            trans_alert_msg = res;
        });
        this.translate.get('No').subscribe(function (res) {
            trans_alert_btn_no = res;
        });
        this.translate.get('Yes').subscribe(function (res) {
            trans_alert_btn_yes = res;
        });
        var alert = this.alertCtrl.create({
            title: trans_alert_title,
            message: trans_alert_msg,
            buttons: [
                {
                    text: trans_alert_btn_no,
                    handler: function () {
                        console.log('Cancel clicked');
                        var state = _this.getNewAddressByName();
                        if (state)
                            _this.newAddAry.primary_address = "no";
                        _this.addMoreAddress();
                    }
                },
                {
                    text: trans_alert_btn_yes,
                    handler: function () {
                        console.log('yes clicked');
                        var state = _this.getNewAddressByName();
                        if (state)
                            _this.newAddAry.primary_address = "yes";
                        _this.addMoreAddress();
                    }
                }
            ]
        });
        alert.present();
    };
    MyaccountPage.prototype.editAddConfirm = function () {
        var _this = this;
        var trans_alert_title = '';
        var trans_alert_msg = '';
        var trans_alert_btn_no = '';
        var trans_alert_btn_yes = '';
        this.translate.get('Confirm this address').subscribe(function (res) {
            trans_alert_title = res;
        });
        this.translate.get('Do you want to set this as primary address?').subscribe(function (res) {
            trans_alert_msg = res;
        });
        this.translate.get('No').subscribe(function (res) {
            trans_alert_btn_no = res;
        });
        this.translate.get('Yes').subscribe(function (res) {
            trans_alert_btn_yes = res;
        });
        var alert = this.alertCtrl.create({
            title: trans_alert_title,
            message: trans_alert_msg,
            buttons: [
                {
                    text: trans_alert_btn_no,
                    handler: function () {
                        console.log('Cancel clicked');
                        var state = _this.updateOldAddress();
                        if (state)
                            _this.userdetail[0].primary_address = "no";
                        _this.updateEditAddress();
                    }
                },
                {
                    text: trans_alert_btn_yes,
                    handler: function () {
                        console.log('yes clicked');
                        var state = _this.updateOldAddress();
                        if (state)
                            _this.userdetail[0].primary_address = "yes";
                        _this.updateEditAddress();
                    }
                }
            ]
        });
        alert.present();
    };
    //confirm alert for registered address set as primary address
    MyaccountPage.prototype.defaultUpdateConfirm = function () {
        var _this = this;
        var trans_alert_title = '';
        var trans_alert_msg = '';
        var trans_alert_btn_no = '';
        var trans_alert_btn_yes = '';
        this.translate.get('Confirm this address').subscribe(function (res) {
            trans_alert_title = res;
        });
        this.translate.get('Do you want to set this as primary address?').subscribe(function (res) {
            trans_alert_msg = res;
        });
        this.translate.get('No').subscribe(function (res) {
            trans_alert_btn_no = res;
        });
        this.translate.get('Yes').subscribe(function (res) {
            trans_alert_btn_yes = res;
        });
        var alert = this.alertCtrl.create({
            title: trans_alert_title,
            message: trans_alert_msg,
            buttons: [
                {
                    text: trans_alert_btn_no,
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: trans_alert_btn_yes,
                    handler: function () {
                        console.log('yes clicked');
                        _this.setPrimaryAddress();
                    }
                }
            ]
        });
        alert.present();
    };
    //api call to set address primary
    MyaccountPage.prototype.setPrimaryAddress = function () {
        var _this = this;
        var trans_load_cont = '';
        this.translate.get('Profile updating...').subscribe(function (res) {
            trans_load_cont = res;
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: trans_load_cont
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        console.log(this.userdetail[0]);
        this.AuthServiceProvider.postData(this.userdetail[0], 'updatePrimaryAddress').then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            if (true == _this.responseData.status) {
                var trans_toast_update_1 = '';
                _this.translate.get('updated as primary address.').subscribe(function (res) {
                    trans_toast_update_1 = res;
                });
                _this.toastmessage = trans_toast_update_1;
                _this.toastViewer(_this.toastmessage);
            }
            else {
            }
        });
    };
    MyaccountPage.prototype.recrangeUser = function ($event) {
        var _this = this;
        console.log($event._value);
        this.recrange = $event._value;
        var user = JSON.parse(localStorage.getItem('userData'));
        var rngObj = {
            "user_id": user.user_id,
            "r_distance": this.recrange
        };
        this.AuthServiceProvider.postData(rngObj, 'updateRDistance').then(function (result) {
            console.log(result);
            _this.responseData = result;
            if (_this.responseData.status == true) {
                _this.RvalueData = _this.recrange;
                // if(this.RvalueData <= 999) {
                _this.Runit = ' M';
                // } else {
                // console.log('yes');
                // this.Runit = 'Kms ';
                //alert(this.RvalueData);
                // this.RvalueData = this.RvalueData/1000;
                // }
            }
        }, function (err) {
            console.log(err);
        });
    };
    MyaccountPage.prototype.toggle = function ($event) {
        var _this = this;
        console.log($event);
        this.dnd = $event;
        var user = JSON.parse(localStorage.getItem('userData'));
        var dndObj = {
            "user_id": user.user_id,
            "notify": this.dnd
        };
        this.AuthServiceProvider.postData(dndObj, 'dnd').then(function (result) {
            _this.responseData = result;
            if (true == _this.responseData.status) {
                console.log('Mode changed.');
                var userDND = JSON.parse(localStorage.getItem('userData'));
                var dnd_chg = void 0;
                if (_this.dnd == true) {
                    dnd_chg = 1;
                }
                else {
                    dnd_chg = 0;
                }
                userDND.do_not_distrub = dnd_chg;
                localStorage.setItem('userData', JSON.stringify(userDND));
            }
            else {
                console.log('Failed.');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MyaccountPage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])('username'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MyaccountPage.prototype, "uelementRef", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])('email'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MyaccountPage.prototype, "eelementRef", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])('pass'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], MyaccountPage.prototype, "pelementRef", void 0);
    MyaccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-myaccount',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/myaccount/myaccount.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'My Account\' | translate}}</ion-title>\n    <a (click)="home()" class="home">\n      <ion-icon name="home"></ion-icon>\n    </a>\n  </ion-navbar>\n\n</ion-header>\n<ion-content class="signin" padding center text-center>\n  <ion-list *ngIf="userdetail" class="custome-width-do-not-distrub-user">\n    <form [formGroup]="form1" (ngSubmit)="profileUpdate()">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-10>\n              <ion-item>\n                <ion-label class="no-pad">\n                  <ion-icon name="ios-contact"></ion-icon>\n                </ion-label>\n                <ion-input class="no-left-pad" formControlName="username" disabled=true type="text" [(ngModel)]="userdetail[0].user_login" #username ></ion-input>\n                <hr class="style14">\n                <ion-input hidden formControlName="user_id" type="text" [(ngModel)]="userdetail[0].user_id"></ion-input>\n                <ion-input hidden formControlName="latitude" type="text" [(ngModel)]="userdetail[0].latitude"></ion-input>\n                <ion-input hidden formControlName="longitude" type="text" [(ngModel)]="userdetail[0].longitude"></ion-input>\n              </ion-item>\n              <ion-item>\n                <ion-label class="no-pad">\n                  <ion-icon name="mail"></ion-icon>\n                </ion-label>\n                <ion-input class="no-left-pad" formControlName="mail" type="email" [email]="true" disabled="{{isDisable}}" type="text" [(ngModel)]="userdetail[0].user_email" #email></ion-input>\n              </ion-item>\n              <div *ngIf="mail.touched && mail.invalid" class="alertstyle">\n                <div *ngIf="mail.errors.email">{{ \'Invalid Email format\' | translate }}</div>\n              </div>\n              <ion-item>\n                <ion-label class="no-pad">\n                  <ion-icon name="lock"></ion-icon>\n                </ion-label>\n                <ion-input class="no-left-pad" formControlName="password" disabled="{{isDisable}}" type="password" [(ngModel)]="userdetail[0].user_pass" #pass></ion-input>\n              </ion-item>\n          </ion-col>\n          <ion-col col-2>\n              <div clear item-end>\n                  <button *ngIf="editProfile"  ion-fab class="signbtn" (click)="profileactive();$event.preventDefault();" mini><ion-icon name="create"></ion-icon></button>\n                  <button *ngIf="cancelProfile" ion-fab class="signbtn" (click)="profilecancel();$event.preventDefault();" mini><ion-icon name="close"></ion-icon></button>\n                  <button *ngIf="submitProfile" [disabled]="!form1.valid" type="submit" ion-fab class="signbtn" mini><ion-icon name="checkmark"></ion-icon></button>\n              </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </form>\n  </ion-list>\n  <ion-list class="custome-width-do-not-distrub-setting">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-12>\n            <div class="setting">\n                <ion-label class="head-lbl notification-label"><h5 class="text-center">{{ \'Notification\' | translate }}</h5></ion-label> \n                <ion-label class="f-lft">{{ \'Receiving range upto\' | translate }} <ion-badge class="f-rt custome-width-do-not-distrub-range" item-left item-end><span>{{RvalueData}}</span>{{Runit}}</ion-badge></ion-label>\n                <ion-item>\n                    <ion-range min="50"  max="5000" step="50" (ionChange)="recrangeUser($event)" [(ngModel)]="recrange" color="secondary">\n                        <ion-label range-left>50 M</ion-label>\n                        <ion-label range-right>5 Km</ion-label>\n                    </ion-range>\n                </ion-item>\n                <ion-item class="no-border">\n                    {{ \'Do not disturb\' | translate }}\n                    <!-- <ion-toggle [(ngModel)]="dnd" (ionChange)="toggle($event);" toggle-class="toggle-calm">Airplane Mode</ion-toggle> -->\n                </ion-item>\n                <ion-list radio-group [(ngModel)]="dnd" (ionChange)="toggle($event);" class="custome-width-do-not-distrub">\n                  <ion-item class="pad-lft-25">\n                    <ion-label>{{\'Off\' | translate}}</ion-label>\n                    <ion-radio class="dnd-off" value="0"></ion-radio>\n                  </ion-item>\n                  <ion-item class="pad-lft-25">\n                    <ion-label>{{\'On\' | translate}}</ion-label>\n                    <ion-radio class="dnd-on" value="1" ></ion-radio>\n                  </ion-item>\n                </ion-list>\n            </div>\n            <!-- <div #map id="map"></div> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/myaccount/myaccount.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_crop__["a" /* Crop */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["_1" /* Renderer2 */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["b" /* FileTransferObject */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* PopoverController */]])
    ], MyaccountPage);
    return MyaccountPage;
}());

//# sourceMappingURL=myaccount.js.map

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 183;

/***/ }),

/***/ 230:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/aboutus/aboutus.module": [
		231
	],
	"../pages/acceptsupport/acceptsupport.module": [
		268
	],
	"../pages/address/address.module": [
		412
	],
	"../pages/contact/contact.module": [
		407
	],
	"../pages/contactus/contactus.module": [
		269
	],
	"../pages/forgotpassword/forgotpassword.module": [
		272
	],
	"../pages/help/help.module": [
		273
	],
	"../pages/invitefriend/invitefriend.module": [
		274
	],
	"../pages/map/map.module": [
		419
	],
	"../pages/modal/modal.module": [
		275
	],
	"../pages/myaccount/myaccount.module": [
		413
	],
	"../pages/newaddress/newaddress.module": [
		408
	],
	"../pages/notification/notification.module": [
		276
	],
	"../pages/offer/offer.module": [
		400
	],
	"../pages/popover/popover.module": [
		401
	],
	"../pages/resetpassword/resetpassword.module": [
		402
	],
	"../pages/security/security.module": [
		404
	],
	"../pages/setting/setting.module": [
		409
	],
	"../pages/shop/shop.module": [
		405
	],
	"../pages/signin/signin.module": [
		411
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 230;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutusPageModule", function() { return AboutusPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__aboutus__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AboutusPageModule = /** @class */ (function () {
    function AboutusPageModule() {
    }
    AboutusPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__aboutus__["a" /* AboutusPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__aboutus__["a" /* AboutusPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], AboutusPageModule);
    return AboutusPageModule;
}());

//# sourceMappingURL=aboutus.module.js.map

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modal_modal__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_google_service_google_maps__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_location_accuracy__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__signin_signin__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var _Marker;
(function (_Marker) {
    _Marker["RED"] = "new-markers/red.svg";
    _Marker["YELLOW"] = "new-markers/yellow.svg";
    _Marker["YELLOWALERT"] = "new-markers/yellow-alert.svg";
    _Marker["GRAY"] = "new-markers/gray.svg";
    _Marker["YELLOWSIR"] = "new-markers/yellow-siren.svg";
    _Marker["BLUE"] = "new-markers/blue.svg";
})(_Marker || (_Marker = {}));
var _AlertState;
(function (_AlertState) {
    _AlertState["N"] = "N";
    _AlertState["B"] = "B";
    _AlertState["S"] = "S";
    _AlertState["BP"] = "BP";
    _AlertState["BC"] = "BC";
    _AlertState["SS"] = "SS";
    _AlertState["SP"] = "SP";
    _AlertState["SC"] = "SC";
    _AlertState["GB"] = "GB";
    _AlertState["GS"] = "GS";
    _AlertState["NS"] = "NS";
    _AlertState["P"] = "P"; //threat was informed to police
})(_AlertState || (_AlertState = {}));
var MapPage = /** @class */ (function () {
    function MapPage(navCtrl, platform, AuthServiceProvider, maps, http, events, loadingCtrl, alertCtrl, toastCtrl, modalCtrl, translate, navParams, geolocation, locationaccuracy, menu, zone, ref) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.AuthServiceProvider = AuthServiceProvider;
        this.maps = maps;
        this.http = http;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.translate = translate;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.locationaccuracy = locationaccuracy;
        this.menu = menu;
        this.zone = zone;
        this.ref = ref;
        this.regionals = [];
        this.addressData = [];
        this.AGMmap = { lat: null, lng: null, zoom: null };
        this.alertdata = {};
        this.data = {};
        this.ddata = {};
        this.gddata = {};
        this.deviceListObj = {};
        this.push = {};
        this.refVar = 'no';
        this.guest = false;
        this.trackState = false;
        this.gmarkers = [];
        this.base_url = 'http://54.93.200.112/naboapi/';
        this.tranText = {
            "Cancelled": "Cancelled",
            "Process": "Your Process cancelled.",
            "sent": "SENT",
            "sentmsg": "your message sent successfully.",
            "canalert": "Cancel Alert",
            "canalertconfirm": "Do you want to Cancel Alert ?",
            "conaction": "Confirm Action",
            "sendalertconfirm": "Do you want to send Alert ?",
            "yes": "Yes",
            "no": "No",
            "cancel": "cancel",
            "neighboursent": "Message sent to your Neighbours.",
            "failed": "Failed",
            "noneighbour": "You have no neighbours",
            "selectneighbour": "Do you want to Select more neighbours Alert ?",
            "wrong": "Somthing went wrong",
            "noaddress": "Cannot determine address at this location.",
            "policeinfo": "Already Police Informed.",
            "wait": "Please wait.."
        };
        this.locateAlert();
        this.pageValue = navParams.get('page');
    }
    //default ionic lifecycle events
    MapPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    MapPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //JSON concodination function
    MapPage.prototype.jsonConcat = function (o1, o2) {
        for (var key in o2) {
            o1[key] = o2[key];
        }
        return o1;
    };
    //conver to float value
    MapPage.prototype.convertString = function (value) {
        return parseFloat(value);
    };
    //function for get user address ID from app component
    MapPage.prototype.locateAlert = function () {
        this.addressID = this.navParams.get('address_id');
        console.log('location of alert ' + this.addressID);
        var add_id = { "address_id": this.addressID };
    };
    MapPage.prototype.toastViewer = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    //get the user details form api for set related UI for user 
    MapPage.prototype.getUser = function () {
        var _this = this;
        var userdata;
        userdata = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
            console.log(result);
            _this.userData = result[0];
            localStorage.setItem('userData', JSON.stringify(_this.userData));
        }, function (err) {
            console.log(err);
        });
        this.maploadview();
    };
    //initiate map with the user's current type of status 
    MapPage.prototype.maploadview = function () {
        var userdata;
        try {
            userdata = JSON.parse(localStorage.getItem('userData'));
        }
        catch (e) {
            console.log(e);
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__signin_signin__["a" /* SigninPage */]);
        }
        if (userdata == null || userdata == '' || userdata == undefined) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__signin_signin__["a" /* SigninPage */]);
        }
        else if (userdata.alert_type == 'SP' || userdata.alert_type == 'BP' || userdata.alert_type == _AlertState.N || userdata.alert_type == _AlertState.BC || userdata.alert_type == _AlertState.SC) {
            console.log("normal");
            this.timeLeft = -1;
            clearInterval(this.interval);
            this.mapview = true;
            this.owner = false;
            this.alertmsg = false;
            this.alertmsgsus = false;
            this.guestalertmsg = false;
            this.alertbtn = _AlertState.N;
            this.ownerbtn = "";
            this.resbtn = "";
        }
        else if (userdata.alert_type == 'B') {
            console.log("Bug");
            this.SrangeAvaUser = localStorage.getItem('user_count');
            this.countdown();
            localStorage.removeItem('sendtype');
            this.mapview = true;
            this.owner = true;
            this.ownerbtn = "";
            this.alertmsg = false;
            this.alertmsgsus = false;
            this.guestalertmsg = false;
            this.alertbtn = "";
            this.resbtn = "";
            this.data.msg = '';
        }
        else if (userdata.alert_type == 'S') {
            console.log("sup");
            this.SrangeAvaUser = localStorage.getItem('user_count');
            this.countdown();
            localStorage.removeItem('sendtype');
            this.mapview = true;
            this.owner = true;
            this.ownerbtn = "";
            this.alertmsg = false;
            this.alertmsgsus = false;
            this.guestalertmsg = false;
            this.alertbtn = "";
            this.resbtn = "";
            this.data.msg = '';
        }
        else if (userdata.status_type == 'P') {
            localStorage.setItem('cancel', 'yes');
        }
        // this.updateUserData();
        // this.updateUserLocation();
    };
    //function for Spot new threat in the map
    MapPage.prototype.locateNewThreat = function () {
        var _this = this;
        this.user_id = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(this.user_id, 'newThreat').then(function (result) {
            console.log(result);
            _this.new_thread = result;
        }, function (err) {
            console.log(err);
        });
    };
    //Alert for notifying user the process was cancelled
    MapPage.prototype.progressCancelAlert = function () {
        var _this = this;
        this.translate.get(this.tranText.Cancelled).subscribe(function (res) {
            _this.title = res;
        });
        this.translate.get(this.tranText.Process).subscribe(function (res) {
            _this.subtitle = res;
        });
        var alert = this.alertCtrl.create({
            title: this.title,
            subTitle: this.subtitle,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        console.log('Yes clicked');
                        localStorage.setItem('cancel', 'no');
                    }
                }
            ]
        });
        alert.present();
    };
    //owner cancel the alert process
    MapPage.prototype.ownercancel = function () {
        var _this = this;
        localStorage.removeItem('onetimepush');
        this.removeMarkers();
        this.infoMsg = false;
        this.ownerbtn = '';
        this.ownerpoli = '';
        var canloader = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        canloader.present();
        this.userData = JSON.parse(localStorage.getItem('alertdata'));
        this.AuthServiceProvider.postData(this.userData, 'cancelpush').then(function (result) {
            _this.responseData = result;
            console.log(_this.responseData);
            if (true == _this.responseData.status) {
                canloader.dismiss();
                console.log('canceled');
                _this.refresh();
                //this.progressCancelAlert();
            }
        }, function (err) {
            console.log(err);
            canloader.dismiss();
        });
    };
    //alert for notifying the user alert was sent
    MapPage.prototype.pushSendAlert = function () {
        var _this = this;
        this.translate.get(this.tranText.sent).subscribe(function (res) {
            _this.title = res;
        });
        this.translate.get(this.tranText.sentmsg).subscribe(function (res) {
            _this.subtitle = res;
        });
        var alert = this.alertCtrl.create({
            title: this.title,
            subTitle: this.subtitle,
            buttons: ['Ok']
        });
        alert.present();
    };
    //alert for notifying the user(owner) alert was sent
    MapPage.prototype.pushSendAlertOwner = function (status, msg) {
        var alert = this.alertCtrl.create({
            title: status,
            subTitle: msg,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        console.log('Yes clicked');
                        //this.refresh();
                    }
                }
            ],
            enableBackdropDismiss: false
        });
        alert.present();
    };
    //open the modal after the alert sent shows thanks message
    MapPage.prototype.pushSendOpenModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_0__modal_modal__["a" /* ModalPage */]);
        modal.present();
    };
    //confirmation for the user  who cancel alert 
    MapPage.prototype.presentAlert = function () {
        var _this = this;
        this.translate.get(this.tranText.canalert).subscribe(function (res) {
            _this.title = res;
        });
        this.translate.get(this.tranText.canalertconfirm).subscribe(function (res) {
            _this.subtitle = res;
        });
        this.translate.get(this.tranText.yes).subscribe(function (res) {
            _this.yes = res;
        });
        this.translate.get(this.tranText.no).subscribe(function (res) {
            _this.no = res;
        });
        var alert = this.alertCtrl.create({
            title: this.title,
            message: this.subtitle,
            buttons: [
                {
                    text: this.yes,
                    role: 'cancel',
                    handler: function () {
                        console.log('Yes clicked');
                        _this.stopTimer();
                        _this.mapReset();
                    }
                },
                {
                    text: this.no,
                    handler: function () {
                        _this.refresh();
                    }
                }
            ]
        });
        alert.present();
    };
    //confirmation for send the guest alert
    MapPage.prototype.sendAlertConfirm = function () {
        var _this = this;
        this.translate.get(this.tranText.conaction).subscribe(function (res) {
            _this.title = res;
        });
        this.translate.get(this.tranText.sendalertconfirm).subscribe(function (res) {
            _this.subtitle = res;
        });
        this.translate.get(this.tranText.yes).subscribe(function (res) {
            _this.yes = res;
        });
        this.translate.get(this.tranText.no).subscribe(function (res) {
            _this.no = res;
        });
        var alert = this.alertCtrl.create({
            title: this.title,
            message: this.subtitle,
            buttons: [
                {
                    text: this.yes,
                    handler: function () {
                        console.log('Cancel clicked');
                        _this.sendguestpush();
                    }
                },
                {
                    text: this.no,
                    handler: function () {
                        console.log('Buy clicked');
                        _this.guestalertmsg = false;
                        _this.guestalertmsgsus = false;
                        _this.gddata['msg'] = "";
                        _this.refresh();
                    }
                }
            ]
        });
        alert.present();
    };
    //send burglary alert for the non-nabo user
    MapPage.prototype.guestAddB = function () {
        this.guestalertmsg = true;
        this.guestalertmsgsus = false;
        var data = JSON.parse(localStorage.getItem('userData'));
        console.log(data);
        this.locdata['user_id'] = data['user_id'];
        this.locdata['user_type'] = _AlertState.GB;
        this.locdata['alert_type'] = _AlertState.B;
        localStorage.setItem('lastLat', this.locdata.latitude);
        localStorage.setItem('lastLng', this.locdata.longitude);
    };
    //send suspicious alert for the non-nabo user
    MapPage.prototype.guestAddS = function () {
        this.guestalertmsgsus = true;
        this.guestalertmsg = false;
        var data = JSON.parse(localStorage.getItem('userData'));
        console.log(data);
        this.locdata['user_id'] = data['user_id'];
        this.locdata['user_type'] = _AlertState.GS;
        this.locdata['alert_type'] = _AlertState.S;
        localStorage.setItem('lastLat', this.locdata.latitude);
        localStorage.setItem('lastLng', this.locdata.longitude);
    };
    //Reset the map and status of user after the cancel process
    MapPage.prototype.mapReset = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem('userData'));
        userdata.alert_type = _AlertState.N;
        this.AuthServiceProvider.postData(userdata, 'ownerCancel').then(function (result) {
            console.log(result);
            _this.responseData = result;
            if (true == _this.responseData.status) {
                console.log(_this.responseData.status);
                _this.refresh();
                _this.owner = false;
            }
            else {
                console.log("Operation failed");
            }
        }, function (err) {
            console.log(err);
        });
    };
    //Map refresh
    MapPage.prototype.refresh = function () {
        var _this = this;
        console.log('page refreshed');
        var data = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(data, 'getUserAddressByCountry').then(function (result) {
            console.log(result);
            _this.addressData = result;
            var userdata = JSON.parse(localStorage.getItem('userData'));
            _this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
                localStorage.setItem('userData', JSON.stringify(result[0]));
                console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));
                var lat = parseFloat(localStorage.getItem('lastLat'));
                var lng = parseFloat(localStorage.getItem('lastLng'));
                _this.othersAddress = localStorage.getItem('lastadd');
                var latlng = { "lat": lat, "lng": lng };
                _this.map.setCenter({ lat: lat, lng: lng });
                _this.infoMsg = "";
                _this.alertbtn = "N";
                _this.ownerbtn = "";
                _this.resbtn = "";
                _this.policealert = "";
                _this.ownerpoli = "";
                //this.getUseraddress(userdata);
                _this.AGMloadMap();
            });
        });
    };
    //get home location of the user 
    MapPage.prototype.getHome = function () {
        var _this = this;
        this.mapview = true;
        this.owner = false;
        this.alertmsg = false;
        this.alertmsgsus = false;
        this.guestalertmsg = false;
        this.ownerbtn = "";
        this.resbtn = "";
        this.policealert = "";
        this.ownerpoli = "";
        this.infoMsg = false;
        console.log('get home clicked');
        var userPrim = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(userPrim, 'getHome').then(function (result) {
            console.log(result);
            _this.responseData = result;
            var user;
            if (_this.responseData.length > 0) {
                user = _this.responseData[0];
            }
            else {
                user = userPrim;
            }
            var lat = parseFloat(user.latitude);
            var lng = parseFloat(user.longitude);
            _this.map.setCenter({ lat: lat, lng: lng });
            _this.othersAddress = user.street_address + ", " + user.city + ", " + user.country + " ";
        }, function (err) {
        });
    };
    //cancel push send process
    MapPage.prototype.cancelpush = function () {
        this.mapview = true;
        this.presentAlert();
    };
    //Registered user send the alert to all
    MapPage.prototype.sendcurpush = function () {
        var _this = this;
        this.owner = "";
        this.translate.get(this.tranText.wait).subscribe(function (res) {
            _this.title = res;
        });
        var loader = this.loadingCtrl.create({ content: this.title });
        loader.present();
        setTimeout(function () {
            loader.dismiss();
            localStorage.setItem('tog', '');
            _this.userDatachange = JSON.parse(localStorage.getItem("userData"));
            _this.userDatachange['alert_type'] = _AlertState.BP;
            localStorage.setItem("userData", JSON.stringify(_this.userDatachange));
            console.log("page move");
            _this.sendtype = localStorage.getItem('sendtype');
            _this.stopTimer();
            _this.translate.get(_this.tranText.canalert).subscribe(function (res) {
                _this.title = res;
            });
            if (_this.sendtype == 'auto') {
                _this.translate.get(_this.tranText.sent).subscribe(function (res) {
                    _this.subtitle = res;
                });
                _this.translate.get(_this.tranText.neighboursent).subscribe(function (res) {
                    _this.yes = res;
                });
                _this.status = _this.subtitle;
                _this.msg = 'Automatically forwarded the alert notification to your neighbours.';
            }
            else {
                _this.translate.get(_this.tranText.sent).subscribe(function (res) {
                    _this.subtitle = res;
                });
                _this.translate.get(_this.tranText.neighboursent).subscribe(function (res) {
                    _this.yes = res;
                });
                _this.status = _this.subtitle;
                _this.msg = _this.yes;
            }
            _this.pushSendAlertOwner(_this.status, _this.msg);
            _this.sendtype = JSON.parse(localStorage.getItem('onetimepush'));
        }, 2000);
        var user = JSON.parse(localStorage.getItem('userData'));
        var addId = JSON.parse(localStorage.getItem('camdata'));
        if (user.platform == 'ios') {
            var address_id = addId['gcm.notification.address_id'];
            user.address_id = address_id;
        }
        else if (user.platform == 'Android') {
            var address_id = addId['address_id'];
            user.address_id = address_id;
            console.log('address_id' + address_id);
        }
        this.AuthServiceProvider.postData(user, 'camPushall').then(function (result) {
            console.log(result);
            //this.owner = "";
            _this.responseData = result;
            if (true === _this.responseData.status) {
                console.log(_this.responseData);
            }
            else {
                _this.translate.get(_this.tranText.failed).subscribe(function (res) {
                    _this.subtitle = res;
                });
                _this.translate.get(_this.tranText.neighboursent).subscribe(function (res) {
                    _this.yes = res;
                });
                _this.status = _this.subtitle;
                _this.msg = 'Message not sent to your Neighbours. you can resend or cancel the process.';
                _this.pushSendAlertOwner(_this.status, _this.msg);
            }
        }, function (err) {
            loader.dismiss();
            console.log(err);
            _this.owner = "";
            _this.status = _this.subtitle;
            _this.msg = err;
            _this.pushSendAlertOwner(_this.status, _this.msg);
        });
    };
    //add marker in map
    MapPage.prototype.addMarker = function (obj) {
        var _this = this;
        var nabo_img;
        if (obj.alert_type == 'B') {
            nabo_img = this.base_url + 'mapicon/new-markers/red.svg';
        }
        else {
            nabo_img = this.base_url + 'mapicon/new-markers/yellow.svg';
        }
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            //position: new google.maps.LatLng(obj.latitude, obj.longitude),
            position: this.map.getCenter(),
            icon: nabo_img,
        });
        this.gmarkers.push(marker);
        google.maps.event.addListener(marker, 'click', function () {
            _this.othersAddress = obj.street_address + ", " + obj.city + ", " + obj.country;
            _this.infoMsg = obj.msg;
            _this.eventTime = obj.datetime;
            _this.map.setCenter(marker.getPosition());
            localStorage.setItem('lastLat', obj.latitude);
            localStorage.setItem('lastLng', obj.longitude);
            if (obj.alert_type == 'B') {
                _this.ownerbtn = 'owner';
                _this.ownerpoli = '';
            }
            else {
                _this.ownerpoli = 'owner';
                _this.ownerbtn = '';
            }
            _this.alertbtn = '';
        });
    };
    //remove temp marker
    MapPage.prototype.removeMarkers = function () {
        for (var i = 0; i < this.gmarkers.length; i++) {
            this.gmarkers[i].setMap(null);
        }
    };
    //send guest alert to all neighbours
    MapPage.prototype.sendguestpush = function () {
        var _this = this;
        this.guestalertmsg = false;
        this.guestalertmsgsus = false;
        this.userDataUpdate = true;
        this.userLocUpdate = true;
        this.translate.get(this.tranText.wait).subscribe(function (res) {
            _this.title = res;
        });
        var loader = this.loadingCtrl.create({
            content: this.title
        });
        setTimeout(function () {
            loader.dismiss();
        }, 5000);
        this.locdata['msg'] = this.gddata['msg'];
        this.locdata['datetime'] = this.getDateTime();
        this.addMarker(this.locdata);
        this.AuthServiceProvider.postData(this.locdata, 'addGuest').then(function (result) {
            console.log(result);
            localStorage.setItem('lastLat', _this.locdata.latitude);
            localStorage.setItem('lastLng', _this.locdata.longitude);
            _this.responseData = result;
            if (_this.responseData.status == true) {
                _this.alertdata = _this.responseData.data[0];
                if (_this.alertdata.status_type === 'B') {
                    var marker = 'red.svg';
                }
                else {
                    var marker = 'yellow.svg';
                }
                _this.addNaboMarker(marker, _this.alertdata.latitude, _this.alertdata.longitude);
                _this.pushSendOpenModal();
                localStorage.setItem('alertdata', JSON.stringify(_this.alertdata));
                _this.alertdata = JSON.parse(localStorage.getItem('alertdata'));
                console.log(_this.alertdata);
                _this.alertdata['alert_type'] = _AlertState.B;
                _this.alertdata['msg'] = _this.gddata['msg'];
                _this.userAllData = JSON.parse(localStorage.getItem("userData"));
                _this.alertdata['sender_id'] = _this.userAllData['user_id'];
                localStorage.setItem('alertdata', JSON.stringify(_this.alertdata));
            }
            console.log("sendpush");
            var cur_user_data = JSON.parse(localStorage.getItem('userData'));
            _this.userData = JSON.parse(localStorage.getItem('alertdata'));
            _this.userData['user_id'];
            _this.userData['distance'] = cur_user_data['r_distance'];
            _this.userData['msg'] = _this.gddata['msg'];
            _this.userData['time'] = _this.getDateTime();
            console.log(_this.userData);
            _this.guest = false;
            _this.AuthServiceProvider.postData(_this.userData, 'gpushall').then(function (result) {
                _this.responseData = result;
                console.log('gest push success');
                _this.userDatachange = JSON.parse(localStorage.getItem("userData"));
                _this.userDatachange['alert_type'] = _AlertState.N;
                localStorage.setItem("userData", JSON.stringify(_this.userDatachange));
                var userdata = JSON.parse(localStorage.getItem('userData'));
                //this.getUseraddress(userdata);
                _this.infoMsg = _this.userData.msg;
                _this.gddata.msg = "";
                console.log("page move");
            }, function (err) {
                console.log(err);
            });
        });
    };
    //get the nabosupport user's address details from api
    MapPage.prototype.getUseraddress = function (data) {
        var _this = this;
        this.AuthServiceProvider.postData(data, 'getUserAddressByCountry').then(function (result) {
            console.log(result);
            _this.removeMarkers();
            _this.addressData = result;
            var userdata = JSON.parse(localStorage.getItem('userData'));
            _this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
                localStorage.setItem('userData', JSON.stringify(result[0]));
                console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));
                var paid = result[0]['paid_member'];
                _this.events.publish('user:paid', paid);
                _this.AGMloadMap();
            });
        });
        return true;
    };
    //refresh address detail
    MapPage.prototype.refreshAddress = function () {
        var _this = this;
        var data = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(data, 'getUserAddressByCountry').then(function (result) {
            console.log(result);
            _this.addressData = result;
            var userdata = JSON.parse(localStorage.getItem('userData'));
            _this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
                localStorage.setItem('userData', JSON.stringify(result[0]));
                console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));
                var lat = localStorage.getItem('lastLat');
                var lng = localStorage.getItem('lastLng');
                _this.map.setCenter({ lat: parseFloat(lat), lng: parseFloat(lng) });
            });
        });
    };
    //get local date and time
    MapPage.prototype.getDateTime = function () {
        // var today = new Date();
        //var dateTime = today.toLocaleString();
        // var date = today.getDate()+ '-' + (today.getMonth() + 1)+'-' +today.getFullYear();
        // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        // var dateTime = date + ' ' + time;
        // return dateTime;
        var today = new Date();
        //var dateTime = today.toLocaleString();
        var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        var time = new Date();
        //var dateTime = date + ' : ' + time.toLocaleString('en-DN', { hour: 'numeric', minute: 'numeric', hour12: true });
        var dateTime = date + ' ' + time.toLocaleString('en-DN', { hour: 'numeric', minute: 'numeric', hour12: false });
        return dateTime;
    };
    //send alert push notification to nearby users 
    MapPage.prototype.alert = function () {
        var _this = this;
        this.alertmsg = false;
        this.alertmsgsus = false;
        this.guestalertmsg = false;
        var alertmsgdata = this.alertdata;
        alertmsgdata['msg'] = this.ddata['msg'];
        alertmsgdata['datetime'] = this.getDateTime();
        localStorage.setItem('alertdata', JSON.stringify(alertmsgdata));
        console.log("data:" + JSON.stringify(alertmsgdata));
        var pushLoader = this.loadingCtrl.create({
            content: 'Sending alert...'
        });
        pushLoader.present();
        this.AuthServiceProvider.postData(alertmsgdata, 'alertmsg').then(function (result) {
            console.log(result);
            localStorage.setItem('tog', '');
            _this.AuthServiceProvider.postData(alertmsgdata, 'push').then(function (result) {
                _this.responseData = result;
                console.log(_this.responseData);
                if (true === _this.responseData.status) {
                    console.log(_this.responseData);
                    _this.infoMsg = alertmsgdata.msg;
                    var latlang = JSON.parse(localStorage.getItem('alertdata'));
                    var center = {
                        "lat": latlang.latitude,
                        "lng": latlang.longitude
                    };
                    pushLoader.dismiss();
                    _this.refresh();
                    _this.ddata.msg = '';
                    _this.pushSendOpenModal();
                }
            }, function (err) {
                pushLoader.dismiss();
                // Error log
                console.log(err);
            });
        }, function (err) {
            pushLoader.dismiss();
        });
    };
    //burglery alert button click 
    MapPage.prototype.burglery = function () {
        var toggle = localStorage.getItem('tog');
        if (toggle == 'y') {
            this.alertdata.alert_type = _AlertState.B;
            this.userData = JSON.parse(localStorage.getItem("userData"));
            this.alertdata.sender_id = this.userData.user_id;
            this.alertmsg = true;
            this.alertmsgsus = false;
        }
        else if (toggle == '') {
            this.guestAddB();
        }
    };
    //suspicious alert button click
    MapPage.prototype.Suspicious = function () {
        var toggle = localStorage.getItem('tog');
        if (toggle == 'y') {
            this.alertdata.alert_type = _AlertState.S;
            this.userData = JSON.parse(localStorage.getItem("userData"));
            this.alertdata['sender_id'] = this.userData['user_id'];
            //localStorage.setItem('alertdata', JSON.stringify(this.alertdata));
            this.alertmsg = false;
            this.alertmsgsus = true;
        }
        else if (toggle == '') {
            this.guestAddS();
            //this.sendAlertConfirm();
        }
    };
    MapPage.prototype.ionViewDidLoad = function () {
        var session = localStorage.getItem('session');
        if (session === '_logged_in') {
            //alert(this.pageValue);
            if (this.pageValue != 'notify' && this.pageValue != 'appcomp') {
                this.getCurrentPosition();
            }
            var data = JSON.parse(localStorage.getItem('userData'));
            //this.getUseraddress(data); //for develop
            this.enableLocation(); //for real device not support for ios
            this.userLocUpdate = false;
            console.log('ionViewDidLoad MapPage');
            var user = JSON.parse(localStorage.getItem('userData'));
            var userstatus = user.status_type;
            var useralert = user.alert_type;
            if (userstatus == 'B' || userstatus == 'S' || userstatus == 'NS' || userstatus == 'SS' || userstatus == 'P') {
                console.log('process reset initiated');
            }
            this.adminData = JSON.parse(localStorage.getItem('adminData'));
            console.log(this.adminData);
            for (var i = 0; i < this.adminData.data.length; i++) {
                console.log('service:' + this.adminData.data[i].data);
            }
            this.alertTime = this.adminData.data[0].data;
            this.refreshTime = this.adminData.data[1].data;
            this.resetTime = this.adminData.data[5].data;
            this.updateUserData();
            this.updateUserLocation();
        }
        else {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__signin_signin__["a" /* SigninPage */]);
        }
    };
    //Enable the GPS on user device
    MapPage.prototype.enableLocation = function () {
        var _this = this;
        this.locationaccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                // the accuracy option will be ignored by iOS
                _this.locationaccuracy.request(_this.locationaccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function (result) {
                    console.log('Request successful' + JSON.stringify(result));
                    var userData = JSON.parse(localStorage.getItem('userData'));
                    _this.getUseraddress(userData);
                }, function (error) {
                    console.log('Error requesting location permissions' + JSON.stringify(error));
                });
            }
        });
    };
    //get current position latitude and longitude
    MapPage.prototype.getCurrentPosition = function () {
        var _this = this;
        var fetch = '';
        // let locLoader:any;
        // this.translate.get('Fetching location...').subscribe(res=>{
        //   fetch = res;
        // });
        var locLoader = this.loadingCtrl.create({
            spinner: 'bubbles',
            cssClass: "map-loading"
        });
        locLoader.present();
        this.options = {
            timeout: 6000, enableHighAccuracy: true, maximumAge: 3800
        };
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            locLoader.dismiss();
            var latlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
            //this.map.setCenter(latlng);
            console.log('from app comp', pos);
            localStorage.setItem('lastLat', JSON.stringify(pos.coords.latitude));
            localStorage.setItem('lastLng', JSON.stringify(pos.coords.longitude));
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'location': latlng }, function (results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        console.log(results[0].formatted_address);
                        _this.othersAddress = results[0].formatted_address;
                        localStorage.setItem('lastadd', _this.othersAddress);
                        _this.AGMloadMap();
                    }
                    else {
                        _this.othersAddress = 'Cannot determine address at this location.';
                    }
                }
            });
        }, function (err) {
            locLoader.dismiss();
            console.log('Error message : ' + err.message);
            _this.toastmessage = 'Cannot determine address at this location.so map load from your saved location.';
            _this.toastViewer(_this.toastmessage);
            var user = JSON.parse(localStorage.getItem('userData'));
            var lat = parseFloat(user.latitude);
            var lng = parseFloat(user.longitude);
            _this.map.setCenter({ lat: lat, lng: lng });
            _this.othersAddress = user.street_address + ", " + user.city + ", " + user.country + " ";
            _this.AGMloadMap();
        });
        // loading.dismiss();
    };
    //load angular google map
    MapPage.prototype.AGMloadMap = function () {
        var _this = this;
        console.log('mapload');
        this.infoMsg = false;
        this.removeMarkers();
        this.AGMmap.lat = parseFloat(localStorage.getItem('lastLat'));
        this.AGMmap.lng = parseFloat(localStorage.getItem('lastLng'));
        this.AGMmap.zoom = 15;
        var latlng = { lat: this.AGMmap.lat, lng: this.AGMmap.lng };
        this.map.setCenter(latlng);
        console.log('lat', this.AGMmap.lat, 'lng', this.AGMmap.lng);
        if (Number.isNaN(this.AGMmap.lat) && Number.isNaN(this.AGMmap.lng)) {
            console.log('call get position');
            //this.getCurrentPosition();
            this.getHome();
        }
        else {
            console.log(this.AGMmap);
            var geocoder = new google.maps.Geocoder();
            var latlng = { lat: this.AGMmap.lat, lng: this.AGMmap.lng };
            geocoder.geocode({ 'location': latlng }, function (results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        console.log(results[0].formatted_address);
                        _this.othersAddress = results[0].formatted_address;
                    }
                    else {
                        _this.othersAddress = 'Cannot determine address at this location.';
                    }
                    _this.maploadview();
                }
            });
        }
    };
    MapPage.prototype.conLocalTime = function (time) {
        var t = time.split(/[- :]/);
        // var d = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
        return d.toUTCString();
    };
    //get latitude longidude from geocode plugin
    MapPage.prototype.getGeocode = function (lat, lng) {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        var latlng = { lat: lat, lng: lng };
        geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    console.log(results[0]);
                    console.log(results[0].formatted_address);
                    _this.othersAddress = results[0].formatted_address;
                    localStorage.setItem('lastadd', _this.othersAddress);
                    _this.alertbtn = _AlertState.N;
                    _this.policealert = "";
                    _this.resbtn = "";
                    _this.ownerbtn = "";
                    _this.ownerpoli = "";
                    localStorage.setItem('tog', '');
                    console.log(_this.othersAddress);
                    var t0 = performance.now();
                    _this.getGustAddress(results[0], latlng);
                    var t1 = performance.now();
                    console.log("Call to get location detail took " + (t1 - t0) + " milliseconds.");
                    _this.userData = JSON.parse(localStorage.getItem('userData'));
                    _this.locationData = {
                        "user_id": _this.userData.user_id,
                        "distance": _this.userData.distance,
                        "latitude": latlng.lat,
                        "longitude": latlng.lng
                    };
                }
                else {
                    _this.alertbtn = "";
                    _this.policealert = "";
                    _this.resbtn = "";
                    _this.ownerbtn = "";
                    _this.ownerpoli = "";
                    _this.translate.get(_this.tranText.noaddress).subscribe(function (res) {
                        _this.alerttxtmsg = res;
                    });
                    _this.othersAddress = _this.alerttxtmsg;
                    console.log(_this.othersAddress);
                    console.log('Cannot determine address at this location.' + status);
                }
            }
        });
    };
    //Event for handle when map ready
    MapPage.prototype.mapReady = function (map) {
        var _this = this;
        this.map = map;
        this.LastLat1 = this.map.getCenter().lat();
        this.LastLng1 = this.map.getCenter().lng();
        this.getGeocode(this.LastLat1, this.LastLng1);
        //maps dragstart
        google.maps.event.addListener(this.map, 'dragstart', function () {
            _this.infoMsg = null;
            _this.alertmsg = false;
            _this.alertmsgsus = false;
            _this.guestalertmsg = false;
            _this.guestalertmsgsus = false;
            _this.userLocUpdate = true;
        });
        //maps dragend
        google.maps.event.addListener(this.map, 'dragend', function () {
            _this.userLocUpdate = true;
            _this.LastLat1 = _this.map.getCenter().lat();
            _this.LastLng1 = _this.map.getCenter().lng();
            console.log(_this.LastLat1, '', _this.LastLng1);
            _this.getGeocode(_this.LastLat1, _this.LastLng1);
        });
    };
    //check the user response for alert
    MapPage.prototype.checkUserResponse = function (userData) {
        var response;
        this.AuthServiceProvider.postData(userData, 'checkUserResponse').then(function (result) {
            console.log(result);
            response = result;
            console.log(response);
        });
        return response;
    };
    //dynamically change marker when on click and display message 
    MapPage.prototype.markerClick = function (address) {
        var _this = this;
        this.map.setCenter({ lat: this.convertString(address.latitude), lng: this.convertString(address.longitude) });
        localStorage.setItem('lastLat', address.latitude);
        localStorage.setItem('lastLng', address.longitude);
        this.othersAddress = address.street_address + ", " + address.city + ", " + address.country;
        localStorage.setItem('lastadd', this.othersAddress);
        localStorage.setItem('lastAdd', this.othersAddress);
        this.alertdata = address;
        localStorage.setItem('alertdata', JSON.stringify(this.alertdata));
        console.log(this.alertdata);
        var markers = this.addressData;
        for (var i = 0; i < markers.length; i++) {
            switch (markers[i].status_type) {
                case (_AlertState.B):
                    markers[i].marker_icon = _Marker.RED;
                    break;
                case (_AlertState.S):
                    markers[i].marker_icon = _Marker.YELLOW;
                    break;
                case (_AlertState.SS):
                    markers[i].marker_icon = _Marker.YELLOWALERT;
                    break;
                case (_AlertState.NS):
                    markers[i].marker_icon = _Marker.GRAY;
                    break;
                case (_AlertState.P):
                    markers[i].marker_icon = _Marker.YELLOWSIR;
                    break;
                default:
                    markers[i].marker_icon = _Marker.BLUE;
                    break;
            }
        }
        this.marker = address;
        var user_id = this.marker.user_id;
        this.marker.marker_icon = "lg-" + this.marker.marker_icon;
        var owner_id = JSON.parse(localStorage.getItem('userData')).user_id;
        if (user_id == owner_id) {
            console.log('its owner');
            this.ownerbtn = "";
            this.policealert = "";
            this.ownerpoli = "";
            this.resbtn = "";
            var _ownerdata = {
                "user_id": this.marker.user_id,
                "address_id": this.marker.address_id,
                "address_type": this.marker.address_type
            };
            if (this.marker.status_type == _AlertState.SS || this.marker.status_type == _AlertState.S) {
                this.ownerpoli = "owner";
                this.ownerbtn = "";
                this.resbtn = "";
                this.alertbtn = "";
                this.infoMsg = this.marker.message;
                this.eventTime = this.marker.time;
            }
            else if (this.marker.status_type == _AlertState.NS) {
                this.ownerpoli = "";
                this.policealert = "";
                this.ownerbtn = "owner";
                this.resbtn = "";
                this.infoMsg = this.marker.message + '(canceled)';
                this.eventTime = this.marker.time;
            }
            else if (this.marker.status_type == _AlertState.P) {
                this.ownerpoli = "";
                this.policealert = "";
                this.ownerbtn = "owner";
                this.resbtn = "";
                this.alertbtn = "";
                var policetxt_1 = '';
                this.translate.get('Already Police Informed.').subscribe(function (res) {
                    policetxt_1 = res;
                });
                this.infoMsg = this.marker.message + '(' + policetxt_1 + ')';
                this.eventTime = this.marker.time;
            }
            else if (this.marker.status_type == "") {
                var hometxt_1 = '';
                this.translate.get('This is your home').subscribe(function (res) {
                    hometxt_1 = res;
                });
                this.ownerbtn = "";
                this.resbtn = "";
                this.alertbtn = "";
                this.policealert = "";
                this.ownerpoli = "";
                this.infoMsg = hometxt_1;
                this.eventTime = "";
            }
            else if (this.marker.status_type == _AlertState.B) {
                this.infoMsg = this.marker.message;
                this.eventTime = this.marker.time;
                this.resbtn = "";
                this.alertbtn = "";
                this.policealert = "";
                this.ownerpoli = "";
                this.ownerbtn = "owner";
            }
            //else if (this.marker.alert_type == 'BP') {
            //   this.infoMsg = this.marker.message;
            //   this.eventTime = this.marker.time;
            //   this.resbtn = "";
            //   this.alertbtn = "";
            //   this.policealert = "";
            //   this.ownerpoli = "";
            //   this.ownerbtn = "owner";
            // } else if (this.marker.alert_type == 'SP') {
            //   this.infoMsg = this.marker.message;
            //   this.eventTime = this.marker.time;
            //   this.resbtn = "";
            //   this.alertbtn = "";
            //   this.policealert = "";
            //   this.ownerbtn = "";
            //   this.ownerpoli = "owner";
            // }
        }
        else {
            console.log('neighbour');
            console.log(this.marker.status_type, ' ', this.marker.alert_type);
            if (this.marker.status_type == "" || this.marker.status_type == 0) {
                console.log(this.marker.status_type);
                localStorage.setItem('tog', 'y');
                var nabotxt_1 = '';
                this.translate.get('Nabosupporters').subscribe(function (res) {
                    nabotxt_1 = res;
                });
                this.alertbtn = _AlertState.N;
                this.ownerbtn = "";
                this.resbtn = "";
                this.policealert = "";
                this.ownerpoli = "";
                this.infoMsg = nabotxt_1;
                this.eventTime = "";
            }
            else if (this.marker.status_type == 'P') {
                var policetxt_2 = '';
                this.translate.get('Already Police Informed.').subscribe(function (res) {
                    policetxt_2 = res;
                });
                this.infoMsg = this.marker.message + '(' + policetxt_2 + ')';
                this.eventTime = this.marker.time;
                this.policealert = "";
                this.ownerpoli = "";
                this.alertbtn = "";
                this.ownerbtn = "";
                this.resbtn = "";
                this.refVar = null;
            }
            else if (this.marker.status_type == 'B') {
                this.infoMsg = this.marker.message;
                this.eventTime = this.marker.time;
                this.resbtn = "";
                this.alertbtn = "";
                this.ownerbtn = "";
                this.ownerpoli = "";
                this.policealert = "";
            }
            else if (this.marker.status_type == 'S' && this.marker.alert_type == 'S') {
                this.infoMsg = this.marker.message;
                this.eventTime = this.marker.time;
                this.resbtn = "";
                this.alertbtn = "";
                this.ownerbtn = "";
                this.ownerpoli = "";
                this.policealert = "";
            }
            else if (this.marker.status_type == 'S' && this.marker.alert_type == 'SP' || this.marker.status_type == 'SS' && this.marker.alert_type == 'SP' || this.marker.status_type == 'S' && this.marker.alert_type == 'BP' || this.marker.status_type == 'SS' && this.marker.alert_type == 'BP') {
                var response;
                var curdata = JSON.parse(localStorage.getItem('userData'));
                var ruserdata = {};
                ruserdata = {
                    "user_id": curdata.user_id,
                    "sus_user_id": this.marker.user_id,
                    "sus_address_id": this.marker.address_id,
                    "address_type": this.marker.address_type
                };
                this.AuthServiceProvider.postData(ruserdata, 'checkUserResponse').then(function (result) {
                    console.log(result);
                    response = result;
                    console.log(response);
                    if (response.status == true) {
                        console.log(response.status);
                        _this.infoMsg = _this.marker.message;
                        _this.eventTime = _this.marker.time;
                        _this.resbtn = "";
                        _this.alertbtn = "";
                        _this.ownerbtn = "";
                        _this.ownerpoli = "";
                        _this.policealert = "SS";
                        localStorage.removeItem('victimData');
                    }
                    else {
                        _this.infoMsg = _this.marker.message;
                        _this.eventTime = _this.marker.time;
                        _this.resbtn = _AlertState.S;
                        _this.alertbtn = "";
                        _this.ownerbtn = "";
                        _this.ownerpoli = "";
                        _this.policealert = "";
                    }
                });
            }
            else if (this.marker.status_type == 'SS') {
                this.policealert = this.marker.status_type;
                this.alertbtn = "";
                this.ownerbtn = "";
                this.resbtn = "";
                this.ownerpoli = "";
                this.infoMsg = this.marker.message;
                this.eventTime = this.marker.time;
            }
            else if (this.marker.status_type == 'NS') {
                var can_1 = '';
                this.translate.get('Cancelled').subscribe(function (res) {
                    can_1 = res;
                });
                this.infoMsg = this.marker.message + ' (' + can_1 + ')';
                this.eventTime = this.marker.time;
                this.policealert = "";
                this.ownerpoli = "";
                this.alertbtn = "";
                this.ownerbtn = "";
                this.resbtn = "";
                this.refVar = null;
            }
            else if (this.marker.status_type == 'S' && this.marker.alert_type == 'N' && this.marker.address_type == 'GS') {
                var response;
                var curdata = JSON.parse(localStorage.getItem('userData'));
                var ruserdata = {};
                ruserdata = {
                    "user_id": curdata.user_id,
                    "sus_user_id": this.marker.user_id,
                    "sus_address_id": this.marker.address_id,
                    "address_type": this.marker.address_type
                };
                this.AuthServiceProvider.postData(ruserdata, 'checkUserResponse').then(function (result) {
                    console.log(result);
                    response = result;
                    console.log(response);
                    if (response.status == true) {
                        console.log(response.status);
                        _this.infoMsg = _this.marker.message;
                        _this.eventTime = _this.marker.time;
                        _this.resbtn = "";
                        _this.alertbtn = "";
                        _this.ownerbtn = "";
                        _this.ownerpoli = "";
                        _this.policealert = "SS";
                        localStorage.removeItem('victimData');
                    }
                    else {
                        _this.infoMsg = _this.marker.message;
                        _this.eventTime = _this.marker.time;
                        _this.resbtn = _AlertState.S;
                        _this.alertbtn = "";
                        _this.ownerbtn = "";
                        _this.ownerpoli = "";
                        _this.policealert = "";
                    }
                });
            }
            else if (this.marker.status_type == 'SS' && this.marker.alert_type == 'N' && this.marker.address_type == 'GS') {
                this.policealert = this.marker.status_type;
                this.alertbtn = "";
                this.ownerbtn = "";
                this.resbtn = "";
                this.ownerpoli = "";
                this.infoMsg = this.marker.message;
                this.eventTime = this.marker.time;
            }
            else if (this.marker.status_type == 'NS' && this.marker.alert_type == 'N' && this.marker.address_type == 'GS') {
                var can_2 = '';
                this.translate.get('Cancelled').subscribe(function (res) {
                    can_2 = res;
                });
                this.infoMsg = this.marker.message + ' (' + can_2 + ')';
                this.eventTime = this.marker.time;
                this.policealert = "";
                this.ownerpoli = "";
                this.alertbtn = "";
                this.ownerbtn = "";
                this.resbtn = "";
                this.refVar = null;
            }
        }
    };
    //set dynamically icon
    MapPage.prototype.getMarkerURL = function (icon) {
        //var url = '/assets/imgs/'+icon;
        var url = this.base_url + 'mapicon/new-markers/' + icon;
        return url;
    };
    //add marker for non-nabo user
    MapPage.prototype.addNaboMarker = function (markerImg, lat, lng) {
        console.log('add marker call');
        var latLang = { lat: lat, lng: lng };
        var cur_img = this.base_url + 'mapicon/new-markers/' + markerImg;
        var marker = new google.maps.Marker({
            map: this.map,
            draggable: false,
            icon: cur_img,
            animation: google.maps.Animation.DROP,
        });
    };
    //get nearby users count when alert to all
    MapPage.prototype.rangeUser = function (event) {
        var _this = this;
        console.log(event);
        var simpleObject = {};
        for (var prop in event) {
            if (!event.hasOwnProperty(prop)) {
                continue;
            }
            if (typeof (event[prop]) == 'object') {
                continue;
            }
            if (typeof (event[prop]) == 'function') {
                continue;
            }
            simpleObject[prop] = event[prop];
        }
        console.log(this.valueData);
        this.userData = JSON.parse(localStorage.getItem("userData"));
        var user_detail = this.userData;
        console.log(user_detail);
        if (event != "start") {
            console.log(simpleObject);
            var value = JSON.stringify(simpleObject);
            this.valueData = JSON.parse(value)._value;
            user_detail.distance = this.valueData;
            localStorage.setItem('userData', JSON.stringify(user_detail));
            if (this.range == 5000) {
                if (this.SrangeAvaUser == 0) {
                    localStorage.setItem('onetimepush', 'true');
                    var cancelData = JSON.parse(localStorage.getItem('userData'));
                    this.AuthServiceProvider.postData(cancelData, 'ownerCancel').then(function (result) {
                        _this.responsedata = result;
                        if (_this.responsedata.status == true) {
                            _this.stopCountdown();
                            _this.getUser();
                        }
                        else {
                        }
                    }, function (err) {
                        console.log(err);
                    });
                    var StopAlert = this.alertCtrl.create({
                        title: 'Process Cancel',
                        message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
                        buttons: [
                            {
                                text: 'Ok',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Cancel clicked');
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    StopAlert.present();
                }
            }
            if (this.valueData <= 999) {
                this.unit = 'Mtrs ';
            }
            else {
                this.unit = 'Kms ';
                this.valueData = this.valueData / 1000;
            }
        }
        else {
            localStorage.setItem('userData', JSON.stringify(user_detail));
            this.valueData = user_detail.distance;
            if (this.valueData == 5000) {
                if (this.SrangeAvaUser == 0) {
                    localStorage.setItem('onetimepush', 'true');
                    console.log('Cancel clicked');
                    var cancelData = JSON.parse(localStorage.getItem('userData'));
                    this.AuthServiceProvider.postData(cancelData, 'ownerCancel').then(function (result) {
                        _this.responsedata = result;
                        if (_this.responsedata.status == true) {
                            _this.stopCountdown();
                            _this.getUser();
                        }
                        else {
                        }
                    }, function (err) {
                        console.log(err);
                    });
                    var StopAlert = this.alertCtrl.create({
                        title: 'Process Cancel',
                        message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
                        buttons: [
                            {
                                text: 'Ok',
                                role: 'cancel',
                                handler: function () {
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    StopAlert.present();
                }
            }
            if (this.valueData <= 999) {
                this.unit = 'Mtrs ';
            }
            else {
                this.unit = 'Kms ';
                this.valueData = this.valueData / 1000;
            }
        }
        this.AuthServiceProvider.postData(user_detail, 'sendallpushNearuser').then(function (result) {
            _this.nearData = result;
            console.log(_this.nearData.length);
            _this.SrangeAvaUser = _this.nearData.length;
            for (var _i = 0, _a = _this.nearData; _i < _a.length; _i++) {
                var regional = _a[_i];
                console.log(regional.latitude + ',' + regional.longitude);
            }
            _this.AuthServiceProvider.postData(user_detail, 'rangevalue').then(function (result) {
                _this.resdata = result;
                console.log(_this.resdata);
                if (true == _this.resdata.status) {
                }
            });
        }, function (err) {
        });
    };
    MapPage.prototype.getNearuser = function () {
        var _this = this;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        var lat = localStorage.getItem('lastLat');
        var lng = localStorage.getItem('lastLng');
        var data = {
            "user_id": this.userData.user_id,
            "latitude": lat,
            "longitude": lng
        };
        this.AuthServiceProvider.postData(data, 'pushNearuser').then(function (result) {
            _this.nearData = result;
            console.log(_this.nearData.length);
            _this.SrangeAvaUser = _this.nearData.count;
        }, function (err) {
        });
    };
    //get nearby nabo user for non-nabo user alert
    MapPage.prototype.grangeUser_old = function (event) {
        var _this = this;
        console.log(event);
        var simpleObject = {};
        for (var prop in event) {
            if (!event.hasOwnProperty(prop)) {
                continue;
            }
            if (typeof (event[prop]) == 'object') {
                continue;
            }
            if (typeof (event[prop]) == 'function') {
                continue;
            }
            simpleObject[prop] = event[prop];
        }
        console.log(this.valueData);
        this.userData = JSON.parse(localStorage.getItem('userData'));
        var user_detail = this.locationData;
        user_detail.distance = this.userData.distance;
        console.log(user_detail);
        if (event != "start") {
            console.log(simpleObject);
            var value = JSON.stringify(simpleObject);
            this.valueData = JSON.parse(value)._value;
            user_detail.distance = this.valueData;
            this.userData = JSON.parse(localStorage.getItem('userData'));
            this.userData.distance = this.valueData;
            localStorage.setItem('userData', JSON.stringify(this.userData));
            if (this.range == 5000) {
                if (this.SrangeAvaUser == 0) {
                    var StopAlert = this.alertCtrl.create({
                        title: 'Process Cancel',
                        message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
                        buttons: [
                            {
                                text: 'OK',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Cancel clicked');
                                    _this.removeGuestUser();
                                    _this.refresh();
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    StopAlert.present();
                }
            }
            if (this.valueData <= 999) {
                this.unit = 'Mtrs ';
            }
            else {
                this.unit = 'Kms ';
                this.valueData = this.valueData / 1000;
            }
        }
        else {
            this.userData = JSON.parse(localStorage.getItem('userData'));
            user_detail.distance = this.userData.distance;
            console.log("user distance" + user_detail.distance);
            this.valueData = user_detail.distance;
            if (this.valueData == 5000) {
                if (this.SrangeAvaUser == 0) {
                    var StopAlert = this.alertCtrl.create({
                        title: 'Process Cancel',
                        message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
                        buttons: [
                            {
                                text: 'Ok',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Cancel clicked');
                                    _this.removeGuestUser();
                                    _this.refresh();
                                }
                            }
                        ],
                        enableBackdropDismiss: false
                    });
                    StopAlert.present();
                }
            }
            if (this.valueData <= 999) {
                this.unit = 'Mtrs ';
            }
            else {
                this.unit = 'Kms ';
                this.valueData = this.valueData / 1000;
            }
        }
        this.AuthServiceProvider.postData(user_detail, 'pushNearuser').then(function (result) {
            _this.nearData = result;
            console.log(_this.nearData.length);
            _this.SrangeAvaUser = _this.nearData.length;
            for (var _i = 0, _a = _this.nearData; _i < _a.length; _i++) {
                var regional = _a[_i];
                console.log(regional.latitude + ',' + regional.longitude);
            }
            _this.AuthServiceProvider.postData(user_detail, 'rangevalue').then(function (result) {
                _this.resdata = result;
                console.log(_this.resdata);
                if (true == _this.resdata.status) {
                }
            });
        }, function (err) {
        });
    };
    //get non-nabouser's location details
    MapPage.prototype.getGustAddress = function (data, coord) {
        var rsltAdrComponent = data.address_components;
        var resultLength = rsltAdrComponent.length;
        if (data != null) {
            var Gdata = new Array;
            var locationdata = new Array();
            for (var i = 0; i < rsltAdrComponent.length; i++) {
                var obj = rsltAdrComponent[i];
                Gdata.push(obj.long_name);
            }
            this.deviceID = localStorage.getItem('deviceID');
            if (Gdata.length == 5) {
                this.locdata = {
                    "street_address": Gdata[1] + " " + Gdata[0],
                    "city": Gdata[2],
                    "country": Gdata[3],
                    "zipcode": Gdata[4],
                    "latitude": coord.lat,
                    "longitude": coord.lng,
                    "is_active": 1,
                    "status_id": 0,
                    "alert_type": _AlertState.N,
                    "distance": 10,
                    "deviceID": this.deviceID
                };
                console.log(this.locdata);
            }
            else if (Gdata.length == 6) {
                this.locdata = {
                    "street_address": Gdata[1] + " " + Gdata[0],
                    "city": Gdata[2],
                    "country": Gdata[4],
                    "zipcode": Gdata[5],
                    "latitude": coord.lat,
                    "longitude": coord.lng,
                    "is_active": 1,
                    "status_id": 0,
                    "alert_type": _AlertState.N,
                    "distance": 10,
                    "deviceID": this.deviceID
                };
                console.log(this.locdata);
            }
            else if (Gdata.length == 7) {
                this.locdata = {
                    "street_address": Gdata[1] + " " + Gdata[0],
                    "city": Gdata[3],
                    "country": Gdata[5],
                    "zipcode": Gdata[6],
                    "latitude": coord.lat,
                    "longitude": coord.lng,
                    "is_active": 1,
                    "status_id": 0,
                    "alert_type": _AlertState.N,
                    "distance": 10,
                    "deviceID": this.deviceID
                };
                console.log(this.locdata);
            }
            else if (Gdata.length == 8) {
                this.locdata = {
                    "street_address": Gdata[1] + " " + Gdata[0],
                    "city": Gdata[4],
                    "country": Gdata[6],
                    "zipcode": Gdata[7],
                    "latitude": coord.lat,
                    "longitude": coord.lng,
                    "is_active": 1,
                    "status_id": 0,
                    "alert_type": _AlertState.N,
                    "distance": 10,
                    "deviceID": this.deviceID
                };
                console.log(this.locdata);
            }
            else if (Gdata.length == 9) {
                this.locdata = {
                    "street_address": Gdata[1] + " " + Gdata[0],
                    "city": Gdata[4],
                    "country": Gdata[7],
                    "zipcode": Gdata[8],
                    "latitude": coord.lat,
                    "longitude": coord.lng,
                    "is_active": 1,
                    "status_id": 0,
                    "alert_type": _AlertState.N,
                    "distance": 10,
                    "deviceID": this.deviceID
                };
                console.log(this.locdata);
            }
            else if (Gdata.length == 10) {
                this.locdata = {
                    "street_address": Gdata[1] + " " + Gdata[0],
                    "city": Gdata[5],
                    "country": Gdata[8],
                    "zipcode": Gdata[9],
                    "latitude": coord.lat,
                    "longitude": coord.lng,
                    "is_active": 1,
                    "status_id": 0,
                    "alert_type": _AlertState.N,
                    "distance": 10,
                    "deviceID": this.deviceID
                };
                console.log(this.locdata);
            }
            console.log(this.locdata);
            localStorage.setItem('lastLat', this.locdata.latitude);
            localStorage.setItem('lastLng', this.locdata.longitude);
        }
        else {
            console.log("No address available");
        }
    };
    //button click for saw the same 
    MapPage.prototype.saw = function () {
        var _this = this;
        this.translate.get(this.tranText.sent).subscribe(function (res) {
            _this.title = res;
        });
        this.translate.get(this.tranText.sentmsg).subscribe(function (res) {
            _this.subtitle = res;
        });
        var alert = this.alertCtrl.create({
            title: this.title,
            subTitle: this.subtitle,
            buttons: ['Ok']
        });
        var userdata = JSON.parse(localStorage.getItem('alertdata'));
        ;
        userdata['cur_user_id'] = JSON.parse(localStorage.getItem('userData'))['user_id'];
        console.log(userdata);
        userdata['res'] = "true";
        console.log(userdata);
        this.AuthServiceProvider.postData(userdata, 'Alertresponse').then(function (result) {
            console.log(result);
            _this.responsedata = result;
            console.log(_this.responsedata.status);
            if (_this.responsedata.status == true) {
                localStorage.removeItem('victimData');
                _this.pushSendOpenModal();
                _this.refresh();
            }
        }, function (err) {
            console.log(err);
            // Error log
        });
    };
    //button click not suspecious
    MapPage.prototype.notsusp = function () {
        var _this = this;
        this.translate.get(this.tranText.sent).subscribe(function (res) {
            _this.title = res;
        });
        this.translate.get(this.tranText.sentmsg).subscribe(function (res) {
            _this.subtitle = res;
        });
        var alert = this.alertCtrl.create({
            title: this.title,
            subTitle: this.subtitle,
            buttons: ['Ok']
        });
        var userdata = JSON.parse(localStorage.getItem('alertdata'));
        ;
        userdata['cur_user_id'] = JSON.parse(localStorage.getItem('userData'))['user_id'];
        console.log(userdata);
        userdata['res'] = "false";
        console.log(userdata);
        this.AuthServiceProvider.postData(userdata, 'Alertresponse').then(function (result) {
            console.log(result);
            _this.responsedata = result;
            if (_this.responsedata.status == true) {
                localStorage.removeItem('victimData');
                _this.pushSendOpenModal();
                _this.refresh();
            }
        }, function (err) {
            // Error log
        });
    };
    MapPage.prototype.countdown = function () {
        var _this = this;
        //this.timeLeft = this.alertTime * 60;
        this.timeLeft = 1 * 60;
        this.interval = setInterval(function () {
            if (_this.timeLeft > 0) {
                _this.StartTimer(_this.timeLeft);
                _this.timeLeft--;
                console.log('time remin:' + _this.timeLeft);
                if (_this.timeLeft == 0) {
                    _this.owner = "";
                    _this.sendcurpush();
                    localStorage.setItem('sendtype', 'auto');
                    console.log('countdown stopped');
                }
            }
            else {
                clearInterval(_this.interval);
            }
        }, 1000);
    };
    MapPage.prototype.stopTimer = function () {
        clearInterval(this.interval);
    };
    //initiate automatic alert sending processs with time delay
    MapPage.prototype.countdown_bk = function () {
        var _this = this;
        console.log('called timer:' + this.alertTime);
        //var sec = this.alertTime * 60;
        var sec = 1 * 60; //for develop
        //var sec: any = localStorage.getItem('timeCheck') ? parseInt(localStorage.getItem('timeCheck')) : seconds;
        this.autoPushCounter = false;
        this.countstatus = __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__["Observable"].interval(1000)
            .takeWhile(function () { return !_this.autoPushCounter; })
            .subscribe(function (x) {
            sec = sec - 1;
            //localStorage.setItem('timeCheck', sec);
            _this.StartTimer(sec);
            if (sec == 0) {
                _this.owner = "";
                _this.sendcurpush();
                localStorage.setItem('sendtype', 'auto');
                console.log('countdown stopped');
            }
        });
        //}
    };
    //function to stop timer
    MapPage.prototype.stopCountdown = function () {
        this.autoPushCounter = true;
        this.countstatus.unsubscribe();
    };
    //function for start timer
    MapPage.prototype.StartTimer = function (second) {
        var secNum = parseInt(second.toString(), 10); // don't forget the second param
        var hours = Math.floor(secNum / 3600);
        var minutes = Math.floor((secNum - (hours * 3600)) / 60);
        var seconds = secNum - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? '0' + hours : hours.toString();
        minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
        secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
        // console.log(hoursString + ':' + minutesString + ':' + secondsString);
        this.autoTime = minutesString + ':' + secondsString;
    };
    //remove accidently add non-nabouser alert 
    MapPage.prototype.removeGuestUser = function () {
        var _this = this;
        var gdata = JSON.parse(localStorage.getItem('alertdata'));
        this.AuthServiceProvider.postData(gdata, 'removeGuest').then(function (result) {
            console.log(result);
            _this.responsedata = result;
            if (_this.responsedata.status == true) {
                console.log(result);
            }
        }, function (err) {
            // Error log
            console.log(err);
        });
    };
    //button click for initimate the process informed to police
    MapPage.prototype.police = function () {
        var _this = this;
        this.ownerpoli = '';
        this.ownerbtn = '';
        this.infoMsg = false;
        var pdata = JSON.parse(localStorage.getItem('alertdata'));
        var data = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(pdata, 'policeInform').then(function (result) {
            console.log(result);
            _this.responsedata = result;
            if (_this.responsedata.status == true) {
                //alert.present();
                console.log(result);
                _this.refresh();
            }
        }, function (err) {
            // Error log
            console.log(err);
        });
    };
    //get messge data from front-end
    MapPage.prototype.burglaryInHouse = function () {
        var _this = this;
        this.translate.get("BURGLARIES IN HOUSE").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.burglar = function () {
        this.ddata = {
            "msg": "BURGLAR BURGLARY"
        };
    };
    MapPage.prototype.burglarShed = function () {
        var _this = this;
        this.translate.get("BURGLAR SHEDDING").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.assult = function () {
        var _this = this;
        this.translate.get("ASSAULT").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.picPocket = function () {
        var _this = this;
        this.translate.get("PICKPOCKET").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.susCar = function () {
        var _this = this;
        this.translate.get("SUSPICIOUS CAR").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.susPerson = function () {
        var _this = this;
        this.translate.get("SUSPICIOUS PERSON").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.susBehavior = function () {
        var _this = this;
        this.translate.get("SUSPICIOUS BEHAVIOR FROM PEOPLE").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.susAttention = function () {
        var _this = this;
        this.translate.get("POSSIBLE ATTENTION TO BURGLARY").subscribe(function (value) {
            _this.ddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestburglaryInHouse = function () {
        var _this = this;
        console.log('Burglaries in houses');
        this.translate.get("BURGLARIES IN HOUSE").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestburglar = function () {
        this.gddata = {
            "msg": "BURGLAR BURGLARY"
        };
    };
    MapPage.prototype.guestburglarShed = function () {
        var _this = this;
        this.translate.get("BURGLAR SHEDDING").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestassult = function () {
        var _this = this;
        this.translate.get("ASSAULT").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestpicPocket = function () {
        var _this = this;
        this.translate.get("PICKPOCKET").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestsusCar = function () {
        var _this = this;
        console.log('Suspicious Car');
        this.translate.get("SUSPICIOUS CAR").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestsusPerson = function () {
        var _this = this;
        this.translate.get("SUSPICIOUS PERSON").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestsusBehavior = function () {
        var _this = this;
        this.translate.get("SUSPICIOUS BEHAVIOR FROM PEOPLE").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    MapPage.prototype.guestsusAttention = function () {
        var _this = this;
        this.translate.get("POSSIBLE ATTENTION TO BURGLARY").subscribe(function (value) {
            _this.gddata = {
                "msg": value
            };
        });
    };
    //close the alert message box
    MapPage.prototype.exit = function () {
        this.alertmsg = false;
        this.alertmsgsus = false;
        this.guestalertmsg = false;
        this.guestalertmsgsus = false;
        this.guest = false;
        this.ddata.msg = '';
        this.gddata.msg = '';
    };
    //close message info window
    MapPage.prototype.infoExit = function () {
        this.infoMsg = false;
    };
    //function to reset process automatically
    MapPage.prototype.processReset = function () {
        var _this = this;
        var reset = 60 * 1000;
        //var reset = this.resetTime * 24 * 60 * 60 * 1000;
        this.stopCondition = false;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        var userdata = {
            "user_id": this.userData.user_id,
            "address_id": this.userData.address_id,
            "address_type": this.userData.address_type
        };
        __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__["Observable"].interval(reset)
            .takeWhile(function () { return !_this.stopCondition; })
            .subscribe(function (i) {
            //This will be called every 10 seconds until `stopCondition` flag is set to true
            _this.AuthServiceProvider.postData(userdata, 'ownerCancel').then(function (result) {
                console.log(result);
                _this.stopCondition = true;
            }, function (err) {
                console.log(err);
            });
        });
    };
    //real time user's status  update
    MapPage.prototype.updateUserData = function () {
        var _this = this;
        var timer = this.refreshTime;
        if (timer == '' || undefined) {
            timer = 1;
        }
        var seconds = timer * 60 * 1000;
        console.log('refersh time in milsec:' + seconds);
        var userData = JSON.parse(localStorage.getItem('userData'));
        console.log('user data updated');
        this.userDataUpdate = false;
        __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__["Observable"].interval(seconds)
            .takeWhile(function () { return !_this.userDataUpdate; })
            .subscribe(function (i) {
            //This will be called every 10 seconds until `stopCondition` flag is set to true
            _this.AuthServiceProvider.postData(userData, 'getUserAddressByCountry').then(function (result) {
                _this.addressData = result;
                console.log(result);
                //this.stopCondition = true;
                var session = localStorage.getItem('session');
                if (session == '_logged_out') {
                    _this.userDataUpdate = true;
                }
            }, function (err) {
                console.log(err);
            });
        });
    };
    MapPage.prototype.updateUserLocation = function () {
        var _this = this;
        var timer = this.refreshTime;
        if (timer == '' || undefined) {
            timer = 1;
        }
        var seconds = timer * 60 * 1000;
        console.log('locatrion update time in milsec:' + seconds);
        this.userLocUpdate = false;
        __WEBPACK_IMPORTED_MODULE_6_rxjs_Rx__["Observable"].interval(seconds)
            .takeWhile(function () { return !_this.userLocUpdate; })
            .subscribe(function (i) {
            //This will be called every 10 seconds until `stopCondition` flag is set to true 
            _this.options = {
                timeout: 6000, enableHighAccuracy: true, maximumAge: 3800
            };
            _this.geolocation.getCurrentPosition(_this.options).then(function (pos) {
                var latlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
                console.log('user location ref', pos);
                localStorage.setItem('lastLat', JSON.stringify(pos.coords.latitude));
                localStorage.setItem('lastLng', JSON.stringify(pos.coords.longitude));
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'location': latlng }, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            console.log(results[0].formatted_address);
                            _this.othersAddress = results[0].formatted_address;
                            localStorage.setItem('lastadd', _this.othersAddress);
                        }
                        else {
                            _this.othersAddress = 'Cannot determine address at this location.';
                        }
                        _this.AGMloadMap();
                        _this.mapview = true;
                        _this.owner = false;
                        _this.alertmsg = false;
                        _this.alertmsgsus = false;
                        _this.guestalertmsg = false;
                        _this.ownerbtn = "";
                        _this.resbtn = "";
                        _this.policealert = "";
                        _this.ownerpoli = "";
                        _this.infoMsg = false;
                    }
                });
            }, function (err) {
                console.log('Error message : ' + err.message);
            });
            var session = localStorage.getItem('session');
            if (session == '_logged_out') {
                _this.userLocUpdate = true;
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_14" /* ViewChild */])('map'),
        __metadata("design:type", Object)
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-map',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/map/map.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div id="over_map" *ngIf="othersAddress">{{othersAddress}}</div>\n  <div id="info_window" *ngIf="infoMsg">\n    <div class="inner">\n      <p>{{infoMsg}}\n        <br><span id="loctime">{{eventTime}}</span></p>\n    </div>\n    <div class="info-exit" *ngIf="infoMsg">\n      <ion-icon name="md-close" (click)="infoExit()"></ion-icon>\n    </div>\n    <div class="arrow-down" *ngIf="infoMsg"></div>\n  </div>\n  <div id="marker"></div>\n  <agm-map [latitude]="AGMmap.lat" \n    [longitude]="AGMmap.lng" \n    [zoom]="AGMmap.zoom" \n    (mapReady)="mapReady($event)" \n    [clickableIcons]="false" \n    [streetViewControl]="false"\n    [zoomControl]="false">\n\n    <agm-marker *ngFor="let address of addressData; let i = index" \n      [latitude]="convertString(address.latitude)" \n      [longitude]="convertString(address.longitude)"\n      [iconUrl]="getMarkerURL(address.marker_icon)" (markerClick)="markerClick(address)">\n    </agm-marker>\n\n  </agm-map>\n  <div class="switch">\n    <div class="alert_msg" *ngIf="alertmsg">\n      <ion-list class="ex-btn">\n          <ion-icon name="md-close" (click)="exit()"></ion-icon>\n      </ion-list>\n      <ion-list>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="burglaryInHouse()">{{ \'BURGLARIES IN HOUSE\' | translate }}</button>\n            </ion-col>\n            <!-- <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="burglar()">{{ \'Burglar burglary\' | translate }}</button>\n            </ion-col> -->\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="burglarShed()">{{\'BURGLAR SHEDDING\' | translate }}</button>\n            </ion-col>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="assult()">{{\'ASSAULT\' | translate}}</button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="picPocket()">{{\'PICKPOCKET\' | translate}}</button>\n            </ion-col>\n            <ion-item>\n              <ion-input type="text" style="text-transform:uppercase;text-align: center;" placeholder="{{\'Or enter alert information\' | translate}}" class="alertinput"\n                name="msg" [(ngModel)]="ddata.msg"></ion-input>\n            </ion-item>\n          </ion-row>\n          <button type="button" [disabled]="!ddata.msg" class="alert" (click)="alert()" ion-button round full>{{\'Send Alert\' | translate}}</button>\n        </ion-grid>\n      </ion-list>\n    </div>\n\n    <div class="alert_msg" *ngIf="alertmsgsus">\n      <ion-list class="ex-btn">\n        <ion-icon name="md-close" (click)="exit()"></ion-icon>\n      </ion-list>\n      <ion-list>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="susCar()">{{\'SUSPICIOUS CAR\' | translate}}</button>\n            </ion-col>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="susPerson()">{{\'SUSPICIOUS PERSON\' | translate}}</button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="susBehavior()">{{\'SUSPICIOUS BEHAVIOR FROM PEOPLE\' | translate}}</button>\n            </ion-col>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="susAttention()">{{\'POSSIBLE ATTENTION TO BURGLARY\' | translate}}</button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-item>\n              <ion-input type="text" style="text-transform:uppercase;text-align:center;" placeholder="{{\'Or enter alert information\' | translate}}" class="alertinput"\n                name="msg" [(ngModel)]="ddata.msg"></ion-input>\n            </ion-item>\n          </ion-row>\n          <button type="button" [disabled]="!ddata.msg" class="alert" (click)="alert()" ion-button round full>{{\'Send Alert\' | translate}}</button>\n        </ion-grid>\n      </ion-list>\n    </div>\n\n    <div class="alert_msg" *ngIf="guestalertmsg">\n      <ion-list class="ex-btn">\n        <ion-icon name="md-close" (click)="exit()"></ion-icon>\n      </ion-list>\n      <ion-list>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestburglaryInHouse()">{{ \'BURGLARIES IN HOUSE\' | translate }}</button>\n            </ion-col>\n            <!-- <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestburglar()">Burglar burglary</button>\n            </ion-col> -->\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestburglarShed()">{{\'BURGLAR SHEDDING\' | translate }}</button>\n            </ion-col>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestassult()">{{\'ASSAULT\' | translate}}</button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestpicPocket()">{{\'PICKPOCKET\' | translate}}</button>\n            </ion-col>\n            <ion-item>\n              <ion-input type="text" style="text-transform:uppercase;text-align:center;" placeholder="{{\'Or enter alert information\' | translate}}" class="alertinput"\n                name="msg" [(ngModel)]="gddata.msg"></ion-input>\n            </ion-item>\n          </ion-row>\n          <button type="button" [disabled]="!gddata.msg" class="alert" (click)="sendAlertConfirm()" ion-button round full>{{\'Send Alert\' | translate}}</button>\n        </ion-grid>\n      </ion-list>\n    </div>\n\n    <div class="alert_msg" *ngIf="guestalertmsgsus">\n      <ion-list class="ex-btn">\n        <ion-icon name="md-close" (click)="exit()"></ion-icon>\n      </ion-list>\n      <ion-list>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestsusCar()">{{\'SUSPICIOUS CAR\' | translate}}</button>\n            </ion-col>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestsusPerson()">{{\'SUSPICIOUS PERSON\' | translate}}</button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestsusBehavior()">{{\'SUSPICIOUS BEHAVIOR FROM PEOPLE\' | translate}}</button>\n            </ion-col>\n            <ion-col col-12>\n              <button ion-button class="alertBtn" (click)="guestsusAttention()"><p>{{\'POSSIBLE ATTENTION TO BURGLARY\' | translate}}</p></button>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-item>\n              <ion-input type="text" style="text-transform:uppercase;text-align:center;" placeholder="{{\'Or enter alert information\' | translate}}" class="alertinput"\n                name="msg" [(ngModel)]="gddata.msg"></ion-input>\n            </ion-item>\n          </ion-row>\n          <button type="button" [disabled]="!gddata.msg" class="alert" (click)="sendAlertConfirm()" ion-button round full>{{\'Send Alert\' | translate}}</button>\n        </ion-grid>\n      </ion-list>\n    </div>\n\n    <div class="btn-group" *ngIf="alertbtn==\'N\'">\n      <button ion-button color="light" class="button btn-w-45" (click)="burglery()" icon-only>{{\'Alarm\' | translate }}</button>\n      <button ion-button color="light" class="button btn-w-45" (click)="Suspicious()" icon-only>{{ \'Suspicious\' | translate }}</button>\n    </div>\n    <div class="btn-group btn-caninfo" *ngIf="resbtn==\'S\'">\n      <button ion-button class="button own-pol" (click)="police()" round full>{{ \'Informed Police\' | translate }} </button>\n      <button ion-button class="button res" (click)="saw()" icon-only>{{ \'Saw the same\' | translate }}</button>\n      <button ion-button class="button redres" (click)="notsusp()" icon-only>{{ \'Not Suspicious\' | translate }}</button>\n    </div>\n    <div class="btn-group" *ngIf="ownerbtn==\'owner\'">\n      <button ion-button class="button own-can" (click)="ownercancel()" icon-only>{{ \'Cancel Process\' | translate }}</button>\n    </div>\n    <div class="btn-group" *ngIf="policealert==\'SS\'">\n      <button ion-button class="button own-pol" (click)="police()" round full>{{ \'Informed Police\' | translate }} </button>\n    </div>\n    <div class="btn-group btn-caninfo" *ngIf="ownerpoli==\'owner\'">\n      <button ion-button class="button can-btn" (click)="ownercancel()" icon-only>{{ \'Cancel Process\' | translate }}</button>\n      <button ion-button class="button pol" (click)="police()" icon-only>{{ \'Informed Police\' | translate }} </button>\n    </div>\n  </div>\n</ion-content>\n<ion-content *ngIf="owner">\n  <ion-card>\n    <ion-badge item-right>{{\'Available User\' | translate}}.{{SrangeAvaUser}}</ion-badge>\n    <ion-item id="contactList-list-item" class="push_tag">\n      <div class="push">\n        <a href="#" class="push_btn">{{ \'Automatically send in\' | translate}} : {{autoTime}} </a>\n      </div>\n    </ion-item>\n    <ion-item id="contactList-list-item" class="push_tag btn-success">\n      <div class="push">\n        <a href="#" class="push_btn" (click)="sendcurpush()">{{ \'Send the alert to all volunteers\' | translate }}\n        </a>\n      </div>\n    </ion-item>\n    <ion-item id="contactList-list-item" class="push_tag btn-danger">\n      <div class="push">\n        <a href="#" class="push_btn" (click)="cancelpush()"> {{ \'Cancel the alert sent by neighbour\' | translate }}\n        </a>\n      </div>\n    </ion-item>\n  </ion-card>\n</ion-content>\n<ion-content id="grange" *ngIf="guest">\n  <ion-badge item-right>{{\'Available User\' | translate}}.{{SrangeAvaUser}}</ion-badge>\n</ion-content>\n<!-- <div >\n  <ul>\n    <li>\n      <a (click)="refresh()">\n        <ion-icon ios="ios-refresh-circle-outline" md="ios-refresh-circle-outline"></ion-icon>\n      </a>\n    </li>\n    <li>\n      <a (click)="getHome()">\n        <ion-icon ios="ios-navigate-outline" md="ios-navigate-outline"></ion-icon>\n      </a>\n    </li>\n    <li>\n      <a (click)="getCurrentPosition()">\n        <ion-icon ios="ios-locate-outline" md="ios-locate-outline"></ion-icon>\n      </a>\n    </li>\n  </ul>\n</div> -->\n<div class="fab-container">\n  <ion-fab bottom right id="ctrl-btns">\n      <button ion-fab mini><ion-icon name="ios-arrow-up"></ion-icon></button>\n      <ion-fab-list id="ctrl-btns-list" side="top">\n        <button ion-fab (click)="refresh()"><ion-icon ios="ios-refresh-circle" md="ios-refresh-circle"></ion-icon></button>\n        <button ion-fab (click)="getHome()"><ion-icon ios="ios-home" md="ios-home"></ion-icon></button>\n        <button ion-fab (click)="getCurrentPosition()"><ion-icon ios="ios-locate" md="ios-locate"></ion-icon></button>\n      </ion-fab-list>\n  </ion-fab>\n</div>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/map/map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_google_service_google_maps__["a" /* GoogleMaps */],
            __WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["P" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1__angular_core__["k" /* ChangeDetectorRef */]])
    ], MapPage);
    return MapPage;
}());

//# sourceMappingURL=map.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GoogleMaps; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GoogleMaps = /** @class */ (function () {
    function GoogleMaps(http, geolocation, AuthServiceProvider) {
        this.http = http;
        this.geolocation = geolocation;
        this.AuthServiceProvider = AuthServiceProvider;
        this.markers = [];
        this.mapData = [];
    }
    //initiate google map
    GoogleMaps.prototype.initMap = function (mapElement) {
        var _this = this;
        this.geocoder = new google.maps.Geocoder();
        this.options = {
            enableHighAccuracy: true
        };
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            _this.currentPos = pos;
            console.log(pos);
            console.log(pos.coords.latitude + ',' + pos.coords.longitude);
            _this.latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            _this.mapOptions = {
                center: _this.latlng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                fullscreenControl: false,
                streetViewControl: false,
                mapTypeControl: false,
                clickableIcons: false
            };
            console.log('map initiated');
            _this.map = new google.maps.Map(mapElement, _this.mapOptions);
            // google.maps.event.addListenerOnce(this.map, 'idle', () => {
            //     this.loadMarkers();
            //     google.maps.event.addListener(this.map, 'dragend', () => {
            //         this.loadMarkers();      
            //     });
            // });
        }, function (err) {
            console.log("error : " + err.message);
        });
    };
    GoogleMaps.prototype.getMap = function (mapElement) {
        return this.initMap(mapElement);
    };
    //load markers when drag
    GoogleMaps.prototype.loadMarkers = function () {
        var center = this.map.getCenter(), bounds = this.map.getBounds(), zoom = this.map.getZoom();
        // Convert to readable format
        var centerNorm = {
            lat: center.lat(),
            lng: center.lng()
        };
        var boundsNorm = {
            northEast: {
                lat: bounds.getNorthEast().lat(),
                lng: bounds.getNorthEast().lng()
            },
            southWest: {
                lat: bounds.getSouthWest().lat(),
                lng: bounds.getSouthWest().lng()
            }
        };
        var boundingRadius = this.getBoundingRadius(centerNorm, boundsNorm);
        var options = {
            lng: centerNorm.lng,
            lat: centerNorm.lat,
            maxDistance: boundingRadius
        };
        this.getMarkers();
        this.getDragendData();
    };
    GoogleMaps.prototype.getDragendData = function () {
        var _this = this;
        this.geocoder = new google.maps.Geocoder();
        console.log("map move");
        this.LastLat = this.map.getCenter().lat();
        this.LastLng = this.map.getCenter().lng();
        var coord = new Array();
        coord.push(this.LastLat, this.LastLng);
        console.log(coord[0] + ',' + coord[1]);
        var latlng = new google.maps.LatLng(this.LastLat, this.LastLng);
        var request = { latLng: latlng };
        console.log(this.LastLat + ',' + this.LastLng);
        this.geocoder.geocode(request, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                _this.othersAddress = results[0].formatted_address;
                // this.locationData = results[0].geometry.location;
                // console.log("drag"+JSON.stringify(this.locationData));
                console.log(_this.othersAddress);
                localStorage.setItem('address', _this.othersAddress);
                //return this.othersAddress
            }
            else {
                console.log(_this.othersAddress);
                //return status;
                //console.log('Cannot determine address at this location.' + status);
                localStorage.setItem('address', "Cannot determine address at this location.");
            }
        });
    };
    GoogleMaps.prototype.getBoundingRadius = function (center, bounds) {
        return this.getDistanceBetweenPoints(center, bounds.northEast, 'km');
    };
    GoogleMaps.prototype.getDistanceBetweenPoints = function (pos1, pos2, units) {
        var earthRadius = {
            miles: 3958.8,
            km: 6371
        };
        var R = earthRadius[units || 'miles'];
        var lat1 = pos1.lat;
        var lon1 = pos1.lng;
        var lat2 = pos2.lat;
        var lon2 = pos2.lng;
        var dLat = this.toRad((lat2 - lat1));
        var dLon = this.toRad((lon2 - lon1));
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
                Math.sin(dLon / 2) *
                Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };
    GoogleMaps.prototype.toRad = function (x) {
        return x * Math.PI / 180;
    };
    //get markers
    GoogleMaps.prototype.getMarkers = function () {
        var _this = this;
        this.AuthServiceProvider.getData('getUseraddress').then(function (result) {
            _this.responseData = result;
            console.log(_this.responseData);
            _this.addMarkers(_this.responseData);
        });
    };
    //add markers
    GoogleMaps.prototype.addMarkers = function (markers) {
        var _this = this;
        var marker;
        var markerLatLng;
        var lat;
        var lng;
        markers.forEach(function (marker) {
            lat = marker.latitude;
            lng = marker.longitude;
            markerLatLng = new google.maps.LatLng(lat, lng);
            if (!_this.markerExists(lat, lng)) {
                marker = new google.maps.Marker({
                    map: _this.map,
                    animation: google.maps.Animation.DROP,
                    position: markerLatLng,
                    icon: 'http://rayi.in/naboapi_v2/mapicon/' + marker.marker_icon,
                });
                var markerData = {
                    lat: lat,
                    lng: lng,
                    marker: marker
                };
                _this.markers.push(markerData);
            }
        });
    };
    //check marker exist
    GoogleMaps.prototype.markerExists = function (lat, lng) {
        var exists = false;
        this.markers.forEach(function (marker) {
            if (marker.lat === lat && marker.lng === lng) {
                exists = true;
            }
        });
        return exists;
    };
    GoogleMaps = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */]])
    ], GoogleMaps);
    return GoogleMaps;
}());

//# sourceMappingURL=google-maps.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptsupportPageModule", function() { return AcceptsupportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__acceptsupport__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AcceptsupportPageModule = /** @class */ (function () {
    function AcceptsupportPageModule() {
    }
    AcceptsupportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__acceptsupport__["a" /* AcceptsupportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__acceptsupport__["a" /* AcceptsupportPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__acceptsupport__["a" /* AcceptsupportPage */]
            ]
        })
    ], AcceptsupportPageModule);
    return AcceptsupportPageModule;
}());

//# sourceMappingURL=acceptsupport.module.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactusPageModule", function() { return ContactusPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contactus__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContactusPageModule = /** @class */ (function () {
    function ContactusPageModule() {
    }
    ContactusPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__contactus__["a" /* ContactusPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contactus__["a" /* ContactusPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__contactus__["a" /* ContactusPage */]]
        })
    ], ContactusPageModule);
    return ContactusPageModule;
}());

//# sourceMappingURL=contactus.module.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactusPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__map_map__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: contact us page (This page will load nabo support contact form)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 






var ContactusPage = /** @class */ (function () {
    function ContactusPage(navCtrl, menu, toastCtrl, AuthServiceProvider, emailComposer, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.toastCtrl = toastCtrl;
        this.AuthServiceProvider = AuthServiceProvider;
        this.emailComposer = emailComposer;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.userData = { "name": "", "mail": "", "subject": "", "message": "" };
        this.form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
            mail: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
            subject: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
            message: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required),
        });
    }
    Object.defineProperty(ContactusPage.prototype, "name", {
        //get the field details from contact us form
        get: function () {
            return this.form.get('name');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactusPage.prototype, "mail", {
        get: function () {
            return this.form.get('mail');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactusPage.prototype, "subject", {
        get: function () {
            return this.form.get('subject');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ContactusPage.prototype, "message", {
        get: function () {
            return this.form.get('message');
        },
        enumerable: true,
        configurable: true
    });
    //redirect to map page
    ContactusPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
    };
    //default Ionic lifecycle event
    ContactusPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactusPage');
    };
    ContactusPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    ContactusPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //send user details as a mail to nabosupport
    ContactusPage.prototype.sendemail = function () {
        var _this = this;
        console.log("mail");
        var toast = this.toastCtrl.create({
            message: 'Your message sent successfully',
            duration: 3000,
            position: 'bottom'
        });
        var toast1 = this.toastCtrl.create({
            message: 'Please fillup the detail to Contact Nabo Support Team',
            duration: 3000,
            position: 'bottom'
        });
        var toast2 = this.toastCtrl.create({
            message: 'Invalid Email format',
            duration: 3000,
            position: 'bottom'
        });
        var userdata = JSON.parse(localStorage.getItem('userData'));
        this.userData['user_id'] = userdata['user_id'];
        console.log(this.userData);
        console.log(this.userData);
        if (this.userData.name && this.userData.mail && this.userData.subject && this.userData.message) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(this.userData.mail) == true) {
                console.log(this.userData);
                this.AuthServiceProvider.postData(this.userData, 'sendmail').then(function (result) {
                    _this.responseData = result;
                    if (true == _this.responseData.status) {
                        var alert_1 = _this.alertCtrl.create({
                            title: 'Sent',
                            subTitle: 'Your Message sent successfully.',
                            buttons: ['Ok']
                        });
                        alert_1.present();
                        _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                    }
                    else {
                        var alert_2 = _this.alertCtrl.create({
                            title: 'Failed',
                            subTitle: 'Your Message sent Failed.',
                            buttons: ['Ok']
                        });
                        alert_2.present();
                    }
                }, function (err) {
                    // Error log
                });
            }
            else {
                toast2.present();
            }
        }
        else {
            toast1.present();
        }
    };
    ContactusPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contactus',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/contactus/contactus.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ \'Contact Nabo Support\' | translate }}</ion-title>\n    <a (click)="home()" class="home">\n      <ion-icon name="home"></ion-icon>\n    </a>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n  <ion-list>\n    <form [formGroup]="form" (ngSubmit)="sendemail()">\n      <ion-item>\n        <!--contact form starts -->\n        <ion-thumbnail item-start>\n          <ion-icon name="ios-contact"></ion-icon>\n        </ion-thumbnail>\n        <ion-label floating>\n          {{ \'NAME\' | translate }}</ion-label>\n        <ion-input type="text" formControlName="name" [(ngModel)]="userData.name"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <ion-icon name="ios-mail"></ion-icon>\n        </ion-thumbnail>\n        <ion-label floating>\n          {{ \'EMAIL\' | translate }}</ion-label>\n        <ion-input type="text" formControlName="mail" [(ngModel)]="userData.mail"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <ion-icon name="ios-document"></ion-icon>\n        </ion-thumbnail>\n        <ion-label floating>\n          {{ \'SUBJECT\' | translate }}</ion-label>\n        <ion-input type="text" formControlName="subject" [(ngModel)]="userData.subject"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <ion-icon name="ios-paper"></ion-icon>\n        </ion-thumbnail>\n        <ion-label floating>\n          {{ \'MESSAGE\' | translate }}</ion-label>\n        <ion-input class="text-in" formControlName="message" [(ngModel)]="userData.message"></ion-input>\n      </ion-item>\n      <button type="submit" class="sign_btn" ion-button round full>Send</button>\n      <!--contact form end -->\n    </form>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/contactus/contactus.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__["a" /* EmailComposer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], ContactusPage);
    return ContactusPage;
}());

//# sourceMappingURL=contactus.js.map

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordPageModule", function() { return ForgotpasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forgotpassword__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ForgotpasswordPageModule = /** @class */ (function () {
    function ForgotpasswordPageModule() {
    }
    ForgotpasswordPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__forgotpassword__["a" /* ForgotpasswordPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__forgotpassword__["a" /* ForgotpasswordPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__forgotpassword__["a" /* ForgotpasswordPage */]]
        })
    ], ForgotpasswordPageModule);
    return ForgotpasswordPageModule;
}());

//# sourceMappingURL=forgotpassword.module.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpPageModule", function() { return HelpPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__help__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var HelpPageModule = /** @class */ (function () {
    function HelpPageModule() {
    }
    HelpPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__help__["a" /* HelpPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__help__["a" /* HelpPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__help__["a" /* HelpPage */]
            ]
        })
    ], HelpPageModule);
    return HelpPageModule;
}());

//# sourceMappingURL=help.module.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvitefriendPageModule", function() { return InvitefriendPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__invitefriend__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var InvitefriendPageModule = /** @class */ (function () {
    function InvitefriendPageModule() {
    }
    InvitefriendPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__invitefriend__["a" /* InvitefriendPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__invitefriend__["a" /* InvitefriendPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__invitefriend__["a" /* InvitefriendPage */]
            ]
        })
    ], InvitefriendPageModule);
    return InvitefriendPageModule;
}());

//# sourceMappingURL=invitefriend.module.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPageModule", function() { return ModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ModalPageModule = /** @class */ (function () {
    function ModalPageModule() {
    }
    ModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__modal__["a" /* ModalPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__modal__["a" /* ModalPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__modal__["a" /* ModalPage */]]
        })
    ], ModalPageModule);
    return ModalPageModule;
}());

//# sourceMappingURL=modal.module.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationPageModule", function() { return NotificationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notification__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NotificationPageModule = /** @class */ (function () {
    function NotificationPageModule() {
    }
    NotificationPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__notification__["a" /* NotificationPage */]]
        })
    ], NotificationPageModule);
    return NotificationPageModule;
}());

//# sourceMappingURL=notification.module.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfferPageModule", function() { return OfferPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__offer__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var OfferPageModule = /** @class */ (function () {
    function OfferPageModule() {
    }
    OfferPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__offer__["a" /* OfferPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__offer__["a" /* OfferPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_2__offer__["a" /* OfferPage */]]
        })
    ], OfferPageModule);
    return OfferPageModule;
}());

//# sourceMappingURL=offer.module.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopoverPageModule", function() { return PopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popover__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PopoverPageModule = /** @class */ (function () {
    function PopoverPageModule() {
    }
    PopoverPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__popover__["a" /* PopoverPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__popover__["a" /* PopoverPage */]),
            ],
        })
    ], PopoverPageModule);
    return PopoverPageModule;
}());

//# sourceMappingURL=popover.module.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpasswordPageModule", function() { return ResetpasswordPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__resetpassword__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ResetpasswordPageModule = /** @class */ (function () {
    function ResetpasswordPageModule() {
    }
    ResetpasswordPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__resetpassword__["a" /* ResetpasswordPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__resetpassword__["a" /* ResetpasswordPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__resetpassword__["a" /* ResetpasswordPage */]]
        })
    ], ResetpasswordPageModule);
    return ResetpasswordPageModule;
}());

//# sourceMappingURL=resetpassword.module.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetpasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signin_signin__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Reset password page (This page send data to api and validate then send reset link inbox)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 






var ResetpasswordPage = /** @class */ (function () {
    function ResetpasswordPage(navCtrl, menu, nativeStorage, toastCtrl, loadingCtrl, AuthServiceProvider) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.nativeStorage = nativeStorage;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.AuthServiceProvider = AuthServiceProvider;
        this.form2 = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormGroup */]({
            password: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */](),
            cpassword: new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormControl */]()
        });
        this.userResetData = { "user_id": "", "password": "", "cpassword": "" };
        localStorage.setItem('userResetData', "");
    }
    Object.defineProperty(ResetpasswordPage.prototype, "password", {
        //get data from form field
        get: function () {
            return this.form2.get('password');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ResetpasswordPage.prototype, "cpassword", {
        get: function () {
            return this.form2.get('cpassword');
        },
        enumerable: true,
        configurable: true
    });
    //ionic lifecycle event
    ResetpasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResetpasswordPage');
    };
    ResetpasswordPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    ResetpasswordPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //send emailID to api for password reset
    ResetpasswordPage.prototype.reset = function () {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: 'Password not matched',
            duration: 3000,
            position: 'bottom'
        });
        var toast1 = this.toastCtrl.create({
            message: 'Please provide Password',
            duration: 3000,
            position: 'bottom'
        });
        var toast2 = this.toastCtrl.create({
            message: 'Your password changed..',
            duration: 3000,
            position: 'bottom'
        });
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Authenticating...'
        });
        setTimeout(function () {
            loading.dismiss();
        }, 500);
        if (this.userResetData.password && this.userResetData.cpassword) {
            if (this.userResetData.password != this.userResetData.cpassword) {
                toast.present();
            }
            else {
                var userdata = JSON.parse(localStorage.getItem('userForgotData'));
                this.userResetData.user_id = userdata.user_id;
                this.AuthServiceProvider.postData(this.userResetData, 'resetpassword').then(function (result) {
                    _this.responseData = result;
                    console.log(result);
                    if (true == _this.responseData.status) {
                        localStorage.setItem('userResetData', JSON.stringify(_this.responseData));
                        console.log(JSON.parse(localStorage.getItem('userResetData')));
                        toast2.present();
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signin_signin__["a" /* SigninPage */]);
                    }
                    else {
                        toast.present();
                    }
                }, function (err) {
                    // Error log
                });
            }
        }
        else {
            if (!(this.userResetData.password) || !(this.userResetData.cpassword)) {
                toast1.present();
            }
        }
    };
    ResetpasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-resetpassword',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/resetpassword/resetpassword.html"*/'<ion-content class="signin" padding center text-center>\n    <ion-title color="wht">Reset Password</ion-title>\n    <img src="assets/imgs/logo.png">\n    <!-- forgotpassword inputs field starts-->\n    <ion-list>\n      <form [formGroup]="form2"  (ngSubmit)="reset()">\n        <ion-item>\n          <ion-label floating>\n            <ion-icon name="lock"></ion-icon> Password</ion-label>\n          <ion-input formControlName="password" type="password" [(ngModel)]="userResetData.password" ></ion-input>\n        </ion-item>\n        \n        <ion-item>\n          <ion-label floating>\n            <ion-icon name="lock"></ion-icon> Confirm Password</ion-label>\n            <ion-input formControlName="cpassword" type="password" [(ngModel)]="userResetData.cpassword" ></ion-input>\n        </ion-item>\n        \n        <button type="submit" class="sign_btn" ion-button  round full>Reset Password</button>\n      \n      </form>\n    </ion-list>   \n  \n  </ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/resetpassword/resetpassword.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */]])
    ], ResetpasswordPage);
    return ResetpasswordPage;
}());

//# sourceMappingURL=resetpassword.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityPageModule", function() { return SecurityPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__security__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SecurityPageModule = /** @class */ (function () {
    function SecurityPageModule() {
    }
    SecurityPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__security__["a" /* SecurityPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__security__["a" /* SecurityPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__security__["a" /* SecurityPage */]]
        })
    ], SecurityPageModule);
    return SecurityPageModule;
}());

//# sourceMappingURL=security.module.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShopPageModule", function() { return ShopPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shop__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ShopPageModule = /** @class */ (function () {
    function ShopPageModule() {
    }
    ShopPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__shop__["a" /* ShopPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__shop__["a" /* ShopPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__shop__["a" /* ShopPage */]]
        })
    ], ShopPageModule);
    return ShopPageModule;
}());

//# sourceMappingURL=shop.module.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShopPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Shop page (This page load product from api for shop)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 




var ShopPage = /** @class */ (function () {
    function ShopPage(provider, navCtrl, menu, navParams, alertCtrl, browser, platform) {
        this.provider = provider;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.browser = browser;
        this.base_url = 'http://rayi.in/nabosupport-web/api/upload/product/';
        this.categoryList = [];
        this.categoryTitle = {
            title: 'Category',
            subTitle: ''
        };
    }
    //redirected to Nabosupport webpage
    ShopPage.prototype.webpage = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Shop page will be redirected to browser',
            message: 'Press Yes to Continue',
            buttons: [
                {
                    text: 'Yes',
                    role: 'cancel',
                    handler: function () {
                        console.log('Yes clicked');
                        var openBrowser = _this.browser.create('http://rayi.in/nabosupport/' + _this.shopURL, '_system', 'location=no');
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        console.log('No clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    //ionic lifecycle event
    ShopPage.prototype.ionViewDidLoad = function () {
        this.getCategoryList();
        this.adminData = JSON.parse(localStorage.getItem('adminData'));
        console.log(this.adminData);
        for (var i = 0; i < this.adminData.data.length; i++) {
            console.log('service:' + this.adminData.data[i].data);
        }
        this.shopURL = this.adminData.data[3].data;
    };
    ShopPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    ShopPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //get category list from api
    ShopPage.prototype.getCategoryList = function () {
        var _this = this;
        this.provider.getData('getCategory').then(function (res) {
            _this.rs = res;
            if (_this.rs.status === true) {
                _this.categoryList = _this.rs.data;
                if (localStorage.getItem('cat_id')) {
                    _this.chooseCategory = _this.categoryList[0].cat_id;
                    _this.onCategoryChange(localStorage.getItem('cat_id'));
                }
                else {
                    // chooseCategory
                    _this.chooseCategory = _this.categoryList[0].cat_id;
                    _this.onCategoryChange(_this.categoryList[0].cat_id);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    //category on change event capture
    ShopPage.prototype.onCategoryChange = function (event) {
        var _this = this;
        if (event) {
            localStorage.setItem('cat_id', event);
            var pro_data = {
                "cat_id": event
            };
            this.provider.postData(pro_data, 'getProduct').then(function (res) {
                _this.rs = res;
                if (_this.rs.status === true) {
                    _this.products = _this.rs.data;
                    if (_this.rs.data.length === 0) {
                        _this.isEmpty = true;
                    }
                    else {
                        _this.isEmpty = false;
                    }
                }
                else {
                    _this.isEmpty = true;
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    ShopPage.prototype.cart = function () {
        console.log('cart click.');
    };
    ShopPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-shop',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/shop/shop.html"*/'<ion-header>\n\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'Shop\' | translate}}</ion-title>\n        <a (click)="cart()" class="home">\n            <ion-icon name="cart" class="cus-margin"></ion-icon>\n            <ion-badge class="cartCount">{{itemCount}}</ion-badge>\n        </a>\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n<ion-content>\n    <ion-item class="selectCat-cls">\n        <ion-select class="select-ion-cls" [(ngModel)]="category" [selectOptions]="categoryTitle" (ionChange)="onCategoryChange($event)">\n            <ion-option value="">Select category</ion-option>\n            <span *ngFor="let list of categoryList">\n                <ion-option [value]="list.cat_id" [selected]="list.cat_id == chooseCategory">{{list.cat_name}}</ion-option>\n            </span>\n        </ion-select>\n    </ion-item>\n    <div *ngIf="isEmpty; then thenTemplateName; else elseTemplateName"></div>\n    <ng-template #thenTemplateName>\n        <ion-item id="contactList-list-item" class="white-clr">Record not found.</ion-item>\n    </ng-template>\n    <ng-template #elseTemplateName>\n        <ion-list class="list-border">\n            <ion-grid>\n                <ion-row>\n                    <ion-col col-md-3 col-sm-6 col-12 *ngFor="let row of products">\n                        <ion-card onclick="webpage()">\n                            <div class="shop-img">\n                                <img [src]="base_url+row.img_path" onerror="this.src=\'assets/imgs/no-img.svg\';" (click)="webpage();">\n                            </div>\n                            <div class="card-title">{{row.name}}</div>\n                            <div class="card-subtitle">$ {{row.price | number:\'1.2-2\'}}</div>\n                            <br/>\n                        </ion-card>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-list>\n    </ng-template>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/shop/shop.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* Platform */]])
    ], ShopPage);
    return ShopPage;
}());

//# sourceMappingURL=shop.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageModule", function() { return ContactPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContactPageModule = /** @class */ (function () {
    function ContactPageModule() {
    }
    ContactPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */]]
        })
    ], ContactPageModule);
    return ContactPageModule;
}());

//# sourceMappingURL=contact.module.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewaddressPageModule", function() { return NewaddressPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__newaddress__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NewaddressPageModule = /** @class */ (function () {
    function NewaddressPageModule() {
    }
    NewaddressPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__newaddress__["a" /* NewaddressPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__newaddress__["a" /* NewaddressPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__newaddress__["a" /* NewaddressPage */],
            ]
        })
    ], NewaddressPageModule);
    return NewaddressPageModule;
}());

//# sourceMappingURL=newaddress.module.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingPageModule", function() { return SettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__setting__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SettingPageModule = /** @class */ (function () {
    function SettingPageModule() {
    }
    SettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */]]
        })
    ], SettingPageModule);
    return SettingPageModule;
}());

//# sourceMappingURL=setting.module.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__map_map__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Setting page (This page load the user alert setting)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 





var SettingPage = /** @class */ (function () {
    function SettingPage(navCtrl, menu, AuthServiceProvider, geolocation, navParams) {
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.AuthServiceProvider = AuthServiceProvider;
        this.geolocation = geolocation;
        this.navParams = navParams;
        this.base_url = 'http://34.216.55.85/naboapi/';
        var dist = JSON.parse(localStorage.getItem('userData'));
        this.valueData = dist.distance;
        this.RvalueData = dist.r_distance;
        console.log(this.valueData);
        console.log(this.RvalueData);
        if (this.valueData <= 999) {
            this.unit = 'Mtrs ';
        }
        else {
            console.log('yes');
            this.unit = 'Kms ';
            this.valueData = this.valueData / 1000;
        }
        if (this.RvalueData <= 999) {
            this.Runit = 'Mtrs ';
        }
        else {
            console.log('yes');
            this.Runit = 'Kms ';
            this.RvalueData = this.RvalueData / 1000;
        }
        this.get_user();
    }
    //redirect to main page
    SettingPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem('userData'));
        var lat = user.latitude;
        var lng = user.longitude;
        localStorage.setItem('lastLat', lat);
        localStorage.setItem('lastLng', lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__map_map__["a" /* MapPage */]);
    };
    //get user details from api
    SettingPage.prototype.get_user = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem('userData'));
        console.log(userdata['user_id']);
        this.AuthServiceProvider.postData(userdata, 'getUser').then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
            _this.out_of_home = user_detail[0].out_of_home;
            if (user_detail[0].do_not_distrub == 1) {
                _this.dnd = "true";
            }
            else {
                _this.dnd = "false";
            }
            _this.valueData = user_detail[0].distance;
            _this.RvalueData = user_detail[0].r_distance;
            if (_this.valueData <= 999) {
                _this.unit = 'Mtrs ';
            }
            else {
                _this.unit = 'Kms ';
                _this.valueData = _this.valueData / 1000;
            }
            if (_this.RvalueData <= 999) {
                _this.Runit = 'Mtrs ';
            }
            else {
                _this.Runit = 'Kms ';
                _this.RvalueData = _this.RvalueData / 1000;
            }
            _this.range = user_detail[0].distance;
            _this.recrange = user_detail[0].r_distance;
            _this.addMap(user_detail[0].latitude, user_detail[0].longitude);
        });
    };
    //load  user location map
    SettingPage.prototype.addMap = function (lat, long) {
        var latLng = new google.maps.LatLng(lat, long);
        var mapOptions = {
            center: latLng,
            zoom: 15,
            fullscreenControl: false,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.addMarker();
    };
    //add marker in map
    SettingPage.prototype.addMarker = function () {
        var _this = this;
        var nabo_img = this.base_url + 'mapicon/new-markers/blue.png';
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: this.map.getCenter(),
            icon: nabo_img,
        });
        var content = "<p>This is your current position !</p>";
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        this.rangeUser("start");
        this.recrangeUser("start");
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(_this.map, marker);
        });
    };
    //ionic lifecycle event
    SettingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingPage');
    };
    SettingPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    SettingPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //event for change user alert sending range when range for change
    SettingPage.prototype.rangeUser = function (event) {
        var _this = this;
        this.submitProfile = true;
        var simpleObject = {};
        for (var prop in event) {
            if (!event.hasOwnProperty(prop)) {
                continue;
            }
            if (typeof (event[prop]) == 'object') {
                continue;
            }
            if (typeof (event[prop]) == 'function') {
                continue;
            }
            simpleObject[prop] = event[prop];
        }
        console.log(this.valueData);
        var user_detail = this.responseData;
        if (event != "start") {
            console.log(simpleObject);
            var value = JSON.stringify(simpleObject);
            this.valueData = JSON.parse(value)._value;
            user_detail[0]['distance'] = this.valueData;
            if (this.valueData <= 999) {
                this.unit = 'Mtrs ';
            }
            else {
                this.unit = 'Kms ';
                this.valueData = this.valueData / 1000;
            }
        }
        else {
            user_detail[0]['distance'] = user_detail[0]['distance'];
            this.valueData = user_detail[0]['distance'];
            if (this.valueData <= 999) {
                this.unit = 'Mtrs ';
            }
            else {
                this.unit = 'Kms ';
                this.valueData = this.valueData / 1000;
            }
        }
        this.AuthServiceProvider.postData(user_detail[0], 'pushNearuser').then(function (result) {
            _this.nearData = result;
            _this.nearData.forEach(function (o) {
                Object.keys(o).forEach(function (k) {
                    if (isFinite(o[k])) {
                        o[k] = +o[k];
                    }
                });
            });
            console.log(_this.nearData);
            if (_this.nearData) {
                var markers = [];
                var _loop_1 = function (regional) {
                    regional.distance = 10;
                    regional.visible = false;
                    regional.current = false;
                    var nabo_img = _this.base_url + 'mapicon/new-markers/blue.png';
                    console.log(regional.latitude + ',' + regional.longitude);
                    var markerData = {
                        position: {
                            lat: regional.latitude,
                            lng: regional.longitude
                        },
                        map: _this.map,
                        icon: nabo_img,
                    };
                    regional.marker = new google.maps.Marker(markerData);
                    markers.push(regional.marker);
                    var content = "naboUSer";
                    var infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
                    regional.marker.addListener('click', function () {
                        for (var _i = 0, _a = _this.nearData; _i < _a.length; _i++) {
                            var c = _a[_i];
                            c.current = false;
                        }
                        _this.currentregional = regional;
                        regional.current = true;
                        var alert_img = _this.base_url + 'mapicon/alert_home.png';
                        var markerData = {
                            position: {
                                lat: regional.latitude,
                                lng: regional.longitude
                            },
                            map: _this.map,
                            icon: alert_img,
                        };
                        infoWindow.open(_this.map, regional.marker);
                        _this.map.panTo(regional.marker.getPosition());
                    });
                };
                for (var _i = 0, _a = _this.nearData; _i < _a.length; _i++) {
                    var regional = _a[_i];
                    _loop_1(regional);
                }
            }
            _this.AuthServiceProvider.postData(user_detail[0], 'rangevalue').then(function (result) {
                _this.responsedata = result;
                console.log(_this.responsedata);
                if (true == _this.responsedata.status) {
                }
            });
        }, function (err) {
            // Error log
        });
    };
    //event for change user alert receiving range when range for change
    SettingPage.prototype.recrangeUser = function (event) {
        var _this = this;
        this.submitProfile = true;
        var simpleRObject = {};
        for (var prop in event) {
            if (!event.hasOwnProperty(prop)) {
                continue;
            }
            if (typeof (event[prop]) == 'object') {
                continue;
            }
            if (typeof (event[prop]) == 'function') {
                continue;
            }
            simpleRObject[prop] = event[prop];
        }
        console.log(this.RvalueData);
        var user_detail = this.responseData;
        if (event != "start") {
            console.log(simpleRObject);
            var value = JSON.stringify(simpleRObject);
            this.RvalueData = JSON.parse(value)._value;
            user_detail[0]['r_distance'] = this.RvalueData;
            if (this.RvalueData <= 999) {
                this.Runit = 'Mtrs ';
            }
            else {
                this.Runit = 'Kms ';
                this.RvalueData = this.RvalueData / 1000;
            }
        }
        else {
            user_detail[0]['r_distance'] = user_detail[0]['r_distance'];
            this.RvalueData = user_detail[0]['r_distance'];
            if (this.RvalueData <= 999) {
                this.Runit = 'Mtrs ';
            }
            else {
                this.Runit = 'Kms ';
                this.RvalueData = this.RvalueData / 1000;
            }
        }
        this.AuthServiceProvider.postData(user_detail[0], 'receiveNearuser').then(function (result) {
            _this.nearData = result;
            _this.nearData.forEach(function (o) {
                Object.keys(o).forEach(function (k) {
                    if (isFinite(o[k])) {
                        o[k] = +o[k];
                    }
                });
            });
            console.log(_this.nearData);
            if (_this.nearData) {
                var markers = [];
                var _loop_2 = function (regional) {
                    regional.distance = 10;
                    regional.visible = false;
                    regional.current = false;
                    var nabo_img = _this.base_url + 'mapicon/new-markers/blue.png';
                    console.log(regional.latitude + ',' + regional.longitude);
                    var markerData = {
                        position: {
                            lat: regional.latitude,
                            lng: regional.longitude
                        },
                        map: _this.map,
                        icon: nabo_img,
                    };
                    regional.marker = new google.maps.Marker(markerData);
                    markers.push(regional.marker);
                    var content = "naboUSer";
                    var infoWindow = new google.maps.InfoWindow({
                        content: content
                    });
                    regional.marker.addListener('click', function () {
                        for (var _i = 0, _a = _this.nearData; _i < _a.length; _i++) {
                            var c = _a[_i];
                            c.current = false;
                        }
                        _this.currentregional = regional;
                        regional.current = true;
                        var alert_img = _this.base_url + 'mapicon/alert_home.png';
                        var markerData = {
                            position: {
                                lat: regional.latitude,
                                lng: regional.longitude
                            },
                            map: _this.map,
                            icon: alert_img,
                        };
                        infoWindow.open(_this.map, regional.marker);
                        _this.map.panTo(regional.marker.getPosition());
                    });
                };
                for (var _i = 0, _a = _this.nearData; _i < _a.length; _i++) {
                    var regional = _a[_i];
                    _loop_2(regional);
                }
            }
            _this.AuthServiceProvider.postData(user_detail[0], 'recRangevalue').then(function (result) {
                _this.responsedata = result;
                console.log(_this.responsedata);
                if (true == _this.responsedata.status) {
                }
            });
        }, function (err) {
            // Error log
        });
    };
    //set user outofhome
    SettingPage.prototype.outofhome = function (event) {
        var _this = this;
        this.submitProfile = true;
        console.log("test" + JSON.stringify(event));
        console.log(event);
        var outofhomedata = this.responseData;
        outofhomedata[0]['outofhome'] = event;
        this.AuthServiceProvider.postData(outofhomedata[0], 'outofhome').then(function (result) {
            _this.responsedata = result;
            if (true == _this.responsedata.status) {
                //  this.get_user();
            }
            console.log(_this.out_of_home);
        });
    };
    //set do not distrub mode
    SettingPage.prototype.notify = function () {
        var _this = this;
        this.submitProfile = true;
        console.log("test" + JSON.stringify(this.dnd));
        var notifydata = this.responseData;
        notifydata[0]["notify"] = this.dnd;
        this.AuthServiceProvider.postData(notifydata[0], 'dnd').then(function (result) {
            _this.responsedata = result;
            if (true == _this.responsedata.status) {
                // this.get_user();
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], SettingPage.prototype, "mapElement", void 0);
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/setting/setting.html"*/'\n<ion-header>\n\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{ \'User Settings\' | translate }}</ion-title>\n        <a (click)="home()" class="home">\n            <ion-icon name="close-circle"></ion-icon>\n        </a>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div #map id="map"></div>\n    <div class="setting">\n        <ion-label><h5 class="text-center">Notification</h5></ion-label> \n        <ion-label >{{ \'Receiving Range upto\' | translate }}</ion-label>\n        <ion-badge item-left><span>{{Runit}}</span>{{RvalueData}}</ion-badge>\n        <ion-item>\n            <ion-range min="100" max="5000" step="100" (ionChange)="recrangeUser($event)" [(ngModel)]="recrange" color="secondary">\n                <ion-label range-left>100 Mtrs</ion-label>\n                <ion-label range-right>5 Kms</ion-label>\n            </ion-range>\n        </ion-item>\n        <ion-item class="no-border">\n            {{ \'Do not disturb\' | translate }}\n            <ion-segment color="dark" class="segment " [(ngModel)]="dnd" (ionChange)="notify()">\n                <ion-segment-button class="on segment-button" value="true">\n                    {{ \'OFF\' | translate }}\n                </ion-segment-button>\n                <ion-segment-button class="off segment-button segment-activated" value="false">\n                    {{ \'ON\' | translate }}\n                </ion-segment-button>\n\n            </ion-segment>\n\n        </ion-item>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/setting/setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninPageModule", function() { return SigninPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signin__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SigninPageModule = /** @class */ (function () {
    function SigninPageModule() {
    }
    SigninPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__signin__["a" /* SigninPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__signin__["a" /* SigninPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__signin__["a" /* SigninPage */]]
        })
    ], SigninPageModule);
    return SigninPageModule;
}());

//# sourceMappingURL=signin.module.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPageModule", function() { return AddressPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__address__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AddressPageModule = /** @class */ (function () {
    function AddressPageModule() {
    }
    AddressPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */]
            ]
        })
    ], AddressPageModule);
    return AddressPageModule;
}());

//# sourceMappingURL=address.module.js.map

/***/ }),

/***/ 413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyaccountPageModule", function() { return MyaccountPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__myaccount__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MyaccountPageModule = /** @class */ (function () {
    function MyaccountPageModule() {
    }
    MyaccountPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__myaccount__["a" /* MyaccountPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__myaccount__["a" /* MyaccountPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__myaccount__["a" /* MyaccountPage */]]
        })
    ], MyaccountPageModule);
    return MyaccountPageModule;
}());

//# sourceMappingURL=myaccount.module.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__agm_core__ = __webpack_require__(420);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var MapPageModule = /** @class */ (function () {
    function MapPageModule() {
    }
    MapPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(), __WEBPACK_IMPORTED_MODULE_4__agm_core__["a" /* AgmCoreModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */]]
        })
    ], MapPageModule);
    return MapPageModule;
}());

//# sourceMappingURL=map.module.js.map

/***/ }),

/***/ 474:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(475);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(479);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 479:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_device__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(789);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_plus__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_native_storage__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_forgotpassword_forgotpassword__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_forgotpassword_forgotpassword_module__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_resetpassword_resetpassword__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_resetpassword_resetpassword_module__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_myaccount_myaccount__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_myaccount_myaccount_module__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_signin_signin__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_signin_signin_module__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_signup_signup__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_signup_signup_module__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_newaddress_newaddress__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_newaddress_newaddress_module__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_address_address__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_address_address_module__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_map_map_module__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_contact_contact__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_contact_contact_module__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_contactus_contactus__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_contactus_contactus_module__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_security_security__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_security_security_module__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_notification_notification__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_notification_notification_module__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_setting_setting__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_setting_setting_module__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_shop_shop__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_shop_shop_module__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_help_help__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_help_help_module__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_offer_offer__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_offer_offer_module__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_modal_modal__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_modal_modal_module__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_acceptsupport_acceptsupport__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_acceptsupport_acceptsupport_module__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__ionic_native_email_composer__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_keyboard__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__ionic_native_file_transfer__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__ionic_native_file__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__ionic_native_camera__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__ionic_native_push__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__ionic_native_diagnostic__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__ionic_native_file_path__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__ngx_translate_http_loader__ = __webpack_require__(798);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__providers_google_service_google_maps__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__agm_core__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__ionic_native_network__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__ionic_native_app_version__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__ionic_native_location_accuracy__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__ionic_native_crop__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__pages_popover_popover__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__pages_popover_popover_module__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__providers_get_user_address_get_user_address__ = __webpack_require__(800);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__pages_invitefriend_invitefriend__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__pages_invitefriend_invitefriend_module__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__pages_aboutus_aboutus__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__pages_aboutus_aboutus_module__ = __webpack_require__(231);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









































































function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_59__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["L" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_49__angular_forms__["c" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_49__angular_forms__["f" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_57__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_57__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_7__angular_http__["b" /* Http */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/aboutus/aboutus.module#AboutusPageModule', name: 'AboutusPage', segment: 'aboutus', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/acceptsupport/acceptsupport.module#AcceptsupportPageModule', name: 'AcceptsupportPage', segment: 'acceptsupport', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contactus/contactus.module#ContactusPageModule', name: 'ContactusPage', segment: 'contactus', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forgotpassword/forgotpassword.module#ForgotpasswordPageModule', name: 'ForgotpasswordPage', segment: 'forgotpassword', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/help/help.module#HelpPageModule', name: 'HelpPage', segment: 'help', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invitefriend/invitefriend.module#InvitefriendPageModule', name: 'InvitefriendPage', segment: 'invitefriend', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/modal/modal.module#ModalPageModule', name: 'ModalPage', segment: 'modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notification/notification.module#NotificationPageModule', name: 'NotificationPage', segment: 'notification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/offer/offer.module#OfferPageModule', name: 'OfferPage', segment: 'offer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/popover/popover.module#PopoverPageModule', name: 'PopoverPage', segment: 'popover', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resetpassword/resetpassword.module#ResetpasswordPageModule', name: 'ResetpasswordPage', segment: 'resetpassword', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/security/security.module#SecurityPageModule', name: 'SecurityPage', segment: 'security', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/shop/shop.module#ShopPageModule', name: 'ShopPage', segment: 'shop', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/newaddress/newaddress.module#NewaddressPageModule', name: 'NewaddressPage', segment: 'newaddress', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting.module#SettingPageModule', name: 'SettingPage', segment: 'setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signin/signin.module#SigninPageModule', name: 'SigninPage', segment: 'signin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/address/address.module#AddressPageModule', name: 'AddressPage', segment: 'address', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/myaccount/myaccount.module#MyaccountPageModule', name: 'MyaccountPage', segment: 'myaccount', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map/map.module#MapPageModule', name: 'MapPage', segment: 'map', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_16__pages_myaccount_myaccount_module__["MyaccountPageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_signin_signin_module__["SigninPageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_signup_signup_module__["a" /* SignupPageModule */],
                __WEBPACK_IMPORTED_MODULE_24__pages_address_address_module__["AddressPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__pages_newaddress_newaddress_module__["NewaddressPageModule"],
                __WEBPACK_IMPORTED_MODULE_12__pages_forgotpassword_forgotpassword_module__["ForgotpasswordPageModule"],
                __WEBPACK_IMPORTED_MODULE_14__pages_resetpassword_resetpassword_module__["ResetpasswordPageModule"],
                __WEBPACK_IMPORTED_MODULE_28__pages_contact_contact_module__["ContactPageModule"],
                __WEBPACK_IMPORTED_MODULE_34__pages_notification_notification_module__["NotificationPageModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_security_security_module__["SecurityPageModule"],
                __WEBPACK_IMPORTED_MODULE_38__pages_shop_shop_module__["ShopPageModule"],
                __WEBPACK_IMPORTED_MODULE_40__pages_help_help_module__["HelpPageModule"],
                __WEBPACK_IMPORTED_MODULE_42__pages_offer_offer_module__["OfferPageModule"],
                __WEBPACK_IMPORTED_MODULE_30__pages_contactus_contactus_module__["ContactusPageModule"],
                __WEBPACK_IMPORTED_MODULE_36__pages_setting_setting_module__["SettingPageModule"],
                __WEBPACK_IMPORTED_MODULE_26__pages_map_map_module__["MapPageModule"],
                __WEBPACK_IMPORTED_MODULE_44__pages_modal_modal_module__["ModalPageModule"],
                __WEBPACK_IMPORTED_MODULE_46__pages_acceptsupport_acceptsupport_module__["AcceptsupportPageModule"],
                __WEBPACK_IMPORTED_MODULE_67__pages_popover_popover_module__["PopoverPageModule"],
                __WEBPACK_IMPORTED_MODULE_70__pages_invitefriend_invitefriend_module__["InvitefriendPageModule"],
                __WEBPACK_IMPORTED_MODULE_72__pages_aboutus_aboutus_module__["AboutusPageModule"],
                __WEBPACK_IMPORTED_MODULE_61__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyAlJZd_87izwp5nfqW1hoqbSwAWmX8X4Cc',
                    libraries: ['places']
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_myaccount_myaccount__["a" /* MyaccountPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_signin_signin__["a" /* SigninPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_address_address__["a" /* AddressPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_newaddress_newaddress__["a" /* NewaddressPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_forgotpassword_forgotpassword__["a" /* ForgotpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_resetpassword_resetpassword__["a" /* ResetpasswordPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_notification_notification__["a" /* NotificationPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_security_security__["a" /* SecurityPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_shop_shop__["a" /* ShopPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_offer_offer__["a" /* OfferPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_contactus_contactus__["a" /* ContactusPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_map_map__["a" /* MapPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_modal_modal__["a" /* ModalPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_acceptsupport_acceptsupport__["a" /* AcceptsupportPage */],
                __WEBPACK_IMPORTED_MODULE_66__pages_popover_popover__["a" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_69__pages_invitefriend_invitefriend__["a" /* InvitefriendPage */],
                __WEBPACK_IMPORTED_MODULE_71__pages_aboutus_aboutus__["a" /* AboutusPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_plus__["a" /* GooglePlus */],
                __WEBPACK_IMPORTED_MODULE_54__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_1__ionic_native_device__["a" /* Device */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_native_storage__["a" /* NativeStorage */],
                __WEBPACK_IMPORTED_MODULE_50__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_51__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_51__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_52__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_47__ionic_native_email_composer__["a" /* EmailComposer */],
                __WEBPACK_IMPORTED_MODULE_58__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_53__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_65__ionic_native_crop__["a" /* Crop */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_62__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_63__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_55__ionic_native_diagnostic__["a" /* Diagnostic */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_48__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_60__providers_google_service_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_56__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_57__ngx_translate_core__["c" /* TranslateService */],
                __WEBPACK_IMPORTED_MODULE_64__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
                __WEBPACK_IMPORTED_MODULE_61__agm_core__["b" /* AgmMap */],
                __WEBPACK_IMPORTED_MODULE_68__providers_get_user_address_get_user_address__["a" /* GetUserAddressProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forgotpassword_forgotpassword__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__newaddress_newaddress__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_plus__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_native_storage__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_diagnostic__ = __webpack_require__(267);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Signin page (This page validate user credentials and send to api for login)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 














var SigninPage = /** @class */ (function () {
    function SigninPage(navCtrl, toastCtrl, googlePlus, translate, nativeStorage, geolocation, loadingCtrl, alertCtrl, menu, AuthServiceProvider, events, browser, diagnostic) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.googlePlus = googlePlus;
        this.translate = translate;
        this.nativeStorage = nativeStorage;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.AuthServiceProvider = AuthServiceProvider;
        this.events = events;
        this.browser = browser;
        this.diagnostic = diagnostic;
        this.amap = { 'lat': 0, 'lng': 0, 'zoom': 15 };
        this.form = new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["b" /* FormGroup */]({
            username: new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_11__angular_forms__["g" /* Validators */].required),
            password: new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_11__angular_forms__["g" /* Validators */].required)
        });
        this.userData = { "username": "", "password": "", "platform": "" };
        this.Googlelogout();
        this.AGMloadMap();
        localStorage.setItem('userFBData', "");
        localStorage.setItem('userGoogleData', "");
    }
    Object.defineProperty(SigninPage.prototype, "username", {
        //get user data from form field
        get: function () {
            return this.form.get('username');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SigninPage.prototype, "password", {
        get: function () {
            return this.form.get('password');
        },
        enumerable: true,
        configurable: true
    });
    //logout from facebook
    // FBlogout() {
    //   this.facebook.logout()
    //     .then(res => this.isLoggedIn = false)
    //     .catch(e => console.log('Error logout from Facebook', e));
    // }
    //logout from google
    SigninPage.prototype.Googlelogout = function () {
        var _this = this;
        this.googlePlus.logout()
            .then(function (res) {
            console.log(res);
            _this.displayName = "";
            _this.email = "";
            _this.familyName = "";
            _this.givenName = "";
            _this.userId = "";
            _this.imageUrl = "";
            _this.isLoggedIn = false;
        })
            .catch(function (err) { return console.error(err); });
    };
    //alert controller for if the user deactivated
    SigninPage.prototype.actAlert = function () {
        var trans_btn_ok = '';
        this.translate.get('ok').subscribe(function (res) {
            trans_btn_ok = res;
        });
        this.activationAlert = this.alertCtrl.create({
            title: 'Deactivated',
            message: 'Your account is deactivated Please contact Nabosupport administrator.',
            buttons: [
                {
                    text: trans_btn_ok,
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        this.activationAlert.present();
    };
    //alert for FCM token not set
    SigninPage.prototype.ndiAlert = function () {
        var trans_btn_ok = '';
        var trans_title_fail = '';
        this.translate.get('ok').subscribe(function (res) {
            trans_btn_ok = res;
        });
        this.translate.get('Failed').subscribe(function (res) {
            trans_title_fail = res;
        });
        this.activationAlert = this.alertCtrl.create({
            title: trans_title_fail,
            message: 'Network error please try later.',
            buttons: [
                {
                    text: trans_btn_ok,
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        this.activationAlert.present();
    };
    //Toast contrlloer
    SigninPage.prototype.toastViewer = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    //loading controller
    SigninPage.prototype.loadingViewer = function (content) {
        var loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: content
        });
        return loading;
    };
    //JSON concodination
    SigninPage.prototype.jsonConcat = function (o1, o2) {
        for (var key in o2) {
            o1[key] = o2[key];
        }
        return o1;
    };
    //get user data from api
    SigninPage.prototype.getUser = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(userdata, 'getUser').then(function (result) {
            console.log(result);
            _this.userAllData = result[0];
            console.log(_this.userAllData);
            localStorage.setItem('userAllData', JSON.stringify(_this.userAllData));
        }, function (err) {
            console.log(err);
        });
    };
    //update user location when they login location
    SigninPage.prototype.updateLocation = function () {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        this.options = {
            enableHighAccuracy: true
        };
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            _this.currentPos = pos;
            var coord = new Array();
            coord.push(pos.coords.latitude, pos.coords.longitude);
            console.log(coord[0] + ',' + coord[1]);
            _this.userData = JSON.parse(localStorage.getItem('userData'));
            var locationData = {
                "user_id": _this.userData['user_id'],
                "lat": coord[0],
                "lang": coord[1]
            };
            _this.AuthServiceProvider.postData(locationData, 'updateUserLocation').then(function (result) {
                console.log(result);
            }, function (err) {
                console.log(err);
            });
        }, function (err) {
            console.log("error : " + err.message);
        });
    };
    SigninPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    SigninPage.prototype.ionViewDidLeave = function () {
        this.menu.swipeEnable(true);
    };
    //Facebook login
    // FBlogin() {
    //   this.getUserPosition();
    //   this.facebook.login(['public_profile', 'user_friends', 'email'])
    //     .then(res => {
    //       if (res.status === "connected") {
    //         this.isLoggedIn = true;
    //         this.getUserDetail(res.authResponse.userID);
    //       } else {
    //         this.isLoggedIn = false;
    //       }
    //     })
    //     .catch(e => {
    //       console.log('Error logging into Facebook', e);
    //     });
    // }
    //get user details
    // getUserDetail(userid) {
    //   this.facebook.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
    //     .then(res => {
    //       console.log(res);
    //       this.users = res;
    //       localStorage.setItem('userFBData', JSON.stringify(this.users));
    //       this.userFBData = JSON.parse(localStorage.getItem('userFBData'));
    //       console.log(this.userFBData);
    //       this.userallData = {};
    //       this.userallData = this.jsonConcat(this.locdata, this.userFBData);
    //       this.platformname = localStorage.getItem('paltform');
    //       this.userallData.platform = this.platformname;
    //       console.log(this.userallData);
    //       if (this.userallData.email) {
    //         this.AuthServiceProvider.postData(this.userallData, 'FBlogin').then((result) => {
    //           this.responseData = result;
    //           if (this.responseData.status == true) {
    //             console.log(this.responseData);
    //             this.toastmessage = 'Login successfully';
    //             this.toastViewer(this.toastmessage);
    //             localStorage.setItem('userData', JSON.stringify(this.responseData));
    //             this.fbsuccess();
    //           } else if(this.responseData.status == false) {
    //             this.toastmessage = 'Login Failed Please Re-try';
    //             this.toastViewer(this.toastmessage);
    //           } else if('deactive' == this.responseData.status ) {
    //             this.actAlert();
    //           } else {
    //             this.toastmessage = 'Login Failed';
    //             this.toastViewer(this.toastmessage);
    //           }
    //         }, (err) => {
    //           // Error log
    //         });
    //       } else {
    //         this.toastmessage = 'Unverified user details';
    //         this.toastViewer(this.toastmessage);
    //       }
    //     });
    // }
    //login function 
    SigninPage.prototype.signin = function () {
        var _this = this;
        if (this.userData.username && this.userData.password) {
            //this.userData['deviceId']='cxHIRxjGUdE:APA91bGJqzhwsBbhxDLnaBXwcl7PLHZu3fDbm9pZ4QMN1kzRiJP5MFRAksg37aySQvhHoKfHTaDlt80AreZIeP9JG5MJc0AYGWAM1v3kT58sQsEgxZO74RdwNAaVazG2PZPlkKqE9m4M';
            this.userData['deviceId'] = localStorage.getItem('deviceID');
            this.platformname = localStorage.getItem('paltform');
            this.userData.platform = this.platformname;
            this.AuthServiceProvider.postData(this.userData, 'login').then(function (result) {
                _this.responseData = result;
                var trans_auth = '';
                _this.translate.get('Authenticating...').subscribe(function (res) {
                    trans_auth = res;
                });
                _this.loadercontent = trans_auth;
                var loader = _this.loadingViewer(_this.loadercontent);
                loader.present();
                if (true == _this.responseData.status) {
                    loader.dismiss();
                    console.log(_this.responseData.data[0]);
                    localStorage.setItem('userData', JSON.stringify(_this.responseData.data[0]));
                    localStorage.setItem('session', '_logged_in');
                    var data = { "type": "loggedin", "lang": _this.responseData.data[0].language };
                    _this.getUserPosition();
                    _this.AuthServiceProvider.getData('getAdminData').then(function (result) {
                        _this.responseData = result;
                        console.log(_this.responseData);
                        localStorage.setItem('adminData', JSON.stringify(_this.responseData));
                        var langData = JSON.parse(localStorage.getItem('userData'));
                        _this.events.publish('user:changed', data);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
                    });
                }
                else if ('first' == _this.responseData.status) {
                    localStorage.setItem('userData', JSON.stringify(_this.responseData.data[0]));
                    loader.dismiss();
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__newaddress_newaddress__["a" /* NewaddressPage */]);
                }
                else if (false == _this.responseData.status) {
                    loader.dismiss();
                    _this.translate.get('Invalid email or password').subscribe(function (res) {
                        _this.toastmessage = res;
                    });
                    _this.toastViewer(_this.toastmessage);
                }
                else if ('deactive' == _this.responseData.status) {
                    loader.dismiss();
                    console.log('deactive');
                    _this.actAlert();
                }
                else if ('NDI' == _this.responseData.status) {
                    loader.dismiss();
                    console.log('no device id');
                    _this.ndiAlert();
                }
                else {
                    loader.dismiss();
                    _this.translate.get('Invalid email or password').subscribe(function (res) {
                        _this.toastmessage = res;
                    });
                    _this.toastViewer(_this.toastmessage);
                }
            }, function (err) {
                // Error log
            });
        }
        else {
            if (!(this.userData.username) && !(this.userData.password)) {
                this.toastmessage = 'Please provide your Email & password';
                this.toastViewer(this.toastmessage);
            }
            else {
                if (((this.userData.username) || !(this.userData.password))) {
                    this.toastmessage = 'Please provide your password';
                    this.toastViewer(this.toastmessage);
                }
                else {
                    this.toastmessage = 'Please provide your Email';
                    this.toastViewer(this.toastmessage);
                }
            }
        }
    };
    //google login start
    SigninPage.prototype.doGoogleLogin = function () {
        var _this = this;
        this.getUserPosition();
        this.googlePlus.login({
            'offline': true
        }).then(function (res) {
            console.log(res);
            _this.displayName = res.displayName;
            _this.email = res.email;
            _this.familyName = res.familyName;
            _this.givenName = res.givenName;
            _this.userId = res.userId;
            _this.imageUrl = res.imageUrl;
            localStorage.setItem('userGoogleData', JSON.stringify(res));
            _this.userGoogleData = JSON.parse(localStorage.getItem('userGoogleData'));
            console.log(_this.userFBData);
            _this.userallData = {};
            _this.userallData = _this.jsonConcat(_this.locdata, _this.userGoogleData);
            _this.platformname = localStorage.getItem('paltform');
            _this.userallData.platform = _this.platformname;
            console.log(_this.userallData);
            if (_this.userallData.email) {
                _this.AuthServiceProvider.postData(_this.userallData, 'FBlogin').then(function (result) {
                    _this.responseData = result;
                    console.log(_this.responseData.status);
                    if (_this.responseData.status == true) {
                        console.log(_this.responseData);
                        _this.toastmessage = 'login successfully.';
                        _this.toastViewer(_this.toastmessage);
                        localStorage.setItem('session', '_logged_in');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
                        localStorage.setItem('userData', JSON.stringify(_this.responseData));
                        _this.fbsuccess();
                    }
                    else if (_this.responseData.status == false) {
                        _this.toastmessage = 'Login Failed Please Re-try.';
                        _this.toastViewer(_this.toastmessage);
                    }
                    else if ('deactive' == _this.responseData.status) {
                        _this.actAlert();
                    }
                    else {
                        _this.toastmessage = 'Login Failed.';
                        _this.toastViewer(_this.toastmessage);
                    }
                }, function (err) {
                    // Error log
                });
            }
            else {
                _this.toastmessage = 'Unverified user details.';
                _this.toastViewer(_this.toastmessage);
            }
        });
        // .catch(err => console.error(err));
    };
    //forgotpassword page link
    SigninPage.prototype.forgot = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__forgotpassword_forgotpassword__["a" /* ForgotpasswordPage */]);
    };
    //redirected to Nabosupport web
    SigninPage.prototype.webpage = function () {
        var _this = this;
        var trans_btn_yes = '';
        var trans_btn_no = '';
        var alerttitle = '';
        var alerttxt = '';
        this.translate.get('Yes').subscribe(function (res) {
            trans_btn_yes = res;
        });
        this.translate.get('No').subscribe(function (res) {
            trans_btn_no = res;
        });
        this.translate.get('forgot link title').subscribe(function (res) {
            alerttitle = res;
        });
        this.translate.get('offer alert text').subscribe(function (res) {
            alerttxt = res;
        });
        var alert = this.alertCtrl.create({
            title: alerttitle,
            message: alerttxt,
            buttons: [
                {
                    text: trans_btn_yes,
                    role: 'cancel',
                    handler: function () {
                        console.log('Yes clicked');
                        //let openBrowser = this.browser.create('http://rayi.in/nabosupport/'+this.offerURL, '_system','location=no');
                        var openBrowser = _this.browser.create('http://nabosupport.dk/min-konto/lost-password/', '_system', 'location=no');
                    }
                },
                {
                    text: trans_btn_no,
                    handler: function () {
                        console.log('No clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    //signup page link
    SigninPage.prototype.signup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
    };
    //page loading start
    SigninPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SigninPage');
        this.getUserPosition();
        console.log(localStorage.getItem('userData'));
        var session = localStorage.getItem('session');
        if (session === '_logged_in') {
            console.log("log2map");
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
        }
        else {
            console.log("log");
        }
    };
    //load agm map geocode
    SigninPage.prototype.AGMloadMap = function () {
        var _this = this;
        this.options = {
            enableHighAccuracy: true
        };
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            console.log(pos);
            _this.amap = {
                'lat': pos.coords.latitude,
                'lng': pos.coords.longitude,
                'zoom': 15
            };
        });
    };
    SigninPage.prototype.fbsuccess = function () {
        localStorage.setItem('session', '_logged_in');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__map_map__["a" /* MapPage */]);
    };
    //get user current location
    SigninPage.prototype.getUserPosition = function () {
        var _this = this;
        var acctxt = '';
        this.translate.get('turn on gps to high accurecy').subscribe(function (res) {
            acctxt = res;
        });
        var geocoder = new google.maps.Geocoder();
        this.options = {
            enableHighAccuracy: true
        };
        this.diagnostic.getLocationMode()
            .then(function (state) {
            if (state == _this.diagnostic.locationMode.BATTERY_SAVING) {
                alert(acctxt);
            }
            else if (state == _this.diagnostic.locationMode.DEVICE_ONLY) {
                alert(acctxt);
            }
            else if (state == _this.diagnostic.locationMode.LOCATION_OFF) {
                alert(acctxt);
            }
            else if (state == _this.diagnostic.locationMode.HIGH_ACCURACY) {
                _this.geolocation.getCurrentPosition(_this.options).then(function (pos) {
                    _this.deviceID = localStorage.getItem('deviceID');
                    _this.currentPos = pos;
                    var coord = new Array();
                    coord.push(pos.coords.latitude, pos.coords.longitude);
                    var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    var request = { latLng: latlng };
                    geocoder.geocode(request, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var result = results[0];
                            var rsltAdrComponent = result.address_components;
                            var resultLength = rsltAdrComponent.length;
                            if (result != null) {
                                var data = new Array;
                                var locationdata = new Array();
                                for (var i = 0; i < rsltAdrComponent.length; i++) {
                                    var obj = rsltAdrComponent[i];
                                    data.push(obj.long_name);
                                }
                                if (data.length == 5) {
                                    _this.locdata = {
                                        "street_address": data[1] + " " + data[0],
                                        "city": data[2],
                                        "country": data[3],
                                        "zipcode": data[4],
                                        "latitude": coord[0],
                                        "longitude": coord[1],
                                        "is_active": 1,
                                        "status_id": 0,
                                        "alert_type": "N",
                                        "distance": 10,
                                        "deviceID": _this.deviceID
                                    };
                                    console.log(_this.locdata);
                                }
                                else if (data.length == 6) {
                                    _this.locdata = {
                                        "street_address": data[1] + " " + data[0],
                                        "city": data[2],
                                        "country": data[4],
                                        "zipcode": data[5],
                                        "latitude": coord[0],
                                        "longitude": coord[1],
                                        "is_active": 1,
                                        "status_id": 0,
                                        "alert_type": "N",
                                        "distance": 10,
                                        "deviceID": _this.deviceID
                                    };
                                }
                                else if (data.length == 7) {
                                    _this.locdata = {
                                        "street_address": data[1] + " " + data[0],
                                        "city": data[3],
                                        "country": data[5],
                                        "zipcode": data[6],
                                        "latitude": coord[0],
                                        "longitude": coord[1],
                                        "is_active": 1,
                                        "status_id": 0,
                                        "alert_type": "N",
                                        "distance": 10,
                                        "deviceID": _this.deviceID
                                    };
                                    console.log(_this.locdata);
                                }
                                else if (data.length == 8) {
                                    _this.locdata = {
                                        "street_address": data[1] + " " + data[0],
                                        "city": data[4],
                                        "country": data[6],
                                        "zipcode": data[7],
                                        "latitude": coord[0],
                                        "longitude": coord[1],
                                        "is_active": 1,
                                        "status_id": 0,
                                        "alert_type": "N",
                                        "distance": 10,
                                        "deviceID": _this.deviceID
                                    };
                                }
                                else if (data.length == 9) {
                                    _this.locdata = {
                                        "street_address": data[1] + " " + data[0],
                                        "city": data[4],
                                        "country": data[7],
                                        "zipcode": data[8],
                                        "latitude": coord[0],
                                        "longitude": coord[1],
                                        "is_active": 1,
                                        "status_id": 0,
                                        "alert_type": "N",
                                        "distance": 10,
                                        "deviceID": _this.deviceID
                                    };
                                }
                                else if (data.length == 10) {
                                    _this.locdata = {
                                        "street_address": data[1] + " " + data[0],
                                        "city": data[5],
                                        "country": data[8],
                                        "zipcode": data[9],
                                        "latitude": coord[0],
                                        "longitude": coord[1],
                                        "is_active": 1,
                                        "status_id": 0,
                                        "alert_type": "N",
                                        "distance": 10,
                                        "deviceID": _this.deviceID
                                    };
                                }
                                console.log(_this.locdata);
                                return _this.locdata;
                            }
                            else {
                                var trans_no_add_1 = '';
                                _this.translate.get("No address available").subscribe(function (res) {
                                    trans_no_add_1 = res;
                                });
                                alert(trans_no_add_1);
                            }
                        }
                    });
                }).catch(function (e) { return console.error(e); });
            }
        });
    };
    SigninPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-signin',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/signin/signin.html"*/'<ion-content class="signin" padding center text-center>\n  <img src="assets/imgs/logo.png">\n  <!-- signin inputs field starts-->\n  <ion-list>\n    <form [formGroup]="form"  (ngSubmit)="signin()">\n      <ion-item>\n          <ion-thumbnail item-start class="icn">\n              <ion-icon name="ios-contact"></ion-icon> \n            </ion-thumbnail> \n        <ion-label floating>\n          {{\'Email id\' | translate}}</ion-label>\n        <ion-input formControlName="username" type="text" [(ngModel)]="userData.username" ></ion-input>\n      </ion-item>\n     \n      <ion-item>\n          <ion-thumbnail item-start class="icn">\n              <ion-icon name="lock"></ion-icon> \n            </ion-thumbnail> \n        <ion-label floating>\n           {{\'Password\' | translate}}</ion-label>\n        <ion-input formControlName="password" type="password" [(ngModel)]="userData.password" ></ion-input>\n      </ion-item>\n     \n      <button type="submit" class="sign_btn" ion-button  round full>{{\'Sign In\' | translate}}</button>\n    \n    </form>\n    <a href="#" (click)="webpage();">{{\'FORGOT PASSWORD ?\' | translate}}</a>\n    <ion-item>\n      <p id="signup-lbl">{{\'signuplink\' | translate}}</p>\n    </ion-item>\n  </ion-list>\n  \n  <!-- following option will be enabled when it need so it commented-->\n  <!-- #####signin inputs field Ends####-->\n  <!-- <span class="acc">{{"DON\'T HAVE AN ACCOUNT ?" | translate}}</span>\n  <a href="#" (click)="signup()">{{\'SIGN UP\' | translate}}</a> -->\n  <!-- <a href="#" (click)="account()">account</a> -->\n\n\n  <!-- facebook, google  login -->\n  <!-- <ion-list padding class="social">\n    <P>{{\'OR SIGN UP WITH\' | translate}}</P>\n     <button ion-button class="sign_btn g" round icon-start (click)="doGoogleLogin()">\n        <ion-icon name="logo-googleplus"></ion-icon>\n      </button>  \n      <button ion-button class="sign_btn f"  icon-right   icon-start round (click)="FBlogin()"><ion-icon name="logo-facebook"></ion-icon></button>\n       -->\n       <!-- <ion-card *ngIf="users">\n         <ion-card-header>{{ users.username }}</ion-card-header>\n         <img [src]="users.picture" />\n        \n           <p>Email: {{ users.email }}</p>\n           <p>First Name: {{ users.first_name }}</p>\n       </ion-card> \n    <ng-template #facebookLogin>\n        <button ion-button class="sign_btn"  icon-right   icon-start (click)="login()">\n          login\n          <ion-icon name="logo-facebook"></ion-icon>\n        </button>\n      </ng-template> -->\n    \n  <!-- </ion-list> -->\n<!--#### facebook, google  login end -->\n\n\n</ion-content>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/signin/signin.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_plus__["a" /* GooglePlus */],
            __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_10__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_diagnostic__["a" /* Diagnostic */]])
    ], SigninPage);
    return SigninPage;
}());

//# sourceMappingURL=signin.js.map

/***/ }),

/***/ 767:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 277,
	"./af.js": 277,
	"./ar": 278,
	"./ar-dz": 279,
	"./ar-dz.js": 279,
	"./ar-kw": 280,
	"./ar-kw.js": 280,
	"./ar-ly": 281,
	"./ar-ly.js": 281,
	"./ar-ma": 282,
	"./ar-ma.js": 282,
	"./ar-sa": 283,
	"./ar-sa.js": 283,
	"./ar-tn": 284,
	"./ar-tn.js": 284,
	"./ar.js": 278,
	"./az": 285,
	"./az.js": 285,
	"./be": 286,
	"./be.js": 286,
	"./bg": 287,
	"./bg.js": 287,
	"./bm": 288,
	"./bm.js": 288,
	"./bn": 289,
	"./bn.js": 289,
	"./bo": 290,
	"./bo.js": 290,
	"./br": 291,
	"./br.js": 291,
	"./bs": 292,
	"./bs.js": 292,
	"./ca": 293,
	"./ca.js": 293,
	"./cs": 294,
	"./cs.js": 294,
	"./cv": 295,
	"./cv.js": 295,
	"./cy": 296,
	"./cy.js": 296,
	"./da": 297,
	"./da.js": 297,
	"./de": 298,
	"./de-at": 299,
	"./de-at.js": 299,
	"./de-ch": 300,
	"./de-ch.js": 300,
	"./de.js": 298,
	"./dv": 301,
	"./dv.js": 301,
	"./el": 302,
	"./el.js": 302,
	"./en-au": 303,
	"./en-au.js": 303,
	"./en-ca": 304,
	"./en-ca.js": 304,
	"./en-gb": 305,
	"./en-gb.js": 305,
	"./en-ie": 306,
	"./en-ie.js": 306,
	"./en-il": 307,
	"./en-il.js": 307,
	"./en-nz": 308,
	"./en-nz.js": 308,
	"./eo": 309,
	"./eo.js": 309,
	"./es": 310,
	"./es-do": 311,
	"./es-do.js": 311,
	"./es-us": 312,
	"./es-us.js": 312,
	"./es.js": 310,
	"./et": 313,
	"./et.js": 313,
	"./eu": 314,
	"./eu.js": 314,
	"./fa": 315,
	"./fa.js": 315,
	"./fi": 316,
	"./fi.js": 316,
	"./fo": 317,
	"./fo.js": 317,
	"./fr": 318,
	"./fr-ca": 319,
	"./fr-ca.js": 319,
	"./fr-ch": 320,
	"./fr-ch.js": 320,
	"./fr.js": 318,
	"./fy": 321,
	"./fy.js": 321,
	"./gd": 322,
	"./gd.js": 322,
	"./gl": 323,
	"./gl.js": 323,
	"./gom-latn": 324,
	"./gom-latn.js": 324,
	"./gu": 325,
	"./gu.js": 325,
	"./he": 326,
	"./he.js": 326,
	"./hi": 327,
	"./hi.js": 327,
	"./hr": 328,
	"./hr.js": 328,
	"./hu": 329,
	"./hu.js": 329,
	"./hy-am": 330,
	"./hy-am.js": 330,
	"./id": 331,
	"./id.js": 331,
	"./is": 332,
	"./is.js": 332,
	"./it": 333,
	"./it.js": 333,
	"./ja": 334,
	"./ja.js": 334,
	"./jv": 335,
	"./jv.js": 335,
	"./ka": 336,
	"./ka.js": 336,
	"./kk": 337,
	"./kk.js": 337,
	"./km": 338,
	"./km.js": 338,
	"./kn": 339,
	"./kn.js": 339,
	"./ko": 340,
	"./ko.js": 340,
	"./ky": 341,
	"./ky.js": 341,
	"./lb": 342,
	"./lb.js": 342,
	"./lo": 343,
	"./lo.js": 343,
	"./lt": 344,
	"./lt.js": 344,
	"./lv": 345,
	"./lv.js": 345,
	"./me": 346,
	"./me.js": 346,
	"./mi": 347,
	"./mi.js": 347,
	"./mk": 348,
	"./mk.js": 348,
	"./ml": 349,
	"./ml.js": 349,
	"./mn": 350,
	"./mn.js": 350,
	"./mr": 351,
	"./mr.js": 351,
	"./ms": 352,
	"./ms-my": 353,
	"./ms-my.js": 353,
	"./ms.js": 352,
	"./mt": 354,
	"./mt.js": 354,
	"./my": 355,
	"./my.js": 355,
	"./nb": 356,
	"./nb.js": 356,
	"./ne": 357,
	"./ne.js": 357,
	"./nl": 358,
	"./nl-be": 359,
	"./nl-be.js": 359,
	"./nl.js": 358,
	"./nn": 360,
	"./nn.js": 360,
	"./pa-in": 361,
	"./pa-in.js": 361,
	"./pl": 362,
	"./pl.js": 362,
	"./pt": 363,
	"./pt-br": 364,
	"./pt-br.js": 364,
	"./pt.js": 363,
	"./ro": 365,
	"./ro.js": 365,
	"./ru": 366,
	"./ru.js": 366,
	"./sd": 367,
	"./sd.js": 367,
	"./se": 368,
	"./se.js": 368,
	"./si": 369,
	"./si.js": 369,
	"./sk": 370,
	"./sk.js": 370,
	"./sl": 371,
	"./sl.js": 371,
	"./sq": 372,
	"./sq.js": 372,
	"./sr": 373,
	"./sr-cyrl": 374,
	"./sr-cyrl.js": 374,
	"./sr.js": 373,
	"./ss": 375,
	"./ss.js": 375,
	"./sv": 376,
	"./sv.js": 376,
	"./sw": 377,
	"./sw.js": 377,
	"./ta": 378,
	"./ta.js": 378,
	"./te": 379,
	"./te.js": 379,
	"./tet": 380,
	"./tet.js": 380,
	"./tg": 381,
	"./tg.js": 381,
	"./th": 382,
	"./th.js": 382,
	"./tl-ph": 383,
	"./tl-ph.js": 383,
	"./tlh": 384,
	"./tlh.js": 384,
	"./tr": 385,
	"./tr.js": 385,
	"./tzl": 386,
	"./tzl.js": 386,
	"./tzm": 387,
	"./tzm-latn": 388,
	"./tzm-latn.js": 388,
	"./tzm.js": 387,
	"./ug-cn": 389,
	"./ug-cn.js": 389,
	"./uk": 390,
	"./uk.js": 390,
	"./ur": 391,
	"./ur.js": 391,
	"./uz": 392,
	"./uz-latn": 393,
	"./uz-latn.js": 393,
	"./uz.js": 392,
	"./vi": 394,
	"./vi.js": 394,
	"./x-pseudo": 395,
	"./x-pseudo.js": 395,
	"./yo": 396,
	"./yo.js": 396,
	"./zh-cn": 397,
	"./zh-cn.js": 397,
	"./zh-hk": 398,
	"./zh-hk.js": 398,
	"./zh-tw": 399,
	"./zh-tw.js": 399
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 767;

/***/ }),

/***/ 789:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_signin_signin__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_address_address__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_help_help__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_offer_offer__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_plus__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_device__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_geolocation__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_network__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_app_version__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_acceptsupport_acceptsupport__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_map_map__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_myaccount_myaccount__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_contact_contact__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_security_security__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_notification_notification__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_invitefriend_invitefriend__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_aboutus_aboutus__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_js_base64__ = __webpack_require__(790);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_js_base64___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25_js_base64__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26_ontime__ = __webpack_require__(795);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26_ontime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_26_ontime__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



























var MyApp = /** @class */ (function () {
    //rootPage:any;
    function MyApp(platform, device, toastCtrl, statusBar, AuthServiceProvider, googlePlus, splashScreen, push, alertCtrl, translate, events, menuCtrl, iab, zone, geolocation, network, appVersion, config, modalCtrl) {
        var _this = this;
        this.platform = platform;
        this.device = device;
        this.toastCtrl = toastCtrl;
        this.AuthServiceProvider = AuthServiceProvider;
        this.googlePlus = googlePlus;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.events = events;
        this.menuCtrl = menuCtrl;
        this.iab = iab;
        this.zone = zone;
        this.geolocation = geolocation;
        this.network = network;
        this.appVersion = appVersion;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_17__pages_map_map__["a" /* MapPage */];
        this._menustate = true;
        this.lang = 'en';
        this.viewSub = false;
        this.langData = localStorage.getItem('userData');
        if (this.langData === '' || this.langData === undefined || this.langData === null) {
            this.lang = 'dn';
            this.initTranslate();
            translate.use(this.lang);
            console.log('selected language is : ' + this.lang);
        }
        else if (this.langData) {
            this.langData = JSON.parse(this.langData);
            this.lang = this.langData.language;
            console.log('selected language is : ' + this.lang);
            this.initTranslate();
            translate.use(this.lang);
        }
        this.getSettings();
        this.events.subscribe('user:changed', function (user) {
            _this.user = user;
            console.log('this from ap comp:' + JSON.stringify(_this.user));
            _this.initAutoPull();
            _this.lang = _this.user.lang;
            console.log('component refresh' + 'lang is' + _this.lang);
            _this.initTranslate();
            translate.use(_this.lang);
        });
        localStorage.removeItem('onetimepush');
        localStorage.removeItem('timeCheck');
        localStorage.removeItem('sendtype');
        var vData = localStorage.getItem('victimData');
        if (vData === 'undefined' || vData === '') {
            localStorage.removeItem('victimData');
        }
        console.log("platform=" + this.device.platform);
        var plat = this.device.platform;
        localStorage.setItem('paltform', plat);
        this.pages = [
            { title: 'Map', component: __WEBPACK_IMPORTED_MODULE_17__pages_map_map__["a" /* MapPage */], icon: 'ios-navigate' },
            // { title: 'My Account', component: MyaccountPage, icon: 'ios-contact' },
            { title: 'Notification', component: __WEBPACK_IMPORTED_MODULE_21__pages_notification_notification__["a" /* NotificationPage */], icon: 'ios-notifications' },
        ];
        this.pages_1 = [
            { title: 'My Alarm', component: __WEBPACK_IMPORTED_MODULE_20__pages_security_security__["a" /* SecurityPage */], icon: 'ios-nuclear' },
            // { title: 'Shop', component: ShopPage, icon: 'ios-cart' },
            { title: 'Help', component: __WEBPACK_IMPORTED_MODULE_2__pages_help_help__["a" /* HelpPage */], icon: 'ios-help-buoy' },
            { title: 'Offers on Alarm', component: __WEBPACK_IMPORTED_MODULE_3__pages_offer_offer__["a" /* OfferPage */], icon: 'ios-pricetag' },
            // { title: 'User Settings', component: SettingPage, icon: 'settings' },
            { title: 'invite friend', component: __WEBPACK_IMPORTED_MODULE_22__pages_invitefriend_invitefriend__["a" /* InvitefriendPage */], icon: 'ios-paper-plane' },
            { title: 'About us', component: __WEBPACK_IMPORTED_MODULE_23__pages_aboutus_aboutus__["a" /* AboutusPage */], icon: 'ios-information-circle' },
        ];
        this.subpages = [
            { title: 'Addresses', component: __WEBPACK_IMPORTED_MODULE_1__pages_address_address__["a" /* AddressPage */], icon: 'ios-home' },
            { title: 'Supporters', component: __WEBPACK_IMPORTED_MODULE_19__pages_contact_contact__["a" /* ContactPage */], icon: 'ios-contacts' },
            { title: 'Account', component: __WEBPACK_IMPORTED_MODULE_18__pages_myaccount_myaccount__["a" /* MyaccountPage */], icon: 'ios-contact' }
        ];
        platform.ready().then(function () {
            _this.appVersion.getVersionNumber().then(function (version) {
                _this.version = version;
                console.log('app version : ' + _this.version);
            });
            events.subscribe('user:paid', function (paid) {
                console.log('Welcome user was', paid);
                if (paid == 1) {
                    _this._ispaid = false;
                }
                else {
                    _this._ispaid = true;
                }
            });
            if (_this.platform.is('android')) {
                console.log("running on Android device!");
                var plat = 'Android';
                localStorage.setItem('paltform', plat);
            }
            if (_this.platform.is('ios')) {
                console.log("running on iOS device!");
                var plat = 'ios';
                localStorage.setItem('paltform', plat);
            }
            if (_this.platform.is('mobileweb')) {
                console.log("running in a browser on mobile!");
            }
            statusBar.styleDefault();
            splashScreen.hide();
            _this.menuCtrl.swipeEnable(false);
            var lastTimeBackPress = 0;
            var timePeriodToExit = 2000;
            localStorage.setItem('tog', '');
            platform.registerBackButtonAction(function () {
                // get current active page
                if (_this._menustate == false) {
                    _this.menuCtrl.close();
                }
                else {
                    var view = _this.nav.getActive();
                    if (view.component.name == "MapPage") {
                        //Double check to exit app
                        if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
                            _this.platform.exitApp(); //Exit from app
                        }
                        else {
                            var title_1 = '';
                            var text_1 = '';
                            var yes_1 = '';
                            var no_1 = '';
                            _this.translate.get('app exit alert title').subscribe(function (res) {
                                title_1 = res;
                            });
                            _this.translate.get('app exit alert text').subscribe(function (res) {
                                text_1 = res;
                            });
                            _this.translate.get('Yes').subscribe(function (res) {
                                yes_1 = res;
                            });
                            _this.translate.get('No').subscribe(function (res) {
                                no_1 = res;
                            });
                            var alert_1 = _this.alertCtrl.create({
                                title: title_1,
                                message: text_1,
                                buttons: [
                                    {
                                        text: yes_1,
                                        role: 'cancel',
                                        handler: function () {
                                            console.log('Yes clicked');
                                            _this.platform.exitApp();
                                        }
                                    },
                                    {
                                        text: no_1,
                                        handler: function () {
                                            console.log('No clicked');
                                        }
                                    }
                                ]
                            });
                            alert_1.present();
                            lastTimeBackPress = new Date().getTime();
                        }
                    }
                    else if (view.component.name == "SigninPage" || view.component.name == "SignupPage") {
                        // go to previous page
                        //this.nav.pop({});
                        _this.platform.exitApp();
                    }
                    else if (view.component.name == "ForgotpasswordPage") {
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_0__pages_signin_signin__["a" /* SigninPage */]);
                    }
                    else if (view.component.name == "NewaddressPage") {
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_0__pages_signin_signin__["a" /* SigninPage */]);
                    }
                    else {
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_17__pages_map_map__["a" /* MapPage */]);
                    }
                }
            });
            //check app has push notification permission
            _this.push.hasPermission()
                .then(function (res) {
                if (res.isEnabled) {
                    console.log('We have permission to send push notifications');
                }
                else {
                    console.log('We do not have permission to send push notifications');
                }
            });
            localStorage.setItem('deviceID', "");
            var options = {
                android: {
                // SENDER_ID:'357015216809'
                },
                ios: {
                    alert: 'true',
                    badge: true,
                    sound: 'false',
                    fcmSandbox: true
                },
                windows: {},
                browser: {
                    pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                }
            };
            var pushObject = _this.push.init(options);
            pushObject.on('notification').subscribe(function (notification) {
                console.log('Received a notification', notification);
                localStorage.setItem('push', '');
                var titletxt;
                if (_this.platform.is('ios')) {
                    _this.translate.get(notification.additionalData['gcm.notification.tickerText']).subscribe(function (res) {
                        titletxt = res;
                    });
                    titletxt = notification.additionalData['gcm.notification.tickerText'];
                }
                else {
                    _this.translate.get(notification.additionalData.tickerText).subscribe(function (res) {
                        titletxt = res;
                    });
                    titletxt = notification.additionalData.tickerText;
                }
                var msg;
                _this.translate.get(notification.additionalData.subtitle).subscribe(function (res) {
                    msg = res;
                });
                _this.vdata = notification.additionalData['vData'];
                _this.sdata = notification.additionalData['sData'];
                var alert = _this.alertCtrl.create({
                    title: titletxt,
                    message: msg,
                    // message: notification.additionalData.subtitle,
                    buttons: [
                        {
                            text: 'Ok',
                            handler: function () {
                                console.log('called');
                                if (_this.platform.is('ios')) {
                                    localStorage.setItem('lastLat', notification.additionalData['gcm.notification.latitude']);
                                    localStorage.setItem('lastLng', notification.additionalData['gcm.notification.longitude']);
                                    localStorage.setItem('camdata', JSON.stringify(notification.additionalData));
                                    console.log('flag:' + notification.additionalData['gcm.notification.flag']);
                                    if (typeof (notification.additionalData['gcm.notification.user_count']) != undefined) {
                                        localStorage.setItem('user_count', notification.additionalData['gcm.notification.user_count']);
                                    }
                                    if (typeof (notification.additionalData['gcm.notification.user_count']) != undefined) {
                                        localStorage.setItem('user_count', notification.additionalData['gcm.notification.user_count']);
                                    }
                                    if (typeof (notification.additionalData['gcm.notification.flag']) != undefined && notification.additionalData['gcm.notification.flag'] != null) {
                                        var accdata = {
                                            'sender_id': notification.additionalData['gcm.notification.sender_id'],
                                            'username': notification.additionalData['gcm.notification.username'],
                                            'email': notification.additionalData['gcm.notification.email']
                                        };
                                        localStorage.setItem('accData', JSON.stringify(accdata));
                                        _this.supportModal();
                                    }
                                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_17__pages_map_map__["a" /* MapPage */], {
                                        'page': 'appcomp'
                                    });
                                }
                                else {
                                    if (notification.additionalData.flag == 'ASR') {
                                        _this.aData = notification.additionalData['aData'];
                                        localStorage.setItem('accData', JSON.stringify(_this.aData));
                                        _this.supportModal();
                                    }
                                    if (_this.vdata) {
                                        console.log('vdata', _this.vdata);
                                        localStorage.setItem('lastLat', _this.vdata.latitude);
                                        localStorage.setItem('lastLng', _this.vdata.longitude);
                                        if (typeof (_this.vdata.user_count) != undefined) {
                                            console.log('user count is : ' + _this.vdata.user_count);
                                            localStorage.setItem('user_count', _this.vdata.user_count);
                                        }
                                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_17__pages_map_map__["a" /* MapPage */], {
                                            'page': 'appcomp'
                                        });
                                    }
                                    else if (_this.sdata) {
                                        console.log('sdata', _this.sdata);
                                        localStorage.setItem('lastLat', _this.sdata.latitude);
                                        localStorage.setItem('lastLng', _this.sdata.longitude);
                                        localStorage.setItem('camdata', JSON.stringify(_this.sdata));
                                        if (typeof (_this.sdata.user_count) != undefined) {
                                            console.log('user count is : ' + _this.sdata.user_count);
                                            localStorage.setItem('user_count', _this.sdata.user_count);
                                        }
                                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_17__pages_map_map__["a" /* MapPage */], {
                                            'page': 'appcomp'
                                        });
                                    }
                                }
                            }
                        }
                    ],
                    enableBackdropDismiss: false
                });
                alert.present();
                console.log('Received a notification', notification);
            });
            //if(this.device.platform == '')
            pushObject.on('registration').subscribe(function (registration) {
                console.log('Device registered', registration);
                _this.deviceID = {};
                _this.deviceID["deviceid"] = registration['registrationId'];
                //this.deviceID["deviceid"]='cxHIRxjGUdE:APA91bGJqzhwsBbhxDLnaBXwcl7PLHZu3fDbm9pZ4QMN1kzRiJP5MFRAksg37aySQvhHoKfHTaDlt80AreZIeP9JG5MJc0AYGWAM1v3kT58sQsEgxZO74RdwNAaVazG2PZPlkKqE9m4M';
                console.log("deviceid =" + _this.deviceID["deviceid"]);
                localStorage.setItem('deviceID', _this.deviceID["deviceid"]);
                console.log(localStorage.getItem('deviceID'));
            });
            pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin', error); });
        });
    }
    MyApp.prototype.get_user = function () {
        var userdata = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
            localStorage.setItem('userData', JSON.stringify(result[0]));
            console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));
        });
    };
    MyApp.prototype.getCurrentPosition = function () {
        this.options = {
            timeout: 10000, enableHighAccuracy: true, maximumAge: 3600
        };
        this.geolocation.getCurrentPosition(this.options).then(function (pos) {
            console.log('from app comp', pos);
            localStorage.setItem('lastLat', JSON.stringify(pos.coords.latitude));
            localStorage.setItem('lastLng', JSON.stringify(pos.coords.longitude));
        });
    };
    MyApp.prototype.menuClosed = function () {
        //this.events.publish('menu:closed', '');
        console.log('menu closeded');
        this._menustate = true;
    };
    MyApp.prototype.menuOpened = function () {
        //this.events.publish('menu:opened', '');
        console.log('menu opened');
        this._menustate = false;
    };
    MyApp.prototype.openPage = function (page) {
        var _this = this;
        if (page.title === 'Shop') {
            var dynamic_data_1 = JSON.parse(localStorage.getItem('adminData'));
            console.log(dynamic_data_1.data[2].data);
            // dynamic_data[3].data;
            var alert_Bro = this.alertCtrl.create({
                title: 'Shop page will be redirected to browser',
                message: 'Press Yes to Continue',
                buttons: [
                    {
                        text: 'Yes',
                        role: 'cancel',
                        handler: function () {
                            console.log('Yes clicked');
                            var openBrowser = _this.iab.create(dynamic_data_1.data[2].data, 'location=no');
                        }
                    },
                    {
                        text: 'No',
                        handler: function () {
                            console.log('No clicked');
                        }
                    }
                ]
            });
            alert_Bro.present();
        }
        // else if (page.title === 'Offers on alarm') {
        //   let dynamic_data = JSON.parse(localStorage.getItem('adminData'));
        //   // dynamic_data[3].data;
        //   let alert_Bro_offer = this.alertCtrl.create({
        //     title: 'Shop page will be redirected to browser',
        //     message: 'Press Yes to Continue',
        //     buttons: [
        //       {
        //         text: 'Yes',
        //         role: 'cancel',
        //         handler: () => {
        //           console.log('Yes clicked');
        //           let openBrowser = this.iab.create(dynamic_data.data[3].data, 'location=no')
        //         }
        //       },
        //       {
        //         text: 'No',
        //         handler: () => {
        //           console.log('No clicked');
        //         }
        //       }
        //     ]
        //   });
        //   alert_Bro_offer.present();
        // } 
        else {
            this.nav.setRoot(page.component);
        }
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        this.userData = JSON.parse(localStorage.getItem('userData'));
        this.AuthServiceProvider.postData(this.userData, 'logout').then(function (result) {
            _this.responseData = result;
            if (true == _this.responseData.status) {
                console.log(_this.responseData);
                localStorage.setItem('session', '_logged_out');
                localStorage.setItem('userData', "");
                //localStorage.setItem('deviceID', "");
                localStorage.setItem('userAllData', "");
                localStorage.setItem('alertdata', "");
                localStorage.setItem('userFBData', "");
                localStorage.setItem('userGoogleData', "");
                localStorage.setItem('tog', "");
                localStorage.setItem('push', "");
                localStorage.removeItem('victimData');
                localStorage.removeItem('cancel');
                localStorage.removeItem('stop');
                localStorage.removeItem('onetimepush');
                localStorage.removeItem('sdata');
                localStorage.removeItem('lastLat');
                localStorage.removeItem('lastLng');
                localStorage.removeItem('lastAdd');
                localStorage.removeItem('adminData');
                _this.Googlelogout();
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_0__pages_signin_signin__["a" /* SigninPage */]);
            }
            else {
            }
        }, function (err) {
            // Error log
        });
    };
    //check user is paid or not
    MyApp.prototype.checkPaid = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem('userData'));
        console.log(userdata['user_id']);
        var user = {
            'user_id': userdata.user_id,
            'address_id': userdata.address_id
        };
        this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            localStorage.setItem('userData', JSON.stringify(user_detail[0]));
            console.log(user_detail[0].paid_member);
            if (user_detail[0].paid_member === 1) {
                _this._ispaid = false;
            }
            else {
                _this._ispaid = true;
            }
        });
    };
    //redirect to payment 
    MyApp.prototype.upgrade = function () {
        var _this = this;
        console.log('upgrade');
        this.platform.ready().then(function () {
            var userdata = JSON.parse(localStorage.getItem('userData'));
            _this.id = userdata.user_id;
            var payScript = "localStorage.setItem('user_id'," + _this.id + ");";
            var browser = _this.iab.create('http://rayi.in/nabosupport/upgrade', "_blank", "location=no,clearsessioncache=yes");
            //on url load stop
            browser.on("loadstop")
                .subscribe(function (event) {
                browser.executeScript({
                    code: payScript
                });
                console.log("loadstart -->", event);
                _this.logout();
            }, function (err) {
                console.log("InAppBrowser loadstop Event Error: " + err);
            });
            browser.on('exit').subscribe(function () {
                console.log('inapp browser closed');
                _this.checkPaid();
            }, function (err) {
                console.error(err);
            });
        });
    };
    //alert for upgrade success
    MyApp.prototype.upgradeConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm action',
            message: 'Relevant message to be here',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('no clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        console.log('yes clicked');
                        _this.upgrade();
                    }
                }
            ]
        });
        alert.present();
    };
    //language change
    MyApp.prototype.selectLang = function (event) {
        var _this = this;
        console.log(event.srcElement.id);
        this.lang = event.srcElement.id;
        this.translate.use(this.lang);
        var userdata = JSON.parse(localStorage.getItem('userData'));
        var setData = {
            'user_id': userdata.user_id,
            'lang': this.lang
        };
        this.AuthServiceProvider.postData(setData, 'setLang').then(function (rs) {
            console.log(rs);
            _this.responseData = rs;
            if (_this.responseData.status == true) {
                _this.get_user();
            }
        }, function (err) {
            console.log(err);
        });
    };
    //google logout
    MyApp.prototype.Googlelogout = function () {
        var _this = this;
        this.googlePlus.logout()
            .then(function (res) {
            console.log(res);
            _this.displayName = "";
            _this.email = "";
            _this.familyName = "";
            _this.givenName = "";
            _this.userId = "";
            _this.imageUrl = "";
            _this.isLoggedIn = false;
        })
            .catch(function (err) { return console.error(err); });
    };
    MyApp.prototype.shop = function (url) {
        var data = __WEBPACK_IMPORTED_MODULE_25_js_base64__["Base64"].encode(localStorage.getItem('userData'));
        var browser = this.iab.create(url);
        browser.show();
    };
    MyApp.prototype.offer = function (url) {
        var browser = this.iab.create(url);
        browser.show();
    };
    MyApp.prototype.paid = function () {
        var browser = this.iab.create('http://rayi.in/nabosupport-web');
        browser.show();
    };
    MyApp.prototype.initTranslate = function () {
        var _this = this;
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        if (browserLang) {
            if (browserLang === 'zh') {
                var browserCultureLang = this.translate.getBrowserCultureLang();
                if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
                    this.translate.use('zh-cmn-Hans');
                }
                else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
                    this.translate.use('zh-cmn-Hant');
                }
            }
            else {
                this.translate.use(this.translate.getBrowserLang());
            }
        }
        else {
            this.translate.use('en'); // Set your language here
        }
        this.translate.get(['BACK_BUTTON_TEXT']).subscribe(function (values) {
            _this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
        });
    };
    MyApp.prototype.initAutoPull = function () {
        var _this = this;
        var dynamic_data = JSON.parse(localStorage.getItem('adminData'));
        var time = dynamic_data.data[4].data;
        console.log("24 time is:" + this.convertTime12to24(time));
        var pullTime = this.convertTime12to24(time);
        var start = false;
        setInterval(function () {
            if (start == true) {
                _this.getSettings();
                start = false;
            }
            else {
                //console.log('not yet...');
            }
        }, 1000);
        __WEBPACK_IMPORTED_MODULE_26_ontime___default()({
            cycle: pullTime
        }, function (ot) {
            console.log('pull time!');
            start = true;
            ot.done();
            return;
        });
    };
    MyApp.prototype.convertTime12to24 = function (time12h) {
        var _a = time12h.split(' '), time = _a[0], modifier = _a[1];
        var _b = time.split(':'), hours = _b[0], minutes = _b[1];
        var sec = '00';
        if (hours === '12') {
            hours = '00';
        }
        if (modifier === 'PM') {
            hours = parseInt(hours, 10) + 12;
        }
        return hours + ':' + minutes + ':' + sec;
    };
    MyApp.prototype.getSettings = function () {
        var _this = this;
        this.AuthServiceProvider.getData('getAdminData').then(function (result) {
            _this.responseData = result;
            console.log(_this.responseData);
            if (_this.responseData.status == true) {
                localStorage.setItem('adminData', JSON.stringify(_this.responseData));
                _this.getCurrentPosition();
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Network error please try again',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present();
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_0__pages_signin_signin__["a" /* SigninPage */]);
            }
        });
    };
    MyApp.prototype.toggleSub = function () {
        this.viewSub = !this.viewSub;
    };
    MyApp.prototype.checkInternet = function () {
        var _this = this;
        var connectSubscription = this.network.onConnect().subscribe(function () {
            console.log('network connected!');
            setTimeout(function () {
                if (_this.network.type === 'wifi') {
                    console.log('we got a wifi connection, woohoo!');
                }
            }, 3000);
        });
        connectSubscription.unsubscribe();
    };
    MyApp.prototype.autoAlertEventStart = function () {
        var _this = this;
        var adminData = JSON.parse(localStorage.getItem('adminData'));
        var a_time = adminData.data[0].data;
        //let g_time = a_time + 3;
        var g_time = 1;
        this.timeLeft = g_time * 60;
        //this.timeLeft = 30;
        this.interval = setInterval(function () {
            if (_this.timeLeft > 0) {
                _this.timeLeft--;
                console.log('time remin:' + _this.timeLeft);
                if (_this.timeLeft == 0) {
                    console.log('time to call');
                    var condition = localStorage.getItem('push');
                    if (condition != 'true') {
                        var user = JSON.parse(localStorage.getItem('userData'));
                        _this.AuthServiceProvider.postData(user, 'camPushall').then(function (result) {
                            console.log(result);
                            _this.responseData = result;
                            if (true === _this.responseData.status) {
                                console.log(_this.responseData);
                                localStorage.setItem('tog', '');
                                _this.userDatachange = JSON.parse(localStorage.getItem("userData"));
                                _this.userDatachange['alert_type'] = "BP";
                                localStorage.setItem("userData", JSON.stringify(_this.userDatachange));
                                localStorage.setItem('timeCheck', '');
                                console.log("page move");
                                localStorage.setItem('onetimepush', 'true');
                                localStorage.setItem('push', 'true');
                                var stat_1;
                                var msg_1;
                                _this.translate.get('SENT').subscribe(function (res) {
                                    stat_1 = res;
                                });
                                _this.translate.get('Automatically forwarded the alert notification to your neighbours.').subscribe(function (res) {
                                    msg_1 = res;
                                });
                                _this.pushSendAlertOwner(stat_1, msg_1);
                            }
                        });
                    }
                }
                else {
                    _this.get_user();
                }
            }
        }, 1000);
    };
    //alert for notifying the user(owner) alert was sent
    MyApp.prototype.pushSendAlertOwner = function (status, msg) {
        var alert = this.alertCtrl.create({
            title: status,
            subTitle: msg,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        console.log('Yes clicked');
                    }
                }
            ],
            enableBackdropDismiss: false
        });
        alert.present();
    };
    MyApp.prototype.supportModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_16__pages_acceptsupport_acceptsupport__["a" /* AcceptsupportPage */]);
        modal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["_14" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/app/app.html"*/'\n<ion-split-pane when="(max-width:0px)">\n        \n    <ion-menu id="loggedInMenu" [content]="content" (ionOpen)="menuOpened()" (ionClose)="menuClosed()" [swipeEnabled]="false" persistent="true">\n\n        <ion-header>\n            <ion-toolbar>\n                <ion-title>Menu</ion-title>\n            </ion-toolbar>\n        </ion-header>\n\n        <ion-content class="outer-content">\n\n            <ion-list>\n                <button ion-item menuClose *ngFor="let p of pages" (click)="openPage(p)">\n                    <ion-icon item-start [name]="p.icon" ></ion-icon>\n                    {{p.title | translate}}\n                </button>\n                <button ion-item (click)="toggleSub();">\n                    <ion-icon item-start name="ios-cog"></ion-icon>\n                    {{\'My Settings\' | translate}}\n                </button>\n                <div *ngIf="viewSub">\n                    <button ion-item menuClose *ngFor="let s of subpages" class="sub-menu" (click)="openPage(s)" id="sub-btn">                   \n                        <ion-icon item-start [name]="s.icon" ></ion-icon>\n                        {{s.title | translate}}               \n                    </button>\n                </div>\n                <button ion-item menuClose *ngFor="let p1 of pages_1" (click)="openPage(p1)">\n                    <ion-icon item-start [name]="p1.icon" ></ion-icon>\n                    {{p1.title | translate}}\n                </button>\n                <!-- <button ion-item menuClose *ngIf="_ispaid" (click)="upgradeConfirm()">\n                    <ion-icon item-start name="heart"></ion-icon>\n                    {{\'Upgrade\' | translate}}\n                </button> -->\n                <button ion-item menuClose (click)="logout()">\n                    <ion-icon item-start ios="ios-log-out" md="md-log-out" ></ion-icon>\n                    {{\'Logout\' | translate}}\n                </button> \n                <div id="lang" ion-item menuClose >\n                    <!-- <h3 >{{\'Change Language\' | translate}}</h3> -->\n                    <ul class="lang-list">\n                        <li id="en" name="en"(click)="selectLang($event)">En</li>\n                        <li id="dn"name="dn" (click)="selectLang($event)">DK</li>\n                    </ul>\n                    <p style="text-align: center;">Nabosupport Version({{version}})</p>\n                </div>\n            </ion-list>\n        </ion-content>\n    </ion-menu>\n    <!-- main navigation -->\n    <ion-nav [root]="rootPage" #content swipeBackEnabled="false" main name="app"></ion-nav>\n\n</ion-split-pane>\n\n\n'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_10__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_google_plus__["a" /* GooglePlus */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_24__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_4__angular_core__["P" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_15__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* Config */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* ModalController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignupPageModule = /** @class */ (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]), __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()],
            exports: [__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]]
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 800:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetUserAddressProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GetUserAddressProvider = /** @class */ (function () {
    function GetUserAddressProvider(http, AuthServiceProvider) {
        this.http = http;
        this.AuthServiceProvider = AuthServiceProvider;
        this.lastImage = null;
        this.fileopen = false;
        this._ispaid = false;
        this.addAddress = false;
        this.mapcondition = false;
        this.imgoption = false;
        this.locationdata = { "hus": "", "street": "", "city": "" };
        this.marker_data = { "latitude": "", "longitude": "" };
        console.log('Hello GetUserAddressProvider Provider');
    }
    //get the user address and details
    GetUserAddressProvider.prototype.get_user = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem('userData'));
        console.log(userdata['user_id']);
        var user = {
            'user_id': userdata.user_id,
            'address_id': userdata.address_id
        };
        this.AuthServiceProvider.postData(userdata, 'getUserdata').then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            _this.marker_data.latitude = _this.responseData[0]['latitude'];
            _this.marker_data.longitude = _this.responseData[0]['longitude'];
            localStorage.setItem('userData', JSON.stringify(user_detail[0]));
            console.log(user_detail[0].paid_member);
            if (user_detail[0].paid_member == 1) {
                _this.AuthServiceProvider.postData(user, 'getPaidUserAddress').then(function (result) {
                    _this.addresses = result;
                    _this._ispaid = true;
                    console.log('user saved addresses ' + _this.addresses);
                }, function (err) {
                    console.log(err);
                });
            }
            console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
            _this.userdetail = user_detail;
            _this.isDisable = true;
            _this.editProfile = true;
            _this.submitProfile = false;
            _this.cancelProfile = false;
            _this.isDisabled = true;
        });
    };
    GetUserAddressProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */]])
    ], GetUserAddressProvider);
    return GetUserAddressProvider;
}());

//# sourceMappingURL=get-user-address.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__map_map__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: Security page (This page load the list of security device connected with app)
// created: Feb 18, 2018
// Author: Bharath
// Revisions:





var SecurityPage = /** @class */ (function () {
    function SecurityPage(navCtrl, menu, AuthServiceProvider, navParams, loadingCtrl, alertCtrl, toastCtrl, platform, translate, actionSheetCtrl, popoverCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menu = menu;
        this.AuthServiceProvider = AuthServiceProvider;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.translate = translate;
        this.actionSheetCtrl = actionSheetCtrl;
        this.popoverCtrl = popoverCtrl;
        this.updateAddress = true;
        this.isEdit = [];
        this.selecRadio = [];
        this._isPhone = false;
        this._ifEdit = false;
        this._isCountry = false;
        this._ispaid = false;
        this._isnormal = false;
        this._isFirst = [];
        this.phoneview = [];
        this.idview = [];
        this.viewCard = true;
        this.editDBtn = [];
        this.updateDBtn = [];
        this._selectOpt = [
            {
                value: "+45"
            },
            {
                value: "+46"
            },
            {
                value: "+47"
            }
        ];
        this.didArr = [];
        this.get_user();
        this.country_title = { title: "Select country" };
        var backAction = platform.registerBackButtonAction(function () {
            console.log("second");
            _this.navCtrl.pop();
            backAction();
        }, 2);
    }
    //redirect to map page
    SecurityPage.prototype.home = function () {
        var user = JSON.parse(localStorage.getItem("userData"));
        var lat = user.latitude;
        var lng = user.longitude;
        // this.navCtrl.setRoot(MapPage,{lat:lat,lng:lng});
        localStorage.setItem("lastLat", lat);
        localStorage.setItem("lastLng", lng);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__map_map__["a" /* MapPage */]);
    };
    // edit time change radio checkbox action
    SecurityPage.prototype.radioSelect = function (val, i) {
        this._isFirst[i] = true;
        if (val === "device_detail") {
            this.phoneview[i] = false;
            this.idview[i] = true;
        }
        else if (val === "phone") {
            this.phoneview[i] = true;
            this.idview[i] = false;
        }
    };
    //get the user address and details
    SecurityPage.prototype.get_user = function () {
        var _this = this;
        var userdata = JSON.parse(localStorage.getItem("userData"));
        var user = {
            user_id: userdata.user_id,
            address_id: userdata.address_id
        };
        this.AuthServiceProvider.postData(userdata, "getUserdata").then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            localStorage.setItem("userData", JSON.stringify(user_detail[0]));
            if (user_detail[0].paid_member == 1) {
                _this.AuthServiceProvider.postData(user, "getPaidUserAddress").then(function (result) {
                    _this.addresses = result;
                    _this.addresses.forEach(function (val, i) {
                        //console.log(val.camtoken_type);
                        if (val.camtoken_type == 'phone') {
                            _this.addresses[i]["cam_phone"] = val.camtoken;
                            _this.phoneview[i] = true;
                            _this.idview[i] = false;
                        }
                        else if (val.camtoken_type == 'device_detail') {
                            _this.addresses[i]["cam_did"] = val.camtoken;
                            _this.phoneview[i] = false;
                            _this.idview[i] = true;
                        }
                    });
                    console.log(_this.addresses);
                    _this._ispaid = true;
                }, function (err) {
                    console.log(err);
                });
            }
            else {
                _this._ispaid = false;
                _this._isnormal = true;
            }
        });
    };
    SecurityPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
    };
    SecurityPage.prototype.ionViewWillLeave = function () {
        this.menu.swipeEnable(true);
    };
    //edit device id
    SecurityPage.prototype.editDID = function (i) {
        if (this.addresses[i].country_code === "") {
            this.addresses[i].country_code = "+45";
        }
        this._ifEdit = true;
        this.isEdit[i] = true;
        this.editDBtn[i] = true;
        this.updateDBtn[i] = true;
        if (this.addresses[i].camtoken_type === "phone") {
            this._isFirst[i] = true;
            this._isPhone = true;
            this._isCountry = true;
            this.didVal = false;
        }
        else if (this.addresses[i].camtoken_type === "device_detail") {
            this._isFirst[i] = true;
            this._isPhone = false;
            this._isCountry = false;
        }
        else {
            this._isFirst[i] = false;
            this._isCountry = false;
            this._isPhone = false;
        }
    };
    //update device id
    SecurityPage.prototype.updateDID = function (add, i, did) {
        var _this = this;
        console.log(this.addresses[i]);
        var updatetxt = '';
        this.translate.get('Device ID updated successfully').subscribe(function (res) {
            updatetxt = res;
        });
        var toast = this.toastCtrl.create({
            message: updatetxt,
            duration: 3000,
            position: "bottom"
        });
        var toastfail = this.toastCtrl.create({
            message: "DeviceID update failed",
            duration: 3000,
            position: "bottom"
        });
        var loading = this.loadingCtrl.create({
            spinner: "crescent",
            content: "DeviceID updating..."
        });
        setTimeout(function () {
            loading.dismiss();
        }, 300);
        this.AuthServiceProvider.postData(this.addresses[i], "updateCamToken").then(function (result) {
            _this.responseData = result;
            var user_detail = _this.responseData;
            if (true == _this.responseData.status) {
                _this.editDBtn[i] = false;
                _this.updateDBtn[i] = false;
                _this._ifEdit = false;
                _this.isEdit[i] = false;
                _this._isFirst[i] = false;
                _this.get_user();
                toast.present();
            }
            else {
                toastfail.present();
            }
        });
    };
    SecurityPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: "page-security",template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/security/security.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'My Alarm\' | translate}}</ion-title>\n    <a (click)="home()" class="home">\n      <ion-icon name="home"></ion-icon>\n    </a>\n  </ion-navbar>\n\n</ion-header>\n<ion-content class="signin" padding center text-center>\n  <ion-list *ngFor="let add of addresses;let i = index">\n    <ion-card *ngIf="_ispaid">\n      <h2 class="add-head" *ngIf="add.primary_address == \'yes\';else normadd">{{\'Primary address\' | translate}}</h2>\n      <ng-template #normadd>\n        <h2 class="add-head">{{\'Other address\' | translate}} {{i}}</h2>\n      </ng-template>\n      <div class="add-bar sec-address" *ngIf="updateAddress">\n        <ion-item>\n          <ion-textarea disabled={{addDisabled}} [value]="add.street_address" type="text" value="{{userdetail[0].street_address}}"></ion-textarea>\n        </ion-item>\n        <ion-item>\n          <!-- <ion-label>{{ \'City\' | translate }}</ion-label> -->\n          <ion-input disabled={{addDisabled}} type="text" [value]="add.city"></ion-input>\n        </ion-item>\n        <ion-item>\n          <!-- <ion-label>{{ \'Country\' | translate }}</ion-label> -->\n          <ion-input disabled={{addDisabled}} type="text" [value]="add.country"></ion-input>\n        </ion-item>\n        <ion-item>\n          <!-- <ion-label>{{ \'Zipcode\' | translate }}</ion-label> -->\n          <ion-input disabled={{addDisabled}} type="text" [value]="add.zipcode"></ion-input>\n        </ion-item>\n        <ion-list radio-group [(ngModel)]="add.camtoken_type">\n          <ion-item>\n            <ion-label>{{\'Device details\' | translate}}</ion-label>\n          </ion-item>\n          <ion-item>\n            <ion-label>{{\'Device ID\' | translate}}</ion-label>\n            <ion-radio [disabled]="!isEdit[i]"  value="device_detail" (ionSelect)="radioSelect(\'device_detail\',i)"></ion-radio>\n          </ion-item>\n          <ion-item *ngIf="idview[i]">\n            <ion-input [disabled]="!isEdit[i]" maxLength="25" class="m-left-23" placeholder="Device ID"  type="text" [(ngModel)]="add.cam_did"></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label>{{\'PHONE NUMBER\' | translate}}</ion-label>\n            <ion-radio value="phone" [disabled]="!isEdit[i]" (ionSelect)="radioSelect(\'phone\',i)"></ion-radio>\n          </ion-item>\n          <ion-item *ngIf="phoneview[i]">\n            <ion-input id="code" maxLength="4" [disabled]="!isEdit[i]" [(ngModel)]="add.country_code"></ion-input>\n            <ion-input id="phone" maxLength="25" [disabled]="!isEdit[i]" class="m-left-23" placeholder="Phone number"  type="number" [(ngModel)]="add.cam_phone"></ion-input>\n          </ion-item>\n        </ion-list>\n        <ion-item>\n          <button class="sm-btn" ion-button small (click)="editDID(i)" [class.hidden]="editDBtn[i]" item-end>\n            <ion-icon id="did-edt" name="create"></ion-icon>\n          </button>\n          <button class="sm-btn" ion-button small (click)="updateDID(add,i,did)" [class.hidden]="!updateDBtn[i]" item-end>\n            <ion-icon id="did-upt" name="checkmark"></ion-icon>\n          </button>\n        </ion-item>\n      </div>\n    </ion-card>\n  </ion-list>\n  <ion-list *ngIf="viewCard">\n    <ion-card class="free-usr" *ngIf="_isnormal">\n      <h3>{{"Become a paid user to connect your \'device/alarm systems\' to Nabosupport" | translate}}</h3>\n    </ion-card>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/security/security.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* PopoverController */]])
    ], SecurityPage);
    return SecurityPage;
}());

//# sourceMappingURL=security.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcceptsupportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AcceptsupportPage = /** @class */ (function () {
    function AcceptsupportPage(navCtrl, navParams, authservice, viewCtrl, toastCtrl, events, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.authservice = authservice;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.translate = translate;
    }
    AcceptsupportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AcceptsupportPage');
    };
    AcceptsupportPage.prototype.acceptReq = function () {
        var _this = this;
        var locdata = JSON.parse(localStorage.getItem('userData'));
        var recdata = JSON.parse(localStorage.getItem('accData'));
        var reqData = {
            'user_id': locdata.user_id,
            'sender_id': recdata.sender_id,
            'email': recdata.email
        };
        this.authservice.postData(reqData, 'acceptReq').then(function (res) {
            _this.responsedata = res;
            console.log(_this.responsedata);
            if (_this.responsedata.status == true) {
                var flaf = 1;
                _this.events.publish('user:ref', flaf);
                _this.toastMessage('thankreq');
                _this.viewCtrl.dismiss();
            }
            else {
                _this.toastMessage('reqfail');
            }
        }, function (err) {
            console.log(err);
            _this.toastMessage('reqfail');
        });
    };
    AcceptsupportPage.prototype.rejectReq = function () {
        var _this = this;
        var locdata = JSON.parse(localStorage.getItem('userData'));
        var recdata = JSON.parse(localStorage.getItem('accData'));
        var reqData = {
            'user_id': locdata.user_id,
            'sender_id': recdata.sender_id,
            'email': recdata.email
        };
        this.authservice.postData(reqData, 'acceptReject').then(function (res) {
            _this.responsedata = res;
            console.log(_this.responsedata);
            if (_this.responsedata.status == true) {
                var flaf = 1;
                _this.events.publish('user:ref', flaf);
                _this.toastMessage('reject');
                _this.viewCtrl.dismiss();
            }
            else {
                _this.toastMessage('reqfail');
            }
        }, function (err) {
            console.log(err);
            _this.toastMessage('reqfail');
        });
    };
    AcceptsupportPage.prototype.toastMessage = function (msg) {
        var toasttxt;
        this.translate.get(msg).subscribe(function (res) {
            toasttxt = res;
        });
        var toast = this.toastCtrl.create({
            message: toasttxt,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    AcceptsupportPage.prototype.modalClose = function () {
        this.viewCtrl.dismiss();
    };
    AcceptsupportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-acceptsupport',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/acceptsupport/acceptsupport.html"*/'<ion-content padding>\n    <ion-icon class="exit-btn" ios="ios-close-circle" md="md-close-circle" (click)="modalClose()"></ion-icon>\n    <div>\n      <p>{{ \'acceptpagehead\' | translate }}</p>\n      <div class="row" >\n        <div class="col" >\n          <button ion-button color="secondary" (click)=\'acceptReq();\'>{{\'acpt\' | translate}}</button>\n        </div>\n        <div class="col" >\n          <button ion-button color="danger" (click)=\'rejectReq();\'>{{ \'rjct\' | translate}}</button>\n        </div>\n      </div>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/acceptsupport/acceptsupport.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]])
    ], AcceptsupportPage);
    return AcceptsupportPage;
}());

//# sourceMappingURL=acceptsupport.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Page name: popover page (This display the more address from myaccountpage for paid users only)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 


var PopoverPage = /** @class */ (function () {
    function PopoverPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    PopoverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PopoverPage');
        this.address = this.navParams.data;
        console.log(this.address);
    };
    PopoverPage.prototype.close = function (data) {
        this.viewCtrl.dismiss(data);
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-popover',template:/*ion-inline-start:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/popover/popover.html"*/'\n<ion-list>\n  <ion-list-header class="add-head">your registered address</ion-list-header>\n  <button ion-item class="add-list" (click)="close(data)" *ngFor="let data of address">\n      <ion-badge *ngIf="data.primary_address ==\'yes\'" class="prim-badge"> P </ion-badge>\n      {{data.street_address}}\n  </button>\n</ion-list>'/*ion-inline-end:"/Users/admin/Desktop/nabosupport_wp_hendrik/Mobile_app/src/pages/popover/popover.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* ViewController */]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=popover.js.map

/***/ })

},[474]);
//# sourceMappingURL=main.js.map