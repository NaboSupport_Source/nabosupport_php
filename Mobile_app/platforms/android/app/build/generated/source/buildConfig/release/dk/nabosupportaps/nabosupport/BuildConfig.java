/**
 * Automatically generated file. DO NOT MODIFY
 */
package dk.nabosupportaps.nabosupport;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "dk.nabosupportaps.nabosupport";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10005;
  public static final String VERSION_NAME = "1.0.5";
}
