/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.soundcloud.android.crop;

public final class R {
    public static final class attr {
        public static final int cropImageStyle = 0x7f020062;
        public static final int highlightColor = 0x7f020083;
        public static final int showCircle = 0x7f0200c9;
        public static final int showHandles = 0x7f0200cb;
        public static final int showThirds = 0x7f0200cd;
    }
    public static final class color {
        public static final int crop__button_bar = 0x7f040031;
        public static final int crop__button_text = 0x7f040032;
        public static final int crop__selector_focused = 0x7f040033;
        public static final int crop__selector_pressed = 0x7f040034;
    }
    public static final class dimen {
        public static final int crop__bar_height = 0x7f050050;
    }
    public static final class drawable {
        public static final int crop__divider = 0x7f060066;
        public static final int crop__ic_cancel = 0x7f060067;
        public static final int crop__ic_done = 0x7f060068;
        public static final int crop__selectable_background = 0x7f060069;
        public static final int crop__texture = 0x7f06006a;
        public static final int crop__tile = 0x7f06006b;
    }
    public static final class id {
        public static final int always = 0x7f07001f;
        public static final int btn_cancel = 0x7f070025;
        public static final int btn_done = 0x7f070026;
        public static final int changing = 0x7f07002c;
        public static final int crop_image = 0x7f070033;
        public static final int done_cancel_bar = 0x7f07003a;
        public static final int never = 0x7f070057;
    }
    public static final class layout {
        public static final int crop__activity_crop = 0x7f09001b;
        public static final int crop__layout_done_cancel = 0x7f09001c;
    }
    public static final class string {
        public static final int crop__cancel = 0x7f0b0030;
        public static final int crop__done = 0x7f0b0031;
        public static final int crop__pick_error = 0x7f0b0032;
        public static final int crop__saving = 0x7f0b0033;
        public static final int crop__wait = 0x7f0b0034;
    }
    public static final class style {
        public static final int Crop = 0x7f0c009e;
        public static final int Crop_ActionButton = 0x7f0c009f;
        public static final int Crop_ActionButtonText = 0x7f0c00a0;
        public static final int Crop_ActionButtonText_Cancel = 0x7f0c00a1;
        public static final int Crop_ActionButtonText_Done = 0x7f0c00a2;
        public static final int Crop_DoneCancelBar = 0x7f0c00a3;
    }
    public static final class styleable {
        public static final int[] CropImageView = { 0x7f020083, 0x7f0200c9, 0x7f0200cb, 0x7f0200cd };
        public static final int CropImageView_highlightColor = 0;
        public static final int CropImageView_showCircle = 1;
        public static final int CropImageView_showHandles = 2;
        public static final int CropImageView_showThirds = 3;
    }
}
