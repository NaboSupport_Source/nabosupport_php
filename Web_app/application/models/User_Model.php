<?php
class User_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function login() {
        
    }
    
    public function getCategory()
    {
        $this->db->select('*')->from('category');
        $this->db->where('status', 1);
        $query=$this->db->get();
        return $query->result();
    }
    
    public function getProductByID($id) {
        $this->db->select('*')->from('product');
        $this->db->where('category',$id);
        $query=$this->db->get();
        return $query->result();
    }
    
    public function setPaid($data) {
        $this->db->insert('payment_log',$data);
        if ($this->db->affected_rows() > 0) {
            $udata=array('paid_member'=>1);
            $this->db->where('user_id',$data['user_id']);
            $this->db->update('user',$udata);
            return ['status'=>true];
        }
        return ['status'=>false];
    }
    
    public function validate($data) {
        $this->db->select('*')->from('user');
        $this->db->where('email_id',$data['email']);
        $this->db->where('password',$data['password']);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $userdata = $query->result();
            //print_r($userdata);exit;
            $dat = $userdata[0];
            $g = 'GB';
            $s = 'GS';
            $id = $dat->user_id;
            $this->db->select('*');
            $this->db->from('user');
            $this->db->join('address', 'user.user_id = address.user_id');
            $this->db->where('user.user_id', $id);
            $this->db->where_not_in('address.address_type',$g,$s);
            $query = $this->db->get();
            return array('status' => true, 'data' => $query->result());
        } else {
            return array('status' => false);
        }
    }
    
    public function check($data) {
        $this->db->select('*')->from('user');
        $this->db->where('email_id',$data['email_id']);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return array('status' => false, 'msg' => 'user already exist.');
        } else {
            $userData = array(
              "username" => $data['username'],
               "age" => $data['age'],
               "phone" => $data['phone'],
               "email_id" => $data['email_id'],
               "password" => $data['password'],
               "alert_type" => $data['alert_type'],
               "is_active" => $data['is_active'],
               "distance" => $data['distance']
            );
            $this->db->insert('user', $userData);
            $id = $this->db->insert_id();
            $q = $this->db->get_where('user', array('user_id' => $id));
            //return $q->row();exit;
            $id = $q->row();
            $addressData = array(
                "user_id" => $id->user_id,
                "street_address" => $data['street_address'],
                "zipcode" => $data['zipcode'],
                "city" => $data['city'],
                "country" => $data['country'],
                "latitude" => $data['latitude'],
                "longitude" => $data['longitude'],
                "is_active" => $data['is_active'],
                "marker" => 'blue.png'
            );
            $this->db->insert('address',$addressData);
            if($this->db->affected_rows() != 1) {
                return array('status' => false, 'msg' => 'Somthing wen wrong try again!.');
            } else {
                return array('status' => true, 'msg' => 'you have registered successfully.');
            }
        }
    }
// admin setting update
public function adminSettingModel($data){
$this->db->where('id',1)->update('admin_setting',array('data'=>$data->tread_alert));
$this->db->where('id',2)->update('admin_setting',array('data'=>$data->refresh));
$this->db->where('id',3)->update('admin_setting',array('data'=>$data->shop_url));
$this->db->where('id',4)->update('admin_setting',array('data'=>$data->offer));
$this->db->where('id',5)->update('admin_setting',array('data'=>$data->pull_time));
$this->db->where('id',6)->update('admin_setting',array('data'=>$data->threat_reset));
}
// get admin setting 
public function getSettingModel(){
return $this->db->select('*')->from('admin_setting')->get()->result();
}
}