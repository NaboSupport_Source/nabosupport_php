<?php
class Shop_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function getCategory()
    {
        $this->db->select('*')->from('category');
        $this->db->where('status', 1);
        $query=$this->db->get();
        return $query->result();
    }
    public function getProduct() {
        $this->db->select('*')->from('product');
        $this->db->where('status', 1);
        $query=$this->db->get();
        return $query->result();
    }
    public function getProductByID($id) {
        if ($id === 'all') {
            $this->db->select('*')->from('product');
            $this->db->where('status', 1);
            $query=$this->db->get();
            return $query->result();
        } else {
            $this->db->select('*')->from('product');
            $this->db->where('category',$id);
            $query=$this->db->get();
            return $query->result();
        }
    }
}

