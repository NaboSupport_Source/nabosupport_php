<style>
    
/* Icon 1 */

.animated-icon1, .animated-icon2, .animated-icon3, .animated-icon4 {
  width: 30px;
  height: 20px;
  position: relative;
  margin: 0px;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .5s ease-in-out;
  -moz-transition: .5s ease-in-out;
  -o-transition: .5s ease-in-out;
  transition: .5s ease-in-out;
  cursor: pointer;
}

.animated-icon1 span, .animated-icon3 span, .animated-icon4 span {
  display: block;
  position: absolute;
  height: 3px;
  width: 100%;
  border-radius: 9px;
  opacity: 1;
  left: 0;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: .25s ease-in-out;
  -moz-transition: .25s ease-in-out;
  -o-transition: .25s ease-in-out;
  transition: .25s ease-in-out;
}

.animated-icon1 span {
    background: #e65100;
}

.animated-icon3 span {
    background: #e3f2fd;
}

.animated-icon4 span {
    background: #f3e5f5;
}

.animated-icon1 span:nth-child(1) {
  top: 0px;
}

.animated-icon1 span:nth-child(2) {
  top: 10px;
}

.animated-icon1 span:nth-child(3) {
  top: 20px;
}

.animated-icon1.open span:nth-child(1) {
  top: 11px;
  -webkit-transform: rotate(135deg);
  -moz-transform: rotate(135deg);
  -o-transform: rotate(135deg);
  transform: rotate(135deg);
}

.animated-icon1.open span:nth-child(2) {
  opacity: 0;
  left: -60px;
}

.animated-icon1.open span:nth-child(3) {
  top: 11px;
  -webkit-transform: rotate(-135deg);
  -moz-transform: rotate(-135deg);
  -o-transform: rotate(-135deg);
  transform: rotate(-135deg);
}

/* Icon 3*/

.animated-icon3 span:nth-child(1) {
  top: 0px;
}

.animated-icon3 span:nth-child(2), .animated-icon3 span:nth-child(3) {
  top: 10px;
}

.animated-icon3 span:nth-child(4) {
  top: 20px;
}

.animated-icon3.open span:nth-child(1) {
  top: 11px;
  width: 0%;
  left: 50%;
}

.animated-icon3.open span:nth-child(2) {
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
}

.animated-icon3.open span:nth-child(3) {
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
}

.animated-icon3.open span:nth-child(4) {
  top: 11px;
  width: 0%;
  left: 50%;
}

/* Icon 4 */

.animated-icon4 span:nth-child(1) {
  top: 0px;
  -webkit-transform-origin: left center;
  -moz-transform-origin: left center;
  -o-transform-origin: left center;
  transform-origin: left center;
}

.animated-icon4 span:nth-child(2) {
  top: 10px;
  -webkit-transform-origin: left center;
  -moz-transform-origin: left center;
  -o-transform-origin: left center;
  transform-origin: left center;
}

.animated-icon4 span:nth-child(3) {
  top: 20px;
  -webkit-transform-origin: left center;
  -moz-transform-origin: left center;
  -o-transform-origin: left center;
  transform-origin: left center;
}

.animated-icon4.open span:nth-child(1) {
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  top: 0px;
  left: 8px;
}

.animated-icon4.open span:nth-child(2) {
  width: 0%;
  opacity: 0;
}

.animated-icon4.open span:nth-child(3) {
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  top: 21px;
  left: 8px;
}        
.navbar{
	background: white;
}
.animated-icon3 span {
    background: #101010 !important;
}
.nav-link{
	color:#101010 !important;
}
.animated-icon3 span:nth-child(1) {
    top: 2px !important;
}
.animated-icon3 span:nth-child(2), .animated-icon3 span:nth-child(3) {
    top: 10px !important;
}
.animated-icon3 span:nth-child(4) {
    top: 18px !important;
}
.animated-icon3, .animated-icon4 {
    width: 25px !important;
}
button:focus {
    outline: none !important;
    outline: none !important; 
}
.nav-link{
	padding: 0.1rem 0rem !important;
	font-size:15px;
}
.admin_data ul{
	list-style:none;
	padding:0;
}
.admin_data li{
	text-decoration:none;
	float:left;
	width:15%;
}
.admin_data i{
	color:black;
	font-size:16px;
	padding:0 !important;
}
.menu_data h6,p{
	margin-bottom:0 !important;
}
.menu_data ul{
	list-style:none;
	padding:0;
}
.menu_data li{
	text-decoration:none;
	float:left;
	width:20%;
}
.menu_data i{
	color:black;
	font-size:16px;
	padding:0 !important;
}
.admin_left{
	float:left;
}
.admin_right{
	float:left;
}
nav a{
    margin: 0;
    font-weight:300;
}
nav a:hover{
    font-weight:bold;
}
.bg-green{
    margin-left:3px;
    vertical-align: text-top;
}
.nav-icn{
    font-size: 30px !important;
    color:#000;
}
.navbar-right {
    left: auto !important;
    right: 0px;
    list-style-type: none;
    float: right;
    margin: 15px 0px!important;
}
.navbar-right li {
    display: inline-block;
    vertical-align: middle;
    padding: 0 5px;
}
.navbar-right li a i{
    color:#fff;
}
.navbar-toggler {
    color:#fff !important;
}
.animated-icon3 span{
    background-color: #fff !important;
}
.navbar-icon{
    width: 90%;
}
.img-logo{
    height: 70px;
    width: 180px;
}
.navbar {
    background: #404244;
}
.navbar li a {
    color:#fff !important;
}
#nav-dev{
    border: 1 !important;
    height: 1px !important;
    color:#000 !important;
}
.navbar-dark .navbar-toggler{
    border-width: 0px !important;
}
</style>
<!--Navbar-->
<nav class="navbar navbar-dark lighten-1 mb-4">

    <!-- Navbar brand -->
    <div class="navbar-icon">
        <img src="<?php echo base_url('assets/img/logo.png'); ?>" class="img-responsive img-logo" alt="logo">
        <ul class="navbar-right">
            <li>
                <a href="Cart"><i class="fa fa-shopping-cart nav-icn" aria-hidden="true"></i> <span id='TotalCart' class="badge bg-pink"></span></a>
            </li>
            <li><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent23" aria-controls="navbarSupportedContent23"
            aria-expanded="false" aria-label="Toggle navigation"><div class="animated-icon3"><span></span><span></span><span></span><span></span></div></button></li>
        </ul>
        <!-- Collapse button -->
    </div>
    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent23">
        <hr id="nav-dev">
        <!-- Links -->
        <!--  <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
                 <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
             </li>
             <li class="nav-item">
                 <a class="nav-link" href="#">Features</a>
             </li>
             <li class="nav-item">
                 <a class="nav-link" href="#">Pricing</a>
             </li>
         </ul> -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 menu_data">
                    <div class="admin_left">
                     
                    </div>
                    <div class="admin_right">
<!--                        <h6>test user</h6>
                        <p>test@gmail.com</p>-->
<!--                        <ul>
                            <li><a href="Profile" class="text-white" data-toggle="tooltip" data-placement="top" title="Profile"><i class="fa fa-user-o" aria-hidden="true"></i></a></li>
                            <li><a href="javascript:;" class="text-white" onclick="" data-toggle="tooltip" data-placement="top" title="Language"><i class="fa fa-globe" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="text-white"data-toggle="tooltip" data-placement="top" title="Log out"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
                        </ul>-->
                    </div>
                </div>
                <div class="col-md-9 admin_data">
                    <ul>
                        <li><a class="nav-link" href="Shop">Shop</a></li>
                        <li><a class="nav-link" href="javascript:;">Offer & Alarm</a></li>
                        <li> <a class="nav-link" href="Camera">Camera</a></li>
                        <?php $paid = $this->session->userdata('logged_in');
                            if($paid['paid_member'] == 0) {
                        ?>      
                                <li><a class="nav-link" href="Upgrade">Upgrade</a> </li>
                        <?php        
                            }
if($paid['user_type_id']=='1'){
echo ' <li><a href="'.base_url('Setting').'">Setting</a></li>';
}
                        ?>
                       
                        <li><a class="nav-link" href="<?php echo base_url('Login/logout'); ?>">Logout</a> </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Links -->

    </div>
    <!-- Collapsible content -->

</nav>
<!--/.Navbar-->
<script>
    $(document).ready(function() {
        $('.animated-icon1,.animated-icon3,.animated-icon4').click(function(){
            $(this).toggleClass('open');
        });
        $.ajax({
            url:base_url+'Shop/getCartCount',
            type: 'get',
            dataType: 'json',
            success:function(rs){
                console.log('cart count:'+JSON.stringify(rs));
                $('#TotalCart').html(rs.items);
            }
        });
    });
</script>