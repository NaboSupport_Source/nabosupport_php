<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title; ?> | Nabosupport</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/spinner.css'); ?>" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/script.js'); ?>"></script>
        <script src="https://unpkg.com/sweetalert2@7.12.10/dist/sweetalert2.all.js"></script>
        <script>
            // global declare base url
            var base_url = '<?php echo base_url(); ?>';
        </script>
    </head>
    <body>
        <div class="cs-loader" id="block">
            <div class="cs-loader-inner">
                <div class="lds-ripple"><div></div><div></div></div>
            </div>
        </div>
        <?php
            include('navbar.php');
        ?>

