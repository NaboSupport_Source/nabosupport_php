<?php
/*
 * Author: Jayamurugan
 * Description: Admin setting page
 * Date : 12-07-2018
 */
?>
<style>
    div.center-align{
        width:320px;
        margin:0 auto;
    }
    input#tread_alert {
        width: 22%;
        font-size: 10px;
        text-align: center;
        margin: 6px 10px 6px 0;
    }

    input#refresh {
        width: 22%;
        font-size: 10px;
        text-align: center;
        margin: 6px 10px 6px 0;
    }

    input#threat_reset {
        width: 22%;
        font-size: 10px;
        text-align: center;
        margin: 6px 10px 6px 0;
    }
    input#pull_time {
        width: 22%;
        font-size: 10px;
        text-align: center;
        margin: 6px 10px 6px 0;
    }
</style>
<div class="col-md-12">
    <div class="form-group">
        <div class="center-align">
            <div class="form-inline">
                <input type="text" class="form-control" id="tread_alert"  placeholder="00 mins">
                <label for="tread_alert">Auto Threat Alert</label>
            </div>
            <div class="form-inline">
                <input type="text" class="form-control" id="refresh" placeholder="00 mins">
                <label for="refresh">Map Reload Interval</label>
            </div>
            <div class="form-inline">
                <input type="text" class="form-control" id="threat_reset" placeholder="00 mins">
                <label for="refresh">Process Reset Time</label>
            </div>
            <div class="form-inline">
                <input type="text" class="form-control" id="pull_time" placeholder="12:00 AM">
                <label for="refresh">Settings Pull Time</label>
            </div>
            <div class="form-group">
                <label for="url">Shopping URL</label>
                <input type="text" class="form-control" id="url" placeholder="URL for Shopping">
            </div>
            <div class="form-group">
                <label for="offer">Offer URL</label>
                <input type="text" class="form-control" id="offer" placeholder="Offers ">
            </div>
            <div class="text-center">
                <button type="button" class="btn btn-primary" onclick="update();">Save</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        getSetting();
    });
    function getSetting() {
        $.getJSON(base_url + 'Setting/getSetting', function (rs) {
            $.each(rs, function (ind, row) {
                if (row.id === '1') {
                    $('#tread_alert').val(row.data);
                } else if (row.id === '2') {
                    $('#refresh').val(row.data);
                } else if (row.id === '3') {
                    $('#url').val(row.data);
                } else if (row.id === '4') {
                    $('#offer').val(row.data);
                } else if (row.id === '5') {
                    $('#pull_time').val(row.data);
                } else if (row.id === '6') {
                    $('#threat_reset').val(row.data);
                }
            });
        });
    }
    function update() {
        $.ajax({
            url: base_url + 'Setting/updateSetting',
            type: 'post',
            data: {tread_alert: $('#tread_alert').val(), refresh: $('#refresh').val(), threat_reset: $('#threat_reset').val(), pull_time:$('#pull_time').val(),shop_url: $('#url').val(), offer: $('#offer').val()},
            dataType: 'json',
            success: function (rs) {
                alert('updated');
            }
        });
    }
</script>
