<?php
/*
 * Author: Bharath
 * Description: Login page
 * Date : 6-07-2018
 */
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title; ?> | Nabosupport</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/login.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/spinner.css'); ?>" />
         <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" />
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIhHBL9VCl5pAkahNbGdwsecyEcB1turM&sensor=true&callback=initMap"
  type="text/javascript">
        </script>
  
<!--        <script src="<?php echo base_url('assets/js/gmap/gmaps.js'); ?>"></script>-->
        <script>
            // global declare base url
            var base_url = '<?php echo base_url(); ?>';
        </script>
    </head>
    <body class="text-center">
        <div class="cs-loader" id="block">
            <div class="cs-loader-inner">
                <div class="lds-ripple"><div></div><div></div></div>
            </div>
        </div>
        <div class="content">
            <div class="logo">
                 <img src="http://rayi.in/nabosupport/assets/img/logo.png" alt="logo">
            </div>
            <!-- Modal -->
            <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Drag the map and Mark your address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div class="container">
                          <div class="row">
                              <div class="col-md-12">
                                  <div id="map"></div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div id="add-bar"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
<!--                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Mark</button>-->
                  </div>
                </div>
              </div>
            </div>
            <form class="form-signin" id="signupForm" novalidate>
    <!--            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>-->
                <div class="form-group">
                    <label for="name" class="sr-only">First name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="First name" autofocus>
                </div>  
                <div class="form-group">
                    <label for="age" class="sr-only">Age</label>
                    <input type="number" name="phone" id="age" class="form-control" placeholder="Your age" autofocus>
                </div>
                <div class="form-group">
                    <label for="phone" class="sr-only">Phone number</label>
                    <input type="tel" name="phone" id="phone" class="form-control" placeholder="Phone number" autofocus>
                </div>   
                <div class="form-group">
                    <label for="email" class="sr-only">Email address</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email address" autofocus>
                </div>
                <div class="form-group">
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="address" class="sr-only">Address</label>
                    <textarea  name="address" id="address" class="form-control" placeholder="address details from map"></textarea>
                </div>
    <!--            <div class="form-group">
                    <button class="btn btn-lg btn-primary btn-block" onclick="openMap();">Click here to open map</button>
                </div>-->
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
                <p>Do you have an account. <a href="<?php echo base_url('Login'); ?>">Login Here</a></p>
                <p class="mt-5 mb-3 text-muted">Nabo support - &copy; <?php echo date('Y'); ?></p>
            </form>
        </div>
        <script>
            /*map code */
            
            $(document).ready(function() {
                var locdata;
                $('#address').click(function() {
                    //alert('clicked'); 
                    $.ajax({
                        type : 'POST',
                        data: '', 
                        url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBIhHBL9VCl5pAkahNbGdwsecyEcB1turM", 
                        success: function(result){
                                console.log(result);
                                var lat = result.location.lat;
                                var lng = result.location.lng;
                                $('#mapModal').modal('show');
                                initMap(lat,lng);
                                var cpos = {lat:lat,lng:lng};
                                geocodePosition(cpos);
                        }
                    });
                    $("#map").css({"width":"100%","height":"400px"});
                });

            });
            
            function initMap(lat,lng)   {      
               var center = new google.maps.LatLng(lat, lng);

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 18,
                    center: center,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                });

                var myMarker = new google.maps.Marker({
                    position: center,
                    draggable: true,
                    map: map
                });

                google.maps.event.addListener(myMarker, 'dragend', function () {
                    map.setCenter(this.getPosition()); // Set map center to marker position
                    updatePosition(this.getPosition().lat(), this.getPosition().lng()); // update position display
                    
                });

                google.maps.event.addListener(map, 'dragend', function () {
                    myMarker.setPosition(this.getCenter()); // set marker position to map center
                    updatePosition(this.getCenter().lat(), this.getCenter().lng()); // update position display
                    var pos = {lat:this.getCenter().lat(), lng:this.getCenter().lng()}
                    geocodePosition(pos);
                });

                function updatePosition(lat, lng) {
                    
                }
            }
            function geocodePosition(pos) {
                var coord = new Array();
                coord.push(pos.lat, pos.lng);
                var geocoder= new google.maps.Geocoder();
                    geocoder.geocode({
                      latLng: pos
                    }, function(responses) {
                      if (responses && responses.length > 0) {
                                console.log(responses[0].formatted_address);
                                document.getElementById('address').value = responses[0].formatted_address;
                                document.getElementById('add-bar').innerHTML = '<p>' + responses[0].formatted_address + '</p>';
                                let result = responses[0];
                                let rsltAdrComponent = result.address_components;
                                let resultLength = rsltAdrComponent.length;
                                if (result != null) {
                                  var data = new Array;
                                  var locationdata = new Array();
                                  for (var i = 0; i < rsltAdrComponent.length; i++) {
                                    var obj = rsltAdrComponent[i];
                                    data.push(obj.long_name);

                                  }
                                  if (data.length == 5) {
                                    locdata = {

                                      "street_address": data[1] + " " + data[0],
                                      "city": data[2],
                                      "country": data[3],
                                      "zipcode": data[4],
                                      "latitude": coord[0],
                                      "longitude": coord[1],
                                      "is_active": 1,
                                      "status_id": 0,
                                      "alert_type":"N",
                                      "distance":5000
                                    };
                                    console.log(locdata);
                                  }
                                  else if (data.length == 6) {
                                    locdata = {

                                      "street_address": data[1] + " " + data[0],
                                      "city": data[2],
                                      "country": data[4],
                                      "zipcode": data[5],
                                      "latitude": coord[0],
                                      "longitude": coord[1],
                                      "is_active": 1,
                                      "status_id": 0,
                                      "alert_type":"N",
                                      "distance":5000
                                    };
                                    console.log(locdata);
                                  }
                                  else if (data.length == 7) {
                                    locdata = {

                                      "street_address": data[1] + " " + data[0],
                                      "city": data[3],
                                      "country": data[5],
                                      "zipcode": data[6],
                                      "latitude": coord[0],
                                      "longitude": coord[1],
                                      "is_active": 1,
                                      "status_id": 0,
                                      "alert_type":"N",
                                      "distance":5000                                    
                                    };
                                    console.log(locdata);
                                  }
                                  else if (data.length == 8) {
                                    locdata = {
                                      "street_address": data[1] + " " + data[0],
                                      "city": data[4],
                                      "country": data[6],
                                      "zipcode": data[7],
                                      "latitude": coord[0],
                                      "longitude": coord[1],
                                      "is_active": 1,
                                      "status_id": 0,
                                      "alert_type":"N",
                                      "distance":5000
                                    };
                                    console.log(locdata);
                                  } else if (data.length == 9) {
                                    locdata = {
                                      "street_address": data[1] + " " + data[0],
                                      "city": data[4],
                                      "country": data[7],
                                      "zipcode": data[8],
                                      "latitude": coord[0],
                                      "longitude": coord[1],
                                      "is_active": 1,
                                      "status_id": 0,
                                      "alert_type":"N",
                                      "distance":5000
                                    };

                                    console.log(locdata);
                                  } else if (data.length == 10) {

                                    locdata = {
                                      "street_address": data[1] + " " + data[0],
                                      "city": data[5],
                                      "country": data[8],
                                      "zipcode": data[9],
                                      "latitude": coord[0],
                                      "longitude": coord[1],
                                      "is_active": 1,
                                      "status_id": 0,
                                      "alert_type":"N",
                                      "distance":5000
                                    };

                                    console.log(locdata);
                                  }
                                  console.log(locdata);
                                } else {
                                  alert("No address available");
                                }
                      } else {
                        console.log('Cannot determine address at this location.');
                      }
                    });
              }
            $(function () {
                $('#signupForm').submit(function(e) {
                    e.preventDefault();
                }).validate({
                    errorElement: 'div',
                    errorClass: 'help-block',
                    rules: {
                        name: {
                            required: {
                                depends: function () {
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            }
                        },
                        email: {
                            required: {
                                depends: function () {
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            },
                            email: true
                        },
                        password: {
                            required: {
                                depends: function () {
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            }
                        },
                        address: {
                            required: {
                                depends: function () {
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            }
                        }
                    },
                    messages: {
                        email: {
                            required: 'Please enter email id',
                            email: 'Please enter valid email id'
                        },
                        password: {
                            required: 'Please enter password'
                        },
                        name: {
                            required: 'Please enter your name'
                        },
                        address: {
                            required: 'Please enter your address'
                        }
                    },
                    submitHandler: function () {
                        //window.location.href=base_url+'Shop';
                        //alert('pass'+$('#password').val());
                        var data = {
                            username:$('#name').val(),
                            age:$('#age').val(),
                            phone:$('#phone').val(),
                            email_id:$('#email').val(),
                            password:$('#password').val(),
                            street_address:locdata.street_address,
                            city:locdata.city,
                            country:locdata.country,
                            zipcode:locdata.zipcode,
                            distance:locdata.distance,
                            is_active:locdata.is_active,
                            latitude:locdata.latitude,
                            longitude:locdata.longitude,
                            alert_type:locdata.alert_type,
                            status_id:locdata.status_id
                        };
                        console.log(data);
                        $.ajax({
                            url: base_url + 'Signup/check',
                            type: 'post',
                            data: data,
                            dataType: 'json',
                            beforeSend: function () {
                                $('#block').show();
                                //$console.log('data'+data);
                            },
                            success: function (rs) {
                                //console.log('res'+rs);
                                if (rs.status === true) {
                                    swal({
                                        title: "Success!",
                                        text: rs.msg,
                                        icon: "success",
                                        confirmButtonText: "OK"
                                    });
                                    window.location.href=base_url+'Login'
                                } else {
                                    swal({
                                        title: "Failed!",
                                        text: rs.msg,
                                        icon: "error",
                                    });
                                }
                            },
                            complete: function () {
                                $('#block').hide();
                            }
                        });
                    }
                });
            });
            //JSON concadination
            function jsonConcat(o1, o2) {
              for (var key in o2) {
                o1[key] = o2[key];
              }
              return o1;
            }
        </script>
    </body>
</html>