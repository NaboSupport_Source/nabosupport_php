<?php
/*
 * Author: Bharath,Jayamurugan
 * Description: Login page
 * Date : 20-06-2018
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title; ?> | Nabosupport</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/login.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/spinner.css'); ?>" />
         <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>" />
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.validate.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script>
            // global declare base url
            var base_url = '<?php echo base_url(); ?>';
        </script>
    </head>
    <body class="text-center">
        <div class="cs-loader" id="block">
            <div class="cs-loader-inner">
                <div class="lds-ripple"><div></div><div></div></div>
            </div>
        </div>
        <div class="content">
        <div class="logo">
             <img src="http://rayi.in/nabosupport/assets/img/logo.png" alt="logo">
        </div>
        <form class="form-signin" id="loginForm" novalidate>
<!--            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>-->
                
            <div class="form-group">
                <label for="email" class="sr-only">Email address</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Email address" autofocus>
            </div>
            <div class="form-group">
                <label for="password" class="sr-only">Password</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            <p>Don't have an account. <a href="<?php echo base_url('Signup'); ?>">Signup Here</a></p>
            <p class="mt-5 mb-3 text-muted">Nabo support - &copy; <?php echo date('Y'); ?></p>
        </form>
        </div>
        <script>
            $(function () {
                $('#loginForm').submit(function(e) {
                    e.preventDefault();
                }).validate({
                    errorElement: 'div',
                    errorClass: 'help-block',
                    rules: {
                        email: {
                            required: {
                                depends: function () {
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            },
                            email: true
                        },
                        password: {
                            required: {
                                depends: function () {
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }
                            }
                        }
                    },
                    messages: {
                        email: {
                            required: 'Please enter email id',
                            email: 'Please enter valid email id'
                        },
                        password: {
                            required: 'Please enter password'
                        }
                    },
                    submitHandler: function () {
                        //window.location.href=base_url+'Shop';
                        //alert('pass'+$('#password').val());
                        $.ajax({
                            url: base_url + 'Login/validate',
                            type: 'post',
                            data: {email: $('#email').val(), password: $('#password').val()},
                            dataType: 'json',
                            beforeSend: function () {
                                $('#block').show();
                                //$console.log('data'+data);
                            },
                            success: function (rs) {
                                //console.log('res'+rs);
                                if (rs.status === true) {
                                    window.location.href=base_url+'Shop'
                                    localStorage.setItem('userData',JSON.stringify(rs.data));
                                } else {

                                }
                            },
                            complete: function () {
                                $('#block').hide();
                            }
                        });
                    }
                });
            });
        </script>
    </body>
</html>