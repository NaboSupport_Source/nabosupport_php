<?php
/*
 * Author: Bharath,Jayamurugan
 * Description: Shop page
 * Date : 20-06-2018
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group text-center">
<!--                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Select category to shop
                </button>
                <div class="dropdown-menu dropdown-menu-right" id="cat-drop">
                  <button class="dropdown-item" type="button">Sticker</button>
                  <button class="dropdown-item" type="button">Random Sticker</button>
                  <button class="dropdown-item" type="button">Light</button>
                </div>-->
                <select name="cars" id="cat-drop">Select category
                    <option value="all">All</option>
                 </select>
            </div>
<!--            <h1>hello <?php $data = $this->session->userdata('logged_in');echo $data['username']; ?></h1>-->
<!--            <h1>requested url is <?php $data = $this->session->userdata('req_url');echo $data; ?></h1>-->
        </div>
    </div>
    <div class="row" id="loadProduct">

    </div>
</div>

<script>
    $(document).ready(function () {
        getCategory();
        getProduct();
        $("#cat-drop").change(function(){
        var cat_id = $('#cat-drop').val();
        console.log('on change id :'+cat_id);
        getProductByID(cat_id);
        });
//        $("#add-cart").click(function() {
//            alert('buy clicked');
//            $.ajax({
//                url:base_url+'Shop/getCartCount',
//                type: 'get',
//                dataType: 'json',
//                success:function(rs){
//                    console.log('cart count:'+JSON.stringify(rs));
//                }
//            });
//        });
    });
    function getProduct() {
        $.ajax({
            url: base_url + 'Shop/getProduct',
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $('#block').show();
            },
            success: function (rs) {
                var out ='';
                $.each(rs,function(ind,row){
                    out = out + '<div class="col-md-4"><p><img class="img-fluid" src="'+base_url+'assets/upload/product/'+row.img_path+'"></p><p>Name : '+row.name+'</p><p>Price : '+row.price+'</p><p><button onclick="addCart('+row.id+',\''+row.name+'\',\''+row.price+'\')" class="btn btn-primary" id="add-cart">Buy</button></p></div>';
                });
                $('#loadProduct').html(out);
            },
            complete: function () {
                $('#block').hide();
            }
        });
    }
    //getcategory
    
    function getCategory() {
        $.ajax({
            url: base_url + 'Shop/getCategory',
            type: 'GET',
            dataType: 'json',
            success: function (rs) {
                $.each(rs.data,function(ind,row){
                    $('#cat-drop').append($('<option>').text(row.cat_name).attr('value', row.cat_id));
               });
            }
        });
    }
    
    function getProductByID(id) {
        $.ajax({
            url: base_url + 'Shop/getProductByID',
            type: 'POST',
            data: {id: id},
            dataType: 'json',
            success: function (rs) {
                var out ='';
                if (rs.status === true) {
                    console.log(rs);
                    $.each(rs.data,function(ind,row){
                        out = out + '<div class="col-md-4"><p><img class="img-fluid" src="'+base_url+'assets/upload/product/'+row.img_path+'"></p><p>Name : '+row.name+'</p><p>Price : '+row.price+'</p><p><button onclick="addCart('+row.id+',\''+row.name+'\',\''+row.price+'\')" class="btn btn-primary">Buy</button></p></div>';
                    });
                    $('#loadProduct').html('');
                    $('#loadProduct').html(out);
                }
            }
        });
    }
    
    // add cart
    function addCart(id, name, price) {
        var qty = 1;
        $.ajax({
            url: base_url + 'Shop/addCart',
            type: 'POST',
            data: {id: id, name: name, price: price, qty: qty},
            dataType: 'json',
            success: function (rs) {
                if (rs.status === true) {
                    console.log(JSON.stringify(rs.session));
                    $.ajax({
                        url:base_url+'Shop/getCartCount',
                        type: 'get',
                        dataType: 'json',
                        success:function(rs){
                            console.log('cart count:'+JSON.stringify(rs));
                            $('#TotalCart').html(rs.items);
                        }
                    });
                }
            }
        });
    }
    
</script>
