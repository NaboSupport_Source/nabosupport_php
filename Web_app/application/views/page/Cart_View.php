<?php
/*
 * Author: Jayamurugan
 * Description: Shop page
 * Date : 20-06-2018
 */
?>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Product name</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Sub total</th>
            <th>Action</th>
        <tr>
    </thead>
    <tbody></tbody>
</table>
<script>
    $(document).ready(function () {
       getCart();
    });
    function getCart() {
        $.ajax({
            url: base_url + 'Shop/getCart',
            type: 'get',
            dataType: 'json',
            beforeSend: function () {
                $('#block').show();
            },
            success: function (rs) {
                if(rs.status===true){
                    $('tbody').empty();
                    $.each(rs.cart,function(ind,row){
                       $('tbody').append('<tr><td>'+row.name+'</td><td><span class="inc-spn" onclick="incQty(\''+ind+'\');"><i class="fa fa-plus-circle" aria-hidden="true"></i></span><input type="text" id="qty'+ind+'" value='+row.qty+'><span class="dec-spn" onclick="decQty(\''+ind+'\');"><i class="fa fa-minus-circle" id="dec-qty" aria-hidden="true"></i></span><span class="upt-spn" onclick="updateCart(\''+ind+'\');"><i class="fa fa-cart-plus" aria-hidden="true"></i></span></td><td>'+row.price+'</td><td>'+parseFloat(row.qty * row.price).toFixed(2)+'</td><td><span class="rm-spn" onclick="deleteCart(\''+ind+'\');"><i class="fa fa-trash" aria-hidden="true"></i></span></td></tr>'); 
                    });
                    $('tbody').append('<tr><td colspan="4" align="right">Grand total </td><td>'+rs.gtotal+'</td></tr>');
                    $('tbody').append('<tr><td colspan="2" align="left"><a href="'+base_url+'Shop"><i class="fa fa-repeat" aria-hidden="true"></i>Continue shopping</a></td><td colspan="3"><a href="'+base_url+'Checkout"><i class="fa fa-credit-card-alt" aria-hidden="true"></i>Checkout</a></td></tr>');
                    getCartCount();
                }else{
                     $('tbody').empty();
                     $('tbody').append('<tr><td class="empty-td" colspan="5" align="center">Cart Empty</td></tr>');
                     getCartCount();
                }
            },
            complete: function () {
                $('#block').hide();
            }
        });
    }
    
    function deleteCart(rowid,) {
        $.ajax({
            url: base_url + 'Shop/removeCartItem',
            type: 'post',
            data:{id:rowid},
            dataType: 'json',
            beforeSend: function () {
                
            },
            success: function (rs) {
                //location.reload();
                getCart();
            },
            complete: function () {
                
            }
        });
    }
    
    function getCartCount() {
        $.ajax({
            url:base_url+'Shop/getCartCount',
            type: 'get',
            dataType: 'json',
            success:function(rs){
                console.log('cart count:'+JSON.stringify(rs));
                $('#TotalCart').html(rs.items);
            }
        });
    }
    
    function incQty(id) {
        console.log('product id'+id);
        var inputQuantityElement = $("#qty"+id);
        var newQuantity = parseInt($(inputQuantityElement).val())+1;
        inputQuantityElement.val(newQuantity);
            
//         $.ajax({
//            url:base_url+'Shop/incCartQty',
//            type: 'post',
//            dataType: 'json',
//            success:function(rs){
//                console.log('cart count:'+JSON.stringify(rs));
//            }
//        });
    }
    
    function decQty(id) {
        console.log('dec qty'+id);
        var inputQuantityElement = $("#qty"+id);
        var newQuantity = parseInt($(inputQuantityElement).val())-1;
        inputQuantityElement.val(newQuantity);
    }
    
    function updateCart(id) {
        var inputQuantityElement = $("#qty"+id);
        var Quantity = parseInt($(inputQuantityElement).val());
        $.ajax({
            url: base_url + 'Shop/updateCartItemQty',
            type: 'post',
            data:{id:id,qty:Quantity},
            dataType: 'json',
            beforeSend: function () {
                
            },
            success: function (rs) {
                //location.reload();
                getCart();
            },
            complete: function () {
                
            }
        });
    }
</script>
