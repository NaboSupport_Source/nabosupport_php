<?php
?>
<section class="success">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead></thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Transaction ID:</td>
                            <td id="txid"><?php echo $_GET['tid']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Order ID:</td>
                            <td id="o_id"><?php echo $_GET['orderid']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Amount:</td>
                            <td id="amt"><?php echo number_format($_GET['amount']/100,2,',','.'); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>    
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="http://rayi.in/nabosupport/shop">Click here for go to main page</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="<?php echo base_url('Login/logout'); ?>">Click here to logout</a>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        var user_id = localStorage.getItem('user_id');
        var tx_id = $('#txid').text();
        var order_id = $('#o_id').text();
        var amount = $('#amt').text();
        $.ajax({
            url: base_url + 'Upgrade/setPaid',
            type: 'POST',
            data: {user_id: user_id, tx_id: tx_id, amount: amount, order_id: order_id},
            dataType: 'json',
            success: function (rs) {
                if (rs.status === true) {
                    
                }
            }
        });
    });
</script>