<?php
?>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form id="cam-form">
                    <div class="form-group">
                      <label for="exampleInputEmail1">CameraID</label>
                      <input type="text" class="form-control" id="camid" aria-describedby="emailHelp" placeholder="Enter CameraID">
<!--                      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Alert message</label>
                      <input type="text" class="form-control" id="alert" placeholder="Enter alert message">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#cam-form').submit(function(e) {
            if ($('#camid').val() === '' && $('#alert').val() === '') {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Fill all required field!'
                })
            } else if($('#camid').val() === ''){
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'CameraID is empty!'
                })
            } else if($('#alert').val() === '') {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Message is empty!'
                })
            } else {
                var url = "http://rayi.in/naboapi_v2/cameraAutoAlert"; // the script where you handle the form input.
                $.ajax({
                    cache: false,
                    type: 'POST',
                    url: url,
                    dataType : 'json',
                    data: { camid:$('#camid').val(), alert:$('#alert').val() }, // serializes the form's elements.
                    success: function(data)
                    {
                        if(data.status === true){
                            alert('Alert sent successfully.');
                            $('#cam-form')[0].reset();
                        } else {
                            alert('Network error please try later.');
                        }
                    }
                });
            }
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    });
</script>