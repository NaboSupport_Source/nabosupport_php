<style>
    .row {
        display: -ms-flexbox; /* IE10 */
        display: flex;
        -ms-flex-wrap: wrap; /* IE10 */
        flex-wrap: wrap;
        margin: 0 -16px;
    }

    .col-25 {
        -ms-flex: 25%; /* IE10 */
        flex: 25%;
    }

    .col-50 {
        -ms-flex: 50%; /* IE10 */
        flex: 50%;
    }

    .col-75 {
        -ms-flex: 75%; /* IE10 */
        flex: 75%;
    }

    .col-25,
    .col-50,
    .col-75 {
        padding: 0 16px;
    }

    .container {
        padding: 5px 20px 15px 20px;
    }

    input[type=text] {
        width: 100%;
        margin-bottom: 20px;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }
    input[type=textarea] {
        width: 100%;
        margin-bottom: 20px;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    label {
        margin-bottom: 10px;
        display: block;
        color:#535557;
    }

    .icon-container {
        margin-bottom: 20px;
        padding: 7px 0;
        font-size: 24px;
    }

    .btn {
        background-color: #4CAF50;
        color: white;
        padding: 12px;
        margin: 10px 0;
        border: none;
        width: 100%;
        border-radius: 3px;
        cursor: pointer;
        font-size: 17px;
    }

    .btn:hover {
        background-color: #45a049;
    }

    span.price {
        float: right;
        color: grey;
    }

</style>
<?php
$MerchantNumber = "600130585"; // Yourpay MerchantID
$accepturl = "http://rayi.in/nabosupport/Success"; // The URL where we should return your customer to. Normally a "Thank you" page
$callbackurl = "http://rayi.in/nabo-payment/"; // The URL where we sent the IPN, normally a programically confirmation page
$time = time();
$amount = 1*100; // Amount in lowest currency, 100 = 1 EUR
$currencycode = 208; // CurrencyCode for your transaction. Click here to see a complete list of currency codes https://en.wikipedia.org/wiki/ISO_4217
$orderid = "10001"; // Yourpay orderID
$ccrg = "0"; // If set to one, then subscriptions is activated
$language = "da-dk"; // Language of payment page – Full list to be found at https://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx
$comments = ""; // Additional comments to the transaction
$ct = "off"; // If CT is set, the transaction-fee will be forwarded to the consumer (and paid by the consumer)
$autocapture = "no";
?>
<section class="checkout">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h3>Checkout details</h3>
                <label for="name"><i class="fa fa-user"></i> Full Name</label>
                <input type="text" id="name" name="firstname" placeholder="John M. Doe">
                <label for="email"><i class="fa fa-envelope"></i> Email</label>
                <input type="text" id="email" name="email" placeholder="john@example.com">
                <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
                <input type="text" id="adr" name="address" placeholder="542 W. 15th Street">
                <label for="city"><i class="fa fa-institution"></i> City</label>
                <input type="text" id="city" name="city" placeholder="New York">
            </div>
            <div class="col-50">
                <label for="country">Country</label>
                <input type="text" id="country" name="state" placeholder="NY">
            </div>
            <div class="col-50">
                <label for="zip">Zip</label>
                <input type="text" id="zip" name="zip" placeholder="10001">
            </div>
            <div class="col-50" id="del-add">
                <label for="add">Enter Delivery address</label>
                <input type="textarea" name="address" id="add" class="new-add">
            </div>
        </div>
        <div class="row">
            <div class="col-50">
                <label><input class="hide" id="hid-add" type="checkbox" checked name="sameadr"> Shipping address same as billing</label>
                <?php echo "<a class='btn' href='https://payments.yourpay.se/betalingsvindue_summary.php?MerchantNumber=$MerchantNumber&ShopPlatform=ManualPayments&accepturl=$accepturl&callbackurl=$callbackurl&time=$time&use3d=0&amount=$amount&CurrencyCode=$currencycode&cartid=$orderid&lang=$language&ct=$ct&ccrg=$ccrg&comments=$comments&autocapture=$autocapture'>Continue to checkout</a>"; ?>
<!--                <button type="submit" class="btn" onclick="payment();">Continue to checkout</button>-->
            </div>          
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        if($('#hid-add').is(":checked")){
            $('#del-add').hide();
        }
        $('#hid-add').change(function () {
            if (!this.checked) 
            //  ^
               $('#del-add').fadeIn('slow');
            else 
                $('#del-add').fadeOut('slow');
        });
        console.log(localStorage.getItem('userData'));
        var userData = JSON.parse(localStorage.getItem('userData'));
        $('#name').val(userData[0]['username']);
        $('#email').val(userData[0]['email_id']);
        $('#adr').val(userData[0]['street_address']);
        $('#city').val(userData[0]['city']);
        $('#zip').val(userData[0]['zipcode']);
        $('#country').val(userData[0]['country']);
    });
    function payment() {
        
    }
</script>