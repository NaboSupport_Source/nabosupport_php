<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('logged_in'))) {
            redirect(base_url());
        }
    }

    public function index() {
        $data['title'] = 'Cart';
        $this->load->view('template/header', $data);
        $this->load->view('page/Cart_View');
        $this->load->view('template/footer');
    }
}
