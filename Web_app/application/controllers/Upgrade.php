<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upgrade extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_Model', 'user');
        if (empty($this->session->userdata('logged_in'))) {
            $req_url = $this->uri->segment(1);
            $this->session->set_userdata('req_url', $req_url);
            redirect(base_url());
        }
    }

    public function index() {
        $data['title'] = 'Upgrade';
        $this->load->view('template/header', $data);
        $this->load->view('page/Upgrade_View');
        $this->load->view('template/footer');
        $this->session->unset_userdata('req_url');
    }

    public function setPaid() {
        $data = array(
            'user_id' => $this->input->post('user_id'),
            'transaction_id' => $this->input->post('tx_id'),
            'amount' => $this->input->post('amount')
        );
        
        echo json_encode(array("status" => true, "data" => $this->user->setPaid($data)));
    }
}

