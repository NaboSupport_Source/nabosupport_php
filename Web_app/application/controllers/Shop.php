<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Shop_Model', 'shop');
        if (empty($this->session->userdata('logged_in'))) {
            redirect(base_url());
        } else if(!empty($this->session->userdata('req_url'))){
            redirect(base_url($this->session->userdata('req_url')));
        }
    }

    public function index() {
        $data['title'] = 'Shop';
        $this->load->view('template/header', $data);
        $this->load->view('page/Shop_View');
        $this->load->view('template/footer');
    }

//Add cart 
    public function addCart() {
        $cart_data = array(
            'id' => $this->input->post('id'),
            'name' => str_replace(',', ' ', $this->input->post('name')),
            'price' => $this->input->post('price'),
            'qty' => $this->input->post('qty')
        );
        $this->cart->insert($cart_data);
        $cart_products = $this->session->userdata('cart_products');
        // Add the new item
        $products = $cart_data;
        $cart_products[] = $products;
        // And put it back into the session
        $this->session->set_userdata('cart_products', $cart_products);
        echo json_encode(array('status' => true,'session'=>$this->session->userdata('cart_products')));
    }

// update cart
    public function updateCart() {
        $data = array(
            'rowid' => $this->input->post('row_id'),
            'qty' => $this->input->post('qty')
        );

        $this->cart->update($data);
        echo json_encode(array('status' => true));
    }

// remove cart
    public function removeCart() {
        $data = array(
            'rowid' => $this->input->post('row_id'),
            'qty' => 0
        );

        $this->cart->update($data);
        echo json_encode(array('status' => true));
    }

//updatecartquantity
    public function updateCartItemQty() {
        $data = array(
            'rowid' => $this->input->post('id'),
            'qty' => $this->input->post('qty')
        );

        $this->cart->update($data);
        echo json_encode(array('status' => true));
    }
// clear cart
    public function clearCart() {
        $this->cart->destroy();
        echo json_encode(array('status' => true));
    }

//    get cart
    public function getCart() {
        if (count($this->cart->contents()) > 0) {
            echo json_encode(array('status' => true, 'cart' => $this->cart->contents(), 'gtotal' => number_format($this->cart->total() ? $this->cart->total() : 0, 2)));
        } else {
            echo json_encode(array('status' => false));
        }
    }

//get cart count
    public function getCartCount() {
        $rows = count($this->cart->contents());
        $data = array(
            'items' => $rows
        );
        echo json_encode($data);
    }
//remove cart item
    public function removeCartItem() {
        $data = array(
            'rowid' => $this->input->post('id'),
            'qty' => 0
        );
        $res = $this->cart->update($data);
        echo json_encode(array('status' => $res));
    }
// get product items
    public function getProduct() {
        echo json_encode($this->shop->getProduct());
    }

    public function getCategory() {
        $data = $this->shop->getCategory();
        // Pass the result to the view
        echo json_encode(array('data'=>$data));
    }
    
    public function getProductByID() {
        $id = $this->input->post('id');
        $data = $this->shop->getProductByID($id);
        echo json_encode(array('status'=>true,'data'=>$data));
    }
    
}