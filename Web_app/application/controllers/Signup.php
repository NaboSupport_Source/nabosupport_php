<?php

/* 
 * Author:Bharath
 * Description:signup page
 * Date:06.07.2018
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_Model', 'user');
    }

    public function index() {
        $data['title'] = 'Signup';
        $this->load->view('page/Signup_View',$data);
    }
    
    public function check() {
        //print_r($this->input->post());
        $userdata = $this->input->post();
        $res = $this->user->check($userdata);
        echo json_encode($res);
    }
}