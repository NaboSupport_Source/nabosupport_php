<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
    if (empty($this->session->userdata('logged_in'))) {
            redirect(base_url());
        }
        $this->load->model('User_Model', 'user');
    }
public function index(){
 $data['title'] = 'Setting';
        $this->load->view('template/header', $data);
        $this->load->view('page/Setting_View');
        $this->load->view('template/footer');
}
public function getSetting(){
echo json_encode($this->user->getSettingModel());
}
public function updateSetting(){
$data =(object) array(
'tread_alert'=>$this->input->post('tread_alert'),
'refresh'=>$this->input->post('refresh'),
'threat_reset'=>$this->input->post('threat_reset'),
'pull_time' =>$this->input->post('pull_time'),
'shop_url'=>$this->input->post('shop_url'),
'offer'=>$this->input->post('offer')
);
$this->user->adminSettingModel($data);
echo json_encode(array('status'=>true));
}
}