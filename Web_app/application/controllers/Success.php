<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Success extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model('User_Model', 'user');
        if (empty($this->session->userdata('logged_in'))) {
            redirect(base_url());
        }
    }

    public function index() {
        $data['title'] = 'Success';
        $this->load->view('template/header', $data);
        $this->load->view('page/Success_View');
        $this->load->view('template/footer');
    }

}

