<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (empty($this->session->userdata('logged_in'))) {
            redirect(base_url());
        }
    }

    public function index() {
        $data['title'] = 'Checkout';
        $this->load->view('template/header', $data);
        $this->load->view('page/Checkout_View');
        $this->load->view('template/footer');
    }
}

