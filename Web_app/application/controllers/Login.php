<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_Model', 'user');
    }

    public function index() {
        $data['title'] = 'Login';
        $this->load->view('page/Login_View',$data);
    }
    
    public function validate() {
        $data = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password')
        );
        $userdata = $this->user->validate($data);
        // print_r($userdata['data']);exit;
        $array = json_decode(json_encode($userdata['data']), True);
        //print_r($array);
        $this->session->set_userdata('logged_in', $array[0]);
        $this->cart->insert($this->session->userdata('cart_products'));
        echo json_encode($userdata);
    }
    
    function logout(){
        $user_data = $this->session->all_userdata();
            foreach ($user_data as $key => $value) {
                if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                    $this->session->unset_userdata($key);
                }
            }
        $this->session->sess_destroy();
        redirect('login');
    }
}
