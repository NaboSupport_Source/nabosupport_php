// Page name: Offers and alarm page (This page load the offers for users from api)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MapPage } from '../map/map';

@IonicPage()
@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html',
})
export class OfferPage {
  //base_url='http://34.216.55.85/naboapi/upload/offer/';
  base_url='http://rayi.in/naboapi_v2/uploads/offer/';
  shop: any;
  itemCount: any;
  offers: any;
  categoryList: any = [];
  chooseCategory: any;
  rs:any;
  isEmpty:boolean;
  categoryTitle: { title: string, subTitle: string };
  category: any;
  adminData:any;
  offerURL:any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public AuthServiceProvider: AuthServiceProvider,
              private translate: TranslateService,
              public alertCtrl:AlertController,
              public browser:InAppBrowser,) {
  }

  //redirect to map page
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat',lat);
    localStorage.setItem('lastLng',lng);
    this.navCtrl.setRoot(MapPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferPage');
    this.getOffers();
    this.adminData = JSON.parse(localStorage.getItem('adminData'));
    //alert(JSON.stringify(this.adminData.data[3].data));
      for(let i=0; i<this.adminData.data.length; i++){
        console.log('service:'+this.adminData.data[i].data);
      }
      this.offerURL = this.adminData.data[3].data;
  }

  //redirected to Nabosupport web
  webpage() {
    let trans_btn_yes='';
    let trans_btn_no='';
    let title='';
    let text='';
    this.translate.get('Yes').subscribe(res=>{
      trans_btn_yes=res;
    });
    this.translate.get('No').subscribe(res=>{
      trans_btn_no=res;
    });
    this.translate.get('offer alert title').subscribe(res=>{
      title=res;
    });
    this.translate.get('offer alert text').subscribe(res=>{
      text = res;
    });
    let alert = this.alertCtrl.create({
      title:title,
      message: text,
      buttons: [
        {
          text:trans_btn_yes ,
          role: 'cancel',
          handler: () => {
            console.log('Yes clicked');
            //let openBrowser = this.browser.create('http://rayi.in/nabosupport/'+this.offerURL, '_system','location=no');
            let openBrowser = this.browser.create(this.offerURL, '_system','location=no');
          }
        },
        {
          text: trans_btn_no,
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    alert.present();
  }
  
  //get offers from api fro paid users
  getOffers() {
    this.AuthServiceProvider.getData('getOffers').then((res) => {
      this.rs = res;
      if (this.rs.status === true) {
        this.offers = this.rs.data;
        console.log('offer object',this.offers);
        if (this.rs.data.length === 0) {
          this.isEmpty = true;
        } else {
          this.isEmpty = false;
        }
      } else {
        this.isEmpty = true;
      }
    }, (err) => {
      console.log(err);
    })
  }
}
