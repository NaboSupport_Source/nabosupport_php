import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyaccountPage } from './myaccount';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [MyaccountPage],
  imports: [IonicPageModule.forChild(MyaccountPage),TranslateModule.forChild()],
  exports:[MyaccountPage]
})
export class MyaccountPageModule { }