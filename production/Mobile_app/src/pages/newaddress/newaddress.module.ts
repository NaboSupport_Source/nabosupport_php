import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewaddressPage } from './newaddress';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NewaddressPage,
  ],
  imports: [
    IonicPageModule.forChild(NewaddressPage),
    TranslateModule.forChild()
  ],
  exports: [
    NewaddressPage,
  ]
})
export class NewaddressPageModule {}
