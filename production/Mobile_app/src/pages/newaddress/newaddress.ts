import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController,ViewController,Events } from 'ionic-angular';
import { MapPage } from '../map/map';
import {SecurityPage} from '../security/security';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { GetUserAddressProvider } from '../../providers/get-user-address/get-user-address';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
declare var google;
@IonicPage()
@Component({
  selector: 'page-newaddress',
  templateUrl: 'newaddress.html',
})
export class NewaddressPage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  acService:any;
  autocompleteItems:any;
  autocomplete:any;
  LastLat1:any;
  LastLng1:any;
  options:any;
  map:any;
  marker:any;
  nabo_img:any;
  geocoder:any;
  currentPos:any;
  latlng:any;
  mapOptions:any;
  _ispaid: any;
  newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "country_code":"", "camtoken": "", "paid_member":"" };
  markers:any;
  addInput:any;
  responseData:any;
  toastmessage:any;
  _isPhone: boolean = false;
  _ifEdit: boolean = true;
  _isCountry: boolean = false;
  _isnormal: boolean = false;
  _isFirst:any;
  country_title: { title: string };
  _selectOpt: any = [
    {
      value: "+45"
    },
    {
      value: "+46"
    },
    {
      value: "+47"
    }
  ];
  constructor(
    private translate: TranslateService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public AuthServiceProvider: AuthServiceProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public geolocation: Geolocation,
    public events: Events
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewaddressPage');
    this.country_title = { title: "Select country" };
    let user = JSON.parse(localStorage.getItem('userData'));
    let paid = user.membership_id;
    if(paid == 1) {
      this._ispaid == true;
      this.newAddAry.paid_member = "1";
    } else {
      this._ispaid == false;
      this.newAddAry.paid_member = "0";
    }
    this.initMap();
  }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  //initiate google map
  initMap() {
    this.geocoder = new google.maps.Geocoder();
    this.options = {
      enableHighAccuracy: true
    };
    let trans_map_load='';
    this.translate.get('Map loading...').subscribe(res=>{
      trans_map_load=res;
    });
    let loader = this.loadingCtrl.create({
      spinner: 'crescent',
      content: trans_map_load
    });
    loader.present();
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {

      this.currentPos = pos;
      console.log(pos);
      console.log(pos.coords.latitude + ',' + pos.coords.longitude)

      this.latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

      this.mapOptions = {
        center: this.latlng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        clickableIcons: false,
        draggable:true
      };
      console.log('map initiated');
      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);
      this.addMarker(pos.coords.latitude, pos.coords.longitude);
      this.updateSearch();
      loader.dismiss();
    }, (err: PositionError) => {
      console.log("error : " + err.message);
      loader.dismiss();
    });
  }

  addMap() {
    let latLng = new google.maps.LatLng(this.LastLat1, this.LastLng1);
    let mapOptions = {
      center: latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      fullscreenControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      draggable: false,
      zoomControl: false
      //draggable: this.mapcondition, zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.LastLat1, this.LastLng1);
    this.updateSearch();
  }

  addMarker(lat, lng) {
    this.nabo_img = 'http://nabosupport.dk/api/v1/mapicon/new-markers/blue.svg';
    this.marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
      icon: this.nabo_img,
    });
    let trans_cnt='';
    this.translate.get('This is your position').subscribe(res=>{
      trans_cnt =res;
    });
    let content = "<p>"+trans_cnt+"</p>";
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(this.marker, 'click', (event) => {
      //infoWindow.open(this.map, marker);
      //alert(JSON.stringify(event));
    });
    let geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(this.map, 'dragend', () => {
      //console.log('marker length : '+this.markers.length);
      if(this.markers != undefined) {
        this.clearMarkers();
      }
      this.LastLat1 = this.map.getCenter().lat();
      this.LastLng1 = this.map.getCenter().lng();
      //this.addMarker(this.LastLat1,this.LastLng1);
      var markerpos = new google.maps.LatLng(this.LastLat1,this.LastLng1);
      this.marker.setPosition(markerpos);
      var coord = new Array();
      coord.push(this.LastLat1, this.LastLng1);
      let user = JSON.parse(localStorage.getItem('userData'));
      this.newAddAry.user_id = user.ID;
      this.newAddAry.latitude = coord[0];
      this.newAddAry.longitude = coord[1];
      console.log(coord[0] + ',' + coord[1]);
      let latlng = new google.maps.LatLng(this.LastLat1, this.LastLng1);
      let request = { latLng: latlng };
      console.log(this.LastLat1 + ',' + this.LastLng1);
      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          //console.log(rsltAdrComponent);
          console.log(result.formatted_address);
          //this.addressDisplayToast(result.formatted_address);
          this.autocomplete.query = result.formatted_address;
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            var data = new Array;
            var locationdata = new Array();
            for (var i = 0; i < rsltAdrComponent.length; i++) {
              var obj = rsltAdrComponent[i];
              data.push(obj.long_name);
            }
            if (data.length == 5) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[3];
              this.newAddAry.city = data[2];
              this.newAddAry.zipcode = data[4];
            }
            else if (data.length == 6) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[4];
              this.newAddAry.city = data[2];
              this.newAddAry.zipcode = data[5];
            } else if (data.length == 7) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[5];
              this.newAddAry.city = data[2];
              this.newAddAry.zipcode = data[6];
            }
            else if (data.length == 8) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[6];
              this.newAddAry.city = data[4];
              this.newAddAry.zipcode = data[7];
            } else if (data.length == 9) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[7];
              this.newAddAry.city = data[4];
              this.newAddAry.zipcode = data[8];
            } else if (data.length == 10) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[8];
              this.newAddAry.city = data[5];
              this.newAddAry.zipcode = data[9];
            }
          }
        }
      });
    });
  }

  addressDisplayToast(address) {
    let toast = this.toastCtrl.create({
      message: address,
      duration: 3000,
      position: 'top'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  updateSearch() {
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types: ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocomplete.query,
      //componentRestrictions: { country: 'AR' } 
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      self.autocompleteItems = [];
      console.log(predictions);
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      });
    });
  }

  toastViewer(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  clearMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers.length = 0;
  }

  chooseItem(item: any) {
    console.log('modal > chooseItem > item > ', item);
    //this.viewCtrl.dismiss(item);
    //console.log(item);
    this.addInput = this.autocomplete.query;
    console.log('query:'+this.addInput);
    this.geocoder = new google.maps.Geocoder;
    this.markers = [];
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
    this.geocoder.geocode({ 'placeId': item.place_id }, (results, status) => {
      console.log(results);
      // if (status === 'OK' && results[0]) {
      let position = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      };
      this.LastLat1 = results[0].geometry.location.lat();
      this.LastLng1 = results[0].geometry.location.lng();
      localStorage.setItem('accLatLng', JSON.stringify(position));
      console.log(position);
      if (results[0] != null) {
        // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
        //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
        var data = new Array;
        let rsltAdrComponent = results[0].address_components;
        let rslength = rsltAdrComponent.length;
        for (var i = 0; i < rslength; i++) {
          var obj = rsltAdrComponent[i];
          data.push(obj.long_name);

        }
        //console.log(locdata);
        let user = JSON.parse(localStorage.getItem('userData'));
        this.newAddAry.user_id = user.ID;
        this.newAddAry.latitude = position.lat;
        this.newAddAry.longitude = position.lng;
        console.log('new address');
        if (data.length == 5) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[3];
          this.newAddAry.city = data[2];
          this.newAddAry.zipcode = data[4];
        }
        else if (data.length == 6) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[4];
          this.newAddAry.city = data[2];
          this.newAddAry.zipcode = data[5];
        } else if (data.length == 7) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[5];
          this.newAddAry.city = data[2];
          this.newAddAry.zipcode = data[6];
        }
        else if (data.length == 8) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[6];
          this.newAddAry.city = data[4];
          this.newAddAry.zipcode = data[7];
        } else if (data.length == 9) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[7];
          this.newAddAry.city = data[4];
          this.newAddAry.zipcode = data[8];
        } else if (data.length == 10) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[8];
          this.newAddAry.city = data[5];
          this.newAddAry.zipcode = data[9];
        }
        console.log('new addres:' + JSON.stringify(this.newAddAry));
        this.autocomplete.query=item.description;
      } else {
        //alert("No address available");
        console.log("No address available");
      }
      var markerpos = new google.maps.LatLng(this.LastLat1,this.LastLng1);
      this.marker.setPosition(markerpos);
      this.map.setCenter({ lat: this.LastLat1, lng: this.LastLng1 });
    });
  }

  // edit time change radio checkbox action
  radioSelect(val) {
    this._isFirst = true;
    if (val === "device_detail") {
      this._isPhone = false;
      this._isCountry = false;
    } else if (val === "phone") {
      this._isPhone = true;
      this._isCountry = true;
    }
  }

  addNewAddress() {
    console.log(this.newAddAry);
    let trans_load_cnt ='';
    this.translate.get('Profile updating...').subscribe(res=>{
      trans_load_cnt=res;
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: trans_load_cnt
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);
    if(this.newAddAry.latitude == '' && this.newAddAry.longitude == '' && this.newAddAry.street_address == '') {
      let trans_toast_msg ='';
      this.translate.get('No address get please select correct address.').subscribe(res=>{
       trans_toast_msg =res;
      });
      this.toastmessage = trans_toast_msg;
      this.toastViewer(this.toastmessage);
    } else {
      this.newAddAry.primary_address = 'yes';
      this.AuthServiceProvider.postData(this.newAddAry, 'addFirstAddress').then((result) => {
        // loading.present();
        this.responseData = result;
        if (true == this.responseData.status) {
          let trans_toast_update_add='';
          this.translate.get('address added successfully').subscribe(res=>{
            trans_toast_update_add=res;
          });
          this.toastmessage = trans_toast_update_add;
          this.toastViewer(this.toastmessage);
          localStorage.setItem('userData',JSON.stringify(this.responseData.data[0]));
          let paidData = JSON.parse(localStorage.getItem('userData'));
          let paid = paidData.paid_member;
          if(paid === "1") {
            //alert('securitypage');
            this.navCtrl.setRoot(SecurityPage);
          } else {
            this.navCtrl.setRoot(MapPage);
            //alert('mappage');
          }    
          localStorage.setItem('session','_logged_in');
          let langData = JSON.parse(localStorage.getItem('userData'));
          let data = {"type":"loggedin","lang":langData.language};
          this.events.publish('user:changed', data);          
        } else {
          let trans_msg_try='';
          this.translate.get('failed please re-try.').subscribe(res=>{
            trans_msg_try=res;
          });
          this.toastmessage = trans_msg_try;
          this.toastViewer(this.toastmessage);
        }
      });
    }
  }
}
