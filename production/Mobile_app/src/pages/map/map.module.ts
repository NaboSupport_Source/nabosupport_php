import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapPage } from './map';
import { TranslateModule } from '@ngx-translate/core';
import { AgmCoreModule } from '@agm/core';
@NgModule({
  declarations: [MapPage],
  imports: [IonicPageModule.forChild(MapPage),TranslateModule.forChild(),AgmCoreModule],
  exports:[MapPage]
})
export class MapPageModule { }