// Page name: map page (This page load the map with alert markers)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { LanguageDirective } from './../../directives/language/language';
import { SettingPage } from './../setting/setting';
import { ModalPage } from './../modal/modal';
import { Component, ViewChild, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { NavController, NavParams, Platform, NavControllerBase, AlertController, LoadingController, ModalController, MenuController, IonicPage, Events, ToastController, DateTime } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { GoogleMaps } from '../../providers/google-service/google-maps';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { SigninPage } from '../signin/signin';
import { load } from 'google-maps';
import { fromEvent } from '../../../node_modules/rxjs/observable/fromEvent';
enum _Marker {
  RED = "new-markers/red.svg",
  YELLOW = "new-markers/yellow.svg",
  YELLOWALERT = "new-markers/yellow-alert.svg",
  GRAY = "new-markers/gray.svg",
  YELLOWSIR = "new-markers/yellow-siren.svg",
  BLUE = "new-markers/blue.svg"
}

enum _AlertState {
  N = "N",    //user in normal state
  B = "B",    //user alert as burglary
  S = "S",    //user alert as suspecious
  BP = "BP",  //user confirm burglary and alert neighbour
  BC = "BC",  //neighbour get burglary alert from neighbour
  SS = "SS",  //suspecious confirmed above 50%
  SP = "SP",  //user confirm suspecious and alert neighbour
  SC = "SC",  //neighbour get suspecious alert from neighbour
  GB = "GB",  //guest  burglary alert address type
  GS = "GS",  //guest suspecious alert address type
  NS = "NS",  //neighbour confirmed not suspecious alert 
  P = "P"     //threat was informed to police
}

declare var google: any;
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})

export class MapPage {
  options: GeolocationOptions;
  currentPos: Geoposition;
  regionals: any = [];
  currentregional: any;
  addressData: any = [];
  @ViewChild('map') mapElement;

  AGMmap = { lat: null, lng: null, zoom: null };
  map: any;
  markers: any;
  userid: any;
  responsedata: any;
  address_id: any;
  lat: any;
  lng: any;
  streetname: any;
  city: any;
  country: any;
  zipcode: any;
  LastLng1: any;
  LastLat1: any;
  alert_type: any;
  alertmsg: any;
  alertmsgsus: any;
  guestalertmsg: any;
  alertbtn: any;
  alertdata: any = {};
  resbtn: any;
  data: any = {};
  ddata: any = {};
  gddata: any = {};
  deviceListObj: any = {};
  marker: any;
  mapview: any;
  owner: any;
  address: any;
  resData: any;
  responseData: any;
  userData: any;
  userAllData: any;
  pushToAll: any;
  owneralertsend: any;
  push: any = {};
  valueData: any;
  nearData: any;
  range: any;
  rangeAvaUser: any;
  resdata: any;
  userdata: any;
  userDatachange: any;
  userupdate: any;
  nabo_img: any;
  submitProfile: any;
  locdata: any;
  deviceID: any;
  alert_input: any;
  othersAddress: any;
  ownerbtn: any;
  click: any;
  resDatac: any;
  altype: any;
  gData: any;
  policealert: any;
  ownerpoli: any;
  resResult: any;
  canData: any;
  refVar: any = 'no';
  autoTime: any;
  status: any;
  msg: any;
  unit: any;
  countstatus: any;
  sendtype: any;
  guestalertmsgsus: any;
  infoMsg: any;
  title: any;
  subtitle: any;
  text: any;
  alerttxtmsg: any;
  yes: any;
  no: any;
  postData: any;
  sdata: any;
  mapStyleData: any;
  SrangeAvaUser: any;
  policeAlert: any;
  sub: any;
  guest: boolean = false;
  locationData: any;
  pushUrl: any;
  user_id: any;
  new_thread: any;
  autoPushCounter:boolean;
  stopCondition: boolean;
  userDataUpdate: boolean;
  userLocUpdate: boolean;
  trackState: boolean = false;
  Blat: any;
  Blng: any;
  trackData: any;
  eventTime: any;
  lastLat: any;
  lastlng: any;
  addressID: any;
  adminData: any;
  alertTime: any;
  refreshTime: any;
  resetTime: any;
  markerUrl: any;
  toastmessage: any;
  pageValue: any;
  timeLeft:number;
  interval:any;
  base_url: any = 'http://54.93.200.112/naboapi/';
  tranText: any = {
    "Cancelled": "Cancelled",
    "Process": "Your Process cancelled.",
    "sent": "SENT",
    "sentmsg": "your message sent successfully.",
    "canalert": "Cancel Alert",
    "canalertconfirm": "Do you want to Cancel Alert ?",
    "conaction": "Confirm Action",
    "sendalertconfirm": "Do you want to send Alert ?",
    "yes": "Yes",
    "no": "No",
    "cancel": "cancel",
    "neighboursent": "Message sent to your Neighbours.",
    "failed": "Failed",
    "noneighbour": "You have no neighbours",
    "selectneighbour": "Do you want to Select more neighbours Alert ?",
    "wrong": "Somthing went wrong",
    "noaddress": "Cannot determine address at this location.",
    "policeinfo": "Already Police Informed.",
    "wait": "Please wait.."

  }
  constructor(public navCtrl: NavController,
    private platform: Platform,
    public AuthServiceProvider: AuthServiceProvider,
    public maps: GoogleMaps,
    public http: Http, public events: Events,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private translate: TranslateService,
    public navParams: NavParams, private geolocation: Geolocation,
    public backgroundGeolocation: BackgroundGeolocation,
    public locationaccuracy: LocationAccuracy,
    private menu: MenuController, public zone: NgZone, private ref: ChangeDetectorRef) {
    this.locateAlert();
    this.pageValue = navParams.get('page');

  }

  //default ionic lifecycle events
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //JSON concodination function
  jsonConcat(o1, o2) {
    for (var key in o2) {
      o1[key] = o2[key];
    }
    return o1;
  }

  //conver to float value
  convertString(value) {
    return parseFloat(value);
  }

  //function for get user address ID from app component
  locateAlert() {
    this.addressID = this.navParams.get('address_id');
    console.log('location of alert ' + this.addressID);
    var add_id = { "address_id": this.addressID };
  }

  toastViewer(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }


  //get the user details form api for set related UI for user 
  getUser() {
    let userdata: any;
    userdata = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
      console.log(result);
      this.userData = result[0];
      localStorage.setItem('userData', JSON.stringify(this.userData));
    }, (err) => {
      console.log(err)
    });
    this.maploadview();
  }

  //initiate map with the user's current type of status 
  maploadview() {
    var userdata: any;
    try {
      userdata = JSON.parse(localStorage.getItem('userData'));
    } catch (e) {
      console.log(e);
      this.navCtrl.setRoot(SigninPage);
    }
    if (userdata == null || userdata == '' || userdata == undefined) {
      this.navCtrl.setRoot(SigninPage);
    } else if (userdata.alert_type == 'SP' || userdata.alert_type == 'BP' || userdata.alert_type == _AlertState.N || userdata.alert_type == _AlertState.BC || userdata.alert_type == _AlertState.SC) {
      console.log("normal");
      this.timeLeft = -1;
      clearInterval(this.interval);
      this.mapview = true;
      this.owner = false;
      this.alertmsg = false;
      this.alertmsgsus = false;
      this.guestalertmsg = false;
      this.alertbtn = _AlertState.N;
      this.ownerbtn = "";
      this.resbtn = "";
    } else if (userdata.alert_type == 'B') {
      console.log("Bug");
      this.SrangeAvaUser = localStorage.getItem('user_count');
      this.countdown();
      localStorage.removeItem('sendtype');
      this.mapview = true;
      this.owner = true;
      this.ownerbtn = "";
      this.alertmsg = false;
      this.alertmsgsus = false;
      this.guestalertmsg = false;
      this.alertbtn = "";
      this.resbtn = "";
      this.data.msg = '';
    } else if (userdata.alert_type == 'S') {
      console.log("sup");
      this.SrangeAvaUser = localStorage.getItem('user_count');
      this.countdown();
      localStorage.removeItem('sendtype');
      this.mapview = true;
      this.owner = true;
      this.ownerbtn = "";
      this.alertmsg = false;
      this.alertmsgsus = false;
      this.guestalertmsg = false;
      this.alertbtn = "";
      this.resbtn = "";
      this.data.msg = '';
    } else if (userdata.status_type == 'P') {
      localStorage.setItem('cancel', 'yes');
    }
    // this.updateUserData();
    // this.updateUserLocation();
  }

  //function for Spot new threat in the map
  locateNewThreat() {
    this.user_id = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(this.user_id, 'newThreat').then((result) => {
      console.log(result);
      this.new_thread = result;
    }, (err) => {
      console.log(err);
    });
  }

  //Alert for notifying user the process was cancelled
  progressCancelAlert() {
    this.translate.get(this.tranText.Cancelled).subscribe(res => {
      this.title = res;
    });
    this.translate.get(this.tranText.Process).subscribe(res => {
      this.subtitle = res;
    });
    let alert = this.alertCtrl.create({
      title: this.title,
      subTitle: this.subtitle,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Yes clicked');
            localStorage.setItem('cancel', 'no');
          }
        }
      ]
    });
    alert.present();
  }
  //owner cancel the alert process
  ownercancel() {
    localStorage.removeItem('onetimepush');
    let canloader = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    canloader.present();
    this.userData = JSON.parse(localStorage.getItem('alertdata'));
    this.AuthServiceProvider.postData(this.userData, 'cancelpush').then((result) => {
      this.responseData = result;
      console.log(this.responseData);
      if (true == this.responseData.status) {
        canloader.dismiss();
        console.log('canceled');
        this.refresh();
        //this.progressCancelAlert();
      }
    }, (err) => {
      console.log(err);
      canloader.dismiss();
    });
  }

  //alert for notifying the user alert was sent
  pushSendAlert() {
    this.translate.get(this.tranText.sent).subscribe(res => {
      this.title = res;
    });
    this.translate.get(this.tranText.sentmsg).subscribe(res => {
      this.subtitle = res;
    });
    let alert = this.alertCtrl.create({
      title: this.title,
      subTitle: this.subtitle,
      buttons: ['Ok']
    });
    alert.present();
  }

  //alert for notifying the user(owner) alert was sent
  pushSendAlertOwner(status, msg) {
    let alert = this.alertCtrl.create({
      title: status,
      subTitle: msg,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Yes clicked');
            //this.refresh();
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  //open the modal after the alert sent shows thanks message
  pushSendOpenModal() {
    let modal = this.modalCtrl.create('ModalPage');
    modal.present()
  }

  //confirmation for the user  who cancel alert 
  presentAlert() {
    this.translate.get(this.tranText.canalert).subscribe(res => {
      this.title = res;
    });
    this.translate.get(this.tranText.canalertconfirm).subscribe(res => {
      this.subtitle = res;
    });
    this.translate.get(this.tranText.yes).subscribe(res => {
      this.yes = res;
    });
    this.translate.get(this.tranText.no).subscribe(res => {
      this.no = res;
    });
    let alert = this.alertCtrl.create({
      title: this.title,
      message: this.subtitle,
      buttons: [
        {
          text: this.yes,
          role: 'cancel',
          handler: () => {
            console.log('Yes clicked');
            this.stopTimer();
            this.mapReset();
          }
        },
        {
          text: this.no,
          handler: () => {
            this.refresh();
          }
        }
      ]
    });
    alert.present();
  }

  //confirmation for send the guest alert
  sendAlertConfirm() {
    this.translate.get(this.tranText.conaction).subscribe(res => {
      this.title = res;
    });
    this.translate.get(this.tranText.sendalertconfirm).subscribe(res => {
      this.subtitle = res;
    });
    this.translate.get(this.tranText.yes).subscribe(res => {
      this.yes = res;
    });
    this.translate.get(this.tranText.no).subscribe(res => {
      this.no = res;
    });

    let alert = this.alertCtrl.create({
      title: this.title,
      message: this.subtitle,
      buttons: [
        {
          text: this.yes,
          handler: () => {
            console.log('Cancel clicked');
            this.sendguestpush();
          }
        },
        {
          text: this.no,
          handler: () => {
            console.log('Buy clicked');
            this.guestalertmsg = false;
            this.guestalertmsgsus = false;
            this.gddata['msg'] = "";
            this.refresh();
          }
        }
      ]
    });
    alert.present();
  }


  //send burglary alert for the non-nabo user
  guestAddB() {
    this.guestalertmsg = true;
    this.guestalertmsgsus = false;
    var data = JSON.parse(localStorage.getItem('userData'));
    console.log(data);
    this.locdata['user_id'] = data['user_id'];
    this.locdata['user_type'] = _AlertState.GB;
    this.locdata['alert_type'] = _AlertState.B;
    localStorage.setItem('lastLat', this.locdata.latitude);
    localStorage.setItem('lastLng', this.locdata.longitude);
  }

  //send suspicious alert for the non-nabo user
  guestAddS() {
    this.guestalertmsgsus = true;
    this.guestalertmsg = false;
    var data = JSON.parse(localStorage.getItem('userData'));
    console.log(data);
    this.locdata['user_id'] = data['user_id'];
    this.locdata['user_type'] = _AlertState.GS;
    this.locdata['alert_type'] = _AlertState.S;
    localStorage.setItem('lastLat', this.locdata.latitude);
    localStorage.setItem('lastLng', this.locdata.longitude);
  }

  //Reset the map and status of user after the cancel process
  mapReset() {
    var userdata = JSON.parse(localStorage.getItem('userData'));
    userdata.alert_type = _AlertState.N;
    this.AuthServiceProvider.postData(userdata, 'ownerCancel').then((result) => {
      console.log(result);
      this.responseData = result;
      if (true == this.responseData.status) {
        console.log(this.responseData.status);
        this.refresh();
        this.owner = false;
      } else {
        console.log("Operation failed");
      }
    }, (err) => {
      console.log(err);
    });
  }

  //Map refresh
  refresh() {
    console.log('page refreshed');
    var data = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(data, 'getUserAddressByCountry').then((result) => {
      console.log(result);
      this.addressData = result;
      var userdata = JSON.parse(localStorage.getItem('userData'));
      this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
        localStorage.setItem('userData', JSON.stringify(result[0]));
        console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));
        var lat = parseFloat(localStorage.getItem('lastLat'));
        var lng = parseFloat(localStorage.getItem('lastLng'));
        this.othersAddress = localStorage.getItem('lastadd');
        var latlng = { "lat": lat, "lng": lng };
        this.map.setCenter({ lat: lat, lng: lng });
        this.infoMsg = "";
        this.alertbtn = "N";
        this.ownerbtn = "";
        this.resbtn = "";
        this.policealert = "";
        this.ownerpoli = "";
        this.getUseraddress(userdata);
      });
    });
  }
  //get home location of the user 
  getHome() {
    this.mapview = true;
    this.owner = false;
    this.alertmsg = false;
    this.alertmsgsus = false;
    this.guestalertmsg = false;
    this.ownerbtn = "";
    this.resbtn = "";
    this.policealert = "";
    this.ownerpoli="";
    this.infoMsg = false;
    console.log('get home clicked');
    var userPrim = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(userPrim,'getHome').then((result) => {
      console.log(result);
      this.responseData = result;
      let user:any;
      if(this.responseData.length>0) {
        user = this.responseData[0];
      } else {
        user = userPrim;
      }
      var lat = parseFloat(user.latitude);
      var lng = parseFloat(user.longitude);
      this.map.setCenter({ lat: lat, lng: lng });
      this.othersAddress = `${user.street_address}, ${user.city}, ${user.country} `;
    },(err) => {

    })
  }

  //cancel push send process
  cancelpush() {
    this.mapview = true;
    this.presentAlert();
  }

  //Registered user send the alert to all
  sendcurpush() {
    this.owner = "";
    this.translate.get(this.tranText.wait).subscribe(res => {
      this.title = res
    });
    let loader = this.loadingCtrl.create({ content: this.title });
    loader.present();
    setTimeout(()=>{
      loader.dismiss();
      localStorage.setItem('tog', '');
      this.userDatachange = JSON.parse(localStorage.getItem("userData"));
      this.userDatachange['alert_type'] = _AlertState.BP;
      localStorage.setItem("userData", JSON.stringify(this.userDatachange));
      console.log("page move");
      this.sendtype = localStorage.getItem('sendtype');
      this.stopTimer();
      this.translate.get(this.tranText.canalert).subscribe(res => {
        this.title = res;
      });
      //this.processReset();
      if (this.sendtype == 'auto') {  
        this.translate.get(this.tranText.sent).subscribe(res => {
          this.subtitle = res;
        });
        this.translate.get(this.tranText.neighboursent).subscribe(res => {
          this.yes = res;
        });
        this.status = this.subtitle;
        this.msg = 'Automatically forwarded the alert notification to your neighbours.';
      } else {
        this.translate.get(this.tranText.sent).subscribe(res => {
          this.subtitle = res;
        });
        this.translate.get(this.tranText.neighboursent).subscribe(res => {
          this.yes = res;
        });
        this.status = this.subtitle;
        this.msg = this.yes;
      }
      this.pushSendAlertOwner(this.status, this.msg);
      this.sendtype = JSON.parse(localStorage.getItem('onetimepush'));
    },2000);
    var user = JSON.parse(localStorage.getItem('userData'));
    let addId = JSON.parse(localStorage.getItem('camdata'));
    if(user.platform == 'ios'){
      let address_id = addId['gcm.notification.address_id'];
      user.address_id = address_id;
    } else if(user.platform == 'Android'){
      let address_id = addId['address_id'];
      user.address_id = address_id;
      console.log('address_id'+address_id);
    }
    this.AuthServiceProvider.postData(user, 'camPushall').then((result) => {
      console.log(result);
      //this.owner = "";
      this.responseData = result;
      if (true === this.responseData.status) {
        console.log(this.responseData);
      } else {
        this.translate.get(this.tranText.failed).subscribe(res => {
          this.subtitle = res;
        });
        this.translate.get(this.tranText.neighboursent).subscribe(res => {
          this.yes = res;
        });
        this.status = this.subtitle;
        this.msg = 'Message not sent to your Neighbours. you can resend or cancel the process.';
        this.pushSendAlertOwner(this.status, this.msg);
      }
    }, (err) => {
      loader.dismiss();
      console.log(err);
      this.owner = "";
      this.status = this.subtitle;
      this.msg = err;
      this.pushSendAlertOwner(this.status, this.msg);
    });
  }

  //send guest alert to all neighbours
  sendguestpush() {
    this.guestalertmsg = false;
    this.guestalertmsgsus = false;
    this.userDataUpdate = true;
    this.userLocUpdate = true;
    this.translate.get(this.tranText.wait).subscribe(res => {
      this.title = res;
    })
    let loader = this.loadingCtrl.create({ 
      content: this.title 
    });
    setTimeout(() => {
      loader.dismiss();
    }, 5000);
    this.locdata['msg'] = this.gddata['msg'];
    this.locdata['datetime'] = this.getDateTime();
    this.AuthServiceProvider.postData(this.locdata, 'addGuest').then((result) => {
      console.log(result);
      localStorage.setItem('lastLat', this.locdata.latitude);
      localStorage.setItem('lastLng', this.locdata.longitude);
      this.responseData = result;
      if (this.responseData.status == true) {
        this.alertdata = this.responseData.data[0];
        if (this.alertdata.status_type === 'B') {
          var marker = 'red.svg';
        } else {
          var marker = 'yellow.svg';
        }
        this.addNaboMarker(marker, this.alertdata.latitude, this.alertdata.longitude);
        this.pushSendOpenModal();
        localStorage.setItem('alertdata', JSON.stringify(this.alertdata));
        this.alertdata = JSON.parse(localStorage.getItem('alertdata'));
        console.log(this.alertdata);
        this.alertdata['alert_type'] = _AlertState.B;
        this.alertdata['msg'] = this.gddata['msg'];
        this.userAllData = JSON.parse(localStorage.getItem("userData"));
        this.alertdata['sender_id'] = this.userAllData['user_id'];
        localStorage.setItem('alertdata', JSON.stringify(this.alertdata));
      }
      console.log("sendpush");
      var cur_user_data = JSON.parse(localStorage.getItem('userData'));
      this.userData = JSON.parse(localStorage.getItem('alertdata'));
      this.userData['user_id']
      this.userData['distance'] = cur_user_data['r_distance'];
      this.userData['msg'] = this.gddata['msg'];
      this.userData['time'] = this.getDateTime();
      console.log(this.userData);
      this.guest = false;
      this.AuthServiceProvider.postData(this.userData, 'gpushall').then((result) => {
        this.responseData = result;
        console.log('gest push success');
        this.userDatachange = JSON.parse(localStorage.getItem("userData"));
        this.userDatachange['alert_type'] = _AlertState.N;
        localStorage.setItem("userData", JSON.stringify(this.userDatachange));
        this.infoMsg = this.userData.msg;
        this.refresh();
        this.gddata.msg = "";
        console.log("page move");
        //this.processReset();
      }, (err) => {
        console.log(err);
      });
    });
  }

  //get the nabosupport user's address details from api
  getUseraddress(data): boolean {
    this.AuthServiceProvider.postData(data, 'getUserAddressByCountry').then((result) => {
      console.log(result);
      this.addressData = result;
      var userdata = JSON.parse(localStorage.getItem('userData'));
      this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
        localStorage.setItem('userData', JSON.stringify(result[0]));
        console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));
        var paid = result[0]['paid_member'];
        this.events.publish('user:paid', paid);
        this.AGMloadMap();
      });
    });
    return true
  }
  //refresh address detail
  refreshAddress() {
    let data = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(data, 'getUserAddressByCountry').then((result) => {
      console.log(result);
      this.addressData = result;

      var userdata = JSON.parse(localStorage.getItem('userData'));
      this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
        localStorage.setItem('userData', JSON.stringify(result[0]));
        console.log("userdata:" + JSON.parse(localStorage.getItem('userData')));
        var lat = localStorage.getItem('lastLat');
        var lng = localStorage.getItem('lastLng');
        this.map.setCenter({ lat: parseFloat(lat), lng: parseFloat(lng) });
      });
    });
  }

  //get local date and time
  getDateTime() {
    // var today = new Date();
    //var dateTime = today.toLocaleString();
    // var date = today.getDate()+ '-' + (today.getMonth() + 1)+'-' +today.getFullYear();
    // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    // var dateTime = date + ' ' + time;
    // return dateTime;
    var today = new Date();
    //var dateTime = today.toLocaleString();
    var date = today.getDate()+ '/' + (today.getMonth() + 1)+'/' +today.getFullYear();
    var time = new Date();
    var dateTime = date + ' : ' + time.toLocaleString('en-DN', { hour: 'numeric', minute: 'numeric', hour12: true });
    return dateTime;
  }

  //send alert push notification to nearby users 
  alert() {
    this.alertmsg = false;
    this.alertmsgsus = false;
    this.guestalertmsg = false;
    var alertmsgdata = this.alertdata;
    alertmsgdata['msg'] = this.ddata['msg'];
    alertmsgdata['datetime'] = this.getDateTime();
    localStorage.setItem('alertdata', JSON.stringify(alertmsgdata));
    console.log("data:" + JSON.stringify(alertmsgdata));
    var pushLoader = this.loadingCtrl.create({
      content: 'Sending alert...'
    });
    pushLoader.present();
    this.AuthServiceProvider.postData(alertmsgdata, 'alertmsg').then((result) => {
      console.log(result);
      localStorage.setItem('tog', '');
      this.AuthServiceProvider.postData(alertmsgdata, 'push').then((result) => {
        this.responseData = result;
        console.log(this.responseData);
        if (true === this.responseData.status) {
          console.log(this.responseData);
          this.infoMsg = alertmsgdata.msg;
          var latlang = JSON.parse(localStorage.getItem('alertdata'));
          var center = {
            "lat": latlang.latitude,
            "lng": latlang.longitude
          }
          pushLoader.dismiss();
          this.refresh();
          this.ddata.msg = '';
          this.pushSendOpenModal();
        }
      }, (err) => {
        pushLoader.dismiss();
        // Error log
        console.log(err);
      });
    },(err) =>{
      pushLoader.dismiss();
    });
  }

  //burglery alert button click 
  burglery() {
    var toggle = localStorage.getItem('tog');
    if (toggle == 'y') {
      this.alertdata.alert_type = _AlertState.B;
      this.userData = JSON.parse(localStorage.getItem("userData"));
      this.alertdata.sender_id = this.userData.user_id;
      this.alertmsg = true;
      this.alertmsgsus = false;
    } else if (toggle == '') {
      this.guestAddB();
    }
  }

  //suspicious alert button click
  Suspicious() {
    var toggle = localStorage.getItem('tog');
    if (toggle == 'y') {
      this.alertdata.alert_type = _AlertState.S;
      this.userData = JSON.parse(localStorage.getItem("userData"));
      this.alertdata['sender_id'] = this.userData['user_id'];
      //localStorage.setItem('alertdata', JSON.stringify(this.alertdata));
      this.alertmsg = false;
      this.alertmsgsus = true;
    } else if (toggle == '') {
      this.guestAddS();
      //this.sendAlertConfirm();
    }
  }

  ionViewDidLoad() {
    let session = localStorage.getItem('session');
    if (session === '_logged_in') {
      //alert(this.pageValue);
      if (this.pageValue != 'notify' && this.pageValue != 'appcomp') {
        this.getCurrentPosition();
      }
      var data = JSON.parse(localStorage.getItem('userData'));
      this.getUseraddress(data); //for develop
      //this.enableLocation(); //for real device not support for ios
      this.userDataUpdate = false;
      this.userLocUpdate = false;
      console.log('ionViewDidLoad MapPage');
      let user = JSON.parse(localStorage.getItem('userData'));
      let userstatus = user.status_type;
      let useralert = user.alert_type;
      if (userstatus == 'B' || userstatus == 'S' || userstatus == 'NS' || userstatus == 'SS' || userstatus == 'P') {
        console.log('process reset initiated');
      }
      this.adminData = JSON.parse(localStorage.getItem('adminData'));
      console.log(this.adminData);
      for (let i = 0; i < this.adminData.data.length; i++) {
        console.log('service:' + this.adminData.data[i].data);
      }
      this.alertTime = this.adminData.data[0].data;
      this.refreshTime = this.adminData.data[1].data;
      this.resetTime = this.adminData.data[5].data;
      this.updateUserData();
      this.updateUserLocation();
    } else {
      this.navCtrl.setRoot(SigninPage);
    }
  }

  //Enable the GPS on user device
  enableLocation() {
    this.locationaccuracy.canRequest().then((canRequest: boolean) => {

      if (canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationaccuracy.request(this.locationaccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          (result) => {
            console.log('Request successful' + JSON.stringify(result));
            let userData = JSON.parse(localStorage.getItem('userData'));
            this.getUseraddress(userData);
          }, (error) => {
            console.log('Error requesting location permissions' + JSON.stringify(error));
          });
      }
    });
  }

  //get current position latitude and longitude
  getCurrentPosition() {
    let fetch = '';
    // let locLoader:any;
    // this.translate.get('Fetching location...').subscribe(res=>{
    //   fetch = res;
    // });
    let locLoader = this.loadingCtrl.create({
      spinner:'bubbles',
      cssClass: "map-loading"
    });
    locLoader.present();
    this.options = {
      timeout: 6000, enableHighAccuracy: true, maximumAge: 3800
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      locLoader.dismiss();
      var latlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
      //this.map.setCenter(latlng);
      console.log('from app comp', pos);
      localStorage.setItem('lastLat', JSON.stringify(pos.coords.latitude));
      localStorage.setItem('lastLng', JSON.stringify(pos.coords.longitude));
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'location': latlng }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            console.log(results[0].formatted_address);
            this.othersAddress = results[0].formatted_address;
            localStorage.setItem('lastadd', this.othersAddress);
            this.AGMloadMap();
          } else {
            this.othersAddress = 'Cannot determine address at this location.';
          }
        }
      });
    }, (err) => {
      locLoader.dismiss();
      console.log('Error message : ' + err.message);
      this.toastmessage = 'Cannot determine address at this location.so map load from your saved location.';
      this.toastViewer(this.toastmessage);
      let user = JSON.parse(localStorage.getItem('userData'));
      var lat = parseFloat(user.latitude);
      var lng = parseFloat(user.longitude);
      this.map.setCenter({ lat: lat, lng: lng });
      this.othersAddress = `${user.street_address}, ${user.city}, ${user.country} `;
      this.AGMloadMap();
    });
    // loading.dismiss();
  }

  //load angular google map
  AGMloadMap() {
    console.log('mapload');
    this.infoMsg = false;
    this.AGMmap.lat = parseFloat(localStorage.getItem('lastLat'));
    this.AGMmap.lng = parseFloat(localStorage.getItem('lastLng'));
    this.AGMmap.zoom = 15;
    var latlng = { lat: this.AGMmap.lat, lng: this.AGMmap.lng };
    this.map.setCenter(latlng);
    console.log('lat', this.AGMmap.lat, 'lng', this.AGMmap.lng);
    if (Number.isNaN(this.AGMmap.lat) && Number.isNaN(this.AGMmap.lng)) {
      console.log('call get position');
      //this.getCurrentPosition();
      this.getHome();
    } else {
      console.log(this.AGMmap);
      var geocoder = new google.maps.Geocoder();
      var latlng = { lat: this.AGMmap.lat, lng: this.AGMmap.lng };
      geocoder.geocode({ 'location': latlng }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            console.log(results[0].formatted_address);
            this.othersAddress = results[0].formatted_address;
          } else {
            this.othersAddress = 'Cannot determine address at this location.';
          }
          this.maploadview();
        }
      });
    }
  }
  conLocalTime(time) {
    var t = time.split(/[- :]/);
    // var d = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    return d.toUTCString();
  }
  //get latitude longidude from geocode plugin
  getGeocode(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    var latlng = { lat: lat, lng: lng };
    geocoder.geocode({ 'location': latlng }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          console.log(results[0]);
          console.log(results[0].formatted_address);
          this.othersAddress = results[0].formatted_address;
          localStorage.setItem('lastadd', this.othersAddress);
          this.alertbtn = _AlertState.N;
          this.policealert = "";
          this.resbtn = "";
          this.ownerbtn = "";
          this.ownerpoli = "";
          localStorage.setItem('tog', '');
          console.log(this.othersAddress);
          var t0 = performance.now();
          this.getGustAddress(results[0], latlng);
          var t1 = performance.now();
          console.log("Call to get location detail took " + (t1 - t0) + " milliseconds.");
          this.userData = JSON.parse(localStorage.getItem('userData'));
          this.locationData = {
            "user_id": this.userData.user_id,
            "distance": this.userData.distance,
            "latitude": latlng.lat,
            "longitude": latlng.lng
          };
        } else {
          this.alertbtn = "";
          this.policealert = "";
          this.resbtn = "";
          this.ownerbtn = "";
          this.ownerpoli = "";
          this.translate.get(this.tranText.noaddress).subscribe(res => {
            this.alerttxtmsg = res
          });
          this.othersAddress = this.alerttxtmsg;
          console.log(this.othersAddress);
          console.log('Cannot determine address at this location.' + status);
        }
      }
    });
  }

  //Event for handle when map ready
  mapReady(map) {
    this.map = map;
    this.LastLat1 = this.map.getCenter().lat();
    this.LastLng1 = this.map.getCenter().lng();
    this.getGeocode(this.LastLat1, this.LastLng1);
    //maps dragstart
    google.maps.event.addListener(this.map, 'dragstart', () => {
      this.infoMsg = null;
      this.alertmsg = false;
      this.alertmsgsus = false;
      this.guestalertmsg = false;
      this.guestalertmsgsus = false;
      this.userLocUpdate = true;
    });
    //maps dragend
    google.maps.event.addListener(this.map, 'dragend', () => {
      this.userLocUpdate = true;
      this.LastLat1 = this.map.getCenter().lat();
      this.LastLng1 = this.map.getCenter().lng();
      console.log(this.LastLat1, '', this.LastLng1);
      this.getGeocode(this.LastLat1, this.LastLng1);
    });
  }

  //check the user response for alert
  checkUserResponse(userData) {
    var response: any;
    this.AuthServiceProvider.postData(userData, 'checkUserResponse').then((result) => {
      console.log(result);
      response = result;
      console.log(response);
    });
    return response;
  }


  //dynamically change marker when on click and display message 
  markerClick(address) {
    this.map.setCenter({ lat: this.convertString(address.latitude), lng: this.convertString(address.longitude) });
    localStorage.setItem('lastLat', address.latitude);
    localStorage.setItem('lastLng', address.longitude);
    this.othersAddress = `${address.street_address}, ${address.city}, ${address.country}`;
    localStorage.setItem('lastadd', this.othersAddress);
    localStorage.setItem('lastAdd', this.othersAddress);
    this.alertdata = address;
    localStorage.setItem('alertdata', JSON.stringify(this.alertdata));
    console.log(this.alertdata);
    let markers = this.addressData;
    for (var i = 0; i < markers.length; i++) {
      switch (markers[i].status_type) {
        case (_AlertState.B):
          markers[i].marker_icon = _Marker.RED;
          break;
        case (_AlertState.S):
          markers[i].marker_icon = _Marker.YELLOW;
          break;
        case (_AlertState.SS):
          markers[i].marker_icon = _Marker.YELLOWALERT;
          break;
        case (_AlertState.NS):
          markers[i].marker_icon = _Marker.GRAY;
          break;
        case (_AlertState.P):
          markers[i].marker_icon = _Marker.YELLOWSIR;
          break;
        default:
          markers[i].marker_icon = _Marker.BLUE;
          break;
      }
    }
    this.marker = address;
    var user_id = this.marker.user_id;
    this.marker.marker_icon = "lg-" + this.marker.marker_icon;
    var owner_id = JSON.parse(localStorage.getItem('userData')).user_id;
    if (user_id == owner_id) {
      console.log('its owner');
      this.ownerbtn = "";
      this.policealert = "";
      this.ownerpoli = "";
      this.resbtn = "";
      var _ownerdata: any = {
        "user_id": this.marker.user_id,
        "address_id": this.marker.address_id,
        "address_type": this.marker.address_type
      };
      if (this.marker.status_type == _AlertState.SS || this.marker.status_type == _AlertState.S) {
        this.ownerpoli = "owner";
        this.ownerbtn = "";
        this.resbtn = "";
        this.alertbtn = "";
        this.infoMsg = this.marker.message;
        this.eventTime = this.marker.time;
      } else if (this.marker.status_type == _AlertState.NS) {
        this.ownerpoli = "";
        this.policealert = "";
        this.ownerbtn = "owner";
        this.resbtn = "";
        this.infoMsg = this.marker.message + '(canceled)';
        this.eventTime = this.marker.time;
      } else if (this.marker.status_type == _AlertState.P) {
        this.ownerpoli = "";
        this.policealert = "";
        this.ownerbtn = "owner";
        this.resbtn = "";
        this.alertbtn = "";
        let policetxt = '';
        this.translate.get('Already Police Informed.').subscribe(res => {
          policetxt = res;
        });
        this.infoMsg = this.marker.message + '('+policetxt+')';
        this.eventTime = this.marker.time;
      } else if (this.marker.status_type == "") {
        let hometxt='';
        this.translate.get('This is your home').subscribe(res => {
          hometxt = res;
        });
        this.ownerbtn = "";
        this.resbtn = "";
        this.alertbtn = "";
        this.policealert = "";
        this.ownerpoli = "";
        this.infoMsg = hometxt;
        this.eventTime = "";
      } 
      else if (this.marker.status_type == _AlertState.B) {
        this.infoMsg = this.marker.message;
        this.eventTime = this.marker.time;
        this.resbtn = "";
        this.alertbtn = "";
        this.policealert = "";
        this.ownerpoli = "";
        this.ownerbtn = "owner";
      } 
      //else if (this.marker.alert_type == 'BP') {
      //   this.infoMsg = this.marker.message;
      //   this.eventTime = this.marker.time;
      //   this.resbtn = "";
      //   this.alertbtn = "";
      //   this.policealert = "";
      //   this.ownerpoli = "";
      //   this.ownerbtn = "owner";
      // } else if (this.marker.alert_type == 'SP') {
      //   this.infoMsg = this.marker.message;
      //   this.eventTime = this.marker.time;
      //   this.resbtn = "";
      //   this.alertbtn = "";
      //   this.policealert = "";
      //   this.ownerbtn = "";
      //   this.ownerpoli = "owner";
      // }
    } else {
      console.log('neighbour');
      console.log(this.marker.status_type, ' ', this.marker.alert_type);
      if (this.marker.status_type == "" || this.marker.status_type == 0) {
        console.log(this.marker.status_type);
        localStorage.setItem('tog', 'y');
        let nabotxt='';
        this.translate.get('Nabosupporters').subscribe(res=>{
          nabotxt = res;
        });
        this.alertbtn = _AlertState.N;
        this.ownerbtn = "";
        this.resbtn = "";
        this.policealert = "";
        this.ownerpoli = "";
        this.infoMsg = nabotxt;
        this.eventTime = "";
      } else if (this.marker.status_type == 'P') {
        let policetxt = '';
        this.translate.get('Already Police Informed.').subscribe(res => {
          policetxt = res;
        });
        this.infoMsg = this.marker.message + '('+policetxt+')';
        this.eventTime = this.marker.time;
        this.policealert = "";
        this.ownerpoli = "";
        this.alertbtn = "";
        this.ownerbtn = "";
        this.resbtn = "";
        this.refVar = null;
      } else if (this.marker.status_type == 'B') {
        this.infoMsg = this.marker.message
        this.eventTime = this.marker.time;
        this.resbtn = "";
        this.alertbtn = "";
        this.ownerbtn = "";
        this.ownerpoli = "";
        this.policealert = "";
      } else if (this.marker.status_type == 'S' && this.marker.alert_type == 'S') {
        this.infoMsg = this.marker.message
        this.eventTime = this.marker.time;
        this.resbtn = "";
        this.alertbtn = "";
        this.ownerbtn = "";
        this.ownerpoli = "";
        this.policealert = "";
      } else if (this.marker.status_type == 'S' && this.marker.alert_type == 'SP' || this.marker.status_type == 'SS' && this.marker.alert_type == 'SP' || this.marker.status_type == 'S' && this.marker.alert_type == 'BP'|| this.marker.status_type == 'SS' && this.marker.alert_type == 'BP') {
        var response: any;
        let curdata = JSON.parse(localStorage.getItem('userData'));
        let ruserdata: any = {};
        ruserdata = {
          "user_id": curdata.user_id,
          "sus_user_id": this.marker.user_id,
          "sus_address_id": this.marker.address_id,
          "address_type": this.marker.address_type
        };
        this.AuthServiceProvider.postData(ruserdata, 'checkUserResponse').then((result) => {
          console.log(result);
          response = result;
          console.log(response);
          if (response.status == true) {
            console.log(response.status);
            this.infoMsg = this.marker.message
            this.eventTime = this.marker.time;
            this.resbtn = "";
            this.alertbtn = "";
            this.ownerbtn = "";
            this.ownerpoli = "";
            this.policealert = "SS";
            localStorage.removeItem('victimData');
          } else {
            this.infoMsg = this.marker.message
            this.eventTime = this.marker.time;
            this.resbtn = _AlertState.S;
            this.alertbtn = "";
            this.ownerbtn = "";
            this.ownerpoli = "";
            this.policealert = "";
          }
        });
      } else if (this.marker.status_type == 'SS') {
        this.policealert = this.marker.status_type;
        this.alertbtn = "";
        this.ownerbtn = "";
        this.resbtn = "";
        this.ownerpoli = "";
        this.infoMsg = this.marker.message
        this.eventTime = this.marker.time;
      } else if (this.marker.status_type == 'NS') {
        let can='';
        this.translate.get('Cancelled').subscribe(res => {
          can = res;
        });
        this.infoMsg = this.marker.message + ' ('+can+')';
        this.eventTime = this.marker.time;
        this.policealert = "";
        this.ownerpoli = "";
        this.alertbtn = "";
        this.ownerbtn = "";
        this.resbtn = "";
        this.refVar = null;
      } else if(this.marker.status_type == 'S' && this.marker.alert_type == 'N' && this.marker.address_type == 'GS') {
        var response: any;
        let curdata = JSON.parse(localStorage.getItem('userData'));
        let ruserdata: any = {};
        ruserdata = {
          "user_id": curdata.user_id,
          "sus_user_id": this.marker.user_id,
          "sus_address_id": this.marker.address_id,
          "address_type": this.marker.address_type
        };
        this.AuthServiceProvider.postData(ruserdata, 'checkUserResponse').then((result) => {
          console.log(result);
          response = result;
          console.log(response);
          if (response.status == true) {
            console.log(response.status);
            this.infoMsg = this.marker.message
            this.eventTime = this.marker.time;
            this.resbtn = "";
            this.alertbtn = "";
            this.ownerbtn = "";
            this.ownerpoli = "";
            this.policealert = "SS";
            localStorage.removeItem('victimData');
          } else {
            this.infoMsg = this.marker.message
            this.eventTime = this.marker.time;
            this.resbtn = _AlertState.S;
            this.alertbtn = "";
            this.ownerbtn = "";
            this.ownerpoli = "";
            this.policealert = "";
          }
        });
      } else if (this.marker.status_type == 'SS'&& this.marker.alert_type == 'N' && this.marker.address_type == 'GS') {
        this.policealert = this.marker.status_type;
        this.alertbtn = "";
        this.ownerbtn = "";
        this.resbtn = "";
        this.ownerpoli = "";
        this.infoMsg = this.marker.message
        this.eventTime = this.marker.time;
      } else if (this.marker.status_type == 'NS'&& this.marker.alert_type == 'N' && this.marker.address_type == 'GS') {
        let can='';
        this.translate.get('Cancelled').subscribe(res => {
          can = res;
        });
        this.infoMsg = this.marker.message + ' ('+can+')';
        this.eventTime = this.marker.time;
        this.policealert = "";
        this.ownerpoli = "";
        this.alertbtn = "";
        this.ownerbtn = "";
        this.resbtn = "";
        this.refVar = null;
      }
    }
  }

  //set dynamically icon
  getMarkerURL(icon) {
    //var url = '/assets/imgs/'+icon;
    var url = this.base_url + 'mapicon/new-markers/' + icon;
    return url;
  }

  //add marker for non-nabo user
  addNaboMarker(markerImg: any, lat, lng) {
    console.log('add marker call');
    var latLang = { lat: lat, lng: lng };
    let cur_img = this.base_url + 'mapicon/new-markers/' + markerImg;
    let marker = new google.maps.Marker({
      map: this.map,
      draggable: false,
      icon: cur_img,
      animation: google.maps.Animation.DROP,
    });
  }


  //get nearby users count when alert to all
  rangeUser(event) {
    console.log(event);
    var simpleObject = {};
    for (var prop in event) {
      if (!event.hasOwnProperty(prop)) {
        continue;
      }
      if (typeof (event[prop]) == 'object') {
        continue;
      }
      if (typeof (event[prop]) == 'function') {
        continue;
      }
      simpleObject[prop] = event[prop];
    }
    console.log(this.valueData);
    this.userData = JSON.parse(localStorage.getItem("userData"));
    var user_detail = this.userData;
    console.log(user_detail);
    if (event != "start") {
      console.log(simpleObject);
      var value = JSON.stringify(simpleObject);
      this.valueData = JSON.parse(value)._value;
      user_detail.distance = this.valueData;
      localStorage.setItem('userData', JSON.stringify(user_detail));
      if (this.range == 5000) {
        if (this.SrangeAvaUser == 0) {
          localStorage.setItem('onetimepush', 'true');
          var cancelData = JSON.parse(localStorage.getItem('userData'));
          this.AuthServiceProvider.postData(cancelData, 'ownerCancel').then((result) => {
            this.responsedata = result;
            if (this.responsedata.status == true) {
              this.stopCountdown();
              this.getUser();
            } else {

            }
          }, (err) => {
            console.log(err);
          });
          let StopAlert = this.alertCtrl.create({
            title: 'Process Cancel',
            message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');

                }
              }
            ],
            enableBackdropDismiss: false
          });
          StopAlert.present();
        }
      }

      if (this.valueData <= 999) {
        this.unit = 'Mtrs ';
      } else {
        this.unit = 'Kms ';
        this.valueData = this.valueData / 1000;
      }
    } else {

      localStorage.setItem('userData', JSON.stringify(user_detail));
      this.valueData = user_detail.distance;
      if (this.valueData == 5000) {
        if (this.SrangeAvaUser == 0) {
          localStorage.setItem('onetimepush', 'true');
          console.log('Cancel clicked');
          var cancelData = JSON.parse(localStorage.getItem('userData'));
          this.AuthServiceProvider.postData(cancelData, 'ownerCancel').then((result) => {
            this.responsedata = result;
            if (this.responsedata.status == true) {
              this.stopCountdown();
              this.getUser();
            } else {

            }
          }, (err) => {
            console.log(err);
          });
          let StopAlert = this.alertCtrl.create({
            title: 'Process Cancel',
            message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {

                }
              }
            ],
            enableBackdropDismiss: false
          });
          StopAlert.present();
        }
      }
      if (this.valueData <= 999) {
        this.unit = 'Mtrs ';
      } else {
        this.unit = 'Kms ';
        this.valueData = this.valueData / 1000;
      }
    }

    this.AuthServiceProvider.postData(user_detail, 'sendallpushNearuser').then((result) => {
      this.nearData = result;
      console.log(this.nearData.length);
      this.SrangeAvaUser = this.nearData.length;
      for (let regional of this.nearData) {
        console.log(regional.latitude + ',' + regional.longitude);
      }
      this.AuthServiceProvider.postData(user_detail, 'rangevalue').then((result) => {
        this.resdata = result;
        console.log(this.resdata);
        if (true == this.resdata.status) {

        }
      });

    }, (err) => {

    });

  }

  getNearuser() {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    let lat = localStorage.getItem('lastLat');
    let lng = localStorage.getItem('lastLng');
    let data = {
      "user_id":this.userData.user_id,
      "latitude":lat,
      "longitude":lng
    };
    this.AuthServiceProvider.postData(data, 'pushNearuser').then((result) => {
      this.nearData = result;
      console.log(this.nearData.length);
      this.SrangeAvaUser = this.nearData.count;
    }, (err) => {

    });
  }

  //get nearby nabo user for non-nabo user alert
  grangeUser_old(event) {
    console.log(event);
    var simpleObject = {};
    for (var prop in event) {
      if (!event.hasOwnProperty(prop)) {
        continue;
      }
      if (typeof (event[prop]) == 'object') {
        continue;
      }
      if (typeof (event[prop]) == 'function') {
        continue;
      }
      simpleObject[prop] = event[prop];
    }
    console.log(this.valueData);
    this.userData = JSON.parse(localStorage.getItem('userData'));
    var user_detail = this.locationData;
    user_detail.distance = this.userData.distance;
    console.log(user_detail);
    if (event != "start") {
      console.log(simpleObject);
      var value = JSON.stringify(simpleObject);
      this.valueData = JSON.parse(value)._value;
      user_detail.distance = this.valueData;
      this.userData = JSON.parse(localStorage.getItem('userData'));
      this.userData.distance = this.valueData;
      localStorage.setItem('userData', JSON.stringify(this.userData));
      if (this.range == 5000) {
        if (this.SrangeAvaUser == 0) {
          let StopAlert = this.alertCtrl.create({
            title: 'Process Cancel',
            message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
            buttons: [
              {
                text: 'OK',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                  this.removeGuestUser();
                  this.refresh();
                }
              }
            ],
            enableBackdropDismiss: false
          });
          StopAlert.present();
        }
      }
      if (this.valueData <= 999) {
        this.unit = 'Mtrs ';
      } else {
        this.unit = 'Kms ';
        this.valueData = this.valueData / 1000;
      }
    } else {
      this.userData = JSON.parse(localStorage.getItem('userData'));
      user_detail.distance = this.userData.distance;
      console.log("user distance" + user_detail.distance);
      this.valueData = user_detail.distance;
      if (this.valueData == 5000) {
        if (this.SrangeAvaUser == 0) {
          let StopAlert = this.alertCtrl.create({
            title: 'Process Cancel',
            message: 'In this current place there is no Nabo users to inform. So we cannot continue this process. Sorry.',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                  this.removeGuestUser();
                  this.refresh();
                }
              }
            ],
            enableBackdropDismiss: false
          });
          StopAlert.present();
        }
      }
      if (this.valueData <= 999) {
        this.unit = 'Mtrs ';
      } else {
        this.unit = 'Kms ';
        this.valueData = this.valueData / 1000;
      }
    }

    this.AuthServiceProvider.postData(user_detail, 'pushNearuser').then((result) => {
      this.nearData = result;
      console.log(this.nearData.length);
      this.SrangeAvaUser = this.nearData.length;
      for (let regional of this.nearData) {
        console.log(regional.latitude + ',' + regional.longitude);
      }
      this.AuthServiceProvider.postData(user_detail, 'rangevalue').then((result) => {
        this.resdata = result;
        console.log(this.resdata);
        if (true == this.resdata.status) {

        }
      });

    }, (err) => {

    });
  }

  //get non-nabouser's location details
  getGustAddress(data: any, coord: any) {
    let rsltAdrComponent = data.address_components;
    let resultLength = rsltAdrComponent.length;
    if (data != null) {
      var Gdata = new Array;
      var locationdata = new Array();
      for (var i = 0; i < rsltAdrComponent.length; i++) {
        var obj = rsltAdrComponent[i];
        Gdata.push(obj.long_name);
      }
      this.deviceID = localStorage.getItem('deviceID');
      if (Gdata.length == 5) {
        this.locdata = {
          "street_address": Gdata[1] + " " + Gdata[0],
          "city": Gdata[2],
          "country": Gdata[3],
          "zipcode": Gdata[4],
          "latitude": coord.lat,
          "longitude": coord.lng,
          "is_active": 1,
          "status_id": 0,
          "alert_type": _AlertState.N,
          "distance": 10,
          "deviceID": this.deviceID
        };
        console.log(this.locdata);
      }
      else if (Gdata.length == 6) {
        this.locdata = {
          "street_address": Gdata[1] + " " + Gdata[0],
          "city": Gdata[2],
          "country": Gdata[4],
          "zipcode": Gdata[5],
          "latitude": coord.lat,
          "longitude": coord.lng,
          "is_active": 1,
          "status_id": 0,
          "alert_type": _AlertState.N,
          "distance": 10,
          "deviceID": this.deviceID
        };
        console.log(this.locdata);
      }
      else if (Gdata.length == 7) {
        this.locdata = {
          "street_address": Gdata[1] + " " + Gdata[0],
          "city": Gdata[3],
          "country": Gdata[5],
          "zipcode": Gdata[6],
          "latitude": coord.lat,
          "longitude": coord.lng,
          "is_active": 1,
          "status_id": 0,
          "alert_type": _AlertState.N,
          "distance": 10,
          "deviceID": this.deviceID
        };
        console.log(this.locdata);
      }
      else if (Gdata.length == 8) {
        this.locdata = {
          "street_address": Gdata[1] + " " + Gdata[0],
          "city": Gdata[4],
          "country": Gdata[6],
          "zipcode": Gdata[7],
          "latitude": coord.lat,
          "longitude": coord.lng,
          "is_active": 1,
          "status_id": 0,
          "alert_type": _AlertState.N,
          "distance": 10,
          "deviceID": this.deviceID
        };
        console.log(this.locdata);
      } else if (Gdata.length == 9) {
        this.locdata = {
          "street_address": Gdata[1] + " " + Gdata[0],
          "city": Gdata[4],
          "country": Gdata[7],
          "zipcode": Gdata[8],
          "latitude": coord.lat,
          "longitude": coord.lng,
          "is_active": 1,
          "status_id": 0,
          "alert_type": _AlertState.N,
          "distance": 10,
          "deviceID": this.deviceID
        };

        console.log(this.locdata);
      } else if (Gdata.length == 10) {

        this.locdata = {
          "street_address": Gdata[1] + " " + Gdata[0],
          "city": Gdata[5],
          "country": Gdata[8],
          "zipcode": Gdata[9],
          "latitude": coord.lat,
          "longitude": coord.lng,
          "is_active": 1,
          "status_id": 0,
          "alert_type": _AlertState.N,
          "distance": 10,
          "deviceID": this.deviceID
        };

        console.log(this.locdata);
      }
      console.log(this.locdata);
      localStorage.setItem('lastLat', this.locdata.latitude);
      localStorage.setItem('lastLng', this.locdata.longitude);
    } else {
      console.log("No address available");
    }
  }

  //button click for saw the same 
  saw() {
    this.translate.get(this.tranText.sent).subscribe(res => {
      this.title = res;
    });
    this.translate.get(this.tranText.sentmsg).subscribe(res => {
      this.subtitle = res;
    });
    let alert = this.alertCtrl.create({
      title: this.title,
      subTitle: this.subtitle,
      buttons: ['Ok']
    });
    var userdata = JSON.parse(localStorage.getItem('alertdata'));;
    userdata['cur_user_id'] = JSON.parse(localStorage.getItem('userData'))['user_id'];

    console.log(userdata);
    userdata['res'] = "true";
    console.log(userdata);
    this.AuthServiceProvider.postData(userdata, 'Alertresponse').then((result) => {
      console.log(result);
      this.responsedata = result;
      console.log(this.responsedata.status);
      if (this.responsedata.status == true) {
        localStorage.removeItem('victimData');
        this.pushSendOpenModal();
        this.refresh();
      }

    }, (err) => {
      console.log(err);
      // Error log
    });
  }

  //button click not suspecious
  notsusp() {
    this.translate.get(this.tranText.sent).subscribe(res => {
      this.title = res;
    });
    this.translate.get(this.tranText.sentmsg).subscribe(res => {
      this.subtitle = res;
    });
    let alert = this.alertCtrl.create({
      title: this.title,
      subTitle: this.subtitle,
      buttons: ['Ok']
    });
    var userdata = JSON.parse(localStorage.getItem('alertdata'));;
    userdata['cur_user_id'] = JSON.parse(localStorage.getItem('userData'))['user_id'];
    console.log(userdata);
    userdata['res'] = "false";
    console.log(userdata);
    this.AuthServiceProvider.postData(userdata, 'Alertresponse').then((result) => {
      console.log(result);
      this.responsedata = result;
      if (this.responsedata.status == true) {
        localStorage.removeItem('victimData');
        this.pushSendOpenModal();
        this.refresh();
      }

    }, (err) => {
      // Error log
    });
  }

  countdown() {
    //this.timeLeft = this.alertTime * 60;
    this.timeLeft = 1 * 60;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.StartTimer(this.timeLeft);
        this.timeLeft--;
        console.log('time remin:' + this.timeLeft);
        if (this.timeLeft == 0) {
          this.owner = "";
          this.sendcurpush();
          localStorage.setItem('sendtype', 'auto');
          console.log('countdown stopped');
        }
      } else {
        clearInterval(this.interval);
      }
    },1000);
  }
  stopTimer() {
    clearInterval(this.interval);
  }
  //initiate automatic alert sending processs with time delay
  countdown_bk() {
    console.log('called timer:' + this.alertTime);
    //var sec = this.alertTime * 60;
    var sec = 1 * 60; //for develop
    //var sec: any = localStorage.getItem('timeCheck') ? parseInt(localStorage.getItem('timeCheck')) : seconds;
    this.autoPushCounter = false;
    this.countstatus = Observable.interval(1000)
    .takeWhile(() => !this.autoPushCounter)
    .subscribe(x => {
      sec = sec - 1;
      //localStorage.setItem('timeCheck', sec);
      this.StartTimer(sec);
      if (sec == 0) {
        this.owner = "";
        this.sendcurpush();
        localStorage.setItem('sendtype', 'auto');
        console.log('countdown stopped');
      }
    });
    //}
  }

  //function to stop timer
  stopCountdown() {
    this.autoPushCounter = true;
    this.countstatus.unsubscribe();
  }

  //function for start timer
  StartTimer(second: number) {

    const secNum = parseInt(second.toString(), 10); // don't forget the second param
    const hours = Math.floor(secNum / 3600);
    const minutes = Math.floor((secNum - (hours * 3600)) / 60);
    const seconds = secNum - (hours * 3600) - (minutes * 60);
    let hoursString = '';
    let minutesString = '';
    let secondsString = '';
    hoursString = (hours < 10) ? '0' + hours : hours.toString();
    minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
    secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
    // console.log(hoursString + ':' + minutesString + ':' + secondsString);
    this.autoTime = minutesString + ':' + secondsString;
  }



  //remove accidently add non-nabouser alert 
  removeGuestUser() {
    var gdata = JSON.parse(localStorage.getItem('alertdata'));
    this.AuthServiceProvider.postData(gdata, 'removeGuest').then((result) => {
      console.log(result);
      this.responsedata = result;
      if (this.responsedata.status == true) {
        console.log(result);
      }

    }, (err) => {
      // Error log
      console.log(err);
    });
  }

  //button click for initimate the process informed to police
  police() {
    let pdata = JSON.parse(localStorage.getItem('alertdata'));
    let data = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(pdata, 'policeInform').then((result) => {
      console.log(result);
      this.responsedata = result;
      if (this.responsedata.status == true) {
        //alert.present();
        console.log(result);
        this.refresh();
      }

    }, (err) => {
      // Error log
      console.log(err);
    });
  }

  //get messge data from front-end
  burglaryInHouse() {
    this.translate.get("BURGLARIES IN HOUSE").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  burglar() {
    this.ddata = {
      "msg": "BURGLAR BURGLARY"
    };
  }

  burglarShed() {
    this.translate.get("BURGLAR SHEDDING").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  assult() {
    this.translate.get("ASSAULT").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  picPocket() {
    this.translate.get("PICKPOCKET").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  susCar() {
    this.translate.get("SUSPICIOUS CAR").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  susPerson() {
    this.translate.get("SUSPICIOUS PERSON").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  susBehavior() {
    this.translate.get("SUSPICIOUS BEHAVIOR FROM PEOPLE").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  susAttention() {
    this.translate.get("POSSIBLE ATTENTION TO BURGLARY").subscribe(value => {
      this.ddata = {
        "msg": value
      };
    });
  }

  guestburglaryInHouse() {
    console.log('Burglaries in houses');
    this.translate.get("BURGLARIES IN HOUSE").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  guestburglar() {
    this.gddata = {
      "msg": "BURGLAR BURGLARY"
    };
  }

  guestburglarShed() {
    this.translate.get("BURGLAR SHEDDING").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  guestassult() {
    this.translate.get("ASSAULT").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  guestpicPocket() {
    this.translate.get("PICKPOCKET").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  guestsusCar() {
    console.log('Suspicious Car');
    this.translate.get("SUSPICIOUS CAR").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  guestsusPerson() {
    this.translate.get("SUSPICIOUS PERSON").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  guestsusBehavior() {
    this.translate.get("SUSPICIOUS BEHAVIOR FROM PEOPLE").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  guestsusAttention() {
    this.translate.get("POSSIBLE ATTENTION TO BURGLARY").subscribe(value => {
      this.gddata = {
        "msg": value
      };
    });
  }

  //close the alert message box
  exit() {
    this.alertmsg = false;
    this.alertmsgsus = false;
    this.guestalertmsg = false;
    this.guestalertmsgsus = false;
    this.guest = false;
    this.ddata.msg = '';
    this.gddata.msg = '';
  }

  //close message info window
  infoExit() {
    this.infoMsg = false;
  }

  //function to reset process automatically
  processReset() {
    var reset = this.resetTime * 24 * 60 * 60 * 1000;
    this.stopCondition = false;
    this.userData = JSON.parse(localStorage.getItem('userData'));
    let userdata = {
      "user_id": this.userData.user_id,
      "address_id": this.userData.address_id,
      "address_type": this.userData.address_type
    };
    Observable.interval(reset)
      .takeWhile(() => !this.stopCondition)
      .subscribe(i => {
        //This will be called every 10 seconds until `stopCondition` flag is set to true
        this.AuthServiceProvider.postData(userdata, 'ownerCancel').then((result) => {
          console.log(result);
          this.stopCondition = true;
        }, (err) => {
          console.log(err);
        });
      })
  }

  //real time user's status  update
  updateUserData() {
    let timer = this.refreshTime;
    if (timer == '' || undefined) {
      timer = 1;
    }
    let seconds = timer * 60 * 1000;
    console.log('refersh time in milsec:' + seconds);
    var userData = JSON.parse(localStorage.getItem('userData'));
    console.log('user data updated');
    this.userDataUpdate = false;
    Observable.interval(seconds)
      .takeWhile(() => !this.userDataUpdate)
      .subscribe(i => {
        //This will be called every 10 seconds until `stopCondition` flag is set to true
        this.AuthServiceProvider.postData(userData, 'getUserAddressByCountry').then((result) => {
          this.addressData = result;
          console.log(result);
          //this.stopCondition = true;
          let session = localStorage.getItem('session');
          if (session == '_logged_out') {
            this.userDataUpdate = true;
          }
        }, (err) => {
          console.log(err);
        });
      });
  }

  updateUserLocation() {
    let timer = this.refreshTime;
    if (timer == '' || undefined) {
      timer = 1;
    }
    let seconds = timer * 60 * 1000;
    console.log('locatrion update time in milsec:' + seconds);
    this.userLocUpdate = false;
    Observable.interval(seconds)
      .takeWhile(() => !this.userLocUpdate)
      .subscribe(i => {
        //This will be called every 10 seconds until `stopCondition` flag is set to true 
        this.options = {
          timeout: 6000, enableHighAccuracy: true, maximumAge: 3800
        };
        this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
          var latlng = { lat: pos.coords.latitude, lng: pos.coords.longitude };
          console.log('user location ref', pos);
          localStorage.setItem('lastLat', JSON.stringify(pos.coords.latitude));
          localStorage.setItem('lastLng', JSON.stringify(pos.coords.longitude));
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({ 'location': latlng }, (results, status) => {
            if (status === 'OK') {
              if (results[0]) {
                console.log(results[0].formatted_address);
                this.othersAddress = results[0].formatted_address;
                localStorage.setItem('lastadd', this.othersAddress);
              } else {
                this.othersAddress = 'Cannot determine address at this location.';
              }
              this.AGMloadMap();
              this.mapview = true;
              this.owner = false;
              this.alertmsg = false;
              this.alertmsgsus = false;
              this.guestalertmsg = false;
              this.ownerbtn = "";
              this.resbtn = "";
              this.policealert = "";
              this.ownerpoli="";
              this.infoMsg = false;
            }
          });
        }, (err) => {
          console.log('Error message : ' + err.message);
        });
        let session = localStorage.getItem('session');
        if (session == '_logged_out') {
          this.userLocUpdate = true;
        }
      });
  }

}
