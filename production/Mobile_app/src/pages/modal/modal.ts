// Page name: modal page (This page load popup(modal) messages )
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component } from '@angular/core';
import {  NavController, NavParams, ViewController, MenuController, IonicPage } from 'ionic-angular';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  constructor(
    private translate: TranslateService,
    public navCtrl: NavController,
    private menu: MenuController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
  ) {
  }

 //default ionic lifecycle event
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //close the popup message
  modalClose() {
   this.viewCtrl.dismiss();
  }

}
