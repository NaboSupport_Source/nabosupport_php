// Page name: Signin page (This page validate user credentials and send to api for login)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { Component } from '@angular/core';
import { ToastController, NavController, LoadingController, MenuController, AlertController, IonicPage,Events } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ForgotpasswordPage } from '../forgotpassword/forgotpassword';
import { MapPage } from '../map/map';
import { NewaddressPage } from '../newaddress/newaddress';
import { MyaccountPage } from '../myaccount/myaccount';
import { NgForm } from '@angular/forms';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

declare var google:any;
@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})

export class SigninPage {
  users: any;
  options: GeolocationOptions;
  currentPos: Geoposition;
  map: any;
  LastLng1: any;
  LastLat1: any;
  amap = { 'lat': 0, 'lng': 0, 'zoom': 15 };
  form = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  //get user data from form field
  get username() {
    return this.form.get('username');
  }
  get password() {
    return this.form.get('password');
  }
  responseData: any;
  isLoggedIn: any;
  userData = { "username": "", "password": "", "platform": "" };
  userdata:any;
  locdata: any;
  userFBData: any;
  userGoogleData: any;
  userallData: any;
  data: any;
  displayName: any;
  email: any;
  familyName: any;
  givenName: any;
  userId: any;
  imageUrl: any;
  deviceID:any;
  userAllData:any;
  platformname:any;
  activationAlert:any;
  toastmessage:any;
  loadercontent:any;
  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private googlePlus: GooglePlus,
    private translate: TranslateService,
    private nativeStorage: NativeStorage,
    public geolocation: Geolocation,
    private loadingCtrl: LoadingController,
    private alertCtrl:AlertController,
    private menu: MenuController,
    public AuthServiceProvider: AuthServiceProvider,
    public events: Events,
    public browser:InAppBrowser,
    private diagnostic: Diagnostic) {
    this.Googlelogout();
    this.AGMloadMap();
    localStorage.setItem('userFBData', "");
    localStorage.setItem('userGoogleData', "");
  }

  //logout from google
  Googlelogout() {
    this.googlePlus.logout()
      .then(res => {
        console.log(res);
        this.displayName = "";
        this.email = "";
        this.familyName = "";
        this.givenName = "";
        this.userId = "";
        this.imageUrl = "";

        this.isLoggedIn = false;
      })
      .catch(err => console.error(err));
  }

  //alert controller for if the user deactivated
  actAlert() {
    let trans_btn_ok='';
    this.translate.get('ok').subscribe(res=>{
      trans_btn_ok =res;
    });
    this.activationAlert = this.alertCtrl.create({
      title:'Deactivated',
      message:'Your account is deactivated Please contact Nabosupport administrator.',
      buttons: [
        {
          text:trans_btn_ok,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    this.activationAlert.present();
  }

  //alert for FCM token not set
  ndiAlert() {
    let trans_btn_ok='';
    let trans_title_fail='';

    this.translate.get('ok').subscribe(res=>{
      trans_btn_ok =res;
    });
    this.translate.get('Failed').subscribe(res=>{
      trans_title_fail =res;
    });
    this.activationAlert = this.alertCtrl.create({
      title:trans_title_fail,
      message:'Network error please try later.',
      buttons: [
        {
          text: trans_btn_ok,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    this.activationAlert.present();
  }

  //Toast contrlloer
  toastViewer(message:any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  //loading controller
  loadingViewer(content:any) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: content
    });
    return loading;
  }

  //JSON concodination
  jsonConcat(o1, o2) {
    for (var key in o2) {
      o1[key] = o2[key];
    }
    return o1;
  }

  //get user data from api
  getUser() {
    var userdata = JSON.parse(localStorage.getItem('userData'));
    this.AuthServiceProvider.postData(userdata,'getUser').then((result) => {
      console.log(result);
      this.userAllData = result[0];
      console.log(this.userAllData);
      localStorage.setItem('userAllData',JSON.stringify(this.userAllData));
      
    }, (err) => {
      console.log(err);
    });
   }

  //update user location when they login location
  updateLocation() {
    let geocoder = new google.maps.Geocoder();
    this.options = {
      enableHighAccuracy: true
    };

    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {

      this.currentPos = pos;
      var coord = new Array();
      coord.push(pos.coords.latitude, pos.coords.longitude);
      console.log(coord[0] + ',' + coord[1]);
      this.userData = JSON.parse(localStorage.getItem('userData'));
      var locationData:any ={
        "user_id":this.userData['user_id'],
        "lat":coord[0],
        "lang":coord[1]
      }; 
      this.AuthServiceProvider.postData(locationData, 'updateUserLocation').then((result) =>{
        console.log(result);
      },(err) => {
        console.log(err);
      });

    }, (err: PositionError) => {
      console.log("error : " + err.message);
    });
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewDidLeave() {
    this.menu.swipeEnable(true);
  }

 

  //login function 
  signin() {
    if (this.userData.username && this.userData.password ) {
      //this.userData['deviceId']='cxHIRxjGUdE:APA91bGJqzhwsBbhxDLnaBXwcl7PLHZu3fDbm9pZ4QMN1kzRiJP5MFRAksg37aySQvhHoKfHTaDlt80AreZIeP9JG5MJc0AYGWAM1v3kT58sQsEgxZO74RdwNAaVazG2PZPlkKqE9m4M';
      this.userData['deviceId']=localStorage.getItem('deviceID');
      this.platformname = localStorage.getItem('paltform');
      this.userData.platform = this.platformname;
      this.AuthServiceProvider.postData(this.userData, 'login').then((result) => {
        this.responseData = result;
        let trans_auth='';
        this.translate.get('Authenticating...').subscribe(res=>{
          trans_auth=res;
        });
        this.loadercontent = trans_auth;
        let loader = this.loadingViewer(this.loadercontent);
        loader.present();
        if (true == this.responseData.status) {
          loader.dismiss();
          console.log(this.responseData.data[0]);
          localStorage.setItem('userData',JSON.stringify(this.responseData.data[0]));
          localStorage.setItem('session','_logged_in');
          var data = {"type":"loggedin","lang":this.responseData.data[0].language};
          this.getUserPosition();
          this.AuthServiceProvider.getData('getAdminData').then((result) => {
            this.responseData = result;
            console.log(this.responseData);
            localStorage.setItem('adminData',JSON.stringify(this.responseData));
            let langData = JSON.parse(localStorage.getItem('userData'));
            this.events.publish('user:changed', data);
            this.navCtrl.setRoot(MapPage);
          });
        }else if('first' == this.responseData.status) {
          localStorage.setItem('userData',JSON.stringify(this.responseData.data[0]));
          loader.dismiss();
          this.navCtrl.setRoot(NewaddressPage);
        }else if(false == this.responseData.status) {
          loader.dismiss();
          this.translate.get('Invalid email or password').subscribe(res => {
            this.toastmessage = res;
          });
          this.toastViewer(this.toastmessage);
        } else if('deactive' == this.responseData.status ) {
          loader.dismiss();
          console.log('deactive');
          this.actAlert();
        } else if('NDI' == this.responseData.status) {
          loader.dismiss();
          console.log('no device id');
          this.ndiAlert();
        }
        else {
          loader.dismiss();
          this.translate.get('Invalid email or password').subscribe(res => {
            this.toastmessage = res;
          });
          this.toastViewer(this.toastmessage);
        }
      }, (err) => {
        // Error log
      });
    } else {
      if (!(this.userData.username) && !(this.userData.password)) {
        this.toastmessage = 'Please provide your Email & password';
        this.toastViewer(this.toastmessage);
      } else {
        if (((this.userData.username) || !(this.userData.password))) {
          this.toastmessage = 'Please provide your password';
          this.toastViewer(this.toastmessage);
        } else {
          this.toastmessage = 'Please provide your Email';
          this.toastViewer(this.toastmessage);
        }
      }

    }
  }

  //google login start
  doGoogleLogin() {
    this.getUserPosition();
    this.googlePlus.login({
      'offline': true
    }).then(res => {
        console.log(res);
        this.displayName = res.displayName;
        this.email = res.email;
        this.familyName = res.familyName;
        this.givenName = res.givenName;
        this.userId = res.userId;
        this.imageUrl = res.imageUrl;
        localStorage.setItem('userGoogleData', JSON.stringify(res));
        this.userGoogleData = JSON.parse(localStorage.getItem('userGoogleData'));
        console.log(this.userFBData);
        this.userallData = {};
        this.userallData = this.jsonConcat(this.locdata, this.userGoogleData);
        this.platformname = localStorage.getItem('paltform');
        this.userallData.platform = this.platformname;
        console.log(this.userallData);
        if (this.userallData.email) {
          this.AuthServiceProvider.postData(this.userallData, 'FBlogin').then((result) => {
            this.responseData = result;
            console.log(this.responseData.status);
            if (this.responseData.status == true) {
              console.log(this.responseData);
              this.toastmessage = 'login successfully.';
              this.toastViewer(this.toastmessage);
              localStorage.setItem('session','_logged_in');
              this.navCtrl.push(MapPage);
              localStorage.setItem('userData', JSON.stringify(this.responseData));
              this.fbsuccess();
            } else if(this.responseData.status == false) {
              this.toastmessage = 'Login Failed Please Re-try.';
              this.toastViewer(this.toastmessage);
            } else if('deactive' == this.responseData.status ) {
              this.actAlert();
            } else {
              this.toastmessage = 'Login Failed.';
              this.toastViewer(this.toastmessage);
            }
          }, (err) => {
            // Error log
          });
        } else {
          this.toastmessage = 'Unverified user details.';
          this.toastViewer(this.toastmessage);
        }
      })
    // .catch(err => console.error(err));
  }
 
 //forgotpassword page link
  forgot() {
    this.navCtrl.push(ForgotpasswordPage);
  }

  //redirected to Nabosupport web
  webpage() {
    let trans_btn_yes='';
    let trans_btn_no='';
    let alerttitle='';
    let alerttxt='';
    this.translate.get('Yes').subscribe(res=>{
      trans_btn_yes=res;
    });
    this.translate.get('No').subscribe(res=>{
      trans_btn_no=res;
    });
    this.translate.get('forgot link title').subscribe(res => {
      alerttitle = res;
    });
    this.translate.get('offer alert text').subscribe(res => {
      alerttxt = res;
    });
    let alert = this.alertCtrl.create({
      title: alerttitle,
      message: alerttxt,
      buttons: [
        {
          text:trans_btn_yes ,
          role: 'cancel',
          handler: () => {
            console.log('Yes clicked');
            //let openBrowser = this.browser.create('http://rayi.in/nabosupport/'+this.offerURL, '_system','location=no');
            let openBrowser = this.browser.create('http://nabosupport.dk/min-konto/lost-password/', '_system','location=no');
          }
        },
        {
          text: trans_btn_no,
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    alert.present();
  }

  //page loading start
  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
    this.getUserPosition();
    console.log(localStorage.getItem('userData'));
    let session = localStorage.getItem('session');
    if (session === '_logged_in') {
      console.log("log2map");  
      this.navCtrl.setRoot(MapPage);
    }else{
      console.log("log");
   
    }
  }

  //load agm map geocode
  AGMloadMap() {
    this.options = {
      enableHighAccuracy: true
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      console.log(pos);
      this.amap ={
        'lat': pos.coords.latitude,
        'lng': pos.coords.longitude,
        'zoom': 15
      };
    })
  }

  fbsuccess() {
    localStorage.setItem('session','_logged_in');
    this.navCtrl.setRoot(MapPage);
  }

  //get user current location
  getUserPosition() {
    var acctxt='';
    this.translate.get('turn on gps to high accurecy').subscribe(res => {
      acctxt =  res;
    });
    let geocoder = new google.maps.Geocoder();
    this.options = {
      enableHighAccuracy: true
    };
    this.diagnostic.getLocationMode()
    .then((state) => {
      if (state == this.diagnostic.locationMode.BATTERY_SAVING) {
        alert(acctxt);
      } else if (state == this.diagnostic.locationMode.DEVICE_ONLY) {
        alert(acctxt);
      } else if (state == this.diagnostic.locationMode.LOCATION_OFF) {
        alert(acctxt);
      } else if (state == this.diagnostic.locationMode.HIGH_ACCURACY) {
      this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      this.deviceID = localStorage.getItem('deviceID');
      this.currentPos = pos;
      var coord = new Array();
      coord.push(pos.coords.latitude, pos.coords.longitude);
      let latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      let request = { latLng: latlng };
      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            var data = new Array;
            var locationdata = new Array();
            for (var i = 0; i < rsltAdrComponent.length; i++) {
              var obj = rsltAdrComponent[i];
              data.push(obj.long_name);

            }
            if (data.length == 5) {
              this.locdata = {

                "street_address": data[1] + " " + data[0],
                "city": data[2],
                "country": data[3],
                "zipcode": data[4],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":10,
                "deviceID":this.deviceID
              };
              console.log(this.locdata);
            }
            else if (data.length == 6) {
              this.locdata = {
                "street_address": data[1] + " " + data[0],
                "city": data[2],
                "country": data[4],
                "zipcode": data[5],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":10,
                "deviceID":this.deviceID
              };
            }
            else if (data.length == 7) {
              this.locdata = {

                "street_address": data[1] + " " + data[0],
                "city": data[3],
                "country": data[5],
                "zipcode": data[6],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":10,
                "deviceID":this.deviceID
              };
              console.log(this.locdata);
            }
            else if (data.length == 8) {
              this.locdata = {
                "street_address": data[1] + " " + data[0],
                "city": data[4],
                "country": data[6],
                "zipcode": data[7],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":10,
                "deviceID":this.deviceID
              };
            } else if (data.length == 9) {
              this.locdata = {
                "street_address": data[1] + " " + data[0],
                "city": data[4],
                "country": data[7],
                "zipcode": data[8],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":10,
                "deviceID":this.deviceID
              };

            } else if (data.length == 10) {

              this.locdata = {
                "street_address": data[1] + " " + data[0],
                "city": data[5],
                "country": data[8],
                "zipcode": data[9],
                "latitude": coord[0],
                "longitude": coord[1],
                "is_active": 1,
                "status_id": 0,
                "alert_type":"N",
                "distance":10,
                "deviceID":this.deviceID
              };

            }
            console.log(this.locdata);
            return this.locdata;
          } else {
            let trans_no_add='';
            this.translate.get("No address available").subscribe(res=>{
              trans_no_add =res;
            });
            alert(trans_no_add);
          }
        }
      });
    }).catch(e => console.error(e));
  }
})
}
}
