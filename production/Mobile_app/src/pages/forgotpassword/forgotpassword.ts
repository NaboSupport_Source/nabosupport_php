// Page name: forgetpassword page (This page will redirect to forgotpassword page)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { MapPage } from './../map/map';
import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ToastController, NavController, LoadingController, AlertController,MenuController,IonicPage } from 'ionic-angular';
import { SigninPage } from '../signin/signin';
import { NgForm } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {

  form1 = new FormGroup({
    email: new FormControl()
  });

  //get emailID from form
  get email() {
    return this.form1.get('email');
  }
  responseData: any;
  userForgotData = { "email": "" };
  constructor(
    private translate: TranslateService,
    public navCtrl: NavController,
    private menu: MenuController,
    private nativeStorage: NativeStorage,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public AuthServiceProvider: AuthServiceProvider,
    public alertCtrl: AlertController,
    public menuCtrl:MenuController,
    public browser:InAppBrowser
  ) {
    localStorage.setItem('userForgotData', "");
    this.menuCtrl.swipeEnable(false,'loggedInMenu');
    this.menuCtrl.enable(false);
  }

  //Ionic lifecycle events
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //get eamil from forgot password form and send to api for validation
  forgot() {
    let toast = this.toastCtrl.create({
      message: 'Invaild Email',
      duration: 3000,
      position: 'bottom'
    });
    let toast1 = this.toastCtrl.create({
      message: 'Please provide a registered Email',
      duration: 3000,
      position: 'bottom'
    });
    let toast2 = this.toastCtrl.create({
      message: 'Your Account verified.. Please check your Inbox to get reset link. ',
      duration: 3000,
      position: 'bottom'
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Processing..'
    });

    loading.present();
    var regu = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (regu.test(this.userForgotData.email) == true) {
      this.AuthServiceProvider.postData(this.userForgotData, 'forgotpassword').then((result) => {
        this.responseData = result;
        console.log(result);
        if (true == this.responseData.status) {
          let trans_success='';
          this.translate.get('Success').subscribe(res=>{
            trans_success=res;
          });
          let trans_btn_ok='';
          this.translate.get('ok').subscribe(res=>{
            trans_btn_ok=res;
          });
          let alert = this.alertCtrl.create({
            title: trans_success,
            subTitle: 'Your Account verified.. Please check your Inbox to get reset link.',
            buttons: [trans_btn_ok]
          });
          loading.dismiss();
          alert.present();
          localStorage.setItem('userForgotData', JSON.stringify(this.responseData));
          this.navCtrl.setRoot(SigninPage);
          console.log(JSON.parse(localStorage.getItem('userForgotData')));
        } else if (false == this.responseData.status) {
          loading.dismiss();
          let trans_title_fail='';
          this.translate.get('Failed').subscribe(res=>{
            trans_title_fail=res;
          });
          let trans_btn_ok='';
          this.translate.get('ok').subscribe(res=>{
            trans_btn_ok=res;
          });
          let alert = this.alertCtrl.create({
            title: trans_title_fail,
            subTitle: 'Account not found please signup now.',
            buttons: [
              {
                text: trans_btn_ok,
                handler: () => {
                  console.log('Yes clicked');
                }
              }
            ]
          });
          alert.present();
        }
      }, (err) => {
        // Error log
      });
    } else if (!(this.userForgotData.email)) {
      loading.dismiss();
      toast1.present();
    } else {
      loading.dismiss();
      let trans_btn_ok='';
      this.translate.get('ok').subscribe(res=>{
        trans_btn_ok=res;
      });
      let alert = this.alertCtrl.create({
        title: 'Invalid',
        subTitle: 'Please provide valid Email.',
        buttons: [
          {
            text: trans_btn_ok,
            handler: () => {
              console.log('Yes clicked');
            }
          }
        ]
      });
      alert.present();
    }


  }
  
  //button click for back to main page
  gotoBack() {
    this.navCtrl.setRoot(SigninPage);
  }

  //redirected to Nabosupport web
  webpage() {
    let trans_btn_yes='';
    let trans_btn_no='';
    this.translate.get('Yes').subscribe(res=>{
      trans_btn_yes=res;
    });
    this.translate.get('No').subscribe(res=>{
      trans_btn_no=res;
    });
    let alert = this.alertCtrl.create({
      title:'This page will be redirected to browser',
      message: 'Press Yes to Continue',
      buttons: [
        {
          text:trans_btn_yes ,
          role: 'cancel',
          handler: () => {
            console.log('Yes clicked');
            //let openBrowser = this.browser.create('http://rayi.in/nabosupport/'+this.offerURL, '_system','location=no');
            let openBrowser = this.browser.create('http://nabosupport.dk/min-konto/lost-password/', '_system','location=no');
          }
        },
        {
          text: trans_btn_no,
          handler: () => {
            console.log('No clicked');
          }
        }
      ]
    });
    alert.present();
  }

}
