import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecurityPage } from './security';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [SecurityPage],
  imports: [IonicPageModule.forChild(SecurityPage),TranslateModule.forChild()],
  exports:[SecurityPage]
})
export class SecurityPageModule { }