// Page name: Notification page (This page load the user unseen notification)
// created: Feb 18, 2018 
// Author: Bharath
// Revisions: 
import { MapPage } from './../map/map';
import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, IonicPage } from 'ionic-angular';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import { removeSummaryDuplicates } from '@angular/compiler';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
  userData:any;
  userMsg:any;
  response:any;
  highlightedDiv:number;
  alertTime:any;
  constructor(public navCtrl: NavController, private menu:MenuController, public AuthServiceProvider:AuthServiceProvider, public navParams: NavParams) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    this.getTime();
    this.getAlertMsg();
  }

  //ionic lifecycle events
  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  //redirect to map page
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat',lat);
    localStorage.setItem('lastLng',lng);
    this.navCtrl.setRoot(MapPage);
  }

  //get alert notification from api
  getAlertMsg() {
    this.AuthServiceProvider.postData(this.userData, 'getAlertMsg').then((result) => {
      console.log(result);
      this.userMsg = result;
      this.highlightedDiv = 1;
    }, (err) => {
      console.log(err);
    });
  }

  notifypage(){
    this.navCtrl.setRoot(MapPage);
  }

  //notification onclick for locate the alert location
  locatAlert(message) {
    console.log(message);
    localStorage.setItem('lastLat',message.latitude);
    localStorage.setItem('lastLng',message.longitude);
    this.navCtrl.setRoot(MapPage,{
      'page':'notify'
    });
  }

  //delete notification from user device
  removeItem(message) {
    console.log(message);
    this.AuthServiceProvider.postData(message, 'removeAlert').then((result) => {
      this.response = result;
      if(this.response.status == true) {
        let index = this.userMsg.indexOf(message);
        this.userMsg.splice(index,1);
        console.log('deleted array'+this.userMsg);
      } else {
        console.log('please try later');
      }
    });
  }

  getTime() {
    let currentDateTime = moment().format("MM-DD-YYYY HH:mm:ss");
    console.log(currentDateTime);
    this.alertTime =  currentDateTime;
  }
}
