import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController,ViewController } from 'ionic-angular';
import { MapPage } from '../map/map';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { GetUserAddressProvider } from '../../providers/get-user-address/get-user-address';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
declare var google;
@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  myaddress: any;
  responseData: any;
  addresses: any;
  _ispaid: any;
  userdetail: any;
  toastmessage: any;
  geocoder: any;
  isDisabled: any = [];
  addEdit: any = [];
  addUpdate: any = [];
  imageURI: any;
  imageFileName: any;
  options: GeolocationOptions;
  currentPos: Geoposition;
  map: any;
  LastLng1: any;
  LastLat1: any;
  marker: any;
  editProfile: any;
  autocompleteItems: any;
  autocomplete: any;
  acService: any;
  placesService: any;
  isDisable: any;
  addDisable:boolean = false;
  users: any;
  submitProfile: any;
  cancelProfile: any;
  lastImage: string = null;
  fileopen = false;
  loading: any;
  content: any;
  search: any;
  markers: any;
  serachopen: any;
  _addressid: any;
  addAddress: boolean = false;
  _addCtrl: boolean = false;
  _editCtrl: boolean = false;
  addupdate: any;
  selectedData: any;
  addaddress: boolean;
  viewCard: boolean = true;
  viewMap: boolean = false;
  editDisabled: boolean = true;
  normalUser:any;
  user:any;
  dis:boolean = true;
  addE:boolean = true;
  addU:boolean = false;
  addInput:any;
  nabo_img:any;
  newAddAry = { "user_id": "", "street_address": "", "city": "", "country": "", "zipcode": "", "latitude": "", "longitude": "", "primary_address": "", "camtoken": "" };
  public mapcondition: boolean = false;
  public imgoption: boolean = false;
  locationdata = { "hus": "", "street": "", "city": "" };
  marker_data = { "latitude": "", "longitude": "" };
  mapOptions:any;
  latlng:any;
  tranText: any = {
  };
  add:any;
  i:any;
  constructor(private translate: TranslateService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public AuthServiceProvider: AuthServiceProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public geolocation: Geolocation,
  ) {
    this.myaddress = 'addresslist';
    this.normalUser = JSON.parse(localStorage.getItem('userData'));
    console.log('nuser data: '+this.normalUser)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddressPage');
    this.getAddresses();
  }

  ngOnInit() {
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  //Onclicking home button redirect to map page
  home() {
    var user = JSON.parse(localStorage.getItem('userData'));
    var lat = user.latitude;
    var lng = user.longitude;
    localStorage.setItem('lastLat', lat);
    localStorage.setItem('lastLng', lng);
    this.navCtrl.setRoot(MapPage);
  }

  getAddresses() {
    var userdata = JSON.parse(localStorage.getItem('userData'));
    console.log(userdata['user_id']);
    var user = {
      'user_id': userdata.user_id,
      'address_id': userdata.address_id
    };
    this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
      this.responseData = result;
      var user_detail = this.responseData;
      console.log(user_detail[0].paid_member);
      if (user_detail[0].paid_member == 1) {
        this.AuthServiceProvider.postData(user, 'getPaidUserAddress').then((result) => {
          this.addresses = result;
          this._ispaid = true;
          console.log('user saved addresses ' + this.addresses);
        }, (err) => {
          console.log(err);
        });
      }
      this.dis = true;
      console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
      this.LastLat1 = user_detail[0].latitude;
      this.LastLng1 = user_detail[0].longitude;
    });
  }

  onSegmentChanged(event) {
    console.log(event);
    if (this.myaddress == 'address' && this._ispaid == true) {
        this.initMap();
    }  
  }

  //initiate google map
  initMap() {
    var loadtxt='';
    this.translate.get('Map loading...').subscribe(res=>{
      loadtxt = res;
    });
    this.geocoder = new google.maps.Geocoder();
    this.options = {
      enableHighAccuracy: true
    };
    let loader = this.loadingCtrl.create({
      spinner: 'crescent',
      content: loadtxt
    });
    loader.present();
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {

      this.currentPos = pos;
      console.log(pos);
      console.log(pos.coords.latitude + ',' + pos.coords.longitude)

      this.latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

      this.mapOptions = {
        center: this.latlng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        clickableIcons: false,
        draggable:true
      };
      console.log('map initiated');
      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);
      this.addMarker(pos.coords.latitude, pos.coords.longitude);
      this.updateSearch();
      loader.dismiss();
    }, (err: PositionError) => {
      console.log("error : " + err.message);
      loader.dismiss();
    });
  }

  addMap() {
    let latLng = new google.maps.LatLng(this.LastLat1, this.LastLng1);
    let mapOptions = {
      center: latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      fullscreenControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      draggable: false,
      zoomControl: false
      //draggable: this.mapcondition, zoomControl: this.mapcondition, scrollwheel: this.mapcondition, disableDoubleClickZoom: this.mapcondition,
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.LastLat1, this.LastLng1);
    this.updateSearch();
  }

  addMarker(lat, lng) {
    this.nabo_img = 'http://nabosupport.dk/api/v1/mapicon/new-markers/blue.svg';
    this.marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
      icon: this.nabo_img,
    });
    google.maps.event.addListener(this.marker, 'click', (event) => {
      //infoWindow.open(this.map, marker);
      //alert(JSON.stringify(event));
    });
    let geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(this.map, 'dragend', () => {
      this.clearMarkers();
      this.LastLat1 = this.map.getCenter().lat();
      this.LastLng1 = this.map.getCenter().lng();
      var markerpos = new google.maps.LatLng(this.LastLat1,this.LastLng1);
      this.marker.setPosition(markerpos);
      var coord = new Array();
      coord.push(this.LastLat1, this.LastLng1);
      let user = JSON.parse(localStorage.getItem('userData'));
      this.newAddAry.user_id = user.user_id;
      this.newAddAry.latitude = coord[0];
      this.newAddAry.longitude = coord[1];
      console.log(coord[0] + ',' + coord[1]);
      let latlng = new google.maps.LatLng(this.LastLat1, this.LastLng1);
      let request = { latLng: latlng };
      console.log(this.LastLat1 + ',' + this.LastLng1);
      geocoder.geocode(request, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          let result = results[0];
          let rsltAdrComponent = result.address_components;
          //console.log(rsltAdrComponent);
          console.log(result.formatted_address);
          //this.addressDisplayToast(result.formatted_address);
          this.autocomplete.query = result.formatted_address;
          let resultLength = rsltAdrComponent.length;
          if (result != null) {
            var data = new Array;
            var locationdata = new Array();
            for (var i = 0; i < rsltAdrComponent.length; i++) {
              var obj = rsltAdrComponent[i];
              data.push(obj.long_name);
            }
            if (data.length == 5) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[3];
              this.newAddAry.city = data[2];
              this.newAddAry.zipcode = data[4];
            }
            else if (data.length == 6) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[4];
              this.newAddAry.city = data[2];
              this.newAddAry.zipcode = data[5];
            } else if (data.length == 7) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[5];
              this.newAddAry.city = data[2];
              this.newAddAry.zipcode = data[6];
            }
            else if (data.length == 8) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[6];
              this.newAddAry.city = data[4];
              this.newAddAry.zipcode = data[7];
            } else if (data.length == 9) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[7];
              this.newAddAry.city = data[4];
              this.newAddAry.zipcode = data[8];
            } else if (data.length == 10) {
              this.newAddAry.street_address = data[1] + " " + data[0];
              this.newAddAry.country = data[8];
              this.newAddAry.city = data[5];
              this.newAddAry.zipcode = data[9];
            }
          }
        }
      });
    });
  }

  addressDisplayToast(address) {
    let toast = this.toastCtrl.create({
      message: address,
      duration: 3000,
      position: 'top'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

  updateSearch() {
    console.log('modal > updateSearch');
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types: ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
      input: this.autocomplete.query,
      //componentRestrictions: { country: 'AR' } 
    }
    this.acService.getPlacePredictions(config, function (predictions, status) {
      console.log('modal > getPlacePredictions > status > ', status);
      self.autocompleteItems = [];
      console.log(predictions);
      predictions.forEach(function (prediction) {
        self.autocompleteItems.push(prediction);
      });
    });
  }

  toastViewer(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  //confirm alert for registered address set as primary address
  defaultUpdateConfirm(add) {
    var conftxt='';
    var confmsg='';
    var no='';
    var yes='';
    this.translate.get('Confirm this address').subscribe(res=>{
      conftxt = res;
    });
    this.translate.get('Do you want to set this as primary address?').subscribe(res=>{
      confmsg=res;
    });
    this.translate.get('No').subscribe(res=>{
      no=res;
    });
    this.translate.get('Yes').subscribe(res=>{
      yes=res;
    });
    let alert = this.alertCtrl.create({
      title: conftxt,
      message: confmsg,
      buttons: [
        {
          text: no,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: yes,
          handler: () => {
            console.log('yes clicked');
            this.setPrimaryAddress(this.add,this.i);
          }
        }
      ]
    });
    alert.present();
  }

  //api call to set address primary
  setPrimaryAddress(add,i) {
    let tit ='';
    this.translate.get("Profile updating...").subscribe(res=>{
      tit = res;
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: tit
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);
    console.log(add);
    this.AuthServiceProvider.postData(add, 'updatePrimaryAddress').then((result) => {
      this.responseData = result;
      var user_detail = this.responseData;
      if (true == this.responseData.status) {
        this.addEdit[i] = false;
        this.addUpdate[i] = false;
        this.isDisabled[i] = false;
        this.getAddresses();
        this.translate.get('updated as primary address.').subscribe(res=>{
          this.toastmessage = res;
        });
        this.toastViewer(this.toastmessage);
      } else {
        this.translate.get('update failed.').subscribe(res=>{
          this.toastmessage = res;
        });
        this.toastViewer(this.toastmessage);
      }
    });
  }
  editAddress(i) {
    this.isDisabled[i] = true;
    this.addEdit[i] = true;
    this.addUpdate[i] = true;
  }
  updateOldAddress(add, i) {
    var address = `${add.street_address}, ${add.city}, ${add.country}, ${add.zipcode} `;
    console.log(address);
    this.geocoder = new google.maps.Geocoder;
    this.geocoder.geocode({ 'address': address }, (results, status) => {
      if (status === 'OK') {
        console.log(results);
        //alert(address);
        let position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };
        console.log(position);
        add.latitude = position.lat;
        add.longitude = position.lng;
        localStorage.setItem('accLatLng', JSON.stringify(add));
        this.updateAddress(add, i);
      } else {
        this.toastmessage = 'Geocode was not successful for the following reason: ' + status;
        this.toastViewer(this.toastmessage);
      }
    });
  }

  updateAddress(add, i) {
    console.log(add);
    let load_cont = '';
    this.translate.get('Address updating...').subscribe(res=>{
      load_cont=res;
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: load_cont
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);
    this.AuthServiceProvider.postData(add, 'updateEditAddress').then((result) => {
      this.responseData = result;
      if (true == this.responseData.status) {
        let aus_title = '';
        this.translate.get("Address updated successfully").subscribe(res=>{
          aus_title=res;
        });
        this.toastmessage = aus_title;
        this.toastViewer(this.toastmessage);
        this.isDisabled[i] = false;
        this.addEdit[i] = false;
        this.addUpdate[i] = false;
        this.getAddresses();
      } else {
        let toast_update_again='';
        this.translate.get('Update failed try again.').subscribe(res=>{
          toast_update_again=res;
        });
        this.toastmessage = toast_update_again;
        this.toastViewer(this.toastmessage);
      }
    });
  }

  deleteAddress(add,i) {
    console.log(add);
    let trans_title ='';
    let trans_msg ='';
    let trans_btn_text_yes="";
    this.translate.get("Address").subscribe(res=>{
      trans_title=res;
    });
    this.translate.get("Do you want to delete the address?").subscribe(res=>{
      trans_msg=res;
    });
    this.translate.get("Yes").subscribe(res=>{
      trans_btn_text_yes=res;
    });
    let trans_btn_text_no = '';
    this.translate.get("No").subscribe(res=>{
      trans_btn_text_no=res;
    });
    let alert = this.alertCtrl.create({
      title: trans_title,
      message: trans_msg,
      buttons: [
        {
          text: trans_btn_text_no,
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: trans_btn_text_yes,
          handler: () => {
            let toast_add_success = '';
            let toast_delete_fail = '';
            this.translate.get("Address deleted successfully.").subscribe(res=>{
              toast_add_success=res;
            });
            this.translate.get("delete failed.please try again.").subscribe(res=>{
              toast_delete_fail=res;
            });
            this.AuthServiceProvider.postData(add, 'deleteAddress').then((result) => {
              this.responseData = result;
              if (true == this.responseData.status) {
                this.toastmessage = toast_add_success;
                this.toastViewer(this.toastmessage);
                this.getAddresses();
                this.addUpdate[i] = true;
                this.addEdit[i] = false;
              } else {
                this.toastmessage = toast_delete_fail;
                this.toastViewer(this.toastmessage);
              }
            });
          }
        }
      ]
    });
    alert.present();
    
  }

  clearMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers.length = 0;
  }

  chooseItem(item: any) {
    console.log('modal > chooseItem > item > ', item);
    //this.viewCtrl.dismiss(item);
    //console.log(item);
    this.addInput = this.autocomplete.query;
    console.log('query:'+this.addInput);
    this.geocoder = new google.maps.Geocoder;
    this.markers = [];
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
    this.geocoder.geocode({ 'placeId': item.place_id }, (results, status) => {
      console.log(results);
      // if (status === 'OK' && results[0]) {
      let position = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      };
      this.LastLat1 = results[0].geometry.location.lat();
      this.LastLng1 = results[0].geometry.location.lng();
      localStorage.setItem('accLatLng', JSON.stringify(position));
      console.log(position);
      if (results[0] != null) {
        // this.marker.buildingNum = rsltAdrComponent[resultLength-8].short_name;
        //  this.marker.streetName = rsltAdrComponent[resultLength-7].short_name;
        var data = new Array;
        let rsltAdrComponent = results[0].address_components;
        let rslength = rsltAdrComponent.length;
        for (var i = 0; i < rslength; i++) {
          var obj = rsltAdrComponent[i];
          data.push(obj.long_name);

        }
        //console.log(locdata);
        let user = JSON.parse(localStorage.getItem('userData'));
        this.newAddAry.user_id = user.user_id;
        this.newAddAry.latitude = position.lat;
        this.newAddAry.longitude = position.lng;
        console.log('new address');
        if (data.length == 5) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[3];
          this.newAddAry.city = data[2];
          this.newAddAry.zipcode = data[4];
        }
        else if (data.length == 6) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[4];
          this.newAddAry.city = data[2];
          this.newAddAry.zipcode = data[5];
        } else if (data.length == 7) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[5];
          this.newAddAry.city = data[2];
          this.newAddAry.zipcode = data[6];
        }
        else if (data.length == 8) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[6];
          this.newAddAry.city = data[4];
          this.newAddAry.zipcode = data[7];
        } else if (data.length == 9) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[7];
          this.newAddAry.city = data[4];
          this.newAddAry.zipcode = data[8];
        } else if (data.length == 10) {
          this.newAddAry.street_address = data[1] + " " + data[0];
          this.newAddAry.country = data[8];
          this.newAddAry.city = data[5];
          this.newAddAry.zipcode = data[9];
        }
        console.log('new addres:' + JSON.stringify(this.newAddAry));
        this.autocomplete.query=item.description;
      } else {
        //alert("No address available");
        console.log("No address available");
      }
      var markerpos = new google.maps.LatLng(this.LastLat1,this.LastLng1);
      this.marker.setPosition(markerpos);
      this.map.setCenter({ lat: this.LastLat1, lng: this.LastLng1 });
    });
  }

  addNewAddress() {
    let trans_load_p_update ='';
    this.translate.get("Profile updating...").subscribe(res=>{
      trans_load_p_update=res;
    });
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: trans_load_p_update
    });
    setTimeout(() => {
      loading.dismiss();
    }, 300);
    if(this.newAddAry.latitude == '' && this.newAddAry.longitude == '' && this.newAddAry.street_address == '') {
      let toast_no_address ='';
      this.translate.get("No address get please select correct address.").subscribe(res=>{
        toast_no_address=res;
      });
      this.toastmessage = toast_no_address;
      this.toastViewer(this.toastmessage);
    } else {
      this.newAddAry.primary_address = 'no';
      this.AuthServiceProvider.postData(this.newAddAry, 'addMoreAddress').then((result) => {
        // loading.present();
        this.responseData = result;
        var user_detail = this.responseData;
        if (true == this.responseData.status) {
          let trans_toast_s_msg = '';
          this.translate.get("Address added successfully").subscribe(res=>{
            trans_toast_s_msg=res;
          });
          this.toastmessage = trans_toast_s_msg;
          this.toastViewer(this.toastmessage);
          this.navCtrl.setRoot(this.navCtrl.getActive().component);
        } else {
          let toast_re_try = '';
          this.translate.get("failed please re-try.").subscribe(res=>{
            toast_re_try=res;
          });
          this.toastmessage = toast_re_try;
          this.toastViewer(this.toastmessage);
        }
      });
    }
  }

  editNAddress() {
    this.dis = false;
    this.addE = false;
    this.addU = true;
  }
  updateNAddress(add) {
    console.log(add);
    var address = `${add.street_address}, ${add.city}, ${add.country}, ${add.zipcode} `;
    console.log(address);
    this.geocoder = new google.maps.Geocoder;
    this.geocoder.geocode({ 'address': address }, (results, status) => {
      if (status === 'OK') {
        console.log(results);
        //alert(address);
        let position = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        };
        console.log(position);
        add.latitude = position.lat;
        add.longitude = position.lng;
        localStorage.setItem('accLatLng', JSON.stringify(add));
        console.log(add);
        let trans_add_update = '';
        this.translate.get('Address updating...').subscribe(res=>{
          trans_add_update=res;
        });
        let loading = this.loadingCtrl.create({
          spinner: 'crescent',
          content: trans_add_update
        });
        setTimeout(() => {
          loading.dismiss();
        }, 300);
        this.AuthServiceProvider.postData(add, 'updateEditAddress').then((result) => {
          this.responseData = result;
          if (true == this.responseData.status) {
            let trans_toast_u_msg="";
            this.translate.get("Address updated successfully").subscribe(res=>{
              trans_toast_u_msg=res;
            });
            this.toastmessage = trans_toast_u_msg;
            this.toastViewer(this.toastmessage);
            this.dis = false;
            this.addE = true;
            this.addU = false;
            this.getAddresses();
          } else {
            let trans_toast_update_fail='';
            this.translate.get('Update failed try again.').subscribe(res=>{
              trans_toast_update_fail=res;
            });
            this.toastmessage = trans_toast_update_fail;
            this.toastViewer(this.toastmessage);
          }
        });
      } else {
        this.toastmessage = 'Geocode was not successful for the following reason: ' + status;
        this.toastViewer(this.toastmessage);
      }
    });
  }
}
