import { BrowserModule } from '@angular/platform-browser';
import { Device } from '@ionic-native/device';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule,IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HttpModule, Http } from '@angular/http';
import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage'; 
import { EmailComposer} from '@ionic-native/email-composer';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'
import { Keyboard } from '@ionic-native/keyboard';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Diagnostic } from '@ionic-native/diagnostic';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { Directive, HostListener } from '@angular/core';
import { GoogleMaps } from '../providers/google-service/google-maps';
import { AgmMap,AgmCoreModule,GoogleMapsAPIWrapper } from '@agm/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { GetUserAddressProvider } from '../providers/get-user-address/get-user-address';
export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    FormsModule,ReactiveFormsModule,
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
    loader: {
         provide: TranslateLoader,
         useFactory: (createTranslateLoader),
         deps: [Http]
       }
    }),
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAlJZd_87izwp5nfqW1hoqbSwAWmX8X4Cc',
      libraries: ['places']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    GooglePlus,
    Push,
    Device,
    NativeStorage,
    Keyboard ,
    EmailComposer,
    StatusBar,
    SplashScreen,
    Geolocation,
    BackgroundGeolocation,
    Diagnostic,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    GoogleMaps,
    InAppBrowser,
    TranslateService,
    LocationAccuracy,
    AgmMap,
    GetUserAddressProvider
  ]
})
export class AppModule {}
