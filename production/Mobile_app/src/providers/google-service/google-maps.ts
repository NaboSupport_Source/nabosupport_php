import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { Geolocation, GeolocationOptions, Geoposition, PositionError } from '@ionic-native/geolocation';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

declare var google:any;
@Injectable()
export class GoogleMaps {
 
    map: any;
    markers: any = [];
    options:any;
    currentPos:any;
    responseData:any;
    othersAddress:any;
    LastLat:any;
    LastLng:any;
    addData:any;
    geocoder:any;
    latlng:any;
    mapOptions:any;
    mapData:any=[];
    constructor(public http: Http,public geolocation: Geolocation,public AuthServiceProvider: AuthServiceProvider) {
 
    }

    //initiate google map
    initMap(mapElement){
        this.geocoder = new google.maps.Geocoder();
        this.options = {
        enableHighAccuracy: true
        };

        this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {

        this.currentPos = pos;
        console.log(pos);
        console.log(pos.coords.latitude + ',' + pos.coords.longitude)

        this.latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        
            this.mapOptions = {
                center: this.latlng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                fullscreenControl: false,
                streetViewControl: false,
                mapTypeControl: false,
                clickableIcons: false
            };
            console.log('map initiated');
            this.map = new google.maps.Map(mapElement, this.mapOptions);
            // google.maps.event.addListenerOnce(this.map, 'idle', () => {
            //     this.loadMarkers();
            //     google.maps.event.addListener(this.map, 'dragend', () => {
            //         this.loadMarkers();      
            //     });
            // });
        }, (err: PositionError) => {
        console.log("error : " + err.message);
        });
    }
    getMap(mapElement) {
        return this.initMap(mapElement);
    }
  //load markers when drag
  loadMarkers(){
        let center = this.map.getCenter(),
           bounds = this.map.getBounds(),
           zoom = this.map.getZoom();
    
       // Convert to readable format
       let centerNorm = {
           lat: center.lat(),
           lng: center.lng()
       };
    
       let boundsNorm = {
           northEast: {
               lat: bounds.getNorthEast().lat(),
               lng: bounds.getNorthEast().lng()
           },
           southWest: {
               lat: bounds.getSouthWest().lat(),
               lng: bounds.getSouthWest().lng()
           }
       };
    
       let boundingRadius = this.getBoundingRadius(centerNorm, boundsNorm);
    
       let options = {
           lng: centerNorm.lng,
           lat: centerNorm.lat,
           maxDistance: boundingRadius
       }
       this.getMarkers();
       this.getDragendData();
    
  }  
  getDragendData():any {
    this.geocoder = new google.maps.Geocoder();
    console.log("map move");
     this.LastLat = this.map.getCenter().lat();
     this.LastLng = this.map.getCenter().lng();
     var coord = new Array();
     coord.push(this.LastLat, this.LastLng);
     console.log(coord[0] + ',' + coord[1]);
     let latlng = new google.maps.LatLng(this.LastLat, this.LastLng);
     let request = { latLng: latlng };
     console.log(this.LastLat + ',' + this.LastLng);
     this.geocoder.geocode(request, (results, status) => {
         if (status == google.maps.GeocoderStatus.OK) {

             this.othersAddress = results[0].formatted_address;
             // this.locationData = results[0].geometry.location;
             // console.log("drag"+JSON.stringify(this.locationData));
             console.log(this.othersAddress);
             localStorage.setItem('address',this.othersAddress);
             //return this.othersAddress
         } else {
             console.log(this.othersAddress);
             //return status;
             //console.log('Cannot determine address at this location.' + status);
             localStorage.setItem('address',"Cannot determine address at this location.");
         }
     });
  }

  getBoundingRadius(center, bounds){
      return this.getDistanceBetweenPoints(center, bounds.northEast, 'km');   
  }

  getDistanceBetweenPoints(pos1, pos2, units){

      let earthRadius = {
          miles: 3958.8,
          km: 6371
      };

      let R = earthRadius[units || 'miles'];
      let lat1 = pos1.lat;
      let lon1 = pos1.lng;
      let lat2 = pos2.lat;
      let lon2 = pos2.lng;

      let dLat = this.toRad((lat2 - lat1));
      let dLon = this.toRad((lon2 - lon1));
      let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      let d = R * c;

      return d;

  }

  toRad(x){
      return x * Math.PI / 180;
  }

  //get markers
  getMarkers(){
    this.AuthServiceProvider.getData('getUseraddress').then((result) => {
        this.responseData = result;
        console.log(this.responseData);
        this.addMarkers(this.responseData);
      });
   }

  //add markers
  addMarkers(markers){
    
      let marker;
      let markerLatLng;
      let lat;
      let lng;
  
      markers.forEach((marker) => {
  
          lat = marker.latitude;
          lng = marker.longitude;
  
          markerLatLng = new google.maps.LatLng(lat, lng);
  
          if(!this.markerExists(lat, lng)){
  
              marker = new google.maps.Marker({
                  map: this.map,
                  animation: google.maps.Animation.DROP,
                  position: markerLatLng,
                  icon: 'http://rayi.in/naboapi_v2/mapicon/'+marker.marker_icon,
              });
  
              let markerData = {
                  lat: lat,
                  lng: lng,
                  marker: marker
              };
  
              this.markers.push(markerData);
  
          }
  
      });
    
   }

  //check marker exist
  markerExists(lat, lng){
 
    let exists = false;
 
    this.markers.forEach((marker) => {
        if(marker.lat === lat && marker.lng === lng){
            exists = true;
        }
    });
 
    return exists;
 
    }
}