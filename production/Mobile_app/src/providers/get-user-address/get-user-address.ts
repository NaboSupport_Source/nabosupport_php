import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@Injectable()
export class GetUserAddressProvider {
  LastLng1: any;
  LastLat1: any;
  marker: any;
  userdetail: {};
  editProfile: any;
  autocompleteItems: any;
  autocomplete: any;
  acService: any;
  placesService: any;
  responseData: any;
  isDisable: any;
  isDisabled: any;
  users: any;
  submitProfile: any;
  cancelProfile: any;
  lastImage: string = null;
  fileopen = false;
  loading: any;
  content: any;
  _ispaid:boolean = false;
  addresses:any;
  addAddress:boolean = false;
  addaddress:boolean;
  public mapcondition:boolean = false;
  public imgoption: boolean = false;
  locationdata = { "hus": "", "street": "", "city": "" };
  marker_data ={"latitude":"","longitude":""};
  constructor(
    public http: Http,
    public AuthServiceProvider: AuthServiceProvider,
  ) {
    console.log('Hello GetUserAddressProvider Provider');
  }

    //get the user address and details
    get_user() {
      var userdata = JSON.parse(localStorage.getItem('userData'));
      console.log(userdata['user_id']);
      var user = {
        'user_id':userdata.user_id,
        'address_id':userdata.address_id
      };
      this.AuthServiceProvider.postData(userdata, 'getUserdata').then((result) => {
        this.responseData = result;
        var user_detail = this.responseData;
        this.marker_data.latitude = this.responseData[0]['latitude'];
        this.marker_data.longitude = this.responseData[0]['longitude'];
        localStorage.setItem('userData',JSON.stringify(user_detail[0]));
        console.log(user_detail[0].paid_member);
        if(user_detail[0].paid_member == 1) {
          this.AuthServiceProvider.postData(user, 'getPaidUserAddress').then((result) => {
            this.addresses = result;
            this._ispaid = true;
            console.log('user saved addresses '+ this.addresses);
          },(err) =>{
            console.log(err);
          });
        }
        console.log(user_detail[0].latitude + ',' + user_detail[0].longitude);
        this.userdetail = user_detail;
        this.isDisable = true;
        this.editProfile = true;
        this.submitProfile = false;
        this.cancelProfile = false;
        this.isDisabled = true;
      });
    }
}
