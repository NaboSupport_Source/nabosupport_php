<?php
header("Access-Control-Allow-Origin: *");
include 'dbConfig.php';
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
 
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
 
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
 
        exit(0);
    }
	$postdata = file_get_contents("php://input");
	//print_r($postdata); exit
	$request = json_decode($postdata);
	//print_r($request);exit;
	//$div = array();
	//unset($request[0]->deviceID);
	//print_r($request);exit;
	$rng_value = $request->valuedata;
	$user_id = $request->user_id;
	$lat=$request->latitude;
	$lng=$request->longitude;
	$sender_id = $request->user_id;
	$alert_type = $request->alert_type;
	$sender_id = $request->user_id;
	$address_id = $request->address_id;
	$street = $request->street_address;
	$city = $request->city;
	$country = $request->country;
	$zipcode = $request->zipcode;
	$address = $street.','.$city.','.$country.','.$zipcode;
	if($alert_type == "B") {
		$owner_alert = "BP";
		$alert ="BC";
	} else if($alert_type == "S"){
		$owner_alert = "SP";
		$alert = "SC";
	}
	try{
		$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.distance as d1 from(SELECT  user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
		// $sql="SELECT  user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND address_type NOT IN('GB') AND address_type NOT IN('GS') HAVING distance <:rang ORDER BY distance LIMIT 0 , 30";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam(':rang', $rng_value);
		$smt->bindParam(':lat', $lat);
		$smt->bindParam(':lng', $lng);
		$smt->bindParam(':user_id', $user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($rows);
		foreach($rows as $value) {
				$nuser_id = $value['user_id'];
				$sql = "SELECT user_id, deviceID from user WHERE user_id =:nuser_id AND do_not_distrub=0";
				$db = getDB();
				$stmt = $db->prepare($sql);
				$stmt->bindParam('nuser_id', $nuser_id);
				$stmt->execute();
				$divIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
				//print_r($divIds);
				foreach($divIds as $divData) {
					$deviceID = $divData->deviceID;
					$message = 'please check my home.';
				
					define( 'API_ACCESS_KEY', 'AIzaSyBIhHBL9VCl5pAkahNbGdwsecyEcB1turM' );
					//$registrationIds = array( $_GET['id'] );
					// prep the bundle
					$msg = array
					(
						'message' 	=> 'NABO ALERT MESSAGE',
						'title'		=> 'Suspicious act has been noted in this address.',
						'subtitle'	=> $address,
						'vData'	=> json_encode($request),
						'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon',
						'priority' 	=> 'high'
					);
					$fields = array
					(
						'registration_ids' 	=> array($deviceID),
						'data'			=> array("message" => $msg)
					);
				 
					$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);
				 
					$ch = curl_init();
					curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					//echo $result;
								
					try {
						$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`) VALUES
													   (:sender_id, :receiver_id, :alert_type, :address_id, :message)";
						$db = getDB();
						$smt = $db->prepare($sql);
						$smt->bindParam('sender_id', $user_id);
						$smt->bindParam('receiver_id', $nuser_id);
						$smt->bindParam('alert_type', $alert);
						$smt->bindParam('address_id', $address_id);
						$smt->bindParam('message', $message);
						$smt->execute();
						//echo '{"step": neighbour alert Change}';
						$sql1 = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id";
						$smt1 = $db->prepare($sql1);
						$smt1->bindParam('receiver_id', $nuser_id);
						$smt1->execute();
						$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
						foreach($rows as $value) {
							$address_id = $value['address_id'];
							$receiver_id = $value['receiver_id'];
							
							$sql = "UPDATE user u, address a SET u.alert_type=:alert WHERE a.address_id=
									:address_id AND u.user_id =:receiver_id AND u.alert_type NOT IN('B') AND u.alert_type NOT IN('BP')
									AND u.alert_type NOT IN('S') AND u.alert_type NOT IN('SP')";
							$db = getDB();
							$smt = $db->prepare($sql);
							$smt->bindParam('address_id', $address_id);
							$smt->bindParam('receiver_id', $receiver_id);
							$smt->bindParam('alert', $alert);
							$smt->execute();
						}
	
					}catch(Exception $e) {
						
						echo '{"status":false, "msg":'.$e->getMessage().'}';
					}
					
				}
		}
		if($result) {
			$sql = "UPDATE user SET alert_type=:owner_alert WHERE user_id=:user_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('owner_alert', $owner_alert);
			$smt->bindParam('user_id', $user_id);
			$smt->execute();
		}
		echo '{"status":true}';

	} catch (Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
	
	?>