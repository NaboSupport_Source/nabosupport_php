-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2017 at 01:41 PM
-- Server version: 5.6.23-72.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rayiipsb_nabo_support`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `street_address` varchar(255) NOT NULL,
  `zipcode` varchar(100) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  `latitude` float(17,14) NOT NULL,
  `longitude` float(17,14) NOT NULL,
  `status_id` int(11) NOT NULL,
  `owner_alert_status` varchar(15) NOT NULL,
  `neighbour_alert_mapview` varchar(15) NOT NULL,
  `Alert_response` varchar(15) NOT NULL,
  `saw_the_same` int(11) NOT NULL DEFAULT '0',
  `not_suspicious` int(11) NOT NULL DEFAULT '0',
  `push_success` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=138 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `latitude`, `longitude`, `status_id`, `owner_alert_status`, `neighbour_alert_mapview`, `Alert_response`, `saw_the_same`, `not_suspicious`, `push_success`) VALUES
(1, 1, '27 Nedervej', '8740', 'BrÃ¦dstrup', 'Denmark', 1, 55.93031311035156, 9.64102935791016, 1, 'N', 'false', '', 0, 0, 0),
(68, 194, '472 Lyngbyvej', '2820', 'Gentofte', 'Denmark', 1, 55.75627517700195, 12.51755046844482, 0, '', '', '', 0, 0, 0),
(77, 203, 'Kalasamudram Kannamangalam - Padavedu Road', '632312', 'Tiruvannamalai', 'India', 1, 12.70682144165039, 79.12308502197266, 1, '', '', '', 0, 0, 0),
(78, 204, 'Subramaniam Colony Railway Station Road', '600042', 'Chennai', 'India', 1, 12.96493244171143, 80.21842956542969, 1, '', '', '', 0, 0, 0),
(85, 212, '5th Street Buildfast Homes', '600117', 'Kanchipuram', 'India', 1, 12.94095134735107, 80.17147064208984, 1, '', '', '', 0, 0, 0),
(86, 213, 'Nesa AllÃ© 1', '2820', 'Gentofte', 'Denmark', 1, 55.75747299194336, 12.51597023010254, 1, '', '', '', 0, 0, 0),
(89, 216, 'BirkhÃ¸jvej 9', '2800', 'Kongens Lyngby', 'Denmark', 1, 55.76175308227539, 12.48254013061523, 1, '', '', '', 0, 0, 0),
(90, 217, 'BirkhÃ¸jvej 7', '2800', 'Kongens Lyngby', 'Denmark', 1, 55.76178359985352, 12.48301792144775, 1, '', '', '', 0, 0, 0),
(91, 218, 'Gammel BagsvÃ¦rdvej 53', '2800', 'Kongens Lyngby', 'Denmark', 1, 55.76601791381836, 12.48877811431885, 0, '', '', '', 0, 0, 0),
(92, 219, 'Medavakkam Main Road 3/192', '600091', 'Chennai', 'India', 1, 12.96743392944336, 80.18909454345703, 0, '', '', '', 0, 0, 0),
(97, 224, 'Ganesh Street 155', '600091', 'Chennai', 'India', 1, 12.96826839447021, 80.18997955322266, 1, '', '', '', 0, 0, 0),
(100, 227, 'BirkhÃ¸jvej 7', '2800', 'Kongens Lyngby', 'Denmark', 1, 55.76177597045898, 12.48298454284668, 1, '', '', '', 0, 0, 0),
(101, 228, 'Velachery Road 3', '600042', 'Chennai', 'India', 1, 12.97093486785889, 80.21892547607422, 1, '', '', '', 0, 0, 0),
(102, 229, 'Lyngbyvej 343A', '2820', 'Gentofte', 'Danmark', 1, 55.74160385131836, 12.53497314453125, 0, '', '', '', 0, 0, 0),
(105, 232, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96643161773682, 80.18952941894531, 0, '', '', '', 0, 0, 0),
(115, 242, 'Macmillan Colony 4/46', '600061', 'Chennai', 'India', 1, 12.98143005371094, 80.19150543212890, 1, '', '', '', 0, 0, 0),
(116, 243, 'Bhuvaneshwari Nagar 3rd Main Road', '600042', 'Chennai', 'India', 1, 12.96953201293945, 80.22004699707031, 1, '', '', '', 0, 0, 0),
(117, 244, 'Lyngbyvej 343A', '2820', 'Gentofte', 'Danmark', 1, 55.74170684814453, 12.53485393524170, 0, '', '', '', 0, 0, 0),
(118, 245, 'Narayana Nagar Masjid Street', '600041', 'Chennai', 'India', 1, 12.94047451019287, 80.24819946289062, 1, '', '', '', 0, 0, 0),
(119, 246, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96646022796631, 80.18952941894531, 0, '', '', '', 0, 0, 0),
(120, 247, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645736694336, 80.18952941894531, 0, '', '', '', 0, 0, 0),
(121, 248, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645927429199, 80.18952941894531, 0, '', '', '', 0, 0, 0),
(122, 249, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645927429199, 80.18952941894531, 0, '', '', '', 0, 0, 0),
(123, 250, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645641326904, 80.18953704833984, 0, '', '', '', 0, 0, 0),
(124, 251, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645832061768, 80.18953704833984, 0, '', '', '', 0, 0, 0),
(126, 253, 'Senthuran Colony Defence Colony Street', '600091', 'Chennai', 'India', 1, 12.96642303466797, 80.18924713134766, 1, '', '', '', 0, 0, 0),
(127, 254, 'Annai Street 3/84', '600091', 'Chennai', 'India', 1, 12.96574306488037, 80.18784332275390, 1, '', '', '', 0, 0, 0),
(128, 255, 'Erikarai Street 1/535', '600091', 'Chennai', 'India', 1, 12.96153163909912, 80.19248199462890, 1, '', '', '', 0, 0, 0),
(129, 256, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645641326904, 80.18953704833984, 0, '', '', '', 0, 0, 0),
(130, 257, 'Senthuran Colony Defence Colony Street', '600091', 'Chennai', 'India', 1, 12.96640014648438, 80.18931579589844, 1, '', '', '', 0, 0, 0),
(131, 258, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645641326904, 80.18953704833984, 0, '', '', '', 0, 0, 0),
(132, 259, 'Arunachalam Nagar Main Road Aruna Flats', '600091', 'Alandur', 'India', 1, 12.95967197418213, 80.18778228759766, 1, '', '', '', 0, 0, 0),
(133, 260, 'Vijayalakshmi Nagar 1st Cross Street', '600117', 'Chennai', 'India', 1, 12.94581985473633, 80.17425537109375, 1, '', '', '', 0, 0, 0),
(134, 261, 'Air Force Station IAF Road', '600046', 'Kanchipuram', 'India', 1, 12.91024208068848, 80.12636566162110, 1, '', '', '', 0, 0, 0),
(135, 262, 'Senthuran Colony 3/110A', '600091', 'Chennai', 'India', 1, 12.96645450592041, 80.18953704833984, 0, '', '', '', 0, 0, 0),
(136, 263, 'Pachaiyamman Nagar Malaimagal Street', '600043', 'Kanchipuram', 'India', 1, 12.96953392028809, 80.16642761230469, 1, '', '', '', 0, 0, 0),
(137, 264, 'Senthuran Colony 3/105', '600091', 'Chennai', 'India', 1, 12.96653175354004, 80.18933868408203, 0, '', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `alert_log`
--

CREATE TABLE IF NOT EXISTS `alert_log` (
  `alert_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `status_id` varchar(5) NOT NULL,
  `address_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`alert_log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `alert_log`
--

INSERT INTO `alert_log` (`alert_log_id`, `sender_id`, `receiver_id`, `status_id`, `address_id`, `message`, `time`) VALUES
(1, 245, 250, 'B', 123, 'dfgfdh', '2017-12-13 12:18:33'),
(2, 228, 212, 'B', 85, 'Test sangeetha', '2017-12-14 03:56:11'),
(3, 243, 245, 'B', 118, 'burglery', '2017-12-14 09:22:44'),
(4, 243, 245, 'B', 118, 'fdhggfdgh', '2017-12-14 10:33:59'),
(5, 254, 253, 'B', 126, 'We are testing rayi test 1 location', '2017-12-14 10:58:10'),
(6, 254, 253, 'B', 126, 'Butgrrly alert 2 rayi test', '2017-12-14 11:22:05'),
(7, 245, 253, 'B', 126, 'alert from vengat', '2017-12-14 11:45:45'),
(8, 254, 224, 'S', 97, 'Test suspense', '2017-12-14 12:02:58'),
(9, 254, 253, 'B', 126, 'BurgL at your house check', '2017-12-14 12:08:28'),
(10, 254, 253, 'B', 126, 'Second time burgl', '2017-12-14 12:10:16'),
(11, 254, 253, 'B', 126, 'Third time burgl', '2017-12-14 12:25:44'),
(12, 257, 224, 'B', 97, 'roberry', '2017-12-14 14:57:17'),
(13, 259, 258, 'B', 131, 'Test', '2017-12-15 05:59:55'),
(14, 259, 258, 'B', 131, 'Jay', '2017-12-15 06:04:26'),
(15, 257, 245, 'B', 118, 'Oh god...', '2017-12-15 08:00:51'),
(16, 257, 245, 'B', 118, 'Test vengat', '2017-12-15 09:21:15'),
(17, 224, 245, 'B', 118, 'Praba test', '2017-12-15 11:01:29'),
(18, 224, 245, 'B', 118, 'Praba yest', '2017-12-15 11:02:24'),
(19, 257, 245, 'S', 118, 'Test praba', '2017-12-15 11:49:32'),
(20, 257, 245, 'B', 118, 'Bharat test', '2017-12-15 12:23:19'),
(21, 257, 245, 'B', 118, 'S to go to j', '2017-12-15 12:41:14'),
(22, 257, 245, 'B', 118, 'Rtyuuuhgfdg', '2017-12-15 12:48:36'),
(23, 245, 243, 'S', 116, 'sus', '2017-12-15 14:42:50'),
(24, 257, 243, 'B', 116, 'Roberry in progress', '2017-12-15 15:54:07'),
(25, 257, 243, 'B', 116, 'Aler', '2017-12-15 15:55:31'),
(26, 257, 243, 'B', 116, 'I push', '2017-12-15 16:35:24'),
(27, 257, 243, 'B', 116, 'Ghh', '2017-12-15 16:37:47'),
(28, 253, 243, 'B', 116, 'Something suspicious ', '2017-12-16 11:33:22'),
(29, 253, 243, 'B', 116, 'Test from sangeetha', '2017-12-16 11:34:13'),
(30, 245, 263, 'B', 136, 'test alert', '2017-12-16 11:50:30');

-- --------------------------------------------------------

--
-- Table structure for table `alert_messages`
--

CREATE TABLE IF NOT EXISTS `alert_messages` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `message` varchar(500) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`msg_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `alert_messages`
--

INSERT INTO `alert_messages` (`msg_id`, `user_id`, `address_id`, `message`, `date`) VALUES
(1, 243, 116, 'Alert one', '2017-12-09 11:22:16'),
(2, 243, 116, 'gfhjhlk;', '2017-12-09 12:20:57'),
(3, 243, 116, 'jkksdtyhkjl', '2017-12-09 12:26:51'),
(4, 243, 116, 'hyjghjkmb,m', '2017-12-09 12:38:29'),
(5, 243, 116, 'kl;kl;kjl;klk', '2017-12-09 13:25:11'),
(6, 243, 116, 'fgjhgjkjkhl;', '2017-12-09 13:31:27'),
(7, 243, 116, 'dsfgfgjhgkjuk', '2017-12-09 13:33:09'),
(8, 216, 89, 'Ramesh 9/12 Test1', '2017-12-09 20:04:20'),
(9, 217, 90, 'Chk door', '2017-12-09 20:05:01'),
(10, 227, 100, 'Alert to myself', '2017-12-09 20:05:32'),
(11, 224, 97, 'test', '2017-12-11 08:45:21'),
(12, 228, 101, 'test vengat', '2017-12-11 09:30:45'),
(13, 227, 100, 'vengat test alert', '2017-12-11 09:32:24'),
(14, 224, 97, 'test alert vengat', '2017-12-11 09:53:10'),
(15, 224, 97, 'test', '2017-12-11 09:53:41'),
(16, 224, 97, 'test alrt vengat', '2017-12-11 09:53:59'),
(17, 228, 101, 'test vengat alert', '2017-12-11 10:07:44'),
(18, 228, 101, 'rayi test', '2017-12-11 15:08:08'),
(19, 224, 97, 'vengat', '2017-12-11 15:46:03'),
(20, 224, 97, 'hai test', '2017-12-11 16:00:10'),
(21, 242, 115, 'alert', '2017-12-12 14:38:22'),
(22, 245, 118, 'vengat alert ', '2017-12-12 14:39:34'),
(23, 250, 123, 'alert', '2017-12-13 11:38:08'),
(24, 250, 123, 'sadfsfgsfdg', '2017-12-13 11:38:22'),
(25, 250, 123, 'sffsgg', '2017-12-13 11:38:59'),
(26, 250, 123, 'dfgdfgfdg', '2017-12-13 11:41:32');

-- --------------------------------------------------------

--
-- Table structure for table `distance`
--

CREATE TABLE IF NOT EXISTS `distance` (
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `city` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `distance`
--

INSERT INTO `distance` (`lat`, `lng`, `city`, `id`) VALUES
(13.0827, 80.2707, 'chennai', 1),
(12.8342, 79.7036, 'kangipuram', 2),
(13.1067, 80.097, 'Avadi', 3),
(13.0551, 80.2221, 'kodambakkam', 4),
(12.9654, 80.2017, 'madipakkam', 5),
(12.9229, 80.1275, 'tambaram', 6),
(13.0102, 80.2157, 'guindy', 7),
(12.976, 80.2212, 'velachery', 8);

-- --------------------------------------------------------

--
-- Table structure for table `my_contact`
--

CREATE TABLE IF NOT EXISTS `my_contact` (
  `my_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  PRIMARY KEY (`my_contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `my_contact`
--

INSERT INTO `my_contact` (`my_contact_id`, `user_id`, `name`, `address`, `email_id`, `phone_number`) VALUES
(1, 1, 'vengades', 'chennai', 'vengades@gmail.com', '9898989877'),
(2, 1, 'vengat', 'chennai', 'vengades@gmail.com', '97987897'),
(3, 1, 'bharath', 'Chennai', 'bharath@gmail.com', '56889'),
(4, 158, 'Vengadesh', 'Xxxx', 'Venkat@gmail.com', '994466558811854788'),
(5, 158, 'Hahbzjn', 'Rrvshbs', 'Vvzhnnz', '965899494594'),
(6, 158, 'Gyhsns', 'Chshnz', 'Fgnsj', '5578986945'),
(7, 254, 'Test my contact', 'Test addresss you have any questions please feel free to call', 'Test@test.com', '13232954');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE IF NOT EXISTS `offer` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `device_name` varchar(255) NOT NULL,
  `device_desc` varchar(255) NOT NULL,
  `device_price` decimal(10,2) NOT NULL,
  `offer_amt` decimal(10,2) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `delivery_address` text NOT NULL,
  `total` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE IF NOT EXISTS `order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment_log`
--

CREATE TABLE IF NOT EXISTS `payment_log` (
  `payment_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`payment_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `category` char(3) NOT NULL,
  `img_path` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `category`, `img_path`, `created_at`, `modified_at`, `status`) VALUES
(1, 'Stripped', '10.00', 's', 'assets/imgs/circle.png', '2017-12-05 08:58:44', '2017-12-05 08:58:44', 1),
(2, 'Rounded', '20.00', 's', 'assets/imgs/circle.png', '2017-12-05 08:58:44', '2017-12-05 08:58:44', 1),
(3, 'Rectangle', '30.00', 's', 'assets/imgs/circle.png', '2017-12-05 08:58:44', '2017-12-05 08:58:44', 1),
(4, 'Square', '40.00', 's', 'assets/imgs/circle.png', '2017-12-05 08:58:44', '2017-12-05 08:58:44', 1),
(5, 'Triangle', '50.00', 's', 'assets/imgs/circle.png', '2017-12-05 08:58:44', '2017-12-05 08:58:44', 1),
(6, 'Stripped', '60.00', 'rs', 'assets/imgs/circle.png', '2017-12-05 09:01:58', '2017-12-05 09:01:58', 1),
(7, 'Rounded', '70.00', 'rs', 'assets/imgs/circle.png', '2017-12-05 09:01:58', '2017-12-05 09:01:58', 1),
(8, 'Rectangle', '80.00', 'rs', 'assets/imgs/circle.png', '2017-12-05 09:01:58', '2017-12-05 09:01:58', 1),
(9, 'Square', '100.00', 'l', 'assets/imgs/circle.png', '2017-12-05 09:01:58', '2017-12-05 09:01:58', 1),
(10, 'Rounded', '120.00', 'l', 'assets/imgs/circle.png', '2017-12-05 09:01:58', '2017-12-05 09:01:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `bought_date` date NOT NULL,
  `bought_time` time NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `security_devices`
--

CREATE TABLE IF NOT EXISTS `security_devices` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_desc` varchar(255) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE IF NOT EXISTS `shop` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `product_desc` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status_log`
--

CREATE TABLE IF NOT EXISTS `status_log` (
  `status_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`status_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expires` datetime DEFAULT NULL,
  `paid_member` varchar(255) NOT NULL,
  `created_on_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `do_not_distrub` smallint(1) NOT NULL,
  `out_of_home` varchar(255) NOT NULL,
  `distance` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `deviceID` varchar(500) NOT NULL,
  `alert_type` varchar(5) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=265 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `email_id`, `photo`, `is_active`, `token`, `expires`, `paid_member`, `created_on_date`, `do_not_distrub`, `out_of_home`, `distance`, `user_type_id`, `deviceID`, `alert_type`) VALUES
(1, 'admin', '123', 'admin@nabo.com', 'assets/imgs/user.jpg', 1, '87eca5880f3fada48565c9618c683882', '2017-12-16 12:42:26', 'yes', '2017-12-16 12:32:26', 1, 'test', 0, 2, '', 'N'),
(194, 'Atshey', '123', 'Atshey@gmail.com', 'assets/imgs/user.jpg', 1, 'e478f0afd3782b05db08cd9c6b5a19ab', '2017-11-28 10:07:21', '', '2017-12-11 15:37:16', 0, '', 0, 2, 'dccb3UMDos0:APA91bFkYhjJHv0yUJO-Dx6QCavaLBWF04o2xLvw3jh7ELyCnGxKQRqztEdW80Pi8Mo2I3sWLhm0Phol1Ny03LqbVTPWW5LHBmWc4sAkqFkm2b1zPQwkBVV0vGlBWplmg-p8pE_pLHNU', 'N'),
(203, 'Prithika', 'prithika', 'Prithi@gmail.com', 'assets/imgs/user.jpg', 1, '203448a755c6e1f3256f656fe6f25bfc', '2017-11-29 18:03:31', '', '2017-12-16 12:26:36', 0, '', 0, 2, 'cB8s9ms7m9Q:APA91bEyyjMpIxQZl3NjeO8XqHi5JHozfUV826-AR9_f66h7RsiogGNKNnlOYp3kBQ29Gs0xltR_9N5falk-4NgdRA-5Ap67F9wZo8YfxtLGehY8KTeNX2qLXfJKnHc_ZgLwcpM2W8_M', 'N'),
(212, 'Dhanalakshmi', 'Dhana', 'Chweetdhana@gmail.com', 'assets/imgs/user.jpg', 1, '3c74a49fdac5b2501789a5cdb148f144', '2017-11-30 16:53:53', '', '2017-12-16 12:26:36', 0, '', 0, 2, 'cvZ7PEGwdms:APA91bGh8nzOebMEWyd-AkeM8t-UKdsiF3HPW2SeLCMWoFUxEub0_TBsC5gpxPGLgLWYyz0MxphVBz7QOT8o9XtutV8d9dS1ohD6BLYAHQM3pC6PZ3mmQpMrsTcJuR-8gdx_aQa5CBJr', 'N'),
(213, 'Packramtest', '123', 'Ramtest@gmail.com', 'assets/imgs/user.jpg', 1, '5bdeb029271667ddb62a98f5cf1295b6', '2017-12-05 12:20:58', '', '2017-12-11 15:37:16', 0, '2', 45, 2, 'cKERyLMuiGk:APA91bFnnsfYCdfcYZaZuRWGAxk0EauN9rBZUfv6H9O08BNXXrLs6sgkptIHhNb14fp-gj8K0XHTziyAzqc3r3sHv1FKMIrG5OPfyCtlCz7l5aDjVeWWo2oZcKpi7HEEICu7DPpYGnGs', 'N'),
(216, 'Rameshneighbour', 'Rameshn', 'Rameshn@gmail.com', 'assets/imgs/user.jpg', 1, '4cb5d901c4e08b34f922483191d39320', '2017-12-11 17:40:26', '', '2017-12-16 12:26:36', 0, '', 10, 2, 'cB8s9ms7m9Q:APA91bEyyjMpIxQZl3NjeO8XqHi5JHozfUV826-AR9_f66h7RsiogGNKNnlOYp3kBQ29Gs0xltR_9N5falk-4NgdRA-5Ap67F9wZo8YfxtLGehY8KTeNX2qLXfJKnHc_ZgLwcpM2W8_M', 'N'),
(217, 'Tr1', '123', 'Tr1@t.com', 'assets/imgs/user.jpg', 1, '88a2b9c94b95fb3609c783a6a48a24d2', '2017-12-03 17:47:30', '', '2017-12-11 15:37:16', 0, '', 10, 2, 'd1m2oagxU-U:APA91bGBhVhl5WLZbRnUhlBKuvOoyWaBJMGV_eUYHmQOc5G6jKf0-NH823UN_J9Y3nzhx0CfNiNaWTrzEAe67LDlvhWM7MDY5kiIbgOL3JbwxJmMZT2NAoFqpxQij_Ml7ifX5hWsLa0r', 'N'),
(218, 'Tr2', '123', 'Tr2@t.com', 'assets/imgs/user.jpg', 1, '71077f7f023ca6c71232f3bcd1b6908c', '2017-12-01 19:45:29', '', '2017-12-11 15:37:16', 0, '', 10, 2, 'd1m2oagxU-U:APA91bGBhVhl5WLZbRnUhlBKuvOoyWaBJMGV_eUYHmQOc5G6jKf0-NH823UN_J9Y3nzhx0CfNiNaWTrzEAe67LDlvhWM7MDY5kiIbgOL3JbwxJmMZT2NAoFqpxQij_Ml7ifX5hWsLa0r', 'N'),
(219, '123', '123', '123@123', 'assets/imgs/user.jpg', 1, '1ce03f8d69780fab6e6af4f6cadab1a2', '2017-12-02 17:19:54', '', '2017-12-16 12:26:36', 0, '', 10, 2, 'ez1fDPuYhTo:APA91bH_dD4Kf6SKfJW3JQqDKu8Z9YThK5Uv3ohXzekSohdbs9zeT4dcvBoxKqsAJx0uoqFW77ur-BTTKSJ66L3rjmT-6_oifqKBeD6ZmQhBA_0t0qqCmUExMao8dpFNoTPKzmmRHJOB', 'N'),
(224, 'Praba', '123', 'Praba@gmail.com', 'assets/imgs/user.jpg', 1, '024a5698507928ee3630f6a111a9a85d', '2017-12-14 12:47:20', '', '2017-12-16 12:26:36', 0, '', 15, 2, 'c1-mGdsk3Pw:APA91bG5NMpurR4eXYsFPY-EANwOLhG9Hky89IEsp_ClRqW6vpzRpl_fnf4KM9NBP4YeADwLnBHL32gNi-aCPEpB76pmrNdKi6-OjHsgW7U7up3pN5tbPDL815cHRJeL1aWcw36JkPh1', 'N'),
(227, 'Packram', '123', 'Packram@gmail.com', 'assets/imgs/user.jpg', 1, 'd16392c82ffbae470c08f931ab44423d', '2017-12-14 07:18:12', '', '2017-12-14 07:08:24', 0, '', 10, 2, 'eELbn3aRDUg:APA91bG4qlYTQe3AjdLQ4eTM4YtIy_4aWFh4_wEJbLhVBvURDzGdPGEZi14rsg_VZ2wqM7hOf1BOeQPlhl30CY5X77-YX0K7ksHtetQ1jNugEV0l-pC9DjsB1j4ipi2iKOgnY7f_MnBR', 'N'),
(228, 'Shange', 'Shange', 'Sanfantas@gmail.com', 'assets/imgs/user.jpg', 1, '0911cb86584bac1db45295a3aad6cd59', '2017-12-11 19:17:47', '', '2017-12-16 12:26:36', 0, '', 10, 2, '', 'N'),
(229, 'Phillip', 'pmba130590', 'Pmbandersen@gmail.com', 'assets/imgs/user.jpg', 1, '82535068b929df3c4d32e171c31715d7', '2017-12-15 19:11:14', '', '2017-12-15 19:01:14', 0, '', 30, 2, 'edPFM0490AA:APA91bHB8jzhRuAYmlqrcvtB-8EGds6T_Hd9G0p1EvmMhAAM-6MC7M2OvJWviDiW56zbv-5B7HGewNXjYXhp1a9bcT9yebgmKfCveP1KFPX68M69Yh2C_Z5i_bkRC6PoUtBR-PhHH0xV', 'N'),
(242, 'Vengat', '321', 'Vengat@gmail.com', 'assets/imgs/user.jpg', 1, 'be5d47f66015f5e6fedf4e3d6c927aa6', '2017-12-09 11:35:12', '', '2017-12-16 12:26:36', 0, '', 10, 2, 'fWyKYTvDjpQ:APA91bGCYZPwCFbmERWeDRmbSdODoUEGg7i_-t_n3e6TunpPI8Rlub6gw-Wh-7p3rYoXiSJzBhTHlXFkM4KEaIAE2rphR1JBdslvDX3W9JKXfaA96XHuaaRdCTCMkJt5klrijTBrD0vm', 'N'),
(243, 'rayi', '123', 'rayi@gmail.com', 'assets/imgs/user.jpg', 1, '5091bd67df6f02f16d9a227a79dbf498', '2017-12-15 14:26:43', '', '2017-12-16 12:26:36', 0, '', 20, 2, '', 'N'),
(244, 'Henrik', '1234', 'hfp@proalarmer.dk', 'assets/imgs/user.jpg', 1, '0b02443066c98328707fe338790d6dbb', '2017-12-16 11:02:41', '', '2017-12-16 10:52:41', 1, '', 15, 2, 'edPFM0490AA:APA91bHB8jzhRuAYmlqrcvtB-8EGds6T_Hd9G0p1EvmMhAAM-6MC7M2OvJWviDiW56zbv-5B7HGewNXjYXhp1a9bcT9yebgmKfCveP1KFPX68M69Yh2C_Z5i_bkRC6PoUtBR-PhHH0xV', 'N'),
(245, 'test ', '123', 'test123@gmail.com', 'assets/imgs/user.jpg', 1, '227d5fca98a8cfb44afd0b0b23dcef58', '2017-12-13 11:31:39', '', '2017-12-16 12:27:48', 0, '', 5, 2, 'cZX4p4mrowc:APA91bETHW3CI0mrtdeu4RD5MdNjI7hmJngSTPTWBOYShKce6KLxKPkTOOiImEHXhhqdMBONfgVgZFu1DpI1uCUTDG3bYhJ4NjBAiptHbATNkxuGUcojDhmWUcibHhJ6Xy-yI45ChTLE', 'N'),
(250, 'Rayi Systems', '', 'rayi.systems@gmail.com', 'assets/imgs/user.jpg', 1, '', NULL, '', '2017-12-16 12:26:36', 0, '', 10, 2, '', 'N'),
(251, 'Vengadeshwaran Subramaniyan', '', 'vengadeschinz@gmail.com', 'assets/imgs/user.jpg', 1, '', NULL, '', '2017-12-16 12:26:36', 0, '', 10, 2, '', 'N'),
(253, 'Rayitest1', 'Rayitest1', 'Rayitest1@gmail.com', 'assets/imgs/user.jpg', 1, 'e820aba4b3b163fe4aa40cede2271ce9', '2017-12-14 11:29:29', '', '2017-12-16 12:26:36', 0, '', 5, 2, 'cB8s9ms7m9Q:APA91bEyyjMpIxQZl3NjeO8XqHi5JHozfUV826-AR9_f66h7RsiogGNKNnlOYp3kBQ29Gs0xltR_9N5falk-4NgdRA-5Ap67F9wZo8YfxtLGehY8KTeNX2qLXfJKnHc_ZgLwcpM2W8_M', 'N'),
(254, 'Sambasiva Moorthy', '', 'sambasivamoorthy@gmail.com', 'assets/imgs/user.jpg', 1, '', NULL, '', '2017-12-16 12:26:36', 0, '', 10, 2, '', 'N'),
(256, 'Rayitest2', 'Rayitest2@', 'Rayitest2@gmail.com', 'assets/imgs/user.jpg', 1, '', NULL, '', '2017-12-16 12:26:36', 0, '', 10, 2, 'cB8s9ms7m9Q:APA91bEyyjMpIxQZl3NjeO8XqHi5JHozfUV826-AR9_f66h7RsiogGNKNnlOYp3kBQ29Gs0xltR_9N5falk-4NgdRA-5Ap67F9wZo8YfxtLGehY8KTeNX2qLXfJKnHc_ZgLwcpM2W8_M', 'N'),
(257, 'Rayitestuser', '123', 'Rayitest3@gmail.com', 'assets/imgs/user.jpg', 1, '', NULL, '', '2017-12-16 12:26:36', 0, '', 5, 2, 'dTDXPwLaDRg:APA91bHUSz4G0SqsBM77jpCLtrmGrUm94Qmg8pyAZzpqNuJD6sCzRwFgpg4A1Fta0Inrgmiw2rCxS5T7oSIav7g6jN_B2xIia11iATSupJYPtkykpDWu-1ZtBa0tX6m68bjdlLvIR511', 'N'),
(259, 'Jay', '123', 'l.jayamurugan@gmail.com', 'assets/imgs/user.jpg', 1, '', NULL, '', '2017-12-16 12:26:36', 0, '', 10, 2, 'ddk1qdeyz6Y:APA91bGuNyCT4DOqw00u1Kh20kgPjj02JhxQwEGQ_ywBxM_NBwMdBDVkZoRnwRe8EN9aIzaqnNg8rRE3fY2FATuzgF7QIMD9jZJ_IBbYpvIxwltUBJfUdDTSosgXSDuP5WikIwI3uzca', 'N'),
(263, 'jay3@gmail.com', '123', 'jay3@gmail.com', 'assets/imgs/user.jpg', 1, 'f49ba8e3e12d50a1f519e1a1489cbdf3', '2017-12-16 12:06:52', '', '2017-12-16 12:27:31', 0, '', 50, 2, '', 'N'),
(264, 'mohanasundaram natesan', '', 'mohanasundaram.rk@gmail.com', 'assets/imgs/user.jpg', 1, '', NULL, '', '2017-12-16 12:26:36', 0, '', 10, 2, '', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) NOT NULL,
  `is_active` smallint(1) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
