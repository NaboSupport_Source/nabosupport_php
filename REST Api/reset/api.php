<?php
header("Access-Control-Allow-Origin: *");
include '../dbConfig.php';
try {
    $pass=$_REQUEST['newPassword'];
    $key=$_REQUEST['key'];
    $db = getDB();
    $stmt = $db->prepare("SELECT * FROM user WHERE pass_reset =:key");
    $stmt->bindValue(':key', $key, PDO::PARAM_STR);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows as $value) {
        $email = $value['email_id'];
    }
    if (count($rows) >= 1) {
        $stmt = $db->prepare("UPDATE user SET password =:pass, pass_reset='' WHERE email_id =:email");
        $stmt->bindValue(':pass', $pass, PDO::PARAM_STR);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        echo json_encode(array('status' => true));
    } else {
        echo json_encode(array('status' => false,'msg'=>'Try again'));
    }
} catch (PDOException $e) {
    echo json_encode(array('status' => false, 'msg' => $e->getMessage()));
}