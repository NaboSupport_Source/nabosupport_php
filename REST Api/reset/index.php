<?php
$title = 'Reset password';
include 'header.php';
$key = '';
if ($_GET['encrypt']) {
    $key = $_GET['encrypt'];
}
?>
<div class="login-box">
    <form id="resetForm" method="post">
        <p class='login-header'>Nabo support Reset Password</p>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" name="newPassword" id="newPassword" placeholder="New password">
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="Confirm password">
        </div>
        <!-- /.col -->
        <div class="col-xs-4 text-right">
            <button type="submit" class="btn btn-primary btn-flat btn-cus">Reset</button>
        </div>
        <!-- /.col -->
    </form>
</div>
<?php
include 'footer.php';
?>
<script>
    var key = '<?php echo $key; ?>';
    $('#resetForm').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        rules: {
            newPassword: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }

                }
            },
            confirmPassword: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }

                },
                equalTo: "#newPassword"
            }
        },
        messages: {
            newPassword: {
                required: 'Please enter new password'
            },
            confirmPassword: {
                required: 'Please enter confirm password',
                equalTo: 'Password not matched'
            }
        },
        submitHandler: function () {
            $.ajax({
                url: 'api.php',
                type: 'post',
                data: {newPassword: $('#newPassword').val(), key: key},
                dataType: 'json',
                success: function (rs) {
                    if (rs.status === true) {
                        window.location.href = 'http://rayi.in/naboapi/reset/password-changed.php';
                    } else {
                        alert(rs.msg);
                    }
                }
            });
        }
    });
</script>
