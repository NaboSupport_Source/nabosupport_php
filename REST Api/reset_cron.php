<?php
include 'dbConfig.php';

function getResetDate() {
	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM admin_setting WHERE status=1 AND service='threat_reset'");
		$stmt->execute();
		$rows = $stmt->fetch();
		return $rows['data'];
		//echo $rows['data'];
		//echo '{"status":true,"date":'.$rows['data'].'}';
	}catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

function cancelpush($user_id,$address_id,$address_type) {
		$alert_type = "N";
		try {
			if($address_type === "GB" || $address_type === "GS") {
				$marker = '';
				$sql = "UPDATE address SET is_active = 0 WHERE address_id=:address_id";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('address_id', $address_id);
				$smt->execute();
				if($smt == true) {
					$sqal = "UPDATE alert_log SET is_active = 0,log_state = 0 WHERE address_id=:address_id";
					$db = getDB();
					$smt = $db->prepare($sqal);
					$smt->bindParam('address_id', $address_id);
					$smt->execute();
				}
				echo '{"status":true,"msg":"remove success."}';
			} else {
				$marker = 'blue.png';
				$sql1 = "UPDATE user u, address a, alert_log al SET u.alert_type=:alert_type, a.status_type='', a.marker=:marker, al.alert_status_type =:alert_type, al.log_state = 0,al.is_active=0 WHERE u.user_id=:user_id AND a.address_id=:address_id AND al.address_id=:address_id AND a.address_type NOT IN ('GS','GB')";
				$db = getDB();
				$smt = $db->prepare($sql1);
				$smt->bindParam('user_id', $user_id);
				$smt->bindParam('address_id', $address_id);
				$smt->bindParam('alert_type', $alert_type);
				$smt->bindParam('marker',$marker);
				$smt->execute();
				// echo '{"status":true,"msg":"cancelled success."}';
			}
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}	
}

function update() {
	$marker = 'blue.png';
	try{
		$sql = "SELECT * FROM address WHERE status_type NOT IN ('N','') ORDER BY address_id asc";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		foreach($rows as $data) {
			$now = time(); // or your date as well
			$start = strtotime($data['time']);
			$datediff = $now - $start;
			$toResetDay = getResetDate();
			$differDay =  round($datediff / (60 * 60 * 24));
			// echo $data['address_id'].'<br>';
			// echo $data['time'].'<br>';
			// echo $differDay.'<br>';
			if($differDay >= $toResetDay) {
				cancelpush($data['user_id'],$data['address_id'],$data['address_type']);
			}
		}
	}catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}

}

update();
?>