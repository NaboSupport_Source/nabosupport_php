<?php
session_start();
header("Access-Control-Allow-Origin: *");
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use PHPMailer\PHPMailer\PHPMailer;
use Twilio\Rest\Client;

include 'dbConfig.php';
require 'autoload.php';
require __DIR__ . '/twilio-php-master/Twilio/autoload.php';
require __DIR__ .'/PHPMailer/src/Exception.php';
require __DIR__ .'/PHPMailer/src/PHPMailer.php';
require __DIR__ .'/PHPMailer/src/SMTP.php';

// include_once($_SERVER['DOCUMENT_ROOT'].'/wp-config.php');
// include_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
// include_once($_SERVER['DOCUMENT_ROOT'].'/wp-includes/wp-db.php');
// include_once($_SERVER['DOCUMENT_ROOT'].'/wp-includes/class-phpass.php');

require_once __DIR__ .'/wordpress/wp-config.php';
require_once __DIR__ .'/wordpress/wp-load.php';
require_once __DIR__ .'/wordpress/wp-includes/wp-db.php';
require_once __DIR__ .'/wordpress/wp-includes/class-phpass.php';

global $userdata;
global $wpdb; 

$app = new Slim\App();
$app->post('/login', 'login');
$app->post('/addFirstAddress','addFirstAddress');
$app->get('/getAdminData','getAdminData');
$app->post('/logout','logout');
$app->post('/signup', 'signup');
$app->post('/insertlocation', 'insertlocation');
$app->post('/getUser', 'getUser');
$app->post('/checkUserExist', 'checkUserExist');
$app->post('/startCron','startCron');
$app->post('/getHome','getHome');
//$app->post('/updateUserLocation','updateUserLocation');
$app->post('/forgotpassword', 'forgotpassword');
$app->post('/resetpassword', 'resetpassword');
$app->post('/product', 'product');
$app->get('/userlist', 'userlist');
$app->get('/productlist', 'productlist');
$app->post('/updateProfile','updateProfile');
$app->post('/addMoreAddress','addMoreAddress');
$app->post('/sendmail','sendmail');
$app->post('/mycontact','mycontact');
$app->post('/getMycontact','getMycontact');
$app->post('/FBlogin','FBlogin');
$app->post('/getNearuser','getNearuser');
$app->get('/getUseraddress','getUseraddress');
$app->post('/getUserAddressByCity','getUserAddressByCity');
$app->post('/getUserAddressByCountry','getUserAddressByCountry');
$app->post('/outofhome','outofhome');
$app->post('/rangevalue','rangevalue');
$app->post('/recRangevalue', 'recRangevalue');
$app->post('/dnd','dnd');
$app->post('/alertmsg','alertmsg');
$app->post('/sendPush', 'sendPush');
$app->post('/ownerCancel', 'ownerCancel');
$app->post('/cancelAlert', 'cancelAlert');
$app->post('/pushNearuser', 'pushNearuser');
$app->post('/changeAlertType', 'changeAlertType');
$app->post('/cancelAlertType','cancelAlertType');
$app->post('/changeCount', 'changeCount');
$app->post('/Alertresponse','Alertresponse');
$app->post('/getAlertMsg', 'getAlertMsg');
$app->post('/updateCount', 'updateCount');
$app->post('/victimcount', 'victimcount');
$app->post('/addGuest', 'addGuest');
$app->post('/guestChangeAlertType', 'guestChangeAlertType');
$app->post('/getUserdata', 'getUserdata');
$app->post('/guestPushNearuser', 'guestPushNearuser');
$app->post('/guestAlertresponse', 'guestAlertresponse');
$app->post('/policeInform', 'policeInform');
$app->post('/getEventMsg', 'getEventMsg');
$app->post('/getGuestUser', 'getGuestUser');
// $app->post('/progressSame', 'progressSame');
// $app->post('/notSuspicious', 'notSuspicious');
// $app->post('/noChange', 'noChange');
$app->get('/getResetDate','getResetDate');
$app->post('/profileImageupload', 'profileImageupload');
$app->post('/updateUserImagename', 'updateUserImagename');
$app->post('/removeGuest', 'removeGuest');
$app->post('/mycontactUpdate', 'mycontactUpdate');
$app->post('/myContactDelete', 'mycontactDelete');
$app->post('/deleteAddress','deleteAddress');
$app->post('/checkUserResponse', 'checkUserResponse');
$app->post('/getUserDetail', 'getUserDetail');
$app->post('/push', 'push');

$app->post('/iospush', 'iospush');
$app->post('/camPushall', 'camPushall');
$app->post('/cancelpush', 'cancelpush');
$app->post('/gpushall', 'gpushall');
$app->post('/inactivepushall', 'inactivepushall');
$app->post('/sendallpushNearuser', 'sendallpushNearuser');
$app->post('/receiveNearuser','receiveNearuser');
$app->post('/sendSMS', 'sendSMS');
$app->post('/newThreat', 'newThreat');
$app->post('/getPaidUserAddress', 'getPaidUserAddress');
$app->post('/getLatLng','getLatLng');
$app->post('/removeAlert','removeAlert');
$app->post('/cameraAutoAlert','cameraAutoAlert');
$app->post('/updateCamToken','updateCamToken');
$app->post('/updatePrimaryAddress','updatePrimaryAddress');
$app->post('/updateCurrentAddress','updateCurrentAddress');
$app->post('/updateEditAddress','updateEditAddress');
//$app->post('/sendMail','sendMail');
$app->post('/testSES','testSES');
$app->post('/updateRDistance','updateRDistance');
/*Web API Functions */
$app->post('/getUserweb', 'getUserweb');
$app->post('/insertProduct', 'insertProduct');
$app->post('/getUserMapStatus', 'getUserMapStatus');
$app->get('/getCategory', 'getCategory');
$app->post('/deleteCategory', 'deleteCategory');
$app->post('/deleteProduct', 'deleteProduct');
$app->post('/insertCategory', 'insertCategory');
$app->post('/getProductList', 'getProductList');
$app->post('/getProduct', 'getProduct');
$app->get('/getOffers','getOffers');
$app->post('/insertCheckout', 'insertCheckout');
$app->post('/orderDetail', 'orderDetail');
$app->post('/setLang', 'setLang');
$app->get('/testmail','testmail');
$app->post('/sendAlertToSupporters','sendAlertToSupporters');
$app->post('/inviteFriend','inviteFriend');
$app->get('/sendEmailAlertTest','sendEmailAlertTest');
$app->post('/acceptReq','acceptReq');
$app->post('/acceptReject','acceptReject');
$app->run();


/*check the header orgin */
// if (isset($_SERVER['HTTP_ORIGIN'])) {
// 		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
// 		header('Access-Control-Allow-Credentials: true');
// 		header('Access-Control-Max-Age: 86400');	// cache for 1 day
// 	}
 
	// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");			

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers:		 {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

function login() {
	
    $postdata = file_get_contents("php://input");
	//print_r($postdata);exit;
	$request = json_decode($postdata);
	$username = $request->username;
	$password = $request->password;
	$deviceid = $request->deviceId;
	$platform = $request->platform;
// 	$platform = "android";
// 	$deviceid="901249089013urojweqrjsakjf";
	$posted_username = $username;
    $posted_password = $password;
    
    $user_name = htmlspecialchars($posted_username,ENT_QUOTES);
    
    $userinfo = get_user_by( 'email', $user_name );

	$is_paid;
    
    $hash = $userinfo->user_pass;
    $wp_hasher = new PasswordHash(8, TRUE);
    $check = $wp_hasher->CheckPassword($posted_password, $hash);
    if($check):
	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM wp_users as wu
		LEFT JOIN wp_pmpro_memberships_users as wpm ON
		wu.ID=wpm.user_id WHERE wu.user_email=:username1 AND wpm.status='active'");
		$stmt->bindParam("username1", $username);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$resData = $rows;
		if (count($rows) > 0) {
			foreach ($rows as $data) {
				$user_id=$data['ID'];
            	$membership_id = $data['membership_id'];
			}
			//print_r($data);exit;
			if($membership_id == 2) {
            	//echo 'free user';exit;
            	$is_paid=0;
            } else {
            	//echo 'paid user';exit;
            	$is_paid=1;
            }
			$stmt = $db->prepare("SELECT *
									FROM user u
									JOIN address a ON u.user_id=a.user_id
									WHERE u.user_id =:user_id AND a.address_type NOT IN('GB','GS') AND a.is_active=1 ORDER BY FIELD(a.primary_address, 'yes') DESC, a.primary_address");
			$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
			$stmt->execute();
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if(count($rows)>0) {
				$cur_time=date("Y-m-d H:i:s");
				$token = md5(microtime() . uniqid(). rand());
				$sql = "UPDATE user SET token =:token, log_in=now(), deviceID=:deviceid, platform=:platform, expires = ADDTIME(now(), '1000'), paid_member=:is_paid WHERE 		user_id=:user_id";
				$stmt = $db->prepare($sql);
				$stmt->bindParam("user_id", $user_id);
				$stmt->bindParam("deviceid", $deviceid);
				$stmt->bindParam("platform", $platform);
				$stmt->bindParam("token", $token);
            	$stmt->bindParam("is_paid", $is_paid);
				$status = $stmt->execute();
				if($status) {
					$usql = "INSERT INTO user_log (`user_id`,`log_in`) VALUES (:user_id,NOW())";
					$ustmt = $db->prepare($usql);
					$ustmt->bindParam("user_id", $user_id);
					$ustatus = $ustmt->execute();
					try {
    					$db = getDB();
    					$stmt = $db->prepare("SELECT u.user_id, a.address_id, u.alert_type,u.language
    								FROM user u
    								JOIN address a ON u.user_id=a.user_id
    								WHERE u.user_id =:user_id AND a.is_active=1 AND a.address_type NOT IN('GB', 'GS')");
    					$stmt->bindParam("user_id", $user_id);
    					$stmt->execute();
    					$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    					echo json_encode (array('status' => true, 'data' => $rows));
					} catch (PDOException $e) {
						echo '{"error":{"text":' . $e->getMessage() . '}}';
					}
				}
			} else {
				echo json_encode (array('status' => 'first','data' => $resData));
			}
		} else {
			echo json_encode (array('status' => false));
		}
	} catch (PDOException $e) {
			echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
	else:
	    echo json_encode (array('status' => false,'msg' => 'wp_check_failed'));
	endif;
}

function addFirstAddress() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	//print_r($request);
	//var_dump($request);exit;
	$user_id = $request->user_id;
	$street_address = $request->street_address;
	$zipcode = $request->zipcode;
	$city = $request->city;
	$country = $request->country;
	$is_active = 1;
	$longitude = $request->longitude;
	$latitude = $request->latitude;
	$primary_address = $request->primary_address;
	$status_id = 1;
	$marker = 'blue.svg';
	$paid_member = $request->paid_member;
	try {
	   $db = getDB();
	   $sql = "INSERT INTO address(`user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `longitude`, `latitude`,`primary_address`, `status_id`, `marker`) VALUES ( :user_id, :street_address,  :zipcode, :city, :country, :is_active, :longitude, :latitude, :primary_address, :status_id, :marker)";
	   $stmt = $db->prepare($sql);
	   $stmt->bindParam("user_id", $user_id);
	   $stmt->bindParam("street_address", $street_address);
	   $stmt->bindParam("zipcode", $zipcode);
	   $stmt->bindParam("city", $city);
	   $stmt->bindParam("country", $country);
	   $stmt->bindParam("is_active", $is_active);
	   $stmt->bindParam("longitude", $longitude);
	   $stmt->bindParam("latitude", $latitude);
	   $stmt->bindParam("primary_address",$primary_address);
	   $stmt->bindParam("status_id", $status_id);
	   $stmt->bindParam("marker", $marker);
	   $rs = $stmt->execute();
	   if($rs) {
			$cur_time=date("Y-m-d H:i:s");
			$token = md5(microtime() . uniqid(). rand());
			$is_active = 1;
			$alert_type="N";
			$do_not_distrub=0;
			$distance=5000;
			$r_distance=5000;
			$user_type_id=2;
			$language='dn';
			$sql = "INSERT INTO user (`user_id`,`is_active`,`alert_type`,`token`,`log_in`,`expires`,`paid_member`, `do_not_distrub`,`distance`,`r_distance`,`user_type_id`,`language`)
			VALUES (:user_id,:is_active,:alert_type,:token,NOW(),ADDTIME(now(), '1000'),:paid_member,:do_not_distrub,:distance,:r_distance,:user_type_id,:language)";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("user_id",$user_id);
			$stmt->bindParam("token", $token);
			$stmt->bindParam("paid_member", $paid_member);
			$stmt->bindParam("is_active", $is_active);
			$stmt->bindParam("alert_type", $alert_type);
			$stmt->bindParam("do_not_distrub", $do_not_distrub);
			$stmt->bindParam("distance", $distance);
			$stmt->bindParam("r_distance", $r_distance);
			$stmt->bindParam("user_type_id", $user_type_id);
			$stmt->bindParam("language", $language);
			$status = $stmt->execute();
			if($status) {
				$db = getDB();
				$stmt = $db->prepare("SELECT u.user_id, u.paid_member, a.address_id, u.alert_type,u.language
							FROM user u
							JOIN address a ON u.user_id=a.user_id
							WHERE u.user_id =:user_id AND a.is_active=1 AND a.address_type NOT IN('GB', 'GS')");
				$stmt->bindParam("user_id", $user_id);
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
				echo json_encode (array('status' => true, 'data' => $rows));
			} else {
				echo '{"status":false}';
			}
	   }
	} catch(Exception $e) {
		echo '{"status":false,"msg":'.$e->getMessage().'}';
	}
}
//for teting on web//
/*user login*/
function login_bk() {
	
	$postdata = file_get_contents("php://input");
	//print_r($postdata);exit;
	 if (isset($postdata)) {
		$request = json_decode($postdata);
		$username = $request->username;
		$password = $request->password;
		$deviceid = $request->deviceId;
		$platform = $request->platform;
		$user_type = $request->user_type_id;
		//echo $username.','.$password.','.$deviceid;exit;
		//if($deviceid == ""){ echo '{"status": "NDI"}';exit; } else 
	 try{	
			// $db = getDB();
			// $stmt = $db->prepare("UPDATE user SET deviceID=:deviceid WHERE email_id=:username1");
			// $stmt->bindValue(':username1', $username, PDO::PARAM_STR);
			// $stmt->bindValue(':deviceid', $deviceid, PDO::PARAM_STR);
			// $res=$stmt->execute();
			//echo "status:"$res;exit;
		
				 if ($username != "") {
			try {
				$db = getDB();
				$stmt = $db->prepare("SELECT * FROM user WHERE email_id=:username1 AND password=:password1 AND is_active=1");
				$stmt->bindParam("username1", $username);
				$stmt->bindParam("password1", $password);
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$_SESSION['userData'] = $rows;
					if (count($rows) > 0) {
						foreach ($rows as $data) {
							$user_id=$data['user_id'];
							$user_type = $data['user_type_id'];
						}
						$cur_time=date("Y-m-d H:i:s");
						$token = md5(microtime() . uniqid(). rand());
						
						// if($deviceid == '') {
							// $marker = 'blue.svg';
							// $sql = "UPDATE user,address SET user.token =:token, user.deviceID=:deviceid, user.platform=:platform, user.expires = ADDTIME(now(), '1000'), address.marker=:marker WHERE user.user_id=address.user_id AND user.user_id=:user_id";
							// $stmt = $db->prepare($sql);
							// $stmt->bindParam("user_id", $user_id);
							// $stmt->bindParam("deviceid", $deviceid);
							// $stmt->bindParam("platform", $platform);
							// $stmt->bindParam("marker", $marker);
							// $stmt->bindParam("token", $token);
							// $status = $stmt->execute();
						// }
						$sql = "UPDATE user SET token =:token, log_in=now(), deviceID=:deviceid, platform=:platform, expires = ADDTIME(now(), '1000') WHERE user_id=:user_id";
						$stmt = $db->prepare($sql);
						$stmt->bindParam("user_id", $user_id);
						$stmt->bindParam("deviceid", $deviceid);
						$stmt->bindParam("platform", $platform);
						$stmt->bindParam("token", $token);
						$status = $stmt->execute();
						if($status) {
							$usql = "INSERT INTO user_log (`user_id`,`log_in`) VALUES (:user_id,NOW())";
							$ustmt = $db->prepare($usql);
							$ustmt->bindParam("user_id", $user_id);
							$ustatus = $ustmt->execute();
						}
						if($user_type == 1) {
							try {
								$db = getDB();
								$stmt = $db->prepare("SELECT *
											FROM user
											WHERE user_id =:user_id AND is_active=1");
								$stmt->bindParam("user_id", $user_id);
								$stmt->execute();
								$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
								echo '{"status": true, "data" : '.json_encode($rows).'}';
							} catch (PDOException $e) {
								echo '{"error":{"text":' . $e->getMessage() . '}}';
							}
						} else {
							try {
							$db = getDB();
							$stmt = $db->prepare("SELECT u.user_id, a.address_id, u.alert_type
										FROM user u
										JOIN address a ON u.user_id=a.user_id
										WHERE u.user_id =:user_id AND a.is_active=1 AND a.address_type NOT IN('GB', 'GS')");
							$stmt->bindParam("user_id", $user_id);
							$stmt->execute();
							$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
							echo '{"status": true, "data" : '.json_encode($rows).'}';
							} catch (PDOException $e) {
								echo '{"error":{"text":' . $e->getMessage() . '}}';
							}
						}
					} else
						echo '{"status": "false"}';
				} catch (PDOException $e) {
						echo '{"error":{"text":' . $e->getMessage() . '}}';
				}
		}
		else {
			echo "Empty username parameter!";
		}
						
	 }catch (PDOException $e) {
						echo '{"error":{"text":' . $e->getMessage() . '}}';
				}
	   
	}
	else {
		echo "Not called properly with username parameter!";
	} 
}

function getAdminData() {
	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM admin_setting WHERE status=1");
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if (count($rows) > 0) {
				echo '{"status": true, "data" : '.json_encode($rows).'}';
			} else {
				echo '{"status": false}';
			}
	}catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

// function login() {
 
	// $postdata = file_get_contents("php://input");
	// //print_r($postdata);exit;
	 // if (isset($postdata)) {
		// $request = json_decode($postdata);
		// $username = $request->username;
		// $password = $request->password;
		// $deviceid = $request->deviceId;
		// $platform = $request->platform;
		// //echo $username.','.$password.','.$deviceid;exit;
			// // if($deviceid == ""){ echo '{"status": false}';exit; } else 
				// try {
					// $db = getDB();
					// $stmt = $db->prepare("SELECT * FROM user WHERE email_id=:username1 AND password=:password1");
					// $stmt->bindValue(':username1', $username, PDO::PARAM_STR);
					// $stmt->bindValue(':password1', $password, PDO::PARAM_STR);
					// $stmt->execute();
					// $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
					// if (count($rows) > 0) {
						// if($deviceid == ""){ echo '{"status": false}';exit; } else 
						// $db = getDB();
						// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						// $stmt = $db->prepare("UPDATE user SET deviceID=:deviceid, platform=:platform WHERE		email_id=:username1 AND is_active=1");
						// $stmt->bindValue(':username1', $username, PDO::PARAM_STR);
						// $stmt->bindValue(':deviceid', $deviceid, PDO::PARAM_STR);
						// $stmt->bindValue(':platform', $platform, PDO::PARAM_STR);
						// $stmt->execute();
						// $affRow = $stmt->rowCount();
						// //echo "status:".$affRow;exit;
						// if ($affRow>0){
							// try {
								// foreach ($rows as $data) {
									// $user_id=$data['user_id'];
								// }
								// $cur_time=date("Y-m-d H:i:s");
								// $token = md5(microtime() . uniqid(). rand());

								// $sql = "UPDATE user SET token = :token, expires = ADDTIME(now(), '1000') WHERE username=:username1 AND password=:password1";
								// $stmt = $db->prepare($sql);
								// $stmt->bindValue(':username1', $username, PDO::PARAM_STR);
								// $stmt->bindValue(':password1', $password, PDO::PARAM_STR);
								// $stmt->bindParam("token", $token);
								// $status = $stmt->execute();
								// $db = getDB();
								// $stmt = $db->prepare("SELECT *
											// FROM user u
											// JOIN address a ON u.user_id=a.user_id
											// WHERE u.user_id =:user_id AND a.is_active=1");
								// $stmt->bindParam("user_id", $user_id);
								// $stmt->execute();
								// $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
								// echo '{"status": true, "data" : '.json_encode($rows).'}';
								// } catch (PDOException $e) {
									// echo '{"error":{"text":' . $e->getMessage() . '}}';
								// }
						
							// } else {
								// echo '{"status": "deactive"}';
							// }
						// } else {
							// echo '{"status": "error"}';
						// }
					// } catch (PDOException $e) {
							// echo '{"error":{"text":' . $e->getMessage() . '}}';
					// }
			
	   
  // }
   // else {
	   // echo "Not called properly with username parameter!";
  // } 
// }	

/*Check the count of nearby nabouser*/	
function checkNearuser() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$distance = $request->distance/1000;
	$lat = $request->lat;
	$lang = $request->lang;
	try{
			$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
			// $sql="SELECT user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN(:user_id) AND address_type !='GB' AND  address_type !='GS' HAVING distance <:rang ORDER BY distance LIMIT 0 , 30";
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			//print_r($rows); exit;
			$devices = array();
			foreach($rows as $data) {
				//print_r($data); exit;
				$user_id = $data['user_id'];
				$sql = "SELECT user_id, deviceID from user WHERE user_id =:user_id AND do_not_distrub=0";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('user_id', $user_id);
				$smt->execute();
				$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
				foreach($rows as $value) {
					$user_id = $value['user_id'];
					$device = $value['deviceID'];
					$devic['user_id'] = $user_id;
					$devic['deviceID'] = $device;
					$devic['message'] = "Please Check my Home..";
				}
				 array_push($devices, $devic);
			}
				//$result = array_shift($devices);
				echo json_encode($devices);
			}
			 catch (Exception $e) {
   
	   $data = array('status' => 'False', 'msg' => $e->getMessage());
   }
}

/*clear the deviceID when user logout*/
function logout(){
	$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$user_id = $request->user_id;
		$deviceid="";
		$platform="";
	try {
		$db = getDB();
			$stmt = $db->prepare("UPDATE user SET deviceID=:deviceid, platform=:platform, log_out=now() WHERE user_id=:user_id");
			$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
			$stmt->bindValue(':deviceid', $deviceid, PDO::PARAM_STR);
			$stmt->bindValue(':platform', $platform, PDO::PARAM_STR);
			$res=$stmt->execute();
			if($res) {
				$lstmt = $db->prepare("UPDATE user_log SET log_out=now() WHERE user_id=:user_id ORDER BY id DESC LIMIT 1");
				$lstmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
				$lres=$lstmt->execute();
				echo '{"status":true}';
			}
	} catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

/*count number of users */
function userlist() {

	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM user");
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($rows);
	} catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

/*select product from database and display in shop page*/
function productlist() {

	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM product");
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($rows);
	} catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

/*user signup into application*/
function signup(){
	$postdata = file_get_contents("php://input");
	//echo $postdata;// exit();
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$username = $request->username;
		$password = $request->password;
		$email_id = $request->email_id;
		$url = "user.jpg";
		$act = 1;
		$uti = 2;
		$street_address = $request->street_address;
		$zipcode = $request->zipcode;
		$city = $request->city;
		$country = $request->country;
		$is_active = $request->is_active;
		$longitude = $request->longitude;
		$latitude = $request->latitude;
		$status_id = $request->status_id;
		$device_id = $request->deviceID;
		$alert_type = $request->alert_type;
		$distance = $request->distance;
		$platform = $request->platform;
		$marker = 'blue.svg';

		if ($username != "") {
			$data = checkUserExist($email_id); 
			if ($data['status'] == 'True') {
			   try {
					   $db = getDB();
					   $sql = "INSERT INTO user(`username`, `password`, `email_id`, `photo`, `is_active`, `alert_type`, `distance`, `user_type_id`, `deviceID`, `platform`) VALUES ( :username, :password,	:email_id, :photo, :is_active, :alert_type, :distance, :user_type_id, :deviceID, :platform)";
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("username", $username);
					   $stmt->bindParam("password", $password);
					   $stmt->bindParam("email_id", $email_id);
					   $stmt->bindParam("photo", $url);
					   $stmt->bindParam("is_active", $act);
					   $stmt->bindParam("alert_type", $alert_type);
					   $stmt->bindParam("user_type_id", $uti);
					   $stmt->bindParam("distance", $distance);
					   $stmt->bindParam("deviceID", $device_id);
					   $stmt->bindParam("platform", $platform);
					   $stmt->execute();
					   $sql = "SELECT user_id FROM user WHERE email_id = :email_id";
					   $stmt1 = $db->prepare($sql);
					   $stmt1->bindParam("email_id", $email_id);
					   $stmt1->execute();
					   $rows = $stmt1->fetchAll(PDO::FETCH_ASSOC);
						foreach ($rows as $data) {
							$user_id=$data['user_id'];
						}
							
							
							$sql = "INSERT INTO address(`user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `longitude`, `latitude`, `status_id`, `marker`) VALUES ( :user_id, :street_address,  :zipcode, :city, :country, :is_active, :longitude, :latitude, :status_id, :marker)";
							   $stmt = $db->prepare($sql);
							   $stmt->bindParam("user_id", $user_id);
							   $stmt->bindParam("street_address", $street_address);
							   $stmt->bindParam("zipcode", $zipcode);
							   $stmt->bindParam("city", $city);
							   $stmt->bindParam("country", $country);
							   $stmt->bindParam("is_active", $is_active);
							   $stmt->bindParam("longitude", $longitude);
							   $stmt->bindParam("latitude", $latitude);
							   $stmt->bindParam("status_id", $status_id);
							   $stmt->bindParam("marker", $marker);
							   $stmt->execute();
							   echo '{"status":true}'; 
							
					} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   }
			}else {
				echo '{"status":false}';
			}
		}else {
			echo "Empty username parameter!";
		}
	}
	else {
		echo "Not called properly with username parameter!";
	}
	
}

function insertlocation() {
	
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		//echo $request; exit();
		$user_id = $request->user_id;
		//echo $user_id; exit();
		$street_address = $request->street_address;
		$zipcode = $request->zipcode;
		$city = $request->city;
		$country = $request->country;
		$is_active = $request->is_active;
		$longitude = $request->longitude;
		$latitude = $request->latitude;
		$status_id = $request->status_id;
		
		
	   try {
			   $db = getDB();
			   $sql = "INSERT INTO address(`user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `longitude`, `latitude`, `status_id`) VALUES ( :user_id, :street_address,  :zipcode, :city, :country, :is_active, :longitude, :latitude, :status_id)";
			   $stmt = $db->prepare($sql);
			   $stmt->bindParam("user_id", $user_id);
			   $stmt->bindParam("street_address", $street_address);
			   $stmt->bindParam("zipcode", $zipcode);
			   $stmt->bindParam("city", $city);
			   $stmt->bindParam("country", $country);
			   $stmt->bindParam("is_active", $is_active);
			   $stmt->bindParam("longitude", $longitude);
			   $stmt->bindParam("latitude", $latitude);
			   $stmt->bindParam("status_id", $status_id);
			   $stmt->execute();
			   echo '{"status":true}';
		} catch(PDOException $e) {
			   echo '{"error":{"text":'. $e->getMessage() .'}}';
		   }
		
	}
	else {
		echo "Not called properly with locationdata!";
	}
}

/*check the user already registered in this application*/
function checkUserExist($email_id) {
   try {
	   $sql = "SELECT email_id FROM user WHERE email_id =:email_id";
	   $db = getDB();
	   $smt = $db->prepare($sql);
	   $smt->bindParam("email_id", $email_id);
	   $smt->execute();
	   $count = $smt->rowCount();
	   $db = null;
	   if ($count > 0) {
		$data = array('status' => 'False');
	   }else{
		$data = array('status' => 'True');
	   }
	  
   } catch (Exception $e) {
   
	   $data = array('status' => 'False', 'msg' => $e->getMessage());
   } finally {
	   //print_r($data);
	   return $data;
   }
}

/*update user's latitude and longitude when login*/
function updateUserLocation() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$lat = $request->lat;
	$lang = $request->lang;
	$sql = "UPDATE address SET latitude=:lat, longitude=:lang WHERE user_id=:user_id";

	try {
		$db = getDB();

		$stmt = $db->prepare($sql);

		$stmt->bindParam("lat", $lat);
		$stmt->bindParam("lang", $lang);
		$stmt->bindParam("user_id", $user_id);
		$status = $stmt->execute();
		$db = null;
		echo '{"status":' . $status . '}';
	} catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

/*admin insert product into database*/
function product($request) {
	$today = date("Y-m-d H:i:s");
	$product = $request->getParsedBody();
	$sql = "INSERT INTO `product`(`product_name`, `quantity`, `price`,`category`, `des`,  `created_at`,`status`) VALUES ( :product_name, :qty,	:price, :category, :des, :created_at, :status)";

	try {
		$db = getDB();

		$stmt = $db->prepare($sql);

		$stmt->bindParam("product_name", $product['pro_name']);
		$stmt->bindParam("price", $product['price']);
		$stmt->bindParam("qty", $product['qty']);
		$stmt->bindParam("category", $product['category']);
		$stmt->bindParam("des", $product['des']);
		$stmt->bindParam("status", $product['status']);
		$stmt->bindParam("created_at", $today);
		$status = $stmt->execute();
		$db = null;
		echo '{"status":' . $status . '}';
	} catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

/*get user details,address and alert status */
function getUser(){
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$user_id = $request->user_id;
		$address_id = $request->address_id;
		$alert_type = $request->alert_type;
			try {
				$db = getDB();
				$stmt = $db->prepare("SELECT *
										FROM user u
										JOIN address a ON u.user_id = a.user_id
										WHERE a.user_id =:user_id AND a.is_active=1 AND a.address_type NOT IN('GB', 'GS')");
				$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
					if (count($rows) > 0) {
						foreach ($rows as $value) {
							$address_id = $value['address_id'];
							$address_type = $value['address_type'];
							
						}
						
							$db = getDB();
							$stmt1 = $db->prepare("SELECT * FROM alert_log WHERE address_id=:address_id AND is_active=1 AND alert_status_type = 'BC' OR alert_status_type = 'SC'");
							$stmt1->bindValue(':address_id', $address_id, PDO::PARAM_STR);
							$stmt1->execute();
							$rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
							$countp = count($rows1);
							$push_success =$countp;
							
							$db = getDB();
							$stmt2 = $db->prepare("SELECT *	FROM alert_log WHERE address_id=:address_id AND is_active=1 AND saw_the_same = 'true'");
							$stmt2->bindValue(':address_id', $address_id, PDO::PARAM_STR);
							$stmt2->execute();
							$rows2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
							$counts = count($rows2);
							$saw_the_same = $counts;
							
							$db = getDB();
							$stmt3 = $db->prepare("SELECT *
													FROM alert_log WHERE address_id=:address_id AND is_active=1 AND saw_the_same = 'false'");
							$stmt3->bindValue(':address_id', $address_id, PDO::PARAM_STR);
							$stmt3->execute();
							$rows3 = $stmt3->fetchAll(PDO::FETCH_ASSOC);
							$countns = count($rows3);
							$not_suspicious = $countns;
							
							$rows[0]['push_success'] = $push_success;
							$rows[0]['saw_the_same'] = $saw_the_same;
							$rows[0]['not_suspicious'] = $not_suspicious;
							echo json_encode($rows);
					} else {
							$db = getDB();
							$stmt = $db->prepare("SELECT *
													FROM user u
													JOIN address a ON u.user_id=a.user_id
													WHERE u.user_id =:user_id AND a.is_active=1 AND a.address_type = ''");
							$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
							$stmt->execute();
							$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
							$rows[0]['push_success'] = 0;
							$rows[0]['saw_the_same'] = 0;
							$rows[0]['not_suspicious'] = 0;
							echo json_encode($rows);
					
					}
				} catch (PDOException $e) {
						echo '{"error":{"text":' . $e->getMessage() . '}}';
				}	
			
		   }
	else {
		echo "Not called properly with username parameter!";
	}
}

/*update user profile details when they edit*/
function updateProfile(){
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$user_id = (int) $request->user_id;
		$username = $request->user_login;
		$email_id = $request->user_email;
		$password = $request->user_pass;
    	//echo $user_id.'-'.$email_id.'-'.$password;exit;
	    try {
			$up = wp_update_user(array('ID' => $user_id,'user_email'=>$email_id,'user_pass' => $password));
        	if($up) {
            	echo '{"status":true}';
            } else {
            	echo '{"status":false}';
            }
		} catch(PDOException $e) {
		    echo '{"error":{"text":'. $e->getMessage() .'}}';
		}
		
	}
	else {
		echo "Not called properly with locationdata!";
	} 
}

/* add more than one address for paid users only */
function addMoreAddress_old() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	//print_r($request);
	var_dump($request);exit;
	$user_id = $request->user_id;
	$street_address = $request->street_address;
	$zipcode = $request->zipcode;
	$city = $request->city;
	$country = $request->country;
	$is_active = 1;
	$longitude = $request->longitude;
	$latitude = $request->latitude;
	$primary_address = $request->primary_address;
	$camtoken = $request->camtoken;
	$status_id = 1;
	$marker = 'blue.svg';
	try {
	   $db = getDB();
	   $sql = "INSERT INTO address(`user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `longitude`, `latitude`,`primary_address`, `camtoken`, `status_id`, `marker`) VALUES ( :user_id, :street_address,  :zipcode, :city, :country, :is_active, :longitude, :latitude, :primary_address, :camtoken, :status_id, :marker)";
	   $stmt = $db->prepare($sql);
	   $stmt->bindParam("user_id", $user_id);
	   $stmt->bindParam("street_address", $street_address);
	   $stmt->bindParam("zipcode", $zipcode);
	   $stmt->bindParam("city", $city);
	   $stmt->bindParam("country", $country);
	   $stmt->bindParam("is_active", $is_active);
	   $stmt->bindParam("longitude", $longitude);
	   $stmt->bindParam("primary_address",$primary_address);
	   $stmt->bindParam("latitude", $latitude);
	   $stmt->bindParam("camtoken", $camtoken);
	   $stmt->bindParam("status_id", $status_id);
	   $stmt->bindParam("marker", $marker);
	   $stmt->execute();
	   echo '{"status":true}';
	} catch(Exception $e) {
		echo '{"status":false,"msg":'.$msg->getMessage().'}';
	}
}

function addMoreAddress() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	//print_r($request);
	//var_dump($request);
	$user_id = $request->user_id;
	$street_address = $request->street_address;
	$zipcode = $request->zipcode;
	$city = $request->city;
	$country = $request->country;
	$is_active = 1;
	$longitude = $request->longitude;
	$latitude = $request->latitude;
	$primary_address = 'no';
	$status_id = 1;
	$marker = 'blue.svg';
	try {
	   $db = getDB();
	   $sql = "INSERT INTO address(`user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `longitude`, `latitude`,`primary_address`, `status_id`, `marker`) VALUES ( :user_id, :street_address,  :zipcode, :city, :country, :is_active, :longitude, :latitude, :primary_address, :status_id, :marker)";
	   $stmt = $db->prepare($sql);
	   $stmt->bindParam("user_id", $user_id);
	   $stmt->bindParam("street_address", $street_address);
	   $stmt->bindParam("zipcode", $zipcode);
	   $stmt->bindParam("city", $city);
	   $stmt->bindParam("country", $country);
	   $stmt->bindParam("is_active", $is_active);
	   $stmt->bindParam("longitude", $longitude);
	   $stmt->bindParam("latitude", $latitude);
	   $stmt->bindParam("primary_address",$primary_address);
	   $stmt->bindParam("status_id", $status_id);
	   $stmt->bindParam("marker", $marker);
	   $rs = $stmt->execute();
	   if($rs) {
	   		echo '{"status":true}';
	   }
	} catch(Exception $e) {
		echo '{"status":false,"msg":'.$msg->getMessage().'}';
	}
}

/*Validate user email and generate password reset link then send as email*/
function forgotpassword() {
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$email = $request->email;
		if ($email != "") {
			try {
				$db = getDB();
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) // Validate email address
				{
					$message =	"Invalid email address please type a valid email!!";
				} 
				$stmt = $db->prepare("SELECT * FROM wp_users WHERE user_email=:email");
				$stmt->bindValue(':email', $email, PDO::PARAM_STR);
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($rows as $data) {
					 $user_id=$data['ID'];
				}
					if(count($rows)>=1)
					{
						$encrypt = md5(microtime() . uniqid(). rand());
						$stmt = $db->prepare("UPDATE user SET pass_reset =:encrypt WHERE user_id=:user_id");
						$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
						$stmt->bindValue(':encrypt', $encrypt, PDO::PARAM_STR);
						$stmt->execute();
						$message = "Your password reset link send to your e-mail address.";
						$to=$email;
						$subject="Forgot Password";
						$from = 'info@rayi.in';
						$body='Hi, <br/> <br/>Your Membership ID is '.$user_id.' <br><br>Click here to reset your password http://nabosupport.dk/api/v1/reset/index.php?encrypt='.$encrypt.'&action=reset	  <br/> <br/>--<br>Rayi.in<br>Solve your problems.';
						$headers = "From: " . strip_tags($from) . "\r\n";
						$headers .= "Reply-To: ". strip_tags($from) . "\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						$state = mail($to,$subject,$body,$headers);
						if($state) {
							echo '{"status": true}';
						} else {
							echo '{"status": false}';
						}
					} else {
						echo '{"status": false, "msg":"Account not found please signup now"}';
					}
				} catch (PDOException $e) {
						echo '{"error":{"text":' . $e->getMessage() . '}}';
				}
		}
		else {
			echo "Empty username parameter!";
		}
	}
	else {
		echo "Not called properly with username parameter!";
	}
}

/*check the password reset link and update new password*/
function resetpassword(){
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$user_id = $request->user_id;
		$password = $request->password;
		$cpassword = $request->cpassword;
		if ($password != "" && $cpassword != "") {
			if($password == $cpassword){
					try {
					$db = getDB();
					$stmt = $db->prepare("UPDATE user SET password=:password WHERE user_id=:user_id");
					$stmt->bindValue(':password', $password, PDO::PARAM_STR);
					$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
					$stmt->execute();
					echo '{"status": true}';
					} catch (PDOException $e) {
							echo '{"error":{"text":' . $e->getMessage() . '}}';
					}
				} else {
					echo '{"msg": password mismatch}';
				}
			
		}
		else {
			echo "Empty username parameter!";
		}
	}
	else {
		echo "Not called properly with username parameter!";
	}
				   
}

/*add contact details to user's contact list*/
function mycontact(){
	$postdata = file_get_contents("php://input");
	if (isset($postdata)) {
	//print_r($postdata);
		$request = json_decode($postdata);
		$name = $request->name;
		$email = $request->mail;
		$num = $request->phone;
		$code = $request->code;
		$phone = $code.$num;
		$user_id = $request->user_id;
		$username = $request->username;
		$time = $request->time;
    	$status=0;
    
    	$db = getDB();
		$stmt = $db->prepare("SELECT * FROM wp_users WHERE user_email=:email");
		$stmt->bindValue(':email', $email, PDO::PARAM_STR);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (count($rows) > 0) {
        	$status=0;
         } else {
        	$status=1;
        }
		
	   try {
					   $db = getDB();
					   $sql = "INSERT INTO my_contact(`user_id`, `name`, `email_id`, `phone_number`,`status`) VALUES ( :user_id, :name, :email, :phone, :status)";
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("user_id", $user_id);
					   $stmt->bindParam("name", $name);
					   $stmt->bindParam("email", $email);
					   $stmt->bindParam("phone", $phone);
       				   $stmt->bindParam("status", $status);
					   $res = $stmt->execute();
					   if($res) {
                       	//echo $email.','.$name.','.$username.','.$user_id;
					   	sendMail($email,$name,$username,$user_id,$time);
					   }
					   echo '{"status":true}';
				} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   }


	}else{
	echo "false";
	}
}

/*get contact list from database*/
function getMycontact(){
		$postdata = file_get_contents("php://input");
	//echo $postdata;exit();
	if (isset($postdata)) {
		$request = json_decode($postdata);
		$user_id = $request->user_id;
		//echo "test";
		//echo $user_id;exit;
			try {
				$db = getDB();
				$stmt = $db->prepare("SELECT * FROM my_contact WHERE user_id=:userid");
				$stmt->bindValue(':userid', $user_id, PDO::PARAM_STR);
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
					if (count($rows) > 0) { 
						echo json_encode($rows);
					} else
						echo '{"status": "false"}';
				} catch (PDOException $e) {
						echo '{"error":{"text":' . $e->getMessage() . '}}';
				}
		   }
	else {
		echo "Not called properly with username parameter!";
	}
}

/*Login with facebook*/
function FBlogin(){
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	if (isset($request)) {
	
	if($request->name){
		$username = $request->name;
		}else{
		$username = $request->displayName;
		}
		$email_id = $request->email;
		$url = "user.jpg";
		$act = 1;
		$uti = 2;
		$street_address = $request->street_address;
		$zipcode = $request->zipcode;
		$city = $request->city;
		$country = $request->country;
		$is_active = $request->is_active;
		$longitude = $request->longitude;
		$latitude = $request->latitude;
		$status_id = $request->status_id;
		$alert_type = $request->alert_type;
		$deviceID = $request->deviceID;
		$distance = $request->distance;
		$platform = $request->platform;
		$marker = 'blue.svg';
		
		if ($email_id != "") {
			$data = checkUserExist($email_id); 
			
			if ($data['status'] == 'True') {
			   try {
					   $db = getDB();
					   $sql = "INSERT INTO user(`username`,`email_id`, `photo`, `is_active`, `distance`, `user_type_id`, `deviceID`, `alert_type`, `platform`) VALUES (:username, :email_id, :photo, :is_active, :distance, :user_type_id, :deviceID, :alert_type, :platform)";
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("username", $username);
					   $stmt->bindParam("email_id", $email_id);
					   $stmt->bindParam("photo", $url);
					   $stmt->bindParam("is_active", $act);
					   $stmt->bindParam("user_type_id", $uti);
					   $stmt->bindParam("distance", $distance);
					   $stmt->bindParam("deviceID", $deviceID);
					   $stmt->bindParam("alert_type", $alert_type);
					   $stmt->bindParam("platform", $platform);
					   $stmt->execute();
					   
					   $sql = "SELECT user_id FROM user WHERE email_id = :email_id";
					   $stmt1 = $db->prepare($sql);
					   $stmt1->bindParam("email_id", $email_id);
					   $stmt1->execute();
					   $rows = $stmt1->fetchAll(PDO::FETCH_ASSOC);
						foreach ($rows as $data) {
							$user_id=$data['user_id'];
						}
							
							
							$sql = "INSERT INTO address(`user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `longitude`, `latitude`, `status_id`, `marker`) VALUES ( :user_id, :street_address,  :zipcode, :city, :country, :is_active, :longitude, :latitude, :status_id, :marker)";
							   $stmt = $db->prepare($sql);
							   $stmt->bindParam("user_id", $user_id);
							   $stmt->bindParam("street_address", $street_address);
							   $stmt->bindParam("zipcode", $zipcode);
							   $stmt->bindParam("city", $city);
							   $stmt->bindParam("country", $country);
							   $stmt->bindParam("is_active", $is_active);
							   $stmt->bindParam("longitude", $longitude);
							   $stmt->bindParam("latitude", $latitude);
							   $stmt->bindParam("status_id", $status_id);
							   $stmt->bindParam("marker", $marker);
							   $stmt->execute();
							   echo '{"status": true,"user_type": "'.$uti.'","user_id": "'.$user_id.'","alert_type": "'.$alert_type.'"}';
							
						
					} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   }
			}else {
			
			 try{
			
			
			//echo "status:"$re
			if($deviceID != "") {
				$db = getDB();
				$stmt = $db->prepare("UPDATE user SET deviceID=:deviceid WHERE email_id=:email_id");
				$stmt->bindParam("email_id", $email_id);
				$stmt->bindParam("deviceid", $deviceID);
				$res=$stmt->execute();
			} else {
				$res==0;
			}
			if ($res==1)
			{
			   try {
				   $sql = "SELECT * FROM user WHERE email_id =:email_id";
				   $db = getDB();
				   $smt = $db->prepare($sql);
				   $smt->bindParam("email_id", $email_id);
				   $smt->execute();
				   $rows = $smt->fetchAll(PDO::FETCH_ASSOC);
				   foreach ($rows as $data) {
										$userid=$data['user_id'];
										$alert_type=$data['alert_type'];
									}

				echo '{"status": true,"user_type": "'.$uti.'","user_id": "'.$userid.'","alert_type": "'.$alert_type.'"}';
			   } catch (Exception $e) {
			   
				   $data = array('status' => 'False', 'msg' => $e->getMessage());
			   }
			}else{
				echo '{"status": false}';
			}
			}catch (Exception $e) {
   
	  
   }
			}
		}else {
			echo "Empty username parameter!";
		}
	}
	else {
		echo "Not called properly with username parameter!";
	}
	
}

/*get nabo user by the city */
function getUserAddressByCity() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	try {
		$sql = "SELECT city FROM address where user_id=:user_id AND address_type NOT IN('GB','GS')";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam("user_id", $user_id);
		$smt->execute();
		$rows = $smt->fetch();
		$city = $rows['city'];
		$sql1 = "SELECT user.user_id, user.deviceID, user.alert_type, username, street_address, status_type, zipcode, city, country, longitude, latitude, distance, address.address_id, address.address_type, address.status_type, address.marker FROM user LEFT JOIN address ON address.user_id = user.user_id WHERE address.is_active=1 AND address.city=:city";
	   $db = getDB();
	   $smt1 = $db->prepare($sql1);
	   $smt1->bindParam("city", $city);
	   $smt1->execute();
	   $rows1 = $smt1->fetchAll(PDO::FETCH_ASSOC);
	   $data=array();
	   foreach($rows1 as $row):
			/* get push count filter by address id, alert status type, active */
			$sql_push_count = 'select count(*) as push_count from alert_log where is_active=1 and address_id=\''.$row['address_id'].'\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
			$db = getDB();
		   $smt_push_count = $db->query($sql_push_count);
		   $push_count=$smt_push_count->fetchObject();
		  
		   /* get push count filter by address id, alert status type, active, saw_the_same=true */
		   $sql_true_count = 'select count(*) as true_count from alert_log where is_active=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'true\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
		   //echo $sql_true_count;
		   $db_true = getDB();
		   $smt_true_count = $db_true->query($sql_true_count);
		   $true_count=$smt_true_count->fetchObject();
			
		   /* get push count filter by address id, alert status type, active, saw_the_same=false */
		   $sql_false_count = 'select count(*) as false_count from alert_log where is_active=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'false\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
		   
		   $db_false = getDB();
		   $smt_false_count = $db_false->query($sql_false_count);
		   $false_count=$smt_false_count->fetchObject();
			$encode= array(
		   'push_success'=>$push_count->push_count,
		   'saw_the_same'=>$true_count->true_count,
		   'not_suspicious'=>$false_count->false_count,
		   'address_id'=>$row['address_id'],
		   'user_id' => $row['user_id'],
		   'deviceID'=>$row['deviceID'],
		   'alert_type' => $row['alert_type'],
		   'address_type'=>$row['address_type'],
		   'status_type' =>$row['status_type'],
		   'marker_icon' => $row['marker'],
		   'username' =>$row['username'],
		   'street_address' =>$row['street_address'],
		   'status_type' =>$row['status_type'],
		   'zipcode' =>$row['zipcode'],
		   'city' =>$row['city'],
		   'country' =>$row['country'],
		   'longitude' =>$row['longitude'],
		   'latitude' =>$row['latitude'],
		   'distance' =>$row['distance']
		   );
		   array_push($data,$encode);
		  
	   endforeach;
	   echo json_encode($data);
	} catch(Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
}

/*backup code not used may be used later*/
function getUserAddressByCountry_bk() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	try {
		$sql = "SELECT country FROM address where user_id=:user_id AND address_type NOT IN('GB','GS')";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam("user_id", $user_id);
		$smt->execute();
		$rows = $smt->fetch();
		$country = $rows['country'];
		$sql1 = "SELECT user.user_id, user.deviceID, user.alert_type, username, street_address, status_type, zipcode, city, country, longitude, latitude, distance, address.address_id, address.address_type, address.status_type, address.marker,address.last_alert,address.time FROM user LEFT JOIN address ON address.user_id = user.user_id WHERE address.is_active=1 AND address.country=:country";
	   $db = getDB();
	   $smt1 = $db->prepare($sql1);
	   $smt1->bindParam("country", $country);
	   $smt1->execute();
	   $rows1 = $smt1->fetchAll(PDO::FETCH_ASSOC);
	   //echo '<pre/>';
	   $data=array();
	   //print_r($rows);exit;
	   foreach($rows1 as $row):
			/* get push count filter by address id, alert status type, active */
			$sql_push_count = 'select count(*) as push_count from alert_log where log_state=1 and address_id=\''.$row['address_id'].'\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
			$db = getDB();
		   $smt_push_count = $db->query($sql_push_count);
		   $push_count=$smt_push_count->fetchObject();
		   //echo $push_count;
		  
		   /* get push count filter by address id, alert status type, active, saw_the_same=true */
		   $sql_true_count = 'select count(*) as true_count from alert_log where log_state=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'true\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
		   //echo $sql_true_count;
		   $db_true = getDB();
		   $smt_true_count = $db_true->query($sql_true_count);
		   $true_count=$smt_true_count->fetchObject();
			
		   /* get push count filter by address id, alert status type, active, saw_the_same=false */
		   $sql_false_count = 'select count(*) as false_count from alert_log where log_state=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'false\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
		   $db_false = getDB();
		   $smt_false_count = $db_false->query($sql_false_count);
		   $false_count=$smt_false_count->fetchObject();
		   /*get percentage */
		   $push  = $push_count->push_count;
		   $true = $true_count->true_count;
		   $false = $false_count->false_count;
		   
		   $sameperc = ($true/$push) * 100;
		   $notsusperc = ($false/$push) * 100;
		   //echo $sameperc;exit;
		    // print_r($push_count);
			// print_r($true_count);exit;
		   // $qur = "SELECT address.*, user.alert_type FROM address INNER JOIN user ON address.user_id = user.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id";
		   // $db = getDB();
		   // $s = $db->prepare($qur);
		   // $s->bindParam("user_id",$user_id);
		   // $s->bindParam("address_id", $row['address_id']);
		   // $s->execute();
		   // $r = $s->fetchAll(PDO::FETCH_ASSOC);
		   //print_r($r);exit;
		   if($row['alert_type'] == 'SP' && $row['status_type'] == 'S' || $row['alert_type'] == 'SP' && $row['status_type'] == 'SS' || $row['alert_type'] == 'SP' && $row['status_type'] == 'NS' || $row['alert_type'] == 'N' && $row['status_type'] == 'S' || $row['alert_type'] == 'N' && $row['status_type'] == 'SS' || $row['alert_type'] == 'N' && $row['status_type'] == 'NS') {
			   //echo 'alert in sp';exit;
			   if(65 < $sameperc) {
				   //echo 'progressSame';exit;
					progressSame($row['address_id'],$row['address_type']);
				} else if(50 < $notsusperc) {
					//echo 'notSuspicious';exit;
					notSuspicious($row['address_id'],$row['address_type']);
				} else if($sameperc > $notsusperc) {
					//echo 'progressSame';exit;
					progressSame($row['address_id'],$row['address_type']);
				} else if($sameperc < $notsusperc) {
					//echo 'notSuspicious';exit;
					notSuspicious($row['address_id'],$row['address_type']);
				} else if($sameperc == $notsusperc) {
					//echo 'noChange';exit;
					noChange($row['address_id'],$row['status_type']);
				}
		   }
		
			$encode= array(
		   'push_success'=>$push_count->push_count,
		   'saw_the_same'=>$true_count->true_count,
		   'not_suspicious'=>$false_count->false_count,
		   'address_id'=>$row['address_id'],
		   'user_id' => $row['user_id'],
		   'deviceID'=>$row['deviceID'],
		   'alert_type' => $row['alert_type'],
		   'address_type'=>$row['address_type'],
		   'status_type' =>$row['status_type'],
		   'marker_icon' => $row['marker'],
		   'username' =>$row['username'],
		   'street_address' =>$row['street_address'],
		   'status_type' =>$row['status_type'],
		   'zipcode' =>$row['zipcode'],
		   'city' =>$row['city'],
		   'country' =>$row['country'],
		   'longitude' =>$row['longitude'],
		   'latitude' =>$row['latitude'],
		   'distance' =>$row['distance'],
		   'message' => $row['last_alert'],
		   'time' => $row['time']
		   );
		   array_push($data,$encode);
		  
	   endforeach;
	   //print_r($data);exit;
	   echo json_encode($data);
	} catch(Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
}

/*get users by thier country*/
function getUserAddressByCountry() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	try {
	   $sql = "SELECT user.user_id, user.deviceID, user.alert_type, username, street_address, status_type, zipcode, city, country, longitude, latitude, distance, address.address_id, address.address_type, address.status_type, address.marker, address.last_alert,address.time FROM user LEFT JOIN address ON address.user_id = user.user_id WHERE address.is_active=1";
	   $db = getDB();
	   $smt = $db->prepare($sql);
	   $smt->execute();
	   $rows = $smt->fetchAll(PDO::FETCH_ASSOC);
	   $data=array();
	   foreach($rows as $row):
			/* get push count filter by address id, alert status type, active */
			$sql_push_count = 'select count(*) as push_count from alert_log where log_state=1 and address_id=\''.$row['address_id'].'\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
			$db = getDB();
		   $smt_push_count = $db->query($sql_push_count);
		   $push_count=$smt_push_count->fetchObject();
		   //echo $push_count;
		  
		   /* get push count filter by address id, alert status type, active, saw_the_same=true */
		   $sql_true_count = 'select count(*) as true_count from alert_log where log_state=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'true\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
		   //echo $sql_true_count;
		   $db_true = getDB();
		   $smt_true_count = $db_true->query($sql_true_count);
		   $true_count=$smt_true_count->fetchObject();
			
		   /* get push count filter by address id, alert status type, active, saw_the_same=false */
		   $sql_false_count = 'select count(*) as false_count from alert_log where log_state=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'false\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
		   $db_false = getDB();
		   $smt_false_count = $db_false->query($sql_false_count);
		   $false_count=$smt_false_count->fetchObject();
		   /*get percentage */
		   $push  = $push_count->push_count;
		   $true = $true_count->true_count;
		   $false = $false_count->false_count;
		   
				// if($sameperc != 0 && $notsusperc !=0) {
				// $sameperc = ($true/$push) * 100;
				// $notsusperc = ($false/$push) * 100;
				// } else {
				// $sameperc = 0;
				// $notsusperc = 0;
				// }
		   		$sameperc = ($true/$push) * 100;
		   		$notsusperc = ($false/$push) * 100;
		   if($row['alert_type'] == 'SP' && $row['status_type'] == 'S' || $row['alert_type'] == 'SP' && $row['status_type'] == 'SS' || $row['alert_type'] == 'SP' && $row['status_type'] == 'NS' || $row['alert_type'] == 'N' && $row['status_type'] == 'S' || $row['alert_type'] == 'N' && $row['status_type'] == 'SS' || $row['alert_type'] == 'N' && $row['status_type'] == 'NS') {
			   
			   if(65 < $sameperc) {
					progressSame($row['address_id'],$row['address_type']);
				} else if(50 < $notsusperc) {
					notSuspicious($row['address_id'],$row['address_type']);
				} else if($sameperc > $notsusperc) {
					progressSame($row['address_id'],$row['address_type']);
				} else if($sameperc < $notsusperc) {
					notSuspicious($row['address_id'],$row['address_type']);
				} else if($sameperc == $notsusperc) {
					noChange($row['address_id'],$row['status_type']);
				}
		   }
			//$new_datetime = DateTime::createFromFormat ( "Y-m-d H:i:s", $row['time'] );
			//$time = date("d/m/y, H:i:s", strtotime($row["time"]));
			$encode= array(
		   'push_success'=>$push_count->push_count,
		   'saw_the_same'=>$true_count->true_count,
		   'not_suspicious'=>$false_count->false_count,
		   'address_id'=>$row['address_id'],
		   'user_id' => $row['user_id'],
		   'deviceID'=>$row['deviceID'],
		   'alert_type' => $row['alert_type'],
		   'address_type'=>$row['address_type'],
		   'status_type' =>$row['status_type'],
		   'marker_icon' => $row['marker'],
		   'username' =>$row['username'],
		   'street_address' =>$row['street_address'],
		   'status_type' =>$row['status_type'],
		   'zipcode' =>$row['zipcode'],
		   'city' =>$row['city'],
		   'country' =>$row['country'],
		   'longitude' =>$row['longitude'],
		   'latitude' =>$row['latitude'],
		   'distance' =>$row['distance'],
		   'message' => $row['last_alert'],
		   'time' => $row["time"]
		   );
		   array_push($data,$encode);
		  
	   endforeach;
	   echo json_encode($data);
	} catch(Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
}

/*get users address details of nearby users*/
function getUseraddress(){
	
	  try {
		   $sql = "SELECT user.user_id, user.deviceID, user.alert_type, username, street_address, status_type, zipcode, city, country, longitude, latitude, distance, address.address_id, address.address_type, address.status_type, address.marker, address.last_alert,address.time FROM user LEFT JOIN address ON address.user_id = user.user_id WHERE address.is_active=1";
		   $db = getDB();
		   $smt = $db->prepare($sql);
		   
		   $smt->execute();
		   $rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		   $data=array();
		   foreach($rows as $row):
				/* get push count filter by address id, alert status type, active */
				$sql_push_count = 'select count(*) as push_count from alert_log where is_active=1 and address_id=\''.$row['address_id'].'\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
				$db = getDB();
			   $smt_push_count = $db->query($sql_push_count);
			   $push_count=$smt_push_count->fetchObject();
			  
			   /* get push count filter by address id, alert status type, active, saw_the_same=true */
			   $sql_true_count = 'select count(*) as true_count from alert_log where is_active=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'true\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
			   //echo $sql_true_count;
			   $db_true = getDB();
			   $smt_true_count = $db_true->query($sql_true_count);
			   $true_count=$smt_true_count->fetchObject();
				
			   /* get push count filter by address id, alert status type, active, saw_the_same=false */
			   $sql_false_count = 'select count(*) as false_count from alert_log where is_active=1 and address_id= \''.$row['address_id'].'\' and saw_the_same=\'false\' and (alert_status_type=\'BC\' or alert_status_type=\'SC\')';
			   
			   $db_false = getDB();
			   $smt_false_count = $db_false->query($sql_false_count);
			   $false_count=$smt_false_count->fetchObject();
			   
			   /*get percentage */
			   $sameperc = ($true_count/$push_count) * 100;
			   $notsusperc = ($false_count/$push_count) * 100;
			   
			   
				$encode= array(
			   'push_success'=>$push_count->push_count,
			   'saw_the_same'=>$true_count->true_count,
			   'not_suspicious'=>$false_count->false_count,
			   'address_id'=>$row['address_id'],
			   'user_id' => $row['user_id'],
			   'deviceID'=>$row['deviceID'],
			   'alert_type' => $row['alert_type'],
			   'address_type'=>$row['address_type'],
			   'status_type' =>$row['status_type'],
			   'marker_icon' => $row['marker'],
			   'username' =>$row['username'],
			   'street_address' =>$row['street_address'],
			   'status_type' =>$row['status_type'],
			   'zipcode' =>$row['zipcode'],
			   'city' =>$row['city'],
			   'country' =>$row['country'],
			   'longitude' =>$row['longitude'],
			   'latitude' =>$row['latitude'],
			   'distance' =>$row['distance'],
			   'message' => $row['last_alert'],
			   'time' => $row['time']
			   );
			   array_push($data,$encode);
			  
		   endforeach;
		   //print_r($data);
		   echo json_encode($data);
		   
	   } catch (Exception $e) {
	   
		   $data = array('status' => 'False', 'msg' => $e->getMessage());
	   }
	
}

/*set user's notification sending range in DB*/
function rangevalue(){
 $postdata = file_get_contents("php://input");
 $request = json_decode($postdata);
   
		$rng_value = $request->distance;
		$user_id = $request->user_id;
	try {
					   $db = getDB();
					   $sql = "UPDATE user SET distance=:rang WHERE user_id=:user_id";
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("user_id", $user_id);
					   $stmt->bindParam("rang", $rng_value);
					
					   $stmt->execute();
					  echo '{"status":true}';
					   
				} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   } 
				  
}

/*set user's notification receiving range in DB*/
function recRangevalue(){
	$postdata = file_get_contents("php://input");
	 $request = json_decode($postdata);
	   
			$rng_value = $request->r_distance;
			$user_id = $request->user_id;
		try {
						   $db = getDB();
						   $sql = "UPDATE user SET r_distance=:rang WHERE user_id=:user_id";
						   $stmt = $db->prepare($sql);
						   $stmt->bindParam("user_id", $user_id);
						   $stmt->bindParam("rang", $rng_value);
						
						   $stmt->execute();
						  echo '{"status":true}';
						   
					} catch(PDOException $e) {
						   echo '{"error":{"text":'. $e->getMessage() .'}}';
					   } 
				  
}

/*get nearby user latitude and longitude*/
function getNearuser(){
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
   
		$rng_value = $request->valuedata/1000;
		$user_id = $request->user_id;
		
		$lat=$request->latitude;
		$lng=$request->longitude;
		try{
			$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			foreach ($row as $data) {
				$address_id = $data['address_id'];
			}
			$db = getDB();
			$stmt1 = $db->prepare("SELECT * FROM alert_log WHERE address_id=:address_id						AND is_active=1");
			$stmt1->bindValue(':address_id', $address_id, PDO::PARAM_STR);
			$stmt1->execute();
			$rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
			$countp = count($rows1);

			$push_success =$countp;
						
			$db = getDB();
			$stmt2 = $db->prepare("SELECT *	FROM alert_log WHERE address_id=:address_id AND is_active=1 AND saw_the_same = 'true'");
			$stmt2->bindValue(':address_id', $address_id, PDO::PARAM_STR);
			$stmt2->execute();
			$rows2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
			$counts = count($rows2);
			$saw_the_same = $counts;
							
			$db = getDB();
			$stmt3 = $db->prepare("SELECT *
									FROM alert_log WHERE address_id=:address_id AND is_active=1 AND saw_the_same = 'false'");
			$stmt3->bindValue(':address_id', $address_id, PDO::PARAM_STR);
			$stmt3->execute();
			$rows3 = $stmt3->fetchAll(PDO::FETCH_ASSOC);
			$countns = count($rows3);
			$not_suspicious = $countns;
							
			$rows[0]['push_success'] = $push_success;
			$rows[0]['saw_the_same'] = $saw_the_same;
			$rows[0]['not_suspicious'] = $not_suspicious;
			
			echo json_encode($rows);
			}
			catch (Exception $e) {
   
				$data = array('status' => 'False', 'msg' => $e->getMessage());
			}
	
}

/*update user when he out of home*/
function outofhome(){
	$postdata = file_get_contents("php://input");
	  $request = json_decode($postdata);
		$ooh = $request->outofhome;
		$userid=$request->user_id;		
	   try {
					   $db = getDB();
					   $sql = "UPDATE user SET out_of_home=:ooh WHERE user_id=:user_id";
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("user_id", $userid);
					   $stmt->bindParam("ooh", $ooh);
					   $stmt->execute();
					   echo '{"status":true}';
				} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   }
}

/*ON or OFF Do Not Distrub mode*/
function dnd_bk(){
	$postdata = file_get_contents("php://input");
	  $request = json_decode($postdata);
	
		$dnd = $request->notify;
		$userid=$request->user_id;		
		if($dnd=="true"){
			$dnd_chg=1;
			}else{
			$dnd_chg=0;
			}
			
	   try {
					   $db = getDB();
					   $sql = "UPDATE user SET do_not_distrub=:dnd WHERE user_id=:user_id";
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("user_id", $userid);
					   $stmt->bindParam("dnd", $dnd_chg);
					
					   $stmt->execute();
					   echo '{"status":true}';
				} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   }
}

function dnd(){
	$postdata = file_get_contents("php://input");
	  $request = json_decode($postdata);
	
		$dnd = $request->notify;
		$userid=$request->user_id;
			
	   try {
					   $db = getDB();
					   $sql = "UPDATE user SET do_not_distrub=:dnd WHERE user_id=:user_id";
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("user_id", $userid);
					   $stmt->bindParam("dnd", $dnd);
					
					   $stmt->execute();
					   echo '{"status":true}';
				} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   }
}

/*update alert message in user table*/
 function alertmsg(){
		$postdata = file_get_contents("php://input");
	
		$request = json_decode($postdata);
	
		$user_id = $request->user_id;
		$alert_type = $request->alert_type;
		$sender_id = $request->sender_id;
		$address_id = $request->address_id;
		$message = $request->msg;
		$datetime = $request->datetime;
		$dnd_chg=0;
		if($alert_type == 'B') {
			$marker = 'red.svg';
		} else {
			$marker = 'yellow.svg';
		}
		 $db = getDB();
			$sql = "UPDATE user u, address a SET u.do_not_distrub=:dnd, u.alert_type =:alert_type, a.status_type=:alert_type, a.marker=:marker, a.last_alert=:message, a.time=:datetime WHERE u.user_id=:user_id AND a.address_id =:address_id";
			$stmt = $db->prepare($sql);
			$stmt->bindParam("user_id", $user_id);
			$stmt->bindParam("alert_type", $alert_type);
			$stmt->bindParam("address_id", $address_id);
			$stmt->bindParam("marker", $marker);
			$stmt->bindParam("dnd", $dnd_chg);
			$stmt->bindParam("message", $message);
			$stmt->bindParam("datetime", $datetime);
			$stmt->execute();
		
		
		try {
					   $db = getDB();
					   $sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`, `time`) VALUES (:sender_id, :receiver_id, :alert_type, :address_id, :message, :time)" ;
					   $stmt = $db->prepare($sql);
					   $stmt->bindParam("sender_id", $sender_id);
					   $stmt->bindParam("receiver_id", $user_id);
					   $stmt->bindParam("address_id", $address_id);
					   $stmt->bindParam("alert_type", $alert_type);
					   $stmt->bindParam("message", $message);
					   $stmt->bindParam("time", $datetime);
					   $stmt->execute();
					   $db = getDB();
					   echo '{"status":true}';
				} catch(PDOException $e) {
					   echo '{"error":{"text":'. $e->getMessage() .'}}';
				   }
} 
 
 /*test purpose*/  
function sendPush() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$deviceID = $request->deviceID;
	define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
	$msg = array
	(
		'message'	=> 'here is a message. message',
		'title'		=> 'This is a title. title',
		'subtitle'	=> 'This is a subtitle. subtitle',
		'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
		'vibrate'	=> 1,
		'sound'		=> 1,
		'largeIcon'	=> 'large_icon',
		'smallIcon'	=> 'small_icon',
		'priority'	=> 'high'
	);
	$fields = array
	(
		'registration_ids'	=> array($deviceID),
		'data'			=> array("message" => $msg)
	);
 
	$headers = array
	(
		'Authorization: key=' . API_ACCESS_KEY,
		'Content-Type: application/json'
	);
 
	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec($ch );
	curl_close( $ch );
	echo '{"status":true, "result":'.$result.'}';
}

/*cancel the alert*/
function cancelAlert() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$alert_type = $request->alert_type;
	$user_id = $request->user_id;
	
	try {
		$sql = "UPDATE user SET alert_type ='N' WHERE user_id =:user_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam("user_id", $user_id);
		$smt->execute();
		echo '{"status":true}';						
						
   } catch (Exception $e) {
   
	   echo '{"error":{"text":'. $e->getMessage() .'}}';
   }
	
}

/*get near nabo user when guest user alert*/
function pushNearuser(){
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
   
		$rng_value = 5;
		$user_id = $request->user_id;
		
		$lat=$request->latitude;
		$lng=$request->longitude;
		try{
// 			$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
    $sql="SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";
			
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			//print_r($rows);exit;
			$usercount = count($rows);
			$countarr = array();
			if($usercount > 0){
			    foreach($rows as $val){
			        $r_user_id = $val['user_id'];
			        $r_address_id = $val['address_id'];
			        $ssql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id";
			        $db = getDB();
        			$ssmt = $db->prepare($ssql);
        			$ssmt->bindParam(':user_id', $r_user_id);
        			$ssmt->bindParam(':address_id', $r_address_id);
        			$ssmt->execute();
        			$srows = $ssmt->fetchAll(PDO::FETCH_ASSOC);
        			//print_r($srows);exit;
        			foreach($srows as $val){
        			    $r_range = $val['r_distance']/1000;
        			    $r_lat  =$val['latitude'];
        			    $r_lng = $val['longitude'];
        			}
        			$sql = "SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";
                			$db = getDB();
                			$smt = $db->prepare($sql);
                			$smt->bindParam(':rang', $r_range);
                			$smt->bindParam(':lat', $r_lat);
                			$smt->bindParam(':lng', $r_lng);
                			$smt->bindParam(':user_id', $user_id);
                			$smt->execute();
                			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
        			        //print_r($rows);
        			        array_push($countarr,$rows);
			    }
			    $usercount = count($countarr);
			    $data = array('status' => 'true', 'count' => $usercount);
			} else {
			    $data = array('status' => 'false', 'count' => '0');
			}
			echo json_encode($data);
			}
			 catch (Exception $e) {
   
	   $data = array('status' => 'False', 'msg' => $e->getMessage());
   }
		
	
}

function getUserCount($user_id,$lat,$lng){
// 	$postdata = file_get_contents("php://input");
// 	$request = json_decode($postdata);
   
		$rng_value = 5;
// 		$user_id = $request->user_id;
		
// 		$lat=$request->latitude;
// 		$lng=$request->longitude;
		try{
// 			$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
    $sql="SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";
			
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			//print_r($rows);exit;
			$usercount = count($rows);
			$countarr = array();
			if($usercount > 0){
			    foreach($rows as $val){
			        $r_user_id = $val['user_id'];
			        $r_address_id = $val['address_id'];
			        $ssql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id";
			        $db = getDB();
        			$ssmt = $db->prepare($ssql);
        			$ssmt->bindParam(':user_id', $r_user_id);
        			$ssmt->bindParam(':address_id', $r_address_id);
        			$ssmt->execute();
        			$srows = $ssmt->fetchAll(PDO::FETCH_ASSOC);
        			//print_r($srows);exit;
        			foreach($srows as $val){
        			    $r_range = $val['r_distance']/1000;
        			    $r_lat  =$val['latitude'];
        			    $r_lng = $val['longitude'];
        			}
        			$sql = "SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";
                			$db = getDB();
                			$smt = $db->prepare($sql);
                			$smt->bindParam(':rang', $r_range);
                			$smt->bindParam(':lat', $r_lat);
                			$smt->bindParam(':lng', $r_lng);
                			$smt->bindParam(':user_id', $user_id);
                			$smt->execute();
                			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
        			        //print_r($rows);
        			        array_push($countarr,$rows);
			    }
			    $usercount = count($countarr);
			    //$data = array('status' => 'true', 'count' => $usercount);
			} else {
			    //$data = array('status' => 'false', 'count' => '0');
			}
			//echo json_encode($data);
			return $usercount;
			}
			 catch (Exception $e) {
   
	   $data = array('status' => 'False', 'msg' => $e->getMessage());
   }
		
	
}

/*get nearby user with thier range when send notification*/
function sendallpushNearuser(){
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
   
		$rng_value = $request->distance/1000;
		$user_id = $request->user_id;
		
		$lat=$request->latitude;
		$lng=$request->longitude;
		try{
			$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
			
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			
			$devices = array();
			foreach($rows as $data) {
				
				$r_sql = "SELECT sender_id FROM alert_log WHERE receiver_id=:user_id AND is_active=1 AND 
						  alert_status_type IN('B','S') ORDER BY time DESC LIMIT 1";
				$db = getDB();
				$stmt = $db->prepare($r_sql);
				$stmt->bindParam('user_id', $user_id);
				$stmt->execute();
				$result = $stmt->fetch();
				$r_user_id = $result['sender_id'];
				
				$user_id = $data['user_id'];
				$sql = "SELECT user_id, deviceID from user WHERE user_id =:user_id AND do_not_distrub=0 AND 
						user_id !=:r_user_id";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('user_id', $user_id);
				$smt->bindParam('r_user_id', $r_user_id);
				$smt->execute();
				$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
				foreach($rows as $value) {
					$user_id = $value['user_id'];
					$device = $value['deviceID'];
					$devic['user_id'] = $user_id;
					$devic['deviceID'] = $device;
					$devic['message'] = "Please Check my Home..";
				}
				 array_push($devices, $devic);
			}
				
				echo json_encode($devices);
			}
			 catch (Exception $e) {
   
	   $data = array('status' => 'False', 'msg' => $e->getMessage());
   }
		
	
}

function receiveNearuser(){
	//echo "testnear";
	$postdata = file_get_contents("php://input");
	//echo $postdata;// exit();
	$request = json_decode($postdata);
   
		$rng_value = $request->r_distance/1000;
		$user_id = $request->user_id;
		//$data=range($rng_value,$user_id);
		//if($data['status']=='TRUE'){
		
		$lat=$request->latitude;
		$lng=$request->longitude;
		//echo $user_id.','.$rng_value.','.$lat.','.$lng;exit;
		try{
			// $sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.distance as d1 from(SELECT  user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
			$sql="SELECT user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN(:user_id) AND address_type !='GB' AND  address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance";
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			//print_r($rows); exit;
			$devices = array();
			foreach($rows as $data) {
				//print_r($data); exit;
				$user_id = $data['user_id'];
				$sql = "SELECT user_id, deviceID from user WHERE user_id =:user_id AND do_not_distrub=0";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('user_id', $user_id);
				$smt->execute();
				$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
				foreach($rows as $value) {
					$user_id = $value['user_id'];
					$device = $value['deviceID'];
					$devic['user_id'] = $user_id;
					$devic['deviceID'] = $device;
					$devic['message'] = "Please Check my Home..";
				}
				 array_push($devices, $devic);
			}
				//$result = array_shift($devices);
				echo json_encode($devices);
			}
			 catch (Exception $e) {
   
	   $data = array('status' => 'False', 'msg' => $e->getMessage());
   }
		/* }else{
		echo "range not store";} */
	
}

/*owner cancel the alert process*/
function ownerCancel() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$address_id = $request->address_id;
	$alert_type = "N";
	$marker = 'blue.svg';
	$address_type = $request->address_type;
	try {
			$sql = "UPDATE user u, address a, alert_log al SET u.alert_type=:alert_type, a.status_type='', a.marker=:marker, al.is_active = 0
			WHERE u.user_id=:user_id AND a.address_id =:address_id AND al.receiver_id =:user_id AND a.address_type NOT IN ('GB','GS')";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
			$smt->bindParam('address_id', $address_id);
			$smt->bindParam('alert_type', $alert_type);
			$smt->bindParam('marker', $marker);
			$smt->execute();
			echo '{"status":true}';
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
}

/*get user response for particular alert*/
function Alertresponse() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
		$res=$request->res;
		$addid=$request->address_id;
		$receiver_id = $request->cur_user_id;
		$sender_id = $request->user_id;
		$alert_type = "N";
		$alert_status_type = "SC";
		$message = $request->message;
		try {
			$sql = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id AND address_id=:address_id AND log_state=1";
			$db = getDB();
			$smt1 = $db->prepare($sql);
			$smt1->bindParam('receiver_id', $receiver_id);
			$smt1->bindParam('address_id', $addid);
			$smt1->execute();
			$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
			if(count($rows)>0) {

				$sql1 = "UPDATE user u,alert_log al SET al.saw_the_same=:res, al.is_active=0, u.alert_type=:alert_type WHERE al.receiver_id=:receiver_id AND al.address_id=:address_id AND u.user_id=:receiver_id";
				$db = getDB();
				$smt = $db->prepare($sql1);
				$smt->bindParam('res', $res);
				$smt->bindParam('address_id', $addid);
				
				$smt->bindParam('receiver_id', $receiver_id);
				$smt->bindParam('alert_type', $alert_type);
				$smt->execute();
				echo '{"status":true}';
			} else {
				$is_active = 0;
				$sql1 = "INSERT INTO alert_log 				(`sender_id`,`receiver_id`,`alert_status_type`,`saw_the_same`,`address_id`,`is_active`,`message`) VALUES 	(:sender_id,:receiver_id,:alert_status_type,:saw_the_same,:address_id,:is_active,:message)";
				$db = getDB();
				$smt = $db->prepare($sql1);
				$smt->bindParam('saw_the_same', $res);
				$smt->bindParam('address_id', $addid);
				$smt->bindParam('sender_id', $sender_id);
				$smt->bindParam('receiver_id', $receiver_id);
				$smt->bindParam('alert_status_type', $alert_status_type);
				$smt->bindParam('is_active',$is_active);
				$smt->bindParam('message',$message);
				$smt->execute();
				echo '{"status":true}';
			}
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
	
}

function getAlertMsg() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		//print_r($request);exit;
		$user_id=$request->user_id;
		
		try {
			$sql = "SELECT * FROM alert_log WHERE receiver_id=:user_id	AND is_active=1 GROUP BY alert_log_id ORDER BY time DESC LIMIT 50";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			$wrap = array();
			if(count($rows)>0) {
			//print_r($rows);exit;
				foreach($rows as $row) {
					$address_id = $row['address_id'];
					$status = $row['alert_status_type'];
					$sender_id = $row['sender_id'];
                	$receiver_id = $row['receiver_id'];
                                
					if($status == 'ASR'):
                	$rsql = "SELECT user_email FROM wp_users WHERE ID=:receiver_id";
					$db = getDB();
					$smtr = $db->prepare($rsql);
					$smtr->bindParam('receiver_id',$receiver_id);
					$smtr->execute();
					$rdata = $smtr->fetch();
                	$email = $rdata['user_email'];
                
					$sql1 = "SELECT * FROM wp_users WHERE ID=:sender_id";
					$db = getDB();
					$smt1 = $db->prepare($sql1);
					$smt1->bindParam('sender_id',$sender_id);
					$smt1->execute();
					$data = $smt1->fetch(PDO::FETCH_ASSOC);
					$time = date("d/m/y, H:i:s", strtotime($row["time"]));
					$inarr = array(
                    	'alert_log_id' => $row['alert_log_id'],
						'sender_id' => $data['ID'],
						'user_name' => $data['user_login'],
						'email' => $email,
                    	'alert_status_type' => $row['alert_status_type'],
                    	'message' => $row['message'],
                    	'time'=>$row['time'],
					);
					else:
					$sql1 = "SELECT latitude,longitude,	street_address,city,country,zipcode FROM address WHERE address_id=:address_id";
					$db = getDB();
					$smt1 = $db->prepare($sql1);
					$smt1->bindParam('address_id',$address_id);
					$smt1->execute();
					$data = $smt1->fetch(PDO::FETCH_ASSOC);
					$time = date("d/m/y, H:i:s", strtotime($row["time"]));
					$inarr = array(
						'alert_log_id'=>$row['alert_log_id'],
						'sender_id'=>$row['sender_id'],
						'receiver_id'=>$row['receiver_id'],
						'alert_status_type'=>$row['alert_status_type'],
						'saw_the_same'=>$row['saw_the_same'],
						'address_id'=>$row['address_id'],
						'is_active'=>$row['is_active'],
						'log_state'=>$row['log_state'],
						'message'=>$row['message'],
						'time'=>$row['time'],
						'latitude'=>$data['latitude'],
						'longitude'=>$data['longitude'],
						'street_address'=>$data['street_address'],
						'city'=>$data['city'],
						'country'=>$data['country'],
						'zipcode'=>$data['zipcode']
					);
					endif;
					array_push($wrap,$inarr);
				}
				echo json_encode($wrap);
			} else {
				
				$rows[0]['message'] = "Currently you have no notification.";
				$rows[0]['state'] = "empty";
				echo json_encode($rows);
			}
			
		} catch (Exception $e) {
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
	
}

function getAlertMsg_bk() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		//print_r($request);exit;
		$user_id=$request->user_id;
		
		try {
			$sql = "SELECT * FROM alert_log WHERE receiver_id=:user_id	AND is_active=1 GROUP BY alert_log_id ORDER BY time DESC LIMIT 50";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			$wrap = array();
			if(count($rows)>0) {
			//print_r($rows);exit;
				foreach($rows as $row) {
					$address_id = $row['address_id'];
					$sql1 = "SELECT latitude,longitude,	street_address,city,country,zipcode FROM address WHERE address_id=:address_id";
					$db = getDB();
					$smt1 = $db->prepare($sql1);
					$smt1->bindParam('address_id',$address_id);
					$smt1->execute();
					$data = $smt1->fetch(PDO::FETCH_ASSOC);
					$time = date("d/m/y, H:i:s", strtotime($row["time"]));
					$inarr = array(
						'alert_log_id'=>$row['alert_log_id'],
						'sender_id'=>$row['sender_id'],
						'receiver_id'=>$row['receiver_id'],
						'alert_status_type'=>$row['alert_status_type'],
						'saw_the_same'=>$row['saw_the_same'],
						'address_id'=>$row['address_id'],
						'is_active'=>$row['is_active'],
						'log_state'=>$row['log_state'],
						'message'=>$row['message'],
						'time'=>$row['time'],
						'latitude'=>$data['latitude'],
						'longitude'=>$data['longitude'],
						'street_address'=>$data['street_address'],
						'city'=>$data['city'],
						'country'=>$data['country'],
						'zipcode'=>$data['zipcode']
					);
					array_push($wrap,$inarr);
				}
				echo json_encode($wrap);
			} else {
				
				$rows[0]['message'] = "Currently you have no notification.";
				$rows[0]['state'] = "empty";
				echo json_encode($rows);
			}
			
		} catch (Exception $e) {
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
	
}

function removeAlert() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$alert_log_id = $request->alert_log_id;
	
	try{
		$sql = "UPDATE alert_log SET is_active=0 WHERE alert_log_id=:alert_log_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('alert_log_id',$alert_log_id);
		$smt->execute();
		if($smt) {
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	} catch(Exception $e) {
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}


/*send automatic alert,notification and sms when camera sensing threat*/
function cameraAutoAlert() {
    $camtoken = $_POST['From'];
	$alert = $_POST['Body'];
	$camtoken=trim($camtoken);
	$sql_sel = "SELECT user_id,address_id FROM address WHERE camtoken=:camtoken AND is_active=1";
	$db = getDB();
	$smt = $db->prepare($sql_sel);
	$smt->bindParam('camtoken',$camtoken);
	$smt->execute(); 
	$rows = $smt->fetch();
	//print_r($rows);exit;
	$user_id = $rows['user_id'];
	$address_id = $rows['address_id'];
	$dnd_chg=0;
	date_default_timezone_set('Europe/Copenhagen');
    $datetime = date('m/d/Y h:i:s a', time());
	//echo  'Server time is : '.$datetime.'.';
    $alert_type = 'B';
    $marker = 'red.svg';
    $message = '';
	$lsql = "SELECT * FROM user WHERE user_id=:user_id";		
	$db = getDB();
	$lsmt = $db->prepare($lsql);
	$lsmt->bindParam('user_id', $user_id);
	$lsmt->execute();
	$lrows = $lsmt->fetch(PDO::FETCH_OBJ);
	//print_r($lrows);exit;
	$lang = $lrows->language;
	if($lang == 'en') {
    	$message = 'Your buglaryalarm has been set of.';
    } else {
    	$message = 'Din tyverialarm er blevet aktiveret.';
    }

		$sql = "UPDATE user u, address a SET u.do_not_distrub=:dnd, u.alert_type =:alert_type, a.status_type=:alert_type, a.marker=:marker, a.last_alert=:message, a.time=:datetime WHERE u.user_id=:user_id AND a.address_id =:address_id";
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("user_id", $user_id);
		$stmt->bindParam("alert_type", $alert_type);
		$stmt->bindParam("address_id", $address_id);
		$stmt->bindParam("marker", $marker);
		$stmt->bindParam("dnd", $dnd_chg);
		$stmt->bindParam("message", $message);
		$stmt->bindParam("datetime", $datetime);
		$stmt->execute();

	   $sender_id = "cam";
	   $sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`, `time`) VALUES (:sender_id, :receiver_id, :alert_type, :address_id, :message, :time)" ;
	   $db = getDB();
	   $stmt = $db->prepare($sql);
	   $stmt->bindParam("sender_id", $sender_id);
	   $stmt->bindParam("receiver_id", $user_id);
	   $stmt->bindParam("address_id", $address_id);
	   $stmt->bindParam("alert_type", $alert_type);
	   $stmt->bindParam("message", $message);
	   $stmt->bindParam("time", $datetime);
	   $stmt->execute();
	   if($stmt){
       	//echo $user_id.','.$address_id;exit;
	       camAutopush($user_id,$address_id,$message);
	   }
}

function camAutopush($user_id,$address_id,$message) {
    try {
			$sql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND user.do_not_distrub=0 AND address.address_id=:address_id";
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
			$smt->bindParam('address_id', $address_id);
			$smt->execute();
			$res = $smt->fetch(PDO::FETCH_ASSOC);
			$count = getUserCount($res['user_id'],$res['latitude'],$res['longitude']);
			$res['user_count'] = $count;
			//print_r($res);exit;
			$rows = (object) $res;
			} catch (Exception $e) {
				
				echo json_encode(array('status' => 'False', 'msg' => $e->getMessage()));
		}
		
		$deviceID = $rows->deviceID;
		$street = $rows->street_address;
		$city = $rows->city;
		$country = $rows->country;
		$zipcode = $rows->zipcode;
		$platform = $rows->platform;
		$lang = $rows->language;
		$title='';
        $text='';
                    if($lang == 'en') {
                        $title = 'Message from Nabosupport';                     
                    } else{
                        $title = 'Besked fra Nabosupport';
                    }
		$address = $street.', '.$city.', '.$country.', '.$zipcode;
		
		define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
		
		if($platform == 'Android') {
			$msg = array
				(
					'alert'		=> $address,
					'message'	=> $message,
					'title'		=> $title,
					'subtitle'	=> $address,
					'sData'	=> json_encode($rows),
					'tickerText'	=> $message,
					'vibrate'	=> 1,
					'sound'		=> 1,
					'content_available' => 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon',
					'priority'	=> 'high'
					
				);
				$fields = array
				(
					'registration_ids'	=> array($deviceID),
					'data'			=> array("message" => $msg)
				);

				$headers = array
				(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);

				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				echo '{"status":true,"result":'.$result.'}';
				
		} else {
				$ch = curl_init("https://fcm.googleapis.com/fcm/send");
				
				//The device token.
				$token = $deviceID; //token here
				
				//Title of the Notification.

				//Body of the Notification.
				$body = $address;
				
				//Creating the notification array.
				$notification = array('title' =>$title , 'text' => $body,'tickerText'=> $message,
				    'subtitle'=> $address,
                    'address_id'=>$rows->address_id,
				    'latitude' => $rows->latitude,
				    'longitude' => $rows->longitude,
					'user_count' => $rows->user_count);

				//This array contains, the token and the notification. The 'to' attribute stores the token.
				$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

				//Generating JSON encoded string form the above array.
				$json = json_encode($arrayToSend);
				//Setup headers:
				$headers = array();
				$headers[] = 'Content-Type: application/json';
				$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

				//Setup curl, add headers and post parameters.
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				$result = curl_exec($ch );
				curl_close( $ch );
				echo '{"status":true,"result":'.$result.'}';
		}
}

function cameraAutoAlert_old() {
	// $postdata = file_get_contents("php://input");
	// $request = json_decode($postdata);
	// print_r($postdata);exit;
	// $camtoken = $request->camid;
	// $alert = $request->alert;
	/* $camtoken = $_POST['camid'];
	$alert = $_POST['alert']; */
	$camtoken = $_POST['From'];
	$alert = $_POST['Body'];
	$camtoken=trim($camtoken);
	/* if ($camtoken==99559364 || $camtoken=='+4599559364' || $camtoken==4599559364){
		//echo $alert;
		//preg_match_all('!\d!', $alert, $matches);
		$int = intval(preg_replace('/[^0-9]+/', '', $alert), 10);
		$camtoken=substr($int, 0, 8);
		//print_r($matches);
		//echo $camtoken;
		//die();
		} */
	//echo $camtoken.'-'.$alert;exit;
	
	
	
	 
	
	$sql_sel = "SELECT user_id,address_id FROM address WHERE camtoken=:camtoken";
	$db = getDB();
	$smt = $db->prepare($sql_sel);
	$smt->bindParam('camtoken',$camtoken);
	$smt->execute(); 
	$rows = $smt->fetch();
	//print_r($rows);exit;
	$user_id = $rows['user_id'];
	$address_id = $rows['address_id'];
	$det= $address_id.' - '.$user_id.' - '.$alert.' - '.$sql_sel;
	
	
	
	/**** To test the data recieved from Twilio ****/
	try {
		   $db = getDB();
		   $sql = "INSERT INTO alert_log (`address_id`, `message`) VALUES ( :address_id, :message)" ;
		   $stmt = $db->prepare($sql);
		   $stmt->bindParam("address_id", $camtoken);
		   $stmt->bindParam("message", $det);
		   $stmt->execute();
		   $db = getDB();
		   //return true;
		   echo 'inserted';
	} catch(PDOException $e) {
		   echo '{"error":{"text":'. $e->getMessage() .'}}';
		   return false;
	   }
	/**** To test the data recieved from Twilio ****/
	
	
	
	
	
	
	
	$updatestate = updateAlertStatus($user_id,$address_id,$alert);
	if($updatestate == true) {
		echo '{"status":true}';
		ownerPush($user_id,$address_id);
		$sql1 = "SELECT user.*, address.* FROM user JOIN address ON user.user_id = address.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id";
		$db = getDB();
		$smt1 = $db->prepare($sql1);
		$smt1->bindParam('user_id',$user_id);
		$smt1->bindParam('address_id',$address_id);
		$smt1->execute();
		$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
		//print_r($rows);
		$pushstate = sendAutoPush($rows[0],$alert);
		if($pushstate == true) {
			sendAutoSMS($rows[0]);
		}
	}
}

/*updater alert status type in DB*/
function updateAlertStatus_old($user_id,$address_id,$message){
	$alert_type = 'B';
	$dnd_chg=0;
	if($alert_type == 'B') {
		$marker = 'red.svg';
	} else {
		$marker = 'yellow.svg';
	}
	 $db = getDB();
	 $sql = "UPDATE user u, address a SET u.do_not_distrub=:dnd, u.alert_type =:alert_type, a.status_type=:alert_type, a.marker=:marker, a.last_alert=:message WHERE u.user_id=:user_id AND a.address_id =:address_id";
		$stmt = $db->prepare($sql);
		$stmt->bindParam("user_id", $user_id);
		$stmt->bindParam("alert_type", $alert_type);
		$stmt->bindParam("address_id", $address_id);
		$stmt->bindParam("marker", $marker);
		$stmt->bindParam("dnd", $dnd_chg);
		$stmt->bindParam("message", $message);
		$stmt->execute();
	
	
	try {
		   $db = getDB();
		   $sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`) VALUES (:sender_id, :receiver_id, :alert_type, :address_id, :message)" ;
		   $stmt = $db->prepare($sql);
		   $stmt->bindParam("sender_id", $sender_id);
		   $stmt->bindParam("receiver_id", $user_id);
		   $stmt->bindParam("address_id", $address_id);
		   $stmt->bindParam("alert_type", $alert_type);
		   $stmt->bindParam("message", $message);
		   $stmt->execute();
		   $db = getDB();
		   return true;
	} catch(PDOException $e) {
		   echo '{"error":{"text":'. $e->getMessage() .'}}';
		   return false;
	   }
}

/*send auto push notification*/
function sendAutoPush($data,$message){
	
	$rng_value = $data['distance']/1000;
	$user_id = $data['user_id'];
	$lat=$data['latitude'];
	$lng=$data['longitude'];
	$sender_id = $data['user_id'];
	$alert_type = 'B';
	$sender_id = $data['user_id'];
	
	$address_id = $data['address_id'];
	$street = $data['street_address'];
	$city = $data['city'];
	$country = $data['country'];
	$zipcode = $data['zipcode'];
	$address = $street.', '.$city.', '.$country.', '.$zipcode;
	
	if($alert_type == "B") {
		$owner_alert = "BP";
		$alert ="BC";
	} else if($alert_type == "S"){
		$owner_alert = "SP";
		$alert = "SC";
	}
	try{
		$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
		
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam(':rang', $rng_value);
		$smt->bindParam(':lat', $lat);
		$smt->bindParam(':lng', $lng);
		$smt->bindParam(':user_id', $user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($rows as $value) {
				
				
				$nuser_id = $value['user_id'];
				$sql = "SELECT user_id, deviceID, platform,language from user WHERE user_id =:nuser_id AND do_not_distrub=0 AND is_active=1";
				$db = getDB();
				$stmt = $db->prepare($sql);
				$stmt->bindParam('nuser_id', $nuser_id);
				$stmt->execute();
				$divIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach($divIds as $divData) {
					$platform = $divData['platform'];
					$deviceID = $divData['deviceID'];
                	$lang = $divData['language'];
					$title='';
        			$text='';
                    if($lang == 'en') {
                        $title = 'Message from Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'A alarm has been registered at this address.';
                                break;
                            case "S":
                                $text = 'Suspicious behavior has been registered at this address.';
                                break;
                           default:
                                $text = 'A alarm has been registered at this address.';
                        }
                    } else{
                        $title = 'Besked fra Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'En alarm er blevet registreret på denne adresse.';
                                break;
                            case "S":
                                $text = 'Mistænkelig adfærd er registreret på denne adresse.';
                                break;
                           default:
                                $text = 'En alarm er blevet registreret på denne adresse.';
                        }
                    }
					define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
					
					if($platform == 'Android') {
						$msg = array
						(
							'alert'		=> $address,
							'message'	=> $text,
							'title'		=> $title,
							'subtitle'	=> $address,
							'vData'	=> json_encode($request),
							'tickerText'	=> $text,
							'vibrate'	=> 1,
							'sound'		=> 1,
							'largeIcon'	=> 'large_icon',
							'smallIcon'	=> 'small_icon',
							'priority'	=> 'high'
						);
						$fields = array
						(
							'registration_ids'	=> array($deviceID),
							'data'			=> array("message" => $msg)
						);
					 
						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);
					 
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$push_result = curl_exec($ch );
						
						curl_close( $ch );
					} else {
						$ch = curl_init("https://fcm.googleapis.com/fcm/send");
						
						$token = $deviceID; //token here
						
						$body = $address;
						
						$notification = array('title' =>$title , 'subtitle'=> $address,'tickerText'=> $text,'vData'=> json_encode($request),'subtitle'=> $address);

						//This array contains, the token and the notification. The 'to' attribute stores the token.
						$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

						//Generating JSON encoded string form the above array.
						$json = json_encode($arrayToSend);
						//Setup headers:
						$headers = array();
						$headers[] = 'Content-Type: application/json';
						$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

						//Setup curl, add headers and post parameters.
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
						curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
						curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						$push_result = curl_exec($ch );
						
						curl_close( $ch );
					}
					
					
					try {	
						$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`) VALUES
													   (:sender_id, :receiver_id, :alert_type, :address_id, :message)";
						$db = getDB();
						$smt = $db->prepare($sql);
						$smt->bindParam('sender_id', $user_id);
						$smt->bindParam('receiver_id', $nuser_id);
						$smt->bindParam('alert_type', $alert);
						$smt->bindParam('address_id', $address_id);
						$smt->bindParam('message', $message);
						$smt->execute();
						//echo '{"step": neighbour alert Change}';
						$sql1 = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id";
						$smt1 = $db->prepare($sql1);
						$smt1->bindParam('receiver_id', $nuser_id);
						$smt1->execute();
						$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
						foreach($rows as $value) {
							$address_id = $value['address_id'];
							$receiver_id = $value['receiver_id'];
							
							$sql = "UPDATE user u, address a SET u.alert_type=:alert WHERE a.address_id=
									:address_id AND u.user_id =:receiver_id AND u.alert_type NOT IN('B') AND u.alert_type NOT IN('BP')
									AND u.alert_type NOT IN('S') AND u.alert_type NOT IN('SP')";
							$db = getDB();
							$smt = $db->prepare($sql);
							$smt->bindParam('address_id', $address_id);
							$smt->bindParam('receiver_id', $receiver_id);
							$smt->bindParam('alert', $alert);
							$smt->execute();
						}
	
					}catch(Exception $e) {
						
						echo '{"status":false, "msg":'.$e->getMessage().'}';
					}
					
				 }
		}
		$sql = "UPDATE user SET alert_type=:owner_alert WHERE user_id=:user_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('owner_alert', $owner_alert);
		$smt->bindParam('user_id', $user_id);
		$smt->execute();
		
		return true;
	} catch (Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
		return false;
	}
}

/*send automatic SMS*/
function sendAutoSMS($data) {
	$user_id = $data['user_id'];
	$street = $data['street_address'];
	$city = $data['city'];
	$country = $data['country'];
	$zipcode = $data['zipcode'];
	$message = $data['last_alert'];
	$address = $street.', '.$city.', '.$country.', '.$zipcode;
	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM my_contact WHERE user_id=:userid");
		$stmt->bindValue(':userid', $user_id, PDO::PARAM_STR);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (count($rows) > 0) { 
			foreach($rows as $input) {
				$phone_number = $input['phone_number'];
				$account_sid = 'AC5be360e4ab74cfb3c88593be2ec958bf';
				$auth_token = 'ddbd806bbe22542e8f2004f712dbe4ce';
				// In production, these should be environment variables. E.g.:
				// $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

				// A Twilio number you own with SMS capabilities
				$twilio_number = "+18058566760";
						$message=strip_tags($message);
				$client = new Client($account_sid, $auth_token);
				$client->messages->create(
				// Where to send a text message (your cell phone?)
				$phone_number,
				array(
					'from' => $twilio_number,
					'body' => 'This is Nabo support test Message.<br>'.$message.'<br>'.$address.'.'
				)
				);
			}
			echo '{"status": true}';
		} else {
			echo '{"status": true}';
		}
	} catch (PDOException $e) {
			echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

/*send notification from neighbour to owner*/
function ownerPush($user_id,$address_id) {
	try {
		
			$sql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND 
			address.address_id=:address_id AND address.address_type NOT IN('GB','GS')";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
			$smt->bindParam('address_id', $address_id);
			$smt->execute();
			$rows = $smt->fetch(PDO::FETCH_OBJ);
				
			} catch (Exception $e) {
				
				echo json_encode(array('status' => 'False', 'msg' => $e->getMessage()));
		}
		
		$deviceID = $rows->deviceID;
		$street = $rows->street_address;
		$city = $rows->city;
		$country = $rows->country;
		$zipcode = $rows->zipcode;
		$platform = $rows->platform;
		$title='';
		$text='';
		if($lang == 'en') {
          $title = 'Message from Nabosupport';
          switch ($alert_type) {
                  case "B":
                       $text = 'A alarm has been registered at this address.';
                       break;
                  case "S":
                       $text = 'Suspicious behavior has been registered at this address.';
                       break;
                  default:
                       $text = 'A alarm has been registered at this address.';
          }
       } else{
          $title = 'Besked fra Nabosupport';
          switch ($alert_type) {
                 case "B":
                       $text = 'En alarm er blevet registreret på denne adresse.';
                       break;
                 case "S":
                       $text = 'Mistænkelig adfærd er registreret på denne adresse.';
                       break;
                 default:
                       $text = 'En alarm er blevet registreret på denne adresse.';
          }
        }
		$address = $street.', '.$city.', '.$country.', '.$zipcode;
		
		define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
		
		// prep the bundle
		if($platform == 'Android') {
			$msg = array
				(
					'alert'		=> $address,
					'message'	=> $text,
					'title'		=> $title,
					'subtitle'	=> $address,
					'sData'	=> json_encode($request),
					'tickerText'	=> $text,
					'vibrate'	=> 1,
					'sound'		=> 1,
					'content_available' => 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon',
					'priority'	=> 'high'
					
				);
				$fields = array
				(
					'registration_ids'	=> array($deviceID),
					'data'			=> array("message" => $msg)
				);

				$headers = array
				(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);

				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				
		} else {
				$ch = curl_init("https://fcm.googleapis.com/fcm/send");
				
				//The device token.
				$token = $deviceID; //token here
				
				//Title of the Notification.

				//Body of the Notification.
				$body = $address;
				
				//Creating the notification array.
				$notification = array('title' =>$title , 'text' => $body,'tickerText'=> $text,'sData'=> json_encode($request),'subtitle'=> $address);

				//This array contains, the token and the notification. The 'to' attribute stores the token.
				$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

				//Generating JSON encoded string form the above array.
				$json = json_encode($arrayToSend);
				//Setup headers:
				$headers = array();
				$headers[] = 'Content-Type: application/json';
				$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

				//Setup curl, add headers and post parameters.
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				$result = curl_exec($ch );
				curl_close( $ch );
				
		}
}

/*Update alert response in DB*/
function updateCount() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		//print_r($request);exit;
		$user_id=$request->user_id;
		$count = $request->pushcount;
		
		try {
			$sql = "UPDATE alert_log SET push_success=:count WHERE user_id=:user_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
			$smt->bindParam('count', $count);
			$smt->execute();
			echo '{"status":true}';
		} catch (Exception $e) {
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
}

/*add guest address in DB*/
function addGuest() {
	$postdata = file_get_contents("php://input");
	
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$street_address = $request->street_address;
	$zipcode = $request->zipcode;
	$city = $request->city;
	$country = $request->country;
	$is_active = $request->is_active;
	$longitude = $request->longitude;
	$latitude = $request->latitude;
	$status_id = 1;
	$user_type = $request->user_type;
	$status_type = $request->alert_type;
	$message = $request->msg;
	$datetime = $request->datetime;
	if($status_type == 'B') {
		$marker = 'red.svg';
	} else {
		$marker = 'yellow.svg';
	}

	try {
		$sql = "INSERT INTO address(`user_id`, `street_address`, `zipcode`, `city`, `country`, `is_active`, `longitude`, `latitude`, `status_id`, `status_type`, `marker`, `address_type`, `last_alert`, `time`) VALUES ( :user_id, :street_address, :zipcode, :city, :country, :is_active, :longitude, :latitude, :status_id, :status_type, :marker, :user_type, :message, :datetime)";
			$db = getDB();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("user_id", $user_id);
			$stmt->bindParam("street_address", $street_address);
			$stmt->bindParam("zipcode", $zipcode);
			$stmt->bindParam("city", $city);
			$stmt->bindParam("country", $country);
			$stmt->bindParam("is_active", $is_active);
			$stmt->bindParam("longitude", $longitude);
			$stmt->bindParam("latitude", $latitude);
			$stmt->bindParam("status_id", $status_id);
			$stmt->bindParam("status_type", $status_type);
			$stmt->bindParam("marker", $marker);
			$stmt->bindParam("user_type", $user_type);
			$stmt->bindParam("message", $message);
			$stmt->bindParam("datetime", $datetime);
			$stmt->execute();
			$address_id = $db->lastInsertId();
			$sql1 = "SELECT * FROM address WHERE address_id=:address_id AND is_active=1";
			$db = getDB();
			$stmt1 = $db->prepare($sql1);
			$stmt1->bindParam("address_id", $address_id);
			$stmt1->execute();
			$rows = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    		//print_r($rows);exit;
			echo '{"status":true, "data":'.json_encode($rows).'}';
	} catch (Exception $e) { 
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}

}

/*get user data from DB*/
function getUserdata(){
	$postdata = file_get_contents("php://input");
	  if (isset($postdata)) {
		$request = json_decode($postdata);
		$user_id = $request->user_id;
		try {
				$db = getDB();
				$stmt = $db->prepare("SELECT *
										FROM wp_users wp
										LEFT JOIN user u ON u.user_id = wp.ID
										LEFT JOIN address a ON a.user_id = wp.ID
										WHERE u.user_id =:user_id AND a.address_type NOT IN('GB','GS') AND a.is_active=1 ORDER BY FIELD(a.primary_address, 'yes') DESC, a.primary_address");
				$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
						echo json_encode($rows);
					
				} catch (PDOException $e) {
						echo '{"error":{"text":' . $e->getMessage() . '}}';
				
				}
	  }
}

function getHome() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM address WHERE user_id=:user_id AND primary_address = 'yes'");
		$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);					
		echo json_encode($rows);					
		} catch (PDOException $e) {
				echo '{"error":{"text":' . $e->getMessage() . '}}';				
		}
}

/*Update the alert status as already informed to police*/
function policeInform() {

$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	//print_r($request);exit;
	$user_id = $request->user_id;
	$address_id = $request->address_id;
	$address_type = $request->address_type;
	$alert_type = "N";
	$police_info = "P";
	if($address_type == 'GB' || $address_type == 'GS') {
		$marker = 'yellow-siren.svg';
		$qury = "UPDATE address SET status_type=:police_info,marker=:marker WHERE address_id=:address_id";
		$db = getDB();
		$smt = $db->prepare($qury);
		$smt->bindParam('address_id', $address_id);
		$smt->bindParam('police_info', $police_info);
		$smt->bindParam('marker', $marker);
		$smt->execute();
	} else {
		$marker = 'yellow-siren.svg';
		$qury = "UPDATE user u, address a SET u.alert_type=:alert_type, a.status_type=:police_info, a.marker=:marker
					WHERE u.user_id=:user_id AND a.user_id =:user_id AND a.address_id =:address_id";
		$db = getDB();
		$smt = $db->prepare($qury);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('alert_type', $alert_type);
		$smt->bindParam('address_id', $address_id);
		$smt->bindParam('police_info', $police_info);
		$smt->bindParam('marker', $marker);
		$smt->execute();
	}
	try {
			$sql = "UPDATE user u, address a, alert_log al SET u.alert_type=:alert_type, a.status_type=:police_info, a.marker=:marker, al.is_active = 0 ,al.log_state = 0 
					WHERE u.user_id=:user_id AND a.user_id =:user_id AND a.address_id =:address_id AND al.sender_id =:user_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
			$smt->bindParam('alert_type', $alert_type);
			$smt->bindParam('address_id', $address_id);
			$smt->bindParam('police_info', $police_info);
			$smt->bindParam('marker', $marker);
			$smt->execute();
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
	
	echo '{"status":true}';

}

/*update the user status after 65% of user confirm the threat*/
function progressSame($address_id,$address_type) {
	
	$alert_type = "SS";
	if($address_type == 'GB' || $address_type == 'GS') {
		$marker = 'yellow-alert.svg';
	} else {
		$marker = 'yellow-alert.svg';
	}
	try {
			$sql = "UPDATE address SET status_type=:alert_type, marker=:marker WHERE address_id=:address_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('address_id', $address_id);
			$smt->bindParam('alert_type', $alert_type);
			$smt->bindParam('marker', $marker);
			$smt->execute();
			
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
}

/*update the user status after 50% of user confirm not a threat*/
function notSuspicious($address_id,$address_type) {
	$alert_type = "NS";
	if($address_type == 'GB' || $address_type == 'GS') {
		$marker = 'gray.svg';
	} else {
		$marker = 'gray.svg';
	}
	try {
			$sql = "UPDATE address SET status_type=:alert_type, marker=:marker WHERE address_id=:address_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('address_id', $address_id);
			$smt->bindParam('alert_type', $alert_type);
			$smt->bindParam('marker', $marker);
			$smt->execute();
			
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
}

/*update user state when the response equal*/
function noChange($address_id,$status_type) {
	
	try {
			$sql = "UPDATE address SET status_type=:status_type WHERE address_id=:address_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('address_id', $address_id);
			$smt->bindParam('status_type', $status_type);
			$smt->execute();
			
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
}

/*upload profile image of users and move to server*/
function profileImageupload() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$target_path = "uploads/";
 
	$target_path = $target_path . basename( $_FILES['file']['name']);
 
	if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
		
		echo 'true';
	} else {
		echo 'false';
		
	}
}

/*update profile image when change*/
function updateUserImagename() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$imagename = $request->photo;
	try {
		$sql = "UPDATE user SET photo =:imagename WHERE user_id=:user_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('imagename', $imagename);
		$smt->execute();
		echo '{"status": true}';
	}catch(Exception $e) {
			
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

/*remove accidently placed alert*/
function removeGuest() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$address_id = $request->address_id;
	try {
		$sql = "UPDATE address SET is_active = 0 WHERE address_id=:address_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('address_id', $address_id);
		$smt->execute();
		echo '{"status": true}';
	}catch(Exception $e) {
			
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

/*user edit and update contact detail*/
function mycontactUpdate(){
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$name = $request->name;
	$mail = $request->mail;
	$num = $request->phone;
	$code = $request->code;
	$phone = $code.$num;
	$user_id = $request->user_id;
	$id = $request->id;
	try {
		$sql = "UPDATE my_contact SET user_id=:user_id,name=:name,email_id=:mail,phone_number=:phone WHERE my_contact_id=:my_contact_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('my_contact_id', $id);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('mail', $mail);
		$smt->bindParam('phone', $phone);
		$smt->bindParam('name', $name);
		$smt->execute();
		echo '{"status": true}';
	}catch(Exception $e) {
			
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

/*check user response for alerts*/
function checkUserResponse() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$user_id = $request->user_id;
	$sus_user_id = $request->sus_user_id;
	$sus_address_id = $request->sus_address_id;
	$address_type = $request->address_type;
	try {
	
		if($address_type === 'GB' || $address_type === 'GS') {
		
			$sql1 = "SELECT message,time FROM alert_log WHERE sender_id=:sus_user_id AND address_id=:sus_address_id AND alert_status_type ='BC' OR alert_status_type ='SC' ORDER BY time DESC LIMIT 1";
			$db = getDB();
			$smtm = $db->prepare($sql1);
			$smtm->bindParam('sus_user_id', $sus_user_id);
			$smtm->bindParam('sus_address_id', $sus_address_id);
			$smtm->execute();
			$msgArr = $smtm->fetch();
			$result = $msgArr['message']; 
			$time = $msgArr['time'];
		} else {
			$sql2 = "SELECT message,time FROM alert_log WHERE receiver_id=:sus_user_id AND address_id=:sus_address_id AND alert_status_type ='B' OR alert_status_type ='S' ORDER BY time DESC";
			$db = getDB();
			$smtm = $db->prepare($sql2);
			$smtm->bindParam('sus_user_id', $sus_user_id);
			$smtm->bindParam('sus_address_id', $sus_address_id);
			$smtm->execute();
			$msgArr = $smtm->fetch();
			$result = $msgArr['message']; 
			$time = $msgArr['time'];
		}
		$sql = "SELECT * FROM alert_log WHERE sender_id=:user_id AND receiver_id=:sus_user_id AND address_id=:sus_address_id AND is_active=1";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('sus_user_id', $sus_user_id);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('sus_address_id', $sus_address_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		if(count($rows) > 0) {
			echo '{"status": true, "msg":"'.$result.'","time":"'.$time.'"}';
		} else {
			$sql = "SELECT * FROM alert_log WHERE sender_id=:sus_user_id AND receiver_id=:user_id AND address_id=:sus_address_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('sus_user_id', $sus_user_id);
			$smt->bindParam('user_id', $user_id);
			$smt->bindParam('sus_address_id', $sus_address_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			if(count($rows) > 0) {
				$sql ="SELECT * 
						FROM alert_log
						WHERE is_active =1
						AND sender_id =:sus_user_id
						AND receiver_id =:user_id
						AND address_id =:sus_address_id
						AND saw_the_same IS NOT NULL 
						ORDER BY time DESC";
				
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('sus_user_id', $sus_user_id);
				$smt->bindParam('user_id', $user_id);
				$smt->bindParam('sus_address_id', $sus_address_id);
				$smt->execute();
				$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
				if(count($rows) > 0) {
					echo '{"status": true, "msg":"'.$result.'","time":"'.$time.'"}';
				} else {
					echo '{"status": false, "msg":"'.$result.'","time":"'.$time.'"}';
				}
			} else {
				echo '{"status": "nothing", "msg":"'.$result.'","time":"'.$time.'"}';
			}
		}	
	}catch(Exception $e) {
			
		echo '{"msg":'.$e->getMessage().'}';
	}
}

/*delete contact detail from DB*/
function myContactDelete(){
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$id = $request->id;
	
	try {
		$sql = "delete from my_contact WHERE my_contact_id=:my_contact_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('my_contact_id', $id);
		
		$smt->execute();
		echo '{"status": true}';
	}catch(Exception $e) {
			
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

/*delete address */
function deleteAddress(){
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$address_id = $request->address_id;
	
	try {
		$sql = "update address set is_active=0 WHERE address_id=:address_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('address_id', $address_id);
		
		$smt->execute();
		echo '{"status": true}';
	}catch(Exception $e) {
			
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

/*send push notification*/	
function push() {

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		//print_r($request);exit;
		$user_id = $request->user_id;
		$alert_type = $request->alert_type;
		$s_user_id = $request->sender_id;
		$message = $request->msg;
		$address_id = $request->address_id;
		try {
		
			$sql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id AND	address.address_type NOT IN('GB','GS')";
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id', $user_id);
        	$smt->bindParam('address_id', $address_id);
			$smt->execute();
			$rows = $smt->fetch(PDO::FETCH_OBJ);
				
			} catch (Exception $e) {
				
				echo json_encode(array('status' => 'False', 'msg' => $e->getMessage()));
		}
		
		$deviceID = $rows->deviceID;
		$street = $rows->street_address;
		$city = $rows->city;
		$country = $rows->country;
		$zipcode = $rows->zipcode;
		$platform = $rows->platform;
		$lang = $rows->language;
		$title='';
        $text='';
                    if($lang == 'en') {
                        $title = 'Message from Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'A alarm has been registered at this address.';
                                break;
                            case "S":
                                $text = 'Suspicious behavior has been registered at this address.';
                                break;
                           default:
                                $text = 'A alarm has been registered at this address.';
                        }
                    } else{
                        $title = 'Besked fra Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'En alarm er blevet registreret på denne adresse.';
                                break;
                            case "S":
                                $text = 'Mistænkelig adfærd er registreret på denne adresse.';
                                break;
                           default:
                                $text = 'En alarm er blevet registreret på denne adresse.';
                        }
                    }
		$address = $street.', '.$city.', '.$country.', '.$zipcode;
		
		define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
		
		if($platform == 'Android') {
			$msg = array
				(
					'alert'		=> $address,
					'message'	=> $text,
					'title'		=> $title,
					'subtitle'	=> $address,
					'sData'	=> json_encode($request),
					'tickerText'	=> $text,
					'vibrate'	=> 1,
					'sound'		=> 1,
					'content_available' => 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon',
					'priority'	=> 'high'
					
				);
				$fields = array
				(
					'registration_ids'	=> array($deviceID),
					'data'			=> array("message" => $msg)
				);

				$headers = array
				(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);

				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec($ch );
				curl_close( $ch );
				//echo '{"status":true,"result":'.$result.'}';
				
		} else {
				$ch = curl_init("https://fcm.googleapis.com/fcm/send");
				
				//The device token.
				$token = $deviceID; //token here
				
				//Title of the Notification.

				//Body of the Notification.
				$body = $address;
				
				//Creating the notification array.
				$notification = array('title' =>$title , 'text' => $body,'tickerText'=> $text,
				    'subtitle'=> $address,
				    'latitude' => $request->latitude,
				    'longitude' => $request->longitude);

				//This array contains, the token and the notification. The 'to' attribute stores the token.
				$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

				//Generating JSON encoded string form the above array.
				$json = json_encode($arrayToSend);
				//Setup headers:
				$headers = array();
				$headers[] = 'Content-Type: application/json';
				$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

				//Setup curl, add headers and post parameters.
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				$result = curl_exec($ch );
				curl_close( $ch );
				//echo '{"status":true,"result":'.$result.'}';
		}
		$allstate = pushall($user_id,$address_id,$request);
		$supstate = sendAlertToSupporters($user_id,$address,$message,$request);
		if($allstate && $supstate) {
		    echo json_encode(array('status'=>true));
		}
}	
/*send push notification to all nearby user*/
function camPushall() {
	$postdata = file_get_contents("php://input");
	
	$camdata = json_decode($postdata);
	//print_r($request);exit;
	$user_id = $camdata->user_id;
	$address_id = $camdata->address_id;
	$sql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id AND address.address_type NOT IN('GB','GS')";
			
	$db = getDB();
	$smt = $db->prepare($sql);
	$smt->bindParam('user_id', $user_id);
    $smt->bindParam('address_id', $address_id);
	$smt->execute();
	$rows = $smt->fetch(PDO::FETCH_OBJ);
	$request = (object) $rows;
	$rng_value = $request->r_distance/1000;
	$user_id = $request->user_id;
	$lat=$request->latitude;
	$lng=$request->longitude;
	$sender_id = $request->user_id;
	$alert_type = $request->alert_type;
	$sender_id = $request->user_id;
	
	$address_id = $request->address_id;
	$own_add = $address_id;
	$street = $request->street_address;
	$city = $request->city;
	$country = $request->country;
	$zipcode = $request->zipcode;
	$message = $request->last_alert;
	$address = $street.', '.$city.', '.$country.', '.$zipcode;
	if($alert_type == "B") {
		$owner_alert = "BP";
		$alert ="BC";
	} else if($alert_type == "S"){
		$owner_alert = "SP";
		$alert = "SC";
	} else if($alert_type == "N") {
    	$owner_alert = "BP";
		$alert ="BC";
    }
	try{
    	if($alert_type == 'N') {
        	//echo 'n';exit;
        	$alert='B';
        	$marker='red.svg';
        	//$sql = "UPDATE user u,address a SET u.alert_type=:owner_alert,a.status_type=:alert,a.marker=:marker WHERE u.user_id=:user_id AND a.address_id=:address_id";
        	$sql = "UPDATE user JOIN address ON user.user_id = address.user_id
  					SET user.alert_type=:owner_alert,address.status_type=:alert,address.marker=:marker 
					WHERE user.user_id=:user_id AND address.address_id=:address_id";
        	$db = getDB();
        	$smt = $db->prepare($sql);
        	$smt->bindParam('owner_alert', $owner_alert);
        	$smt->bindParam('user_id', $user_id);
        	$smt->bindParam('address_id', $own_add);
        	$smt->bindParam('alert', $alert);
        	$smt->bindParam('marker', $marker);
        	$smt->execute();
        	if($smt) {
            	pushAllNeighbours($request,$alert);
    			sendAlertToSupporters($user_id,$address,$message,$request);
            }
        } else {
        	//echo 'B';exit;
        	$alert='B';
        	$marker='red.svg';
        	//$sql = "UPDATE user u,address a SET u.alert_type=:owner_alert,a.status_type=:alert,a.marker=:marker WHERE u.user_id=:user_id AND a.address_id=:address_id";
        	$sql = "UPDATE user JOIN address ON user.user_id = address.user_id
  					SET user.alert_type=:owner_alert,address.status_type=:alert,address.marker=:marker 
					WHERE user.user_id=:user_id AND address.address_id=:address_id";
        	$db = getDB();
        	$smt = $db->prepare($sql);
        	$smt->bindParam('owner_alert', $owner_alert);
        	$smt->bindParam('user_id', $user_id);
        	$smt->bindParam('address_id', $own_add);
        	$smt->bindParam('alert', $alert);
        	$smt->bindParam('marker', $marker);
        	$smt->execute();
        	if($smt) {
            	pushAllNeighbours($request,$alert);
    			sendAlertToSupporters($user_id,$address,$message,$request);
            }
        }
	} catch (Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
}

function pushAllNeighbours($request,$alert){
		
    $rng_value = $request->r_distance/1000;
    $user_id = $request->user_id;
    $lat=$request->latitude;
    $lng=$request->longitude;
    $sender_id = $request->user_id;
    $alert_type = $request->alert_type;
    $sender_id = $request->user_id;

    $address_id = $request->address_id;
    $own_add = $address_id;
    $street = $request->street_address;
    $city = $request->city;
    $country = $request->country;
    $zipcode = $request->zipcode;
    $message = $request->last_alert;
    $address = $street.', '.$city.', '.$country.', '.$zipcode;
		try{

    		$sql="SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";		
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			//print_r($rows);exit;
			$usercount = count($rows);
			$countarr = array();
			if($usercount > 0){
			    foreach($rows as $val){
			        $r_user_id = $val['user_id'];
			        $r_address_id = $val['address_id'];
			        $ssql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id";
			        $db = getDB();
        			$ssmt = $db->prepare($ssql);
        			$ssmt->bindParam(':user_id', $r_user_id);
        			$ssmt->bindParam(':address_id', $r_address_id);
        			$ssmt->execute();
        			$srows = $ssmt->fetchAll(PDO::FETCH_ASSOC);
        			//print_r($srows);exit;
        			foreach($srows as $val){
        			    $r_range = $val['r_distance']/1000;
        			    $r_lat  =$val['latitude'];
        			    $r_lng = $val['longitude'];
        			}
        			$sql = "SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";
                			$db = getDB();
                			$smt = $db->prepare($sql);
                			$smt->bindParam(':rang', $r_range);
                			$smt->bindParam(':lat', $r_lat);
                			$smt->bindParam(':lng', $r_lng);
                			$smt->bindParam(':user_id', $user_id);
                			$smt->execute();
                			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
        			        //print_r($rows);
        			        array_push($countarr,$rows);
			    }
			    //$usercount = count($countarr);
			    // echo '<pre>';
			    // print_r($countarr);
			    // echo '</pre>';exit;
            	foreach($countarr[0] as $divData) {
                	$sql="SELECT * FROM user WHERE user_id=:user_id AND do_not_distrub != 1";		
            		$db = getDB();
            		$smt = $db->prepare($sql);
            		$smt->bindParam(':user_id', $divData['user_id']);
            		$smt->execute();
            		$drows = $smt->fetchAll(PDO::FETCH_ASSOC);
					$platform = $drows[0]['platform'];
					$deviceID = $drows[0]['deviceID'];
                	$nuser_id = $drows[0]['user_id'];
                	$lang = $drows[0]['language'];
					// echo '<pre>';
					// print_r($drows);
					// echo '</pre>';
					$title='';
        			$text='';
                    if($lang == 'en') {
                        $title = 'Message from Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'A alarm has been registered at this address.';
                                break;
                            case "S":
                                $text = 'Suspicious behavior has been registered at this address.';
                                break;
                           default:
                                $text = 'A alarm has been registered at this address.';
                        }
                    } else{
                        $title = 'Besked fra Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'En alarm er blevet registreret på denne adresse.';
                                break;
                            case "S":
                                $text = 'Mistænkelig adfærd er registreret på denne adresse.';
                                break;
                           default:
                                $text = 'En alarm er blevet registreret på denne adresse.';
                        }
                    }
					define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
					
					if($platform == 'Android') {
                    	//print_r($request);exit;
						$msg = array
						(
							'alert'		=> $address,
							'message'	=> $text,
							'title'		=> $title,
							'subtitle'	=> $address,
							'vData'	=> json_encode($request),
							'tickerText'	=> $text,
							'vibrate'	=> 1,
							'sound'		=> 1,
							'largeIcon'	=> 'large_icon',
							'smallIcon'	=> 'small_icon',
							'priority'	=> 'high'
						);
						$fields = array
						(
							'registration_ids'	=> array($deviceID),
							'data'			=> array("message" => $msg)
						);
					 
						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);
					 
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$push_result = curl_exec($ch );
						
						curl_close( $ch );
					} else {
						$ch = curl_init("https://fcm.googleapis.com/fcm/send");
						
						$token = $deviceID; //token here
						
						$body = $address;
						
						$notification = array('title' =>$title , 'subtitle'=> $address,'tickerText'=> $text,'vData'=> json_encode($request),'subtitle'=> $address);

						//This array contains, the token and the notification. The 'to' attribute stores the token.
						$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

						//Generating JSON encoded string form the above array.
						$json = json_encode($arrayToSend);
						//Setup headers:
						$headers = array();
						$headers[] = 'Content-Type: application/json';
						$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

						//Setup curl, add headers and post parameters.
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
						curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
						curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						$push_result = curl_exec($ch );
						
						curl_close( $ch );
					}
					
					try {
							
						$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`) VALUES
													   (:sender_id, :receiver_id, :alert_type, :address_id, :message)";
						$db = getDB();
						$smt = $db->prepare($sql);
						$smt->bindParam('sender_id', $user_id);
						$smt->bindParam('receiver_id', $nuser_id);
						$smt->bindParam('alert_type', $alert);
						$smt->bindParam('address_id', $address_id);
						$smt->bindParam('message', $message);
						$smt->execute();
						
						$sql1 = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id";
						$smt1 = $db->prepare($sql1);
						$smt1->bindParam('receiver_id', $nuser_id);
						$smt1->execute();
						$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
						foreach($rows as $value) {
							$address_id = $value['address_id'];
							$receiver_id = $value['receiver_id'];
							
							$sql = "UPDATE user u, address a SET u.alert_type=:alert WHERE a.address_id=
									:address_id AND u.user_id =:receiver_id AND u.alert_type NOT IN('B') AND u.alert_type NOT IN('BP')
									AND u.alert_type NOT IN('S') AND u.alert_type NOT IN('SP')";
							$db = getDB();
							$smt = $db->prepare($sql);
							$smt->bindParam('address_id', $address_id);
							$smt->bindParam('receiver_id', $receiver_id);
							$smt->bindParam('alert', $alert);
							$smt->execute();
						}
	
					}catch(Exception $e) {
						
						echo '{"status":false, "msg":'.$e->getMessage().'}';
					}
					
				 }
			    $data = array('status' => true, 'count' => $usercount);
			} else {
			    $data = array('status' => false, 'count' => '0');
			}
        	return $data;
			}catch (Exception $e) {
   
	   			$data = array('status' => 'False', 'msg' => $e->getMessage());
   			}
		
	
}

/*old code*/
function camPushall_bk() {
	$postdata = file_get_contents("php://input");
	
	$request = json_decode($postdata);
	
	$rng_value = $request->distance/1000;
	$user_id = $request->user_id;
	$lat=$request->latitude;
	$lng=$request->longitude;
	$sender_id = $request->user_id;
	$alert_type = $request->alert_type;
	$sender_id = $request->user_id;
	
	$address_id = $request->address_id;
	$own_add = $address_id;
	$street = $request->street_address;
	$city = $request->city;
	$country = $request->country;
	$zipcode = $request->zipcode;
	$message = $request->last_alert;
	$address = $street.', '.$city.', '.$country.', '.$zipcode;
	if($alert_type == "B") {
		$owner_alert = "BP";
		$alert ="BC";
	} else if($alert_type == "S"){
		$owner_alert = "SP";
		$alert = "SC";
	} else if($alert_type == "N") {
    	$owner_alert = "BP";
		$alert ="BC";
    }
	try{
		$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
		
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam(':rang', $rng_value);
		$smt->bindParam(':lat', $lat);
		$smt->bindParam(':lng', $lng);
		$smt->bindParam(':user_id', $user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);

		foreach($rows as $value) {
				// $r_sql = "SELECT sender_id FROM alert_log WHERE receiver_id=:user_id AND is_active=1 AND 
				// alert_status_type IN('B','S') ORDER BY time DESC LIMIT 1";
				// $db = getDB();
				// $stmt = $db->prepare($r_sql);
				// $stmt->bindParam('user_id', $user_id);
				// $stmt->execute();
				// $result = $stmt->fetch();
				// $r_user_id = $result['sender_id'];
				
				$nuser_id = $value['user_id'];
				$sql = "SELECT user_id, deviceID, platform from user WHERE user_id =:nuser_id AND do_not_distrub=0 AND is_active=1  GROUP BY user_id";
				$db = getDB();
				$stmt = $db->prepare($sql);
				$stmt->bindParam('nuser_id', $nuser_id);
				// $stmt->bindParam('r_user_id', $r_user_id);
				$stmt->execute();
				$divIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach($divIds as $divData) {
					$platform = $divData['platform'];
					$deviceID = $divData['deviceID'];
					
					define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
					
					if($platform == 'Android') {
						$msg = array
						(
							'alert'		=> $address,
							'message'	=> 'Some Suspicious act has been noted in this address.',
							'title'		=> 'NABO ALERT MESSAGE',
							'subtitle'	=> $address,
							'vData'	=> json_encode($request),
							'tickerText'	=> 'Some Suspicious act has been noted in this address.',
							'vibrate'	=> 1,
							'sound'		=> 1,
							'largeIcon'	=> 'large_icon',
							'smallIcon'	=> 'small_icon',
							'priority'	=> 'high'
						);
						$fields = array
						(
							'registration_ids'	=> array($deviceID),
							'data'			=> array("message" => $msg)
						);
					 
						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);
					 
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$push_result = curl_exec($ch );
						
						curl_close( $ch );
					} else {
						$ch = curl_init("https://fcm.googleapis.com/fcm/send");
						
						$token = $deviceID; //token here
						
						$body = $address;
						
						$notification = array('title' =>"NABO ALERT MESSAGE" , 'subtitle'=> $address,'tickerText'=> 'Some Suspicious act has been noted in this address.','vData'=> json_encode($request),'subtitle'=> $address);

						//This array contains, the token and the notification. The 'to' attribute stores the token.
						$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

						//Generating JSON encoded string form the above array.
						$json = json_encode($arrayToSend);
						//Setup headers:
						$headers = array();
						$headers[] = 'Content-Type: application/json';
						$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

						//Setup curl, add headers and post parameters.
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
						curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
						curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						$push_result = curl_exec($ch );
						
						curl_close( $ch );
					}
					
					
					try {
							
						$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`) VALUES
													   (:sender_id, :receiver_id, :alert_type, :address_id, :message)";
						$db = getDB();
						$smt = $db->prepare($sql);
						$smt->bindParam('sender_id', $user_id);
						$smt->bindParam('receiver_id', $nuser_id);
						$smt->bindParam('alert_type', $alert);
						$smt->bindParam('address_id', $address_id);
						$smt->bindParam('message', $message);
						$smt->execute();
						
						$sql1 = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id";
						$smt1 = $db->prepare($sql1);
						$smt1->bindParam('receiver_id', $nuser_id);
						$smt1->execute();
						$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
						foreach($rows as $value) {
							$address_id = $value['address_id'];
							$receiver_id = $value['receiver_id'];
							
							$sql = "UPDATE user u, address a SET u.alert_type=:alert WHERE a.address_id=
									:address_id AND u.user_id =:receiver_id AND u.alert_type NOT IN('B') AND u.alert_type NOT IN('BP')
									AND u.alert_type NOT IN('S') AND u.alert_type NOT IN('SP')";
							$db = getDB();
							$smt = $db->prepare($sql);
							$smt->bindParam('address_id', $address_id);
							$smt->bindParam('receiver_id', $receiver_id);
							$smt->bindParam('alert', $alert);
							$smt->execute();
						}
	
					}catch(Exception $e) {
						
						echo '{"status":false, "msg":'.$e->getMessage().'}';
					}
					
				 }
		}
    //echo $owner_alert.'-'.$user_id.'-'.$own_add;exit;
    	if($alert_type == 'N') {
        	//echo 'n';exit;
        	$alert='B';
        	$marker='red.svg';
        	//$sql = "UPDATE user u,address a SET u.alert_type=:owner_alert,a.status_type=:alert,a.marker=:marker WHERE u.user_id=:user_id AND a.address_id=:address_id";
        	$sql = "UPDATE user JOIN address ON user.user_id = address.user_id
  					SET user.alert_type=:owner_alert,address.status_type=:alert,address.marker=:marker 
					WHERE user.user_id=:user_id AND address.address_id=:address_id";
        	$db = getDB();
        	$smt = $db->prepare($sql);
        	$smt->bindParam('owner_alert', $owner_alert);
        	$smt->bindParam('user_id', $user_id);
        	$smt->bindParam('address_id', $own_add);
        	$smt->bindParam('alert', $alert);
        	$smt->bindParam('marker', $marker);
        	$smt->execute();
        	$date = 0;  //get date form admin setting
        	$NewDate=Date('y-m-d', strtotime("+".$date." days"));
        	//echo $NewDate;exit;
        	//echo date('Y-m-d');exit;
        	$today = date('Y-m-d');
        	if (strtotime($today) == strtotime($NewDate)) {
            	echo '{"status":true}';
            //processReset($user_id,$address_id,$address_type);
        	}		
        } else {
        	//echo 'B';exit;
        	$alert='B';
        	$marker='red.svg';
        	//$sql = "UPDATE user u,address a SET u.alert_type=:owner_alert,a.status_type=:alert,a.marker=:marker WHERE u.user_id=:user_id AND a.address_id=:address_id";
        	$sql = "UPDATE user JOIN address ON user.user_id = address.user_id
  					SET user.alert_type=:owner_alert,address.status_type=:alert,address.marker=:marker 
					WHERE user.user_id=:user_id AND address.address_id=:address_id";
        	$db = getDB();
        	$smt = $db->prepare($sql);
        	$smt->bindParam('owner_alert', $owner_alert);
        	$smt->bindParam('user_id', $user_id);
        	$smt->bindParam('address_id', $own_add);
        	$smt->bindParam('alert', $alert);
        	$smt->bindParam('marker', $marker);
        	$smt->execute();
        	$date = 0;  //get date form admin setting
        	$NewDate=Date('y-m-d', strtotime("+".$date." days"));
        	//echo $NewDate;exit;
        	//echo date('Y-m-d');exit;
        	$today = date('Y-m-d');
        	if (strtotime($today) == strtotime($NewDate)) {
            	echo '{"status":true}';
            //processReset($user_id,$address_id,$address_type);
        	}		
        }	
		
	} catch (Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
}

/*send push notification to all nearby user*/
function pushall($user_id,$address_id,$request) {
		$db = getDB();
		$stmt = $db->prepare("SELECT *
								FROM wp_users wp
								LEFT JOIN user u ON u.user_id = wp.ID
								LEFT JOIN address a ON a.user_id = wp.ID
								WHERE u.user_id =:user_id AND a.address_id =:address_id AND a.address_type NOT IN('GB','GS') AND a.is_active=1 ORDER BY FIELD(a.primary_address, 'yes') DESC, a.primary_address");
		$stmt->bindValue(':user_id', $user_id, PDO::PARAM_STR);
		$stmt->bindValue(':address_id', $address_id, PDO::PARAM_STR);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($rows);exit;
		$rng_value = $rows[0]['distance']/1000;
		$user_id = $rows[0]['user_id'];
		$lat=$rows[0]['latitude'];
		$lng=$rows[0]['longitude'];
		$sender_id = $rows[0]['user_id'];
		$alert_type = $rows[0]['alert_type'];
		$sender_id = $rows[0]['user_id'];
		$address_id = $rows[0]['address_id'];
		$street = $rows[0]['street_address'];
		$city = $rows[0]['city'];
		$country = $rows[0]['country'];
		$zipcode = $rows[0]['zipcode'];
		$message = $rows[0]['last_alert'];
		$address = $street.', '.$city.', '.$country.', '.$zipcode;
		if($alert_type == "B") {
			$owner_alert = "BP";
			$alert ="BC";
		} else if($alert_type == "S"){
			$owner_alert = "SP";
			$alert = "SC";
		}
		try{
			$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
			
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam(':rang', $rng_value);
			$smt->bindParam(':lat', $lat);
			$smt->bindParam(':lng', $lng);
			$smt->bindParam(':user_id', $user_id);
			$smt->execute();
			$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
			foreach($rows as $value) {
					$r_sql = "SELECT sender_id FROM alert_log WHERE receiver_id=:user_id AND is_active=1 AND 
					alert_status_type IN('B','S') ORDER BY time DESC LIMIT 1";
					$db = getDB();
					$stmt = $db->prepare($r_sql);
					$stmt->bindParam('user_id', $user_id);
					$stmt->execute();
					$result = $stmt->fetch();
					$r_user_id = $result['sender_id'];
					
					$nuser_id = $value['user_id'];
					$sql = "SELECT user_id, deviceID, platform from user WHERE user_id =:nuser_id AND do_not_distrub=0 AND is_active=1 AND user_id !=:r_user_id GROUP BY user_id";
					$db = getDB();
					$stmt = $db->prepare($sql);
					$stmt->bindParam('nuser_id', $nuser_id);
					$stmt->bindParam('r_user_id', $r_user_id);
					$stmt->execute();
					$divIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
					
					foreach($divIds as $divData) {
						$platform = $divData['platform'];
						$deviceID = $divData['deviceID'];
                    	$lang = $divData['language'];
						$title='';
        				$text='';
                    if($lang == 'en') {
                        $title = 'Message from Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'A alarm has been registered at this address.';
                                break;
                            case "S":
                                $text = 'Suspicious behavior has been registered at this address.';
                                break;
                           default:
                                $text = 'A alarm has been registered at this address.';
                        }
                    } else{
                        $title = 'Besked fra Nabosupport';
                        switch ($alert_type) {
                            case "B":
                                $text = 'En alarm er blevet registreret på denne adresse.';
                                break;
                            case "S":
                                $text = 'Mistænkelig adfærd er registreret på denne adresse.';
                                break;
                           default:
                                $text = 'En alarm er blevet registreret på denne adresse.';
                        }
                    }
						define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
						
						if($platform == 'Android') {
							$msg = array
							(
								'alert'		=> $address,
								'message'	=> $text,
								'title'		=> $title,
								'subtitle'	=> $address,
								'vData'	=> json_encode($request),
								'tickerText'	=> 'Some Suspicious act has been noted in this address.',
								'vibrate'	=> 1,
								'sound'		=> 1,
								'largeIcon'	=> 'large_icon',
								'smallIcon'	=> 'small_icon',
								'priority'	=> 'high'
							);
							$fields = array
							(
								'registration_ids'	=> array($deviceID),
								'data'			=> array("message" => $msg)
							);
						 
							$headers = array
							(
								'Authorization: key=' . API_ACCESS_KEY,
								'Content-Type: application/json'
							);
						 
							$ch = curl_init();
							curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
							$push_result = curl_exec($ch );
							
							curl_close( $ch );
						} else {
							$ch = curl_init("https://fcm.googleapis.com/fcm/send");
							
							$token = $deviceID; //token here
							
							$body = $address;
							
				// 			$notification = array('title' =>"NABO ALERT MESSAGE" , 'subtitle'=> $address,'tickerText'=> 'Suspicious act has been noted in this address.','vData'=> json_encode($request),'subtitle'=> $address);
							$notification = array('title' =>$title , 'text' => $body,'tickerText'=> $text,
            				    'subtitle'=> $address,
            				    'latitude' => $request->latitude,
            				    'longitude' => $request->longitude);

							//This array contains, the token and the notification. The 'to' attribute stores the token.
							$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

							//Generating JSON encoded string form the above array.
							$json = json_encode($arrayToSend);
							//Setup headers:
							$headers = array();
							$headers[] = 'Content-Type: application/json';
							$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

							//Setup curl, add headers and post parameters.
							curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
							curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
							curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							$push_result = curl_exec($ch );
							
							curl_close( $ch );
						}
						
						
						try {
								
							$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`) VALUES
														   (:sender_id, :receiver_id, :alert_type, :address_id, :message)";
							$db = getDB();
							$smt = $db->prepare($sql);
							$smt->bindParam('sender_id', $user_id);
							$smt->bindParam('receiver_id', $nuser_id);
							$smt->bindParam('alert_type', $alert);
							$smt->bindParam('address_id', $address_id);
							$smt->bindParam('message', $message);
							$smt->execute();
							
							$sql1 = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id";
							$smt1 = $db->prepare($sql1);
							$smt1->bindParam('receiver_id', $nuser_id);
							$smt1->execute();
							$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
							foreach($rows as $value) {
								$address_id = $value['address_id'];
								$receiver_id = $value['receiver_id'];
								
								$sql = "UPDATE user u, address a SET u.alert_type=:alert WHERE a.address_id=
										:address_id AND u.user_id =:receiver_id AND u.alert_type NOT IN('B') AND u.alert_type NOT IN('BP')
										AND u.alert_type NOT IN('S') AND u.alert_type NOT IN('SP')";
								$db = getDB();
								$smt = $db->prepare($sql);
								$smt->bindParam('address_id', $address_id);
								$smt->bindParam('receiver_id', $receiver_id);
								$smt->bindParam('alert', $alert);
								$smt->execute();
							}
		
						}catch(Exception $e) {
							
							echo '{"status":false, "msg":'.$e->getMessage().'}';
						}
						
					 }
			}
			$sql = "UPDATE user SET alert_type=:owner_alert WHERE user_id=:user_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('owner_alert', $owner_alert);
			$smt->bindParam('user_id', $user_id);
			$smt->execute();
			
			//echo '{"status":true}';
// 			$date = 0;  //get date form admin setting
// 			$NewDate=Date('y-m-d', strtotime("+".$date." days"));
			//echo $NewDate;exit;
			//echo date('Y-m-d');exit;
// 			$today = date('Y-m-d');


			
// 			if (strtotime($today) == strtotime($NewDate)) {
// 				echo '{"status":true}';
// 				//processReset($user_id,$address_id,$address_type);
// 			}			
			
		} catch (Exception $e) {
			$data = array('status' => 'False', 'msg' => $e->getMessage());
		}
		return true;
	}

/*chcek supporters type*/
function sendAlertToSupporters($user_id,$address,$message,$request) {
//  $postdata = file_get_contents("php://input");
// 	$request = json_decode($postdata);
// 	$user_id = $request->user_id;
// 	$address = "some address";
// 	$message = "Some message";
	$req = $request;
	//print_r($req);exit;
	//echo $address;exit;
    $sql = "select email_id from my_contact where user_id=:user_id group by email_id";
    $db = getDB();
	$stmt = $db->prepare($sql);
	$stmt->bindParam('user_id', $user_id);
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
//echo json_encode($rows);
    foreach($rows as $value) {
        $email_id = $value['email_id'];
        $sql = "select * from wp_users 
        left join user on wp_users.ID = user.user_id where 
        user_email=:email_id";
        $stmt = $db->prepare($sql);
    	$stmt->bindParam('email_id', $email_id);
    	$stmt->execute();
    	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	//echo json_encode($rows);
    	if(empty($rows)){
    	    //print_r($rows);
    	    sendEmailAlert($email_id,$address,$message);
    	} else {
    	    //echo $rows[0]['deviceID'];exit;
    	    $deviceID = $rows[0]['deviceID'];
    	    $platform = $rows[0]['platform'];
        	$lang = $rows[0]['language'];
    	    sendPushAlert($lang,$deviceID,$address,$message,$platform,$req);
    	}
    }
    return true;
}

function sendEmailAlert($email,$address,$message) {
    //$id = 'bharathitat@gmail.com';
    $id = $email;
		$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = "mail.nabosupport.dk";
$mail->Port = 465;
$mail->SMTPAuth = true;
$mail->SMTPAutoTLS = false;
$mail->SMTPSecure = 'ssl'; // To enable TLS/SSL encryption change to 'tls'
$mail->AuthType = "CRAM-MD5";
$mail->Username = "noreply@nabosupport.dk";
$mail->Password = "GOQOm})fhYAL";
$mail->setFrom('noreply@nabosupport.dk', 'Nabosupport');
$mail->addAddress($id); //(Send the test to yourself)
$mail->Subject = 'Nabosupport Alert Message';
$mail->isHTML(true);                                 // Set email format to HTML
    $mail->Body    = "
        <p>Alert,</p>
        <p>            
         ".$message." in below address
        </p>
        <p>
        ".$address."
        </p>";

    if(!$mail->send()) {
        $app->flash("error", "We're having trouble with our mail servers at the moment.  Please try again later, or contact us directly by phone.");
        error_log('Mailer Error: ' . $mail->errorMessage());
        $app->halt(500);
    } else if($mail->send()) {
        //echo json_encode(array('status'=>true));
    }   
}
function sendPushAlert($lang,$deviceID,$address,$message,$platform,$request) {
	//echo $address;exit;
	$title='';
    $text='';
                	if($lang == 'en') {
                    	$title = 'Message from Nabosupport';
                    	switch ($alert_type) {
    						case "B":
        						$text = 'A alarm has been registered at this address.';
        						break;
    						case "S":
        						$text = 'Suspicious behavior has been registered at this address.';
        						break;
   		 				   default:
        						$text = 'A alarm has been registered at this address.';
						}
                    } else{
                    	$title = 'Besked fra Nabosupport';
                    	switch ($alert_type) {
    						case "B":
        						$text = 'En alarm er blevet registreret på denne adresse.';
        						break;
    						case "S":
        						$text = 'Mistænkelig adfærd er registreret på denne adresse.';
        						break;
   		 				   default:
        						$text = 'En alarm er blevet registreret på denne adresse.';
						}
                    }
    define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
	if($platform == 'Android') {
		$msg = array
		(
			'alert'		=> $address,
			'message'	=> $text,
			'title'		=> $title,
			'subtitle'	=> $address,
			'vData'	=> json_encode($request),
			'tickerText'	=> $text,
			'vibrate'	=> 1,
			'sound'		=> 1,
			'largeIcon'	=> 'large_icon',
			'smallIcon'	=> 'small_icon',
			'priority'	=> 'high'
		);
		$fields = array
		(
			'registration_ids'	=> array($deviceID),
			'data'			=> array("message" => $msg)
		);
	 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
	 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$push_result = curl_exec($ch );
		
		curl_close( $ch );
		//echo json_encode(array('status'=>true));
	} else {
		$ch = curl_init("https://fcm.googleapis.com/fcm/send");
		
		$token = $deviceID; //token here
		
		$body = $address;
		
// 			$notification = array('title' =>"NABO ALERT MESSAGE" , 'subtitle'=> $address,'tickerText'=> 'Suspicious act has been noted in this address.','vData'=> json_encode($request),'subtitle'=> $address);
		$notification = array('title' =>$title , 'text' => $body,'tickerText'=> $text,
		    'subtitle'=> $address,
		    'latitude' => $request->latitude,
		    'longitude' => $request->longitude);

		//This array contains, the token and the notification. The 'to' attribute stores the token.
		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

		//Generating JSON encoded string form the above array.
		$json = json_encode($arrayToSend);
		//Setup headers:
		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

		//Setup curl, add headers and post parameters.
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		$push_result = curl_exec($ch );
		
		curl_close( $ch );
		//echo json_encode(array('status'=>true));
	}
}
/*send notification when process cancel*/
function cancelpush() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		
		$user_id = $request->user_id;
		$lat=$request->latitude;
		$lng=$request->longitude;
		$sender_id = $request->user_id;
		$alert_type = $request->alert_type;
		$address_id = $request->address_id;
		$address_type = $request->address_type;
		$street = $request->street_address;
		$city = $request->city;
		$country = $request->country;
		$zipcode = $request->zipcode;
		
		$alert_type = "N";
		try {
				if($address_type === "GB" || $address_type === "GS") {
					$marker = '';
					$sql = "UPDATE address SET is_active = 0 WHERE address_id=:address_id";
					$db = getDB();
					$smt = $db->prepare($sql);
					$smt->bindParam('address_id', $address_id);
					$smt->execute();
					if($smt == true) {
						$sqal = "UPDATE alert_log SET is_active = 0,log_state = 0 WHERE address_id=:address_id";
						$db = getDB();
						$smt = $db->prepare($sqal);
						$smt->bindParam('address_id', $address_id);
						$smt->execute();
					}
					echo '{"status":true,"msg":"remove success."}';
				} else {
					$marker = 'blue.svg';
					$sql1 = "UPDATE user u, address a, alert_log al SET u.alert_type=:alert_type, a.status_type='', a.marker=:marker, al.alert_status_type =:alert_type, al.log_state = 0,al.is_active=0 WHERE u.user_id=:user_id AND a.address_id=:address_id AND al.address_id=:address_id AND a.address_type NOT IN ('GS','GB')";
					$db = getDB();
					$smt = $db->prepare($sql1);
					$smt->bindParam('user_id', $user_id);
					$smt->bindParam('address_id', $address_id);
					$smt->bindParam('alert_type', $alert_type);
					$smt->bindParam('marker',$marker);
					$smt->execute();
					echo '{"status":true,"msg":"cancelled success."}';
				}
			}catch(Exception $e) {
				
				echo '{"status":false, "msg":'.$e->getMessage().'}';
			}	
}

function gpushall() {
	$postdata = file_get_contents("php://input");
	//print_r($postdata); exit
	$request = json_decode($postdata);
	//print_r($request);exit;
	//$div = array();
	//unset($request[0]->deviceID);
	//print_r($request);exit;
	//$rng_value = $request->distance/1000*1.609344;
	$rng_value = 5;
	$user_id = $request->user_id;
	$lat=$request->latitude;
	$lng=$request->longitude;
	$sender_id = $request->user_id;
	$alert_type = $request->status_type;
	$sender_id = $request->user_id;
	$address_id = $request->address_id;
	$street = $request->street_address;
	$city = $request->city;
	$country = $request->country;
	$zipcode = $request->zipcode;
	$message = $request->msg;
	$time = $request->time;
	$address = $street.', '.$city.', '.$country.', '.$zipcode;
	if($alert_type == "B") {
		$owner_alert = "BP";
		$alert ="BC";
	} else if($alert_type == "S"){
		$owner_alert = "SP";
		$alert = "SC";
	}
	try{
    	$sql = "SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";
		// $sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
		// $sql="SELECT user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND address_type NOT IN('GB') AND address_type NOT IN('GS') HAVING distance <:rang ORDER BY distance LIMIT 0 , 30";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam(':rang', $rng_value);
		$smt->bindParam(':lat', $lat);
		$smt->bindParam(':lng', $lng);
		$smt->bindParam(':user_id', $user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		//echo json_encode($rows);exit;
    	$countarr = array(); 
    	foreach($rows as $val){
			$r_user_id = $val['user_id'];
        	$saddress_id = $val['address_id'];
        	//echo $r_user_id;
			$ssql = "SELECT * FROM user LEFT JOIN address on user.user_id = address.user_id WHERE user.user_id=:user_id AND address.address_id=:address_id AND user.is_active=1";
			$db = getDB();
        	$ssmt = $db->prepare($ssql);
        	$ssmt->bindParam(':user_id', $r_user_id);
        	$ssmt->bindParam(':address_id', $saddress_id);
        	$ssmt->execute();
        	$srows = $ssmt->fetchAll(PDO::FETCH_ASSOC);
        	//print_r($srows);exit;
        	foreach($srows as $val){
        		//$r_range = $val['r_distance']/1000*1.609344;
        		$r_range = $val['r_distance']/1000;
        		$r_lat  =$val['latitude'];
        		$r_lng = $val['longitude'];
        	}
        	$sql = "SELECT  user_id, address_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND is_active NOT IN (0) AND address_type NOT IN('GB') AND address_type NOT IN('GS') GROUP BY user_id HAVING distance <:rang ORDER BY distance";
            $db = getDB();
            $smt = $db->prepare($sql);
            $smt->bindParam(':rang', $r_range);
            $smt->bindParam(':lat', $r_lat);
            $smt->bindParam(':lng', $r_lng);
            $smt->bindParam(':user_id', $user_id);
            $smt->execute();
            $rows = $smt->fetchAll(PDO::FETCH_ASSOC);
        	//print_r($rows);
        	array_push($countarr,$rows);
			}
    // echo '<pre>';
    // print_r($countarr);
    // echo'</pre>';exit;
		foreach($countarr as $value) {
				$nuser_id = $value[0]['user_id'];
				$sql = "SELECT user_id, deviceID,platform,language from user WHERE user_id =:nuser_id AND do_not_distrub=0 AND is_active=1 GROUP BY user_id";
				$db = getDB();
				$stmt = $db->prepare($sql);
				$stmt->bindParam('nuser_id', $nuser_id);
				$stmt->execute();
				$divIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
				//print_r($divIds);
				foreach($divIds as $divData) {
					$platform = $divData['platform'];
					$deviceID = $divData['deviceID'];
					$lang = $divData['language'];
                	$title='';
                	$text='';
                	if($lang == 'en') {
                    	$title = 'Message from Nabosupport';
                    	switch ($alert_type) {
    						case "B":
        						$text = 'A alarm has been registered at this address.';
        						break;
    						case "S":
        						$text = 'Suspicious behavior has been registered at this address.';
        						break;
   		 				   default:
        						$text = 'A alarm has been registered at this address.';
						}
                    } else{
                    	$title = 'Besked fra Nabosupport';
                    	switch ($alert_type) {
    						case "B":
        						$text = 'En alarm er blevet registreret på denne adresse.';
        						break;
    						case "S":
        						$text = 'Mistænkelig adfærd er registreret på denne adresse.';
        						break;
   		 				   default:
        						$text = 'En alarm er blevet registreret på denne adresse.';
						}
                    }
					define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
					//$registrationIds = array( $_GET['id'] );
					// prep the bundle
					if($platform == 'Android') {
						$msg = array
						(
							'alert'		=> $address,
							//'message'		=> 'Suspicious act has been noted in this address.',
							'title'		=> $title,
							'subtitle'	=> $address,
							'vData'	=> json_encode($request),
							'tickerText'	=> $text,
							'vibrate'	=> 1,
							'sound'		=> 1,
							'largeIcon'	=> 'large_icon',
							'smallIcon'	=> 'small_icon',
							'priority'	=> 'high'
						);
						$fields = array
						(
							'registration_ids'	=> array($deviceID),
							'data'			=> array("message" => $msg)
						);
					 
						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);
					 
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$push_result = curl_exec($ch );
						curl_close( $ch );
					} else {
						$ch = curl_init("https://fcm.googleapis.com/fcm/send");
						//echo $platform;exit;
						//The device token.
						$token = $deviceID; //token here
						//print_r($deviceID);exit;
						//Title of the Notification.

						//Body of the Notification.
						$body = $address;
						//print_r($address);exit;
						//Creating the notification array.
				// 		$notification = array('title' =>"NABO ALERT MESSAGE" , 'alert' => $address,'subtitle'=> $address,'vData'=> json_encode($request),'tickerText'=> 'Suspicious act has been noted in this address.');
				    $notification = array('title' =>$title , 'text' => $body,'tickerText'=> $text,
            				    'subtitle'=> $address,
            				    'latitude' => $request->latitude,
            				    'longitude' => $request->longitude);

						//This array contains, the token and the notification. The 'to' attribute stores the token.
						$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

						//Generating JSON encoded string form the above array.
						$json = json_encode($arrayToSend);
						//Setup headers:
						$headers = array();
						$headers[] = 'Content-Type: application/json';
						$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

						//Setup curl, add headers and post parameters.
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
						curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
						curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						$push_result = curl_exec($ch );
						curl_close( $ch );
					}
					//echo $result;
								
					try {
						$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`,`time`) VALUES
													   (:sender_id, :receiver_id, :alert_type, :address_id, :message, :time)";
						$db = getDB();
						$smt = $db->prepare($sql);
						$smt->bindParam('sender_id', $user_id);
						$smt->bindParam('receiver_id', $nuser_id);
						$smt->bindParam('alert_type', $alert);
						$smt->bindParam('address_id', $address_id);
						$smt->bindParam('message', $message);
						$smt->bindParam('time', $time);
						$smt->execute();
						//echo '{"step": neighbour alert Change}';
						$sql1 = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id";
						$smt1 = $db->prepare($sql1);
						$smt1->bindParam('receiver_id', $nuser_id);
						$smt1->execute();
						$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
						foreach($rows as $value) {
							$address_id = $value['address_id'];
							$receiver_id = $value['receiver_id'];
							
							$sql = "UPDATE user u, address a SET u.alert_type=:alert WHERE a.address_id=
									:address_id AND u.user_id =:receiver_id AND u.alert_type NOT IN('B') AND u.alert_type NOT IN('BP')
									AND u.alert_type NOT IN('S') AND u.alert_type NOT IN('SP')";
							$db = getDB();
							$smt = $db->prepare($sql);
							$smt->bindParam('address_id', $address_id);
							$smt->bindParam('receiver_id', $receiver_id);
							$smt->bindParam('alert', $alert);
							$smt->execute();
						}
	
					}catch(Exception $e) {
						
						echo '{"status":false, "msg":'.$e->getMessage().'}';
					}
					
				}
		}
		if($push_result) {
			$sql = "UPDATE user SET alert_type=:owner_alert WHERE user_id=:user_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('owner_alert', $owner_alert);
			$smt->bindParam('user_id', $user_id);
			$smt->execute();
		}
		echo '{"status":true}';

	} catch (Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
}

function gpushall_old() {
	$postdata = file_get_contents("php://input");
	//print_r($postdata); exit
	$request = json_decode($postdata);
	//print_r($request);exit;
	//$div = array();
	//unset($request[0]->deviceID);
	//print_r($request);exit;
	$rng_value = $request->distance/1000;
	$user_id = $request->user_id;
	$lat=$request->latitude;
	$lng=$request->longitude;
	$sender_id = $request->user_id;
	$alert_type = $request->status_type;
	$sender_id = $request->user_id;
	$address_id = $request->address_id;
	$street = $request->street_address;
	$city = $request->city;
	$country = $request->country;
	$zipcode = $request->zipcode;
	$message = $request->msg;
	$time = $request->time;
	$address = $street.', '.$city.', '.$country.', '.$zipcode;
	if($alert_type == "B") {
		$owner_alert = "BP";
		$alert ="BC";
	} else if($alert_type == "S"){
		$owner_alert = "SP";
		$alert = "SC";
	}
	try{
		$sql = "select * from (select nearuser.user_id as user_id,(nearuser.distance * 1000) as d,user.r_distance as d1 from(SELECT	 user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id !=:user_id AND address_type !='GB' AND address_type !='GS' GROUP BY user_id HAVING distance <:rang ORDER BY distance) as nearuser left join user on nearuser.user_id=user.user_id) a where d1>=d";
		// $sql="SELECT user_id, SQRT( POW( 69.1 * ( latitude - :lat ) , 2 ) + POW( 69.1 * ( :lng - longitude ) * COS( latitude / 57.3 ) , 2 ) ) AS distance FROM address WHERE user_id NOT IN (:user_id) AND address_type NOT IN('GB') AND address_type NOT IN('GS') HAVING distance <:rang ORDER BY distance LIMIT 0 , 30";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam(':rang', $rng_value);
		$smt->bindParam(':lat', $lat);
		$smt->bindParam(':lng', $lng);
		$smt->bindParam(':user_id', $user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		echo json_encode($rows);exit;
		foreach($rows as $value) {
				$nuser_id = $value['user_id'];
				$sql = "SELECT user_id, deviceID,platform from user WHERE user_id =:nuser_id AND do_not_distrub=0 AND is_active=1 GROUP BY user_id";
				$db = getDB();
				$stmt = $db->prepare($sql);
				$stmt->bindParam('nuser_id', $nuser_id);
				$stmt->execute();
				$divIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
				//print_r($divIds);
				foreach($divIds as $divData) {
					$platform = $divData['platform'];
					$deviceID = $divData['deviceID'];
				
					define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
					//$registrationIds = array( $_GET['id'] );
					// prep the bundle
					if($platform == 'Android') {
						$msg = array
						(
							'alert'		=> $address,
							//'message'		=> 'Suspicious act has been noted in this address.',
							'title'		=> 'NABO ALERT MESSAGE',
							'subtitle'	=> $address,
							'vData'	=> json_encode($request),
							'tickerText'	=> 'Some Suspicious act has been noted in this address.',
							'vibrate'	=> 1,
							'sound'		=> 1,
							'largeIcon'	=> 'large_icon',
							'smallIcon'	=> 'small_icon',
							'priority'	=> 'high'
						);
						$fields = array
						(
							'registration_ids'	=> array($deviceID),
							'data'			=> array("message" => $msg)
						);
					 
						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);
					 
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$push_result = curl_exec($ch );
						curl_close( $ch );
					} else {
						$ch = curl_init("https://fcm.googleapis.com/fcm/send");
						//echo $platform;exit;
						//The device token.
						$token = $deviceID; //token here
						//print_r($deviceID);exit;
						//Title of the Notification.

						//Body of the Notification.
						$body = $address;
						//print_r($address);exit;
						//Creating the notification array.
				// 		$notification = array('title' =>"NABO ALERT MESSAGE" , 'alert' => $address,'subtitle'=> $address,'vData'=> json_encode($request),'tickerText'=> 'Suspicious act has been noted in this address.');
				    $notification = array('title' =>"NABO ALERT MESSAGE" , 'text' => $body,'tickerText'=> 'Suspicious act has been noted in this address.',
            				    'subtitle'=> $address,
            				    'latitude' => $request->latitude,
            				    'longitude' => $request->longitude);

						//This array contains, the token and the notification. The 'to' attribute stores the token.
						$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

						//Generating JSON encoded string form the above array.
						$json = json_encode($arrayToSend);
						//Setup headers:
						$headers = array();
						$headers[] = 'Content-Type: application/json';
						$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

						//Setup curl, add headers and post parameters.
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
						curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
						curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						$push_result = curl_exec($ch );
						curl_close( $ch );
					}
					//echo $result;
								
					try {
						$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `address_id`, `message`,`time`) VALUES
													   (:sender_id, :receiver_id, :alert_type, :address_id, :message, :time)";
						$db = getDB();
						$smt = $db->prepare($sql);
						$smt->bindParam('sender_id', $user_id);
						$smt->bindParam('receiver_id', $nuser_id);
						$smt->bindParam('alert_type', $alert);
						$smt->bindParam('address_id', $address_id);
						$smt->bindParam('message', $message);
						$smt->bindParam('time', $time);
						$smt->execute();
						//echo '{"step": neighbour alert Change}';
						$sql1 = "SELECT * FROM alert_log WHERE receiver_id=:receiver_id";
						$smt1 = $db->prepare($sql1);
						$smt1->bindParam('receiver_id', $nuser_id);
						$smt1->execute();
						$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
						foreach($rows as $value) {
							$address_id = $value['address_id'];
							$receiver_id = $value['receiver_id'];
							
							$sql = "UPDATE user u, address a SET u.alert_type=:alert WHERE a.address_id=
									:address_id AND u.user_id =:receiver_id AND u.alert_type NOT IN('B') AND u.alert_type NOT IN('BP')
									AND u.alert_type NOT IN('S') AND u.alert_type NOT IN('SP')";
							$db = getDB();
							$smt = $db->prepare($sql);
							$smt->bindParam('address_id', $address_id);
							$smt->bindParam('receiver_id', $receiver_id);
							$smt->bindParam('alert', $alert);
							$smt->execute();
						}
	
					}catch(Exception $e) {
						
						echo '{"status":false, "msg":'.$e->getMessage().'}';
					}
					
				}
		}
		if($push_result) {
			$sql = "UPDATE user SET alert_type=:owner_alert WHERE user_id=:user_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('owner_alert', $owner_alert);
			$smt->bindParam('user_id', $user_id);
			$smt->execute();
		}
		echo '{"status":true}';

	} catch (Exception $e) {
		$data = array('status' => 'False', 'msg' => $e->getMessage());
	}
}


/*send SMS from twilio*/
function sendSMS() {
	$postdata = file_get_contents("php://input");
	//print_r($postdata); exit
	$request = json_decode($postdata);
	//print_r($request);exit;
	$user_id = $request->user_id;
	$street = $request->street_address;
	$city = $request->city;
	$country = $request->country;
	$zipcode = $request->zipcode;
	$message = $request->last_alert;
	$address = $street.', '.$city.', '.$country.', '.$zipcode;
	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM my_contact WHERE user_id=:userid");
		$stmt->bindValue(':userid', $user_id, PDO::PARAM_STR);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
			if (count($rows) > 0) { 
				//echo json_encode($rows);
				foreach($rows as $input) {
					$phone_number = $input['phone_number'];
					$account_sid = 'AC5be360e4ab74cfb3c88593be2ec958bf';
					$auth_token = 'ddbd806bbe22542e8f2004f712dbe4ce';
					// In production, these should be environment variables. E.g.:
					// $auth_token = $_ENV["TWILIO_ACCOUNT_SID"]

					// A Twilio number you own with SMS capabilities
					$twilio_number = "+18058566760";

					$client = new Client($account_sid, $auth_token);
					$client->messages->create(
					// Where to send a text message (your cell phone?)
					$phone_number,
					array(
						'from' => $twilio_number,
						'body' => 'This is Nabo support test Message. '.$message.' '.$address.'.'
					)
					);
				}
				echo '{"status": true}';
			} else {
				echo '{"status": false}';
			}
		} catch (PDOException $e) {
				echo '{"error":{"text":' . $e->getMessage() . '}}';
		}
	
}

/*get more address when paid user login*/
function getPaidUserAddress() {
	$postdata = file_get_contents("php://input");
	//print_r($postdata); exit
	$request = json_decode($postdata);
	//print_r($request);exit;
	$user_id = $request->user_id;
	try {
		$db = getDB();
		$data = array();
		$sql = "SELECT * FROM address WHERE user_id=:user_id AND address_type NOT IN('GB','GS') AND is_active=1 ORDER BY FIELD(primary_address, 'yes') DESC, primary_address";
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id',$user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		//print_r($rows);exit;
		foreach($rows as $row) {
			$encode= array(
			   'address_id'=>$row['address_id'],
			   'user_id' => $row['user_id'],
			   'street_address' =>$row['street_address'],
			   'zipcode' =>$row['zipcode'],
			   'city' =>$row['city'],
			   'country' =>$row['country'],
			   'longitude' =>$row['longitude'],
			   'latitude' =>$row['latitude'],
			   'camtoken_type' => $row['camtoken_type'],
			   'primary_address' => $row['primary_address']
			   );
			   if($row['camtoken_type'] == 'device_detail') {
			       $encode['camtoken'] = $row['camtoken'];
			       $encode['country_code'] = '';
			   } else if($row['camtoken_type'] == 'phone') {
			       $encode['camtoken'] = substr($row['camtoken'],3);
			       $encode['country_code'] = substr($row['camtoken'],0,3);
			   } else {
			        $encode['camtoken'] = $row['camtoken'];
			        $encode['country_code'] = '';
			   }
			array_push($data,$encode);
		}
		echo json_encode($data);
	} catch(Exception $e) {
		$data['status'] = false;
		$data['msg'] = $e->getMessage();
		echo json_encode($data);
	}
}


/*API FOR WEB */
/*this functions not used now it migrated to codeigniter*/
function getUserweb(){
	$sql = "select * from user";
	$stmt1 = $db->prepare($sql);
	$stmt1->execute();
	$data = [];
	$rows = $stmt1->fetchAll(PDO::FETCH_ASSOC);
	while ($row = $rows) {
		 array_push($data, $row);
	 }
		return $data;
}
function insertProduct() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$name = $request->name;
	$price = $request->price;
	$category = $request->status;
	$img_path = $request->img_path;
	try {
			$path = '../upload/product/';
			if (!is_dir($path)) {
				mkdir($path, 0777, TRUE);
			}
			$id = uniqid() . date('Ymd');
			$method = new user();
			$images = DecodeImage($path, $id, $postdata->img);
			if ($images->status == true) {
				$sql = "INSERT INTO product (`name`,`price`,`category`,`status`,`img_path`) VALUES
											   (:name, :price, :status, :img_path)";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('name', $name);
				$smt->bindParam('price', $price);
				$smt->bindParam('status', $status);
				$smt->bindParam('img_path', $img_path);
				$smt->execute();
				if($smt == 1) {
					echo '{"status":true}';
				} else {
					echo '{"status":false}';
				}
			}
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
}

function getUserMapStatus() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->id;
	
	try {
		$sql = "select user_type_id,user_id,alert_type,distance from user where user_id=:user_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id', $user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		$result = json_encode($rows);
		if(count($rows)>0) {
			echo '{"status":true, "result":'.$result.'}';
		}
			echo '{"status":false}';
	} catch(Exception $e) {
		echo '{"status":true, "msg":'.$e->getMessage().'}';
	}
}

function getCategory() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	try {
		$sql = "select * from category where status=1";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		$result = json_encode($rows);
		if(count($rows)>0) {
			echo '{"status":true, "data":'.$result.'}';
		} else {
			echo '{"status":false}';
		}
	} catch(Exception $e) {
		echo '{"status":true, "msg":'.$e->getMessage().'}';
	}
}

function deleteCategory() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$id = $request->id;
	try {
		$sql = "update category set status=0 where cat_id=:id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('id', $id);
		$smt->execute();
		if($smt == 1) {
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	}catch(Exception $e) {
		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

function deleteProduct() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$id = $request->id;
	try {
		$sql = "update product set status=0 where id=:id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('id', $id);
		$smt->execute();
		if($smt == 1) {
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	}catch(Exception $e) {
		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

function insertCategory() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$cat_name = $request->cat_name;
	try {
			$sql = "insert into category(`cat_name`,`status`) VALUES
										   (:cat_name, 1)";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('cat_name', $cat_name);
			$smt->execute();
			if($smt == 1) {
				echo '{"status":true}';
			} else {
				echo '{"status":false}';
			}
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}
}
function getProductList() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	try {
		$sql = "select p.*,c.cat_name from product as p left join category as c on c.cat_id=p.category where p.status=1";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		$result = json_encode($rows);
		if(count($rows)>0) {
			echo '{"status":true, "result":'.$result.'}';
		}
			echo '{"status":false}';
	} catch(Exception $e) {
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}


 function DecodeImage($dir, $fname, $image) {
		$base = explode(',', $image)[0];
		$extension = explode(';', explode('/', $base)[1])[0];
		if ($image !== '') {
			$data = $image;
			$data = str_replace($base, '', $data);
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data); // Decode image using base64_decode
			$filename = $fname . '.' . $extension; //Now you can put this image data to your desired file using file_put_contents function like below:
			$success = file_put_contents($dir . $filename, $data);
			return (object) array('status' => true, 'filename' => $filename, 'filepath' => $dir);
		} else {
			return (object) array('status' => false, 'filename' => '', 'filepath' => '');
		}
}
	
function getProduct() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$cat_id = $request->cat_id;
	try {
		$sql = "select * from product where category=:cat_id and status=1";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('cat_id', $cat_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		$result = json_encode($rows);
		if(count($rows)>0) {
			echo '{"status":true, "data":'.$result.'}';
		} else {
			echo '{"status":false}';
		}
	} catch(Exception $e) {
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

function getOffers() {
	try {
		$sql = "select * from offer where is_active=1";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		$result = json_encode($rows);
		if(count($rows)>0) {
			echo '{"status":true, "data":'.$result.'}';
		} else {
			echo '{"status":false}';
		}
	} catch(Exception $e) {
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}
function insertCheckout() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$address = $request->address;
	$total = $request->total;
	try {
		$sql = "INSERT INTO `order`(`user_id`, `delivery_address`, `total`, `status`) VALUES
									(:user_id, :address, :total, 1)";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('address', $address);
		$smt->bindParam('total', $total);
		$smt->execute();
		$id = $db->lastInsertId();
		if($smt == 1) {
			echo '{"id":'.$id.',"status":true}';
		} else {
			echo '{"status":false}';
		}
	} catch(Exception $e) {
		echo '{"status":false,"msg":'.$e->getMessage().'}';
	}
}

function orderDetail() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$id = $request->id;
	$price = $request->price;
	$qty = $request->total;
	try {
		$sql = "insert into order_detail(order_id,product_id,price,qty,status) VALUES
									(:id, :price, :qty, 1)";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('id', $id);
		$smt->bindParam('price', $price);
		$smt->bindParam('qty', $qty);
		$smt->execute();
		$id = $db->lastInsertId();
		if($smt == 1) {
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	} catch(Exception $e) {
		echo '{"status":false,"msg":'.$e->getMessage().'}';
	}
}

function newThreat() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$response = array();
	$result = array();
	try {
		$sql = "SELECT * FROM alert_log WHERE receiver_id=:user_id AND is_active=1 AND saw_the_same IS NULL OR saw_the_same='' ORDER BY time DESC";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id', $user_id);
		$smt->execute();
		$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
		foreach($rows as $data) {
			$response['address_id'] = $data['address_id'];
			$response['alert_status_type'] = $data['alert_status_type'];
		}
		array_push($result, $response);
		echo json_encode($result);
	} catch(Exception $e) {
		echo '{"status":false,"msg":'.$e->getMessage().'}';
	}
}

function updateCamToken() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$address_id = $request->address_id;
	$camtoken = $request->camtoken;
	$cam_phone = $request->cam_phone;
	$cam_did = $request->cam_did;
	$country_code = $request->country_code;
	$camtoken_type = $request->camtoken_type;
	$cam_token_merge='';
	//echo $user_id.','.$camtoken.','.$address_id;exit;
	//print_r($request);exit;
	if($camtoken_type == 'phone') {
	    $cam_token_merge = $country_code.$cam_phone;
	} else if($camtoken_type == 'device_detail') {
	    $cam_token_merge = $cam_did;
	}
	try {
		$sql = "update address set camtoken=:camtoken,camtoken_type=:camtoken_type where user_id=:user_id AND address_id=:address_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('camtoken', $cam_token_merge);
		$smt->bindParam('camtoken_type', $camtoken_type);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('address_id', $address_id);
		$smt->execute();
		if($smt == 1) {
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	}catch(Exception $e) {
		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

function updatePrimaryAddress() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$address_id = $request->address_id;
	$primary_address= 'yes';

	//echo $user_id.','.$camtoken.','.$address_id;exit;
	//print_r($request);exit;
	try {
		$sql = "update address set primary_address=:primary_address where address_id=:address_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('primary_address', $primary_address);
		$smt->bindParam('address_id', $address_id);
		$smt->execute();
		if($smt == 1) {
			$no = 'no';
			$sql_no = "update address set primary_address=:no where address_id NOT IN(:address_id)";
			$db = getDB();
			$smt_no = $db->prepare($sql_no);
			$smt_no->bindParam('no', $no);
			$smt_no->bindParam('address_id', $address_id);
			$smt_no->execute();
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	}catch(Exception $e) {
		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}
function updateCurrentAddress() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$address_id = $request->address_id;
	$primary_address= $request->primary_address;
	$street_address = $request->street_address;
	$zipcode = $request->zipcode;
	$city = $request->city;
	$country = $request->country;
	$longitude = $request->longitude;
	$latitude = $request->latitude;
	$camtoken = $request->camtoken;
	//echo $user_id.','.$camtoken.','.$address_id;exit;
	//print_r($request);exit;
	try {
		$sql = "update address set street_address=:street_address, zipcode=:zipcode, city=:city, country=:country,primary_address=:primary_address, latitude=:latitude,longitude=:longitude,camtoken=:camtoken where user_id=:user_id AND address_id=:address_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('street_address',$street_address);
		$smt->bindParam('zipcode',$zipcode);
		$smt->bindParam('city',$city);
		$smt->bindParam('country',$country);
		$smt->bindParam('latitude',$latitude);
		$smt->bindParam('longitude',$longitude);
		$smt->bindParam('primary_address', $primary_address);
		$smt->bindParam('camtoken',$camtoken);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('address_id', $address_id);
		$smt->execute();
		if($smt == 1) {			
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	}catch(Exception $e) {
		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

function sendMail($id,$frndName,$userName,$userid,$time) {

	//$id = "bharathitat@gmail.com";
	$sender_id = $userid;
	$sql = "SELECT * FROM wp_users WHERE user_email=:email";
	$db = getDB();
	$smt = $db->prepare($sql);
	$smt->bindParam('email', $id);
	$smt->execute();
	$rows = $smt->fetchAll(PDO::FETCH_ASSOC);
	$time = $time;
	//print_r($rows);exit;
	if(count($rows) <= 0) {
		// $msql = "SELECT * FROM my_contact WHERE email_id=:email";
		// $db = getDB();
		// $msmt = $db->prepare($msql);
		// $msmt->bindParam('email', $id);
		// $msmt->execute();
		// $mrows = $msmt->fetchAll(PDO::FETCH_ASSOC);
		// if(count($mrows) <= 0):
		$db = getDB();
		$pstmt = $db->prepare("SELECT * FROM admin_setting WHERE status=1 AND service='promo_code'");
		$pstmt->execute();
		$prows = $pstmt->fetch();
		$promo_code = $prows['data'];

		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = "mail.nabosupport.dk";
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->SMTPAutoTLS = false;
		$mail->SMTPSecure = 'ssl'; // To enable TLS/SSL encryption change to 'tls'
		$mail->AuthType = "CRAM-MD5";
		$mail->Username = "noreply@nabosupport.dk";
		$mail->Password = "GOQOm})fhYAL";
		$mail->setFrom('noreply@nabosupport.dk', 'Nabosupport');
		$mail->addAddress($id); 
		//$mail->addAddress('l.jayamurugan@gmail.com'); //(Send the test to yourself)
		$mail->Subject = 'NaboSupport Invitation';
		$mail->isHTML(true);                                 // Set email format to HTML
    	$mail->Body    = "<p>Kære <b>".$frndName."</b></p>
    				       <p>Detter er en invitation fra <b>".$userName."</b>  som gerne vil have dig med som nabosupporter.</p>
                       	   <p>".$userName."  er allerede blevet en del af Nabosupports store netværk af naboer og alarmsystemer.</p>
                           <p>Nabosupport er en ny og moderne overvågningsplatform, som samler alle dine opmærksomme naboer i nærområdet sammen med deres tyverialarmer. På den måde kan du og dine naboer reagere hurtigt på mistænkelig adfærd og alarmer og på den måde hjælpe hinanden med at skabe et trygt nærområde.</p>
                           <p>Hvis det lyder interessant, så besøg os på  www.nabosupport.dk  og tilmeld dig og få gratis adgang til alle vores funktioner med det samme. Brug rabatkoden \"".$promo_code."\".</p>
                           <p>Hvis du har spørgsmål omkring denne mail, så kontakt os på  info@nabosupport.dk .</p>
                           
                       <address>
                       	De bedste hilsner<br>
                        <br>
     				   	NaboSupport Aps<br>
     					Skolelodden 6<br>
     					3550 Slangerup<br>
     					Denmark<br>
                        <br>
                        www.NaboSupport.dk
                       </address>";
	
    $mail->Body    .= "<p>Dear <b>".$frndName."</b></p>
    				       <p>This is an invitation from <b>".$userName."</b>  who would like you to be a nabosupporter.</p>
                       	   <p>".$userName."   is all ready a part of Nabosupports great network of neighbours and burglar alarms.</p>
                           <p>Nabosupport is a new and modern monitoring platform, that combine all of you neighbours in your neighbourhood, and there burglar alarms. In that way you and your neighbours can respond quickly to suspicious behavior and alarms, and help each other to create a safe neighborhood.</p>
                           <p>If you believe that this sound interesting, go visit us at  www.nabosupport.dk  and submit, and get free access to all of our functionalities. Use the promocode \"".$promo_code."\".</p>
                           <p>If you have any questions about this mail, please contact us at  info@nabosupport.dk .</p>
                           
                       <address>
                       	Best regards<br>
                        <br>
     				   	NaboSupport Aps<br>
     					Skolelodden 6<br>
     					3550 Slangerup<br>
     					Denmark<br>
                        <br>
                        www.NaboSupport.dk
                       </address>";
    
			if (!$mail->send()) {
      			$app->flash("error", "We're having trouble with our mail servers at the moment.  Please try again later, or contact us directly by phone.");
        		error_log('Mailer Error: ' . $mail->errorMessage());
        		$app->halt(500);
			} else {
    			return true;
			}
    	//endif;
    	} else {
    		$user_id = $rows[0]['ID'];
    		$username = $rows[0]['user_login'];
    		$email = $rows[0]['user_email'];
    		$dsql = "SELECT * FROM user WHERE user_id=:user_id";
    		$db = getDB();
   			$dsmt = $db->prepare($dsql);
    		$dsmt->bindParam('user_id', $user_id);
    		$dsmt->execute();
    		$drows = $dsmt->fetchAll(PDO::FETCH_ASSOC);
    		//print_r($drows);exit;
    		$arr = array('deviceID' => $drows[0]['deviceID'],
                         'platform' => $drows[0]['platform'],
                         'lang' => $drows[0]['language'],
                         'receiver_id' => $drows[0]['user_id'],
                         'sender_id' => $sender_id,
                         'username' => $username,
                         'email' => $email,
                         'time' => $time
                        );
    		$data = (object) $arr;
    		//print_r($data);exit;
    		sendNotification($data);
    	}
      
}

function sendNotification($dataObj) {
	$deviceID  = $dataObj->deviceID;
	$platform  = $dataObj->platform;
	$lang = $dataObj->lang;
	$time = $dataObj->time;
	$request = array('sender_id' => $dataObj->sender_id, 'user_name' => $dataObj->username, 'email' => $dataObj->email);
	$title='';
	$message='';
	$username='';
	if($lang == 'en') {
    	$title = 'Invitation from a nabosupporter';
    	$message = 'A nabossuporter wants to add you as his supporter';
    	$username = 'Username: ';
    } else {
    	$title = 'Invitation fra en nabosupporter';
    	$message = 'En nabossuporter vil gerne tilføje dig som sin supporter';
    	$username = 'Brugernavn: ';
    }
	$user = $username.$dataObj->username;
	define( 'API_ACCESS_KEY', 'AIzaSyCvOngEZQjbaCqLL476f81LoUQi6pjEfIk' );
	if($platform == 'Android') {
			$msg = array
					(
						'alert'		=> $user,
						'message'		=> $message,
						'title'		=> $title,
						'subtitle'	=> $user,
						'aData'	=> json_encode($request),
						'tickerText'	=> $message,
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon',
						'priority'	=> 'high'
					);
			$fields = array
						(
							'registration_ids'	=> array($deviceID),
							'data'			=> array("message" => $msg,'flag'=>'ASR')
						);
					 
			$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);
					 
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$push_result = curl_exec($ch );
			curl_close( $ch );
			} else {
				$ch = curl_init("https://fcm.googleapis.com/fcm/send");
				$token = $deviceID; //token here
				$body = $address;
				$notification = array('title' =>$title , 'text' => $body,'tickerText'=> $message,
            				    'subtitle'=> $user,
            				    'sender_id' => $dataObj->sender_id,
            				    'username' => $dataObj->username,
                                'email' => $dataObj->email,
                                 'flag' => 'ASR');

						//This array contains, the token and the notification. The 'to' attribute stores the token.
				$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high','content_available'=>true);

						//Generating JSON encoded string form the above array.
				$json = json_encode($arrayToSend);
						//Setup headers:
				$headers = array();
				$headers[] = 'Content-Type: application/json';
				$headers[] = 'Authorization: key='. API_ACCESS_KEY; // key here

						//Setup curl, add headers and post parameters.
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");																	 
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
				curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);		 

				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				$push_result = curl_exec($ch );
				curl_close( $ch );
			}
			$alert = 'ASR';
			$sql = "INSERT INTO alert_log (`sender_id`, `receiver_id`, `alert_status_type`, `message`, `time`) VALUES
													   (:sender_id, :receiver_id, :alert_type, :message, :time)";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('sender_id', $dataObj->sender_id);
			$smt->bindParam('receiver_id', $dataObj->receiver_id);
			$smt->bindParam('alert_type', $alert);
			$smt->bindParam('message', $message);
			$smt->bindParam('time', $time);
			$smt->execute();
			return true;
}

function acceptReq() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->sender_id;
	$email = $request->email;
	$receiver_id = $request->user_id;
	try{
		$sql = "update my_contact set status=1 where user_id=:user_id and email_id=:email";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id',$user_id);
		$smt->bindParam('email',$email);
		$smt->execute();
		if($smt == 1) {
        	$sql = "update alert_log set is_active=0 where sender_id=:user_id and receiver_id=:receiver_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id',$user_id);
			$smt->bindParam('receiver_id',$receiver_id);
			$smt->execute();
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
    } catch(Exception $e) {		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

function acceptReject() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->sender_id;
	$email = $request->email;
	$receiver_id = $request->user_id;
	try{
		$sql = "update my_contact set status=2 where user_id=:user_id and email_id=:email";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id',$user_id);
		$smt->bindParam('email',$email);
		$smt->execute();
		if($smt == 1) {
        	$sql = "update alert_log set is_active=0 where sender_id=:user_id and receiver_id=:receiver_id";
			$db = getDB();
			$smt = $db->prepare($sql);
			$smt->bindParam('user_id',$user_id);
			$smt->bindParam('receiver_id',$receiver_id);
			$smt->execute();
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
    } catch(Exception $e) {		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}

function testSES() {
	echo '{"usernmae":'.$_SESSION['userData'][0]['username'].'}';
}

function updateEditAddress() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$address_id = $request->address_id;
	$primary_address= $request->primary_address;
	$street_address = $request->street_address;
	$zipcode = $request->zipcode;
	$city = $request->city;
	$country = $request->country;
	$longitude = $request->longitude;
	$latitude = $request->latitude;
	//echo $user_id.','.$camtoken.','.$address_id;exit;
	//print_r($request);exit;
	try {
		$sql = "update address set street_address=:street_address, zipcode=:zipcode, city=:city, country=:country,primary_address=:primary_address, latitude=:latitude,longitude=:longitude where user_id=:user_id AND address_id=:address_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('street_address',$street_address);
		$smt->bindParam('zipcode',$zipcode);
		$smt->bindParam('city',$city);
		$smt->bindParam('country',$country);
		$smt->bindParam('latitude',$latitude);
		$smt->bindParam('longitude',$longitude);
		$smt->bindParam('primary_address', $primary_address);
		$smt->bindParam('user_id', $user_id);
		$smt->bindParam('address_id', $address_id);
		$smt->execute();
		if($smt == 1) {			
			echo '{"status":true}';
		} else {
			echo '{"status":false}';
		}
	}catch(Exception $e) {
		
		echo '{"status":false, "msg":'.$e->getMessage().'}';
	}
}


function processReset($user_id,$address_id,$address_type) {
		$alert_type = "N";
		try {
			if($address_type === "GB" || $address_type === "GS") {
				$marker = '';
				$sql = "UPDATE address SET is_active = 0 WHERE address_id=:address_id";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('address_id', $address_id);
				$smt->execute();
				if($smt == true) {
					$sqal = "UPDATE alert_log SET is_active = 0,log_state = 0 WHERE address_id=:address_id";
					$db = getDB();
					$smt = $db->prepare($sqal);
					$smt->bindParam('address_id', $address_id);
					$smt->execute();
				}
				echo '{"status":true,"msg":"remove success."}';
			} else {
				$marker = 'blue.svg';
				$sql1 = "UPDATE user u, address a, alert_log al SET u.alert_type=:alert_type, a.status_type='', a.marker=:marker, al.alert_status_type =:alert_type, al.log_state = 0,al.is_active=0 WHERE u.user_id=:user_id AND a.address_id=:address_id AND al.address_id=:address_id AND a.address_type NOT IN ('GS','GB')";
				$db = getDB();
				$smt = $db->prepare($sql1);
				$smt->bindParam('user_id', $user_id);
				$smt->bindParam('address_id', $address_id);
				$smt->bindParam('alert_type', $alert_type);
				$smt->bindParam('marker',$marker);
				$smt->execute();
				echo '{"status":true,"msg":"cancelled success."}';
			}
		}catch(Exception $e) {
			
			echo '{"status":false, "msg":'.$e->getMessage().'}';
		}	
}

function getResetDate() {
	try {
		$db = getDB();
		$stmt = $db->prepare("SELECT * FROM admin_setting WHERE status=1 AND service='threat_reset'");
		$stmt->execute();
		$rows = $stmt->fetch();
		return $rows['data'];
		//echo $rows['data'];
		//echo '{"status":true,"date":'.$rows['data'].'}';
	}catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

function startCron() {
	$marker = 'blue.svg';
	try{
		$sql = "UPDATE address SET status_type = '', marker=:marker WHERE address_type NOT IN('GB','GS')";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('marker', $marker);
		$smt->execute();
	}catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}
}

function updateRDistance() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$r_distance = $request->r_distance;
	try{
		$sql = "UPDATE user SET r_distance = :r_distance WHERE user_id=:user_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id',$user_id);
		$smt->bindParam('r_distance', $r_distance);
		$smt->execute();
		if($smt) {
			echo '{"status":true,"msg":"range updated."}';
		}
	}catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}

}
function setLang() {
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$user_id = $request->user_id;
	$lang = $request->lang;
	try{
		$sql = "UPDATE user SET language = :lang WHERE user_id=:user_id";
		$db = getDB();
		$smt = $db->prepare($sql);
		$smt->bindParam('user_id',$user_id);
		$smt->bindParam('lang', $lang);
		$smt->execute();
		if($smt) {
			echo '{"status":true,"msg":"Language updated."}';
		}
	}catch (PDOException $e) {
		echo '{"error":{"text":' . $e->getMessage() . '}}';
	}

}
function testmail() {
    $id = 'bharathitat@gmail.com';
	$mail = new PHPMailer;
    $mail->setFrom('info@rayi.in');
    $mail->addAddress($id);
    $mail->addReplyTo('no-reply@rayi.in');

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Test email alert';
    $mail->Body    = "
        <p>Hi,</p>
        <p>            
         This is test nabo alert message
        </p>";

    if(!$mail->send()) {
        $app->flash("error", "We're having trouble with our mail servers at the moment.  Please try again later, or contact us directly by phone.");
        error_log('Mailer Error: ' . $mail->errorMessage());
        $app->halt(500);
    }     
}

function sendEmailAlertTestBH() {
    //$id = 'bharathitat@gmail.com';
    $postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
    $id = $request->email;
	$message = $request->msg;
	$address = $request->add;
	$mail = new PHPMailer(true);
	$mail->IsSMTP();
    //$mail->setFrom('Noreply@nabosupport.dk');
    $mail->addReplyTo('Noreply@nabosupport.dk');
	$mail->SMTPAuth = true; // There was a syntax error here (SMPTAuth)
	$mailer->SMTPSecure = 'ssl';
	$mail->Host = "mail.nabosupport.dk";
	$mail->Mailer = "smtp";
	$mail->Port = 465;
	$mail->SMTPDebug  = 2;
	$mail->Username = "Noreply@nabosupport.dk";
	$mail->Password = "GOQOm})fhYAL";
	// $mail->SMTPOptions = array(
	// 'ssl' => array(
	// 'verify_peer' => false,
	// 'verify_peer_name' => false,
	// 'allow_self_signed' => true
	// )
	// );
    $mail->isHTML(true);                                  // Set email format to HTML
	$mail->From = "Noreply@nabosupport.dk";
	$mail->addAddress($id);
    $mail->Subject = 'Nabosupport Alert Message';
    $mail->Body    = "
        <p>Alert,</p>
        <p>            
         ".$message." in below address
        </p>
        <p>
        ".$address."
        </p>";

    if(!$mail->send()) {
        $app->flash("error", "We're having trouble with our mail servers at the moment.  Please try again later, or contact us directly by phone.");
        error_log('Mailer Error: ' . $mail->errorMessage());
        $app->halt(500);
    } else if($mail->send()) {
        echo json_encode(array('status'=>true));
    }   
}
function sendEmailAlertTest(){
$mail = new PHPMailer;
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = "mail.nabosupport.dk";
$mail->Port = 465;
$mail->SMTPAuth = true;
$mail->SMTPAutoTLS = false;
$mail->SMTPSecure = 'ssl'; // To enable TLS/SSL encryption change to 'tls'
$mail->AuthType = "CRAM-MD5";
$mail->Username = "noreply@nabosupport.dk";
$mail->Password = "GOQOm})fhYAL";
$mail->setFrom('noreply@nabosupport.dk', 'Nabosupport');
$mail->addAddress('l.jayamurugan@gmail.com', 'JK'); //(Send the test to yourself)
$mail->Subject = 'PHPMailer SMTP test';
$mail->isHTML(true);
$mail->Body    = 'This is the HTML message body <b>in bold!</b>';

if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
}

function inviteFriend(){
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$frndName = $request->name;
	$userName = $request->username;
	$email = $request->email;
	$db = getDB();
	$stmt = $db->prepare("SELECT * FROM admin_setting WHERE status=1 AND service='promo_code'");
	$stmt->execute();
	$rows = $stmt->fetch();
	$promo_code = $rows['data'];

	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = "mail.nabosupport.dk";
	$mail->Port = 465;
	$mail->SMTPAuth = true;
	$mail->SMTPAutoTLS = false;
	$mail->SMTPSecure = 'ssl'; // To enable TLS/SSL encryption change to 'tls'
	$mail->AuthType = "CRAM-MD5";
	$mail->Username = "noreply@nabosupport.dk";
	$mail->Password = "GOQOm})fhYAL";
	$mail->setFrom('noreply@nabosupport.dk', 'Nabosupport');
	$mail->addAddress($email); 
	//$mail->addAddress('l.jayamurugan@gmail.com'); //(Send the test to yourself)
	$mail->Subject = 'NaboSupport Invitation';
	$mail->isHTML(true);                                 // Set email format to HTML
	$mail->Body    = "<p>Kære <b>".$frndName."</b></p>
    				       <p>Detter er en invitation fra <b>".$userName."</b>  som gerne vil have dig med som nabosupporter.</p>
                       	   <p>".$userName."  er allerede blevet en del af Nabosupports store netværk af naboer og alarmsystemer.</p>
                           <p>Nabosupport er en ny og moderne overvågningsplatform, som samler alle dine opmærksomme naboer i nærområdet sammen med deres tyverialarmer. På den måde kan du og dine naboer reagere hurtigt på mistænkelig adfærd og alarmer og på den måde hjælpe hinanden med at skabe et trygt nærområde.</p>
                           <p>Hvis det lyder interessant, så besøg os på  www.nabosupport.dk  og tilmeld dig og få gratis adgang til alle vores funktioner med det samme. Brug rabatkoden \"".$promo_code."\".</p>
                           <p>Hvis du har spørgsmål omkring denne mail, så kontakt os på  info@nabosupport.dk .</p>
                           
                       <address>
                       	De bedste hilsner<br>
                        <br>
     				   	NaboSupport Aps<br>
     					Skolelodden 6<br>
     					3550 Slangerup<br>
     					Denmark<br>
                        <br>
                        www.NaboSupport.dk
                       </address>";
	
    $mail->Body    .= "<p>Dear <b>".$frndName."</b></p>
    				       <p>This is an invitation from <b>".$userName."</b>  who would like you to be a nabosupporter.</p>
                       	   <p>".$userName."   is all ready a part of Nabosupports great network of neighbours and burglar alarms.</p>
                           <p>Nabosupport is a new and modern monitoring platform, that combine all of you neighbours in your neighbourhood, and there burglar alarms. In that way you and your neighbours can respond quickly to suspicious behavior and alarms, and help each other to create a safe neighborhood.</p>
                           <p>If you believe that this sound interesting, go visit us at  www.nabosupport.dk  and submit, and get free access to all of our functionalities. Use the promocode \"".$promo_code."\".</p>
                           <p>If you have any questions about this mail, please contact us at  info@nabosupport.dk .</p>
                           
                       <address>
                       	Best regards<br>
                        <br>
     				   	NaboSupport Aps<br>
     					Skolelodden 6<br>
     					3550 Slangerup<br>
     					Denmark<br>
                        <br>
                        www.NaboSupport.dk
                       </address>";

    if(!$mail->send()) {
        $app->flash("error", "We're having trouble with our mail servers at the moment.  Please try again later, or contact us directly by phone.");
        error_log('Mailer Error: ' . $mail->errorMessage());
        $app->halt(500);
    } else if($mail->send()) {
        echo json_encode(array('status'=>true));
    }   
}
