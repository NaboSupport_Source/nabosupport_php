<?php
header("Access-Control-Allow-Origin: *");
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

include 'dbConfig.php';
require 'autoload.php';

$app = new Slim\App();	
	
	header("Access-Control-Allow-Origin: *");
	if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	 
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	 
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	 
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	 
			exit(0);
		}
	
	
	
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		//print_r($request);exit;
		$user_id = $request->user_id;
		$lat=$request->latitude;
		$lng=$request->longitude;
		$sender_id = $request->user_id;
		$alert_type = $request->alert_type;
		$address_id = $request->address_id;
		$street = $request->street_address;
		$city = $request->city;
		$country = $request->country;
		$zipcode = $request->zipcode;
		//$address = $street.','.$city.','.$country.','.$zipcode;
		$alert_type = "N";
		try {
				$sql = "UPDATE user u, address a, alert_log al SET u.alert_type=:alert_type, a.status_type='', al.alert_status_type =:alert_type, al.is_active = 0 WHERE u.user_id=:user_id AND a.user_id =:user_id AND al.receiver_id =:user_id AND u.address_id=a.address_id AND a.address_id=:address_id";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('user_id', $user_id);
				$smt->bindParam('address_id', $address_id);
				$smt->bindParam('alert_type', $alert_type);
				$smt->execute();
			}catch(Exception $e) {
				
				echo '{"status":false, "msg":'.$e->getMessage().'}';
			}
		//print_r($request);exit;
		$sql1 = "SELECT * FROM alert_log WHERE sender_id=:sender_id AND address_id=:address_id";
							$smt1 = $db->prepare($sql1);
							$smt1->bindParam('sender_id', $sender_id);
							$smt1->bindParam('address_id', $address_id);
							$smt1->execute();
							$rows = $smt1->fetchAll(PDO::FETCH_ASSOC);
		//print_r($rows);exit;
		foreach($rows as $value) {
			$g_sender_id = $value['sender_id'];
			$g_receiver_id = $value['receiver_id'];
			$g_address_id = $value['address_id'];
			$g_alert_type = "N";
			try {
				$sql = "UPDATE user u, alert_log a SET a.alert_status_type =:alert_type, a.is_active=0, u.alert_type=:alert_type WHERE a.address_id=:address_id AND a.receiver_id=:receiver_id AND a.sender_id=:sender_id AND u.user_id=:receiver_id AND a.alert_status_type NOT IN ('B')";
				$db = getDB();
				$smt = $db->prepare($sql);
				$smt->bindParam('receiver_id', $g_receiver_id);
				$smt->bindParam('sender_id', $g_sender_id);
				$smt->bindParam('alert_type', $g_alert_type);
				$smt->bindParam('address_id', $g_address_id);
				$smt->execute();
				
				$sql1 = "SELECT receiver_id FROM alert_log WHERE sender_id=:sender_id AND address_id=:address_id ORDER BY time DESC LIMIT 1";
				$db = getDB();
				$smt1 = $db->prepare($sql1);
				$smt1->bindParam('sender_id', $g_sender_id);
				$smt1->bindParam('address_id', $g_address_id);
				$smt1->execute();
				$result = $smt1->fetch();
				//print_r($result);exit;
				foreach($result as $value){
					$r_receiver_id = $value['receiver_id'];
				}
				//print_r($r_receiver_id);exit;
				$sql2 = "SELECT user_id, deviceID from user WHERE user_id =:receiver_id AND do_not_distrub=0";
				$db = getDB();
				$stmt = $db->prepare($sql2);
				$stmt->bindParam('receiver_id', $r_receiver_id);
				$stmt->execute();
				$divIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
				//print_r($divIds);exit;
				foreach($divIds as $divData) {
				
					$deviceID = $divData['deviceID'];
					//print_r($deviceID);
					define( 'API_ACCESS_KEY', 'AIzaSyBIhHBL9VCl5pAkahNbGdwsecyEcB1turM' );
					//$registrationIds = array( $_GET['id'] );
					// prep the bundle
					$msg = array
					(
						'message' 	=> 'NABO ALERT CANCEL MESSAGE',
						'title'		=> 'House Owner cancelled the alert.',
						'subtitle'	=> 'CANCELED.',
						'vData'	=> false,
						'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
						'vibrate'	=> 1,
						'sound'		=> 1,
						'largeIcon'	=> 'large_icon',
						'smallIcon'	=> 'small_icon',
						'priority' 	=> 'high'
					);
					$fields = array
					(
						'registration_ids' 	=> array($deviceID),
						'data'			=> array("message" => $msg)
					);
				 
					$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);
				 
					$ch = curl_init();
					curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec($ch );
					curl_close( $ch );
					echo $result;
					//echo '{"status":true}';
					//echo $result;
				}
			}catch(Exception $e) {
				
				echo '{"status":false, "msg":'.$e->getMessage().'}';
			}
			
		}
		echo '{"status":true}';	
?>