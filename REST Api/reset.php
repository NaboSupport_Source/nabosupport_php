<?php
header("Access-Control-Allow-Origin: *");
	include 'dbConfig.php';
	if(isset($_GET['action'])) {
		$action = $_GET['action'];
		$key = $_GET['encrypt'];
		if($action == 'reset') {
			echo '<html>
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
			<div class="container">
			  <div class="row">
			   <div class="col-sm-6 col-md-4">
				<form action="" method="POST" enctype="multipart/form-data">
				  <div class="form-group">
				    <div class="input-group">
					<input type="password" class="form-control" name="password" placeholder="Enter new password"/>
				  </div>
					</div>
				  <div class="form-group">
				    <div class="input-group">
					<input type="password" class="form-control" name="cpassword" placeholder="Confirm your new password"/>
				  </div>
					</div>
				<div class="form-group">
				    <div class="input-group">
					<input type="submit" class="btn btn-success form-control" name="resetpass" value="Reset"/>
				</form>
				  </div>
					</div>
</div>
</html>';
		}
		if(isset($_POST['resetpass'])) {
			$pass = $_POST['password'];
			$cpass = $_POST['cpassword'];
			//echo $key.','.$pass.','.$cpass;exit;
			try {
				$db = getDB();
				$stmt = $db->prepare("SELECT * FROM user WHERE pass_reset =:key");
				$stmt->bindValue(':key', $key, PDO::PARAM_STR);
				$stmt->execute();
				$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach($rows as $value) {
					$email = $value['email_id'];
				}
				if(count($rows)>=1) {
					$stmt = $db->prepare("UPDATE user SET password =:pass, pass_reset='' WHERE email_id =:email");
					$stmt->bindValue(':pass', $pass, PDO::PARAM_STR);
					$stmt->bindValue(':email', $email, PDO::PARAM_STR);
					$stmt->execute();
					echo '{"status": true}';
				} else {
					echo '{"status": false}';
				}				
			} catch (PDOException $e) {
				echo '{"error":{"text":' . $e->getMessage() . '}}';
			}
		}
	}
?>